# Rootable

Please consult [wiki](../../wikis) for more information.

## Licensing
See the [LICENSE](LICENSE) file for licensing information as it pertains to files in this repository.

# Development

## Getting Started

### Requirements

To run the specs or fire up the server, be sure you have these installed (and running):

* Ruby 2.7 (see [.ruby-version](.ruby-version)).
* Node.js 14.10.1 (see [.node-version](.node-version)).
* External Dependencies (`brew bundle --no-lock`, see [Brewfile](Brewfile))

If you are running a macbook with an Apple M1 processor, installing ruby versions older than 2.7.2 may give you some errors.
If you are using rbenv to install 2.7.1 you can run this command to bypass them:
```
$ CFLAGS="-Wno-error=implicit-function-declaration" rbenv install 2.7.1
```

### First Time Setup

#### `bin/setup`

After cloning, run [./bin/setup](bin/setup) to install missing gems and prepare the database.

Then run seeds:

```
$ bundle exec rake db:seed
$ RAILS_ENV=test bundle exec rake db:seed
```

If the above commands fail with a migration error, run this command to fix it:
```
rake db:migrate RAILS_ENV=test
```

Note, `rake db:sample_data` (run as part of setup) loads a small set of data for development. Check out
[db/sample_data.rb](db/sample_data.rb) for details.

#### `.env`

The `bin/setup` script will create a `.env` file that defines settings for your local environment. Do not check this into source control. Refer to the [environment variables](#environment-variables) section below for what can be specified in `.env`.

### Running the Specs

To run all Ruby specs:

    $ ./bin/rake

To run all the javascript specs:

    $ yarn test

### Running the Application Locally

The easiest way to run the app is using `yarn start`. This starts all the processes defined in `Procfile.dev`, including the Rails server and the webpack dev server.

    $ yarn start

The app will then be accessible at <http://localhost:3000>.

### Webpack Dev Server

By default, webpacker will compile assets on demand. In other words, you don’t need to precompile all assets ahead of time — webpacker lazily compiles assets it has not served yet. However, you will need to manually reload your browser to see new changes when you edit an asset.

Alternatively, for live code reloading, you can run `./bin/webpack-dev-server` in a separate terminal from `rails s`. This done for you automatically if you use `yarn start` to run the app. Asset requests are proxied to the dev server, and it will automatically refresh your browser when it detects changes to the pack.

If you stop the dev server, Rails automatically reverts back to on-demand compilation.

## Conventions

### Git

* Branch `development` is auto-deployed to acceptance.
* Branch `main` is auto-deployed to production.
* Create feature branches off of `development` using the naming convention
  `(features|chores|bugs)/a-brief-description-######`, where ###### is the tracker id.
* Rebase your feature branch before merging into `development` to produce clean/compact merge bubbles.
* Always retain merge commits when merging into `development` (e.g. `git merge --no-ff branchname`).
* Use `git merge development` (fast-forward, no merge commit) from `main`.
* Craft atomic commits that make sense on their own and can be easily cherry-picked or reverted if necessary.

### Code Style

Rubocop is configured to enforce the style guide for this project.

## Additional/Optional Development Details

### Styling
Two different styling systems are used within the application.
The Admin experience is rendered from the server and utilizes [Material Design](https://material.io) for styling its components.
The Volunteer experience is rendered from the client and utilizes [Material UI](https://material-ui.com/) for styling its components.

#### Server-Side Rendered Pages
Server-side rendered pages uses the [simplest approach](https://github.com/material-components/material-components-web/blob/master/docs/integrating-into-frameworks.md#the-simple-approach-wrapping-mdc-web-vanilla-components)
towards styling its components using [Material Design](https://material.io). Theming is applied by [overriding CSS custom properties](https://github.com/material-components/material-components-web-components/blob/master/docs/theming.md).
See [_layout.scss](app/javascript/stylesheets/_layout.scss) for where we've set theming CSS custom properties.
    
#### Client-Side Rendered React 

### React State Management With Contexts
The react application uses react contexts to hold application state. We prefer to create smaller bite sized contexts scoped to particular slices of the application, and reserve the global application context to hold state that is truly global - such as the currently logged in user or theme settings. For each slice we define a dispatch and state context, which the `useReducer` hook produces values for. In this way we maintain the usual `action -> reducer -> render` data flow as seen in typical react/redux applications:

```
                        _____
                       |     |
                       | API |
                       |_____|
                          ^
                          |
     _________       _____|______       __________       _________
    |         |     |            |     |          |     |         |    
    | actions |---->| middleware |---->| reducers |---->| context |    
    |_________|     |____________|     |__________|     |_________|    
         ^                                                   |
         |                                                   |
         |               __________________                  |
         |              |                  |                 |
          --------------| react components |<---------------
                        |__________________|

```

#### Spinning Up A Context
Creating a new context can be simplified using the `useProvider` custom hook. This hook takes a state and dispatch context, a reducer, and an initial state. It returns a `Provider` component and a `connect` function. Here is an example of setting up a context using the `useProvider` custom hook:

```
// domains/my-domain/context.js
import * as React from 'react';
import { defaultState, reducer } from './reducer';
import useProvider from '../../../reduxlike-contexts/useProvider';

const MyDomainStateContext = React.createContext();
const MyDomainDispatchContext = React.createContext();

const [MyDomainProvider, connectMyDomain] = useProvider(
  MyDomainStateContext,
  MyDomainDispatchContext,
  reducer,
  defaultState,
);

export { MyDomainProvider, connectMyDomain };
```

In order to connect to the state in this context, wrap a component tree in the resulting provider:

```
// domains/my-domain/AComponent.jsx
import { MyDomainProvider } from './context'
...
<MyDomainProvider>
  <ChildComponent />
</MyDomainProvider>
...
```

You may then `connect` any child of this component tree:

```
// domains/my-domain/ChildComponent.jsx
import { connectMyDomain } from './context'
...
const ChildComponent = ({ dispatch, height, width }) => { ... }

export default connectMyDomain(ChildComponent)
...
```

This will inject the `dispatch` function and each top level value in the context's state as props.

In this example, if the context's state looks like `{height: 5, width: 10}`, then `connectMyDomain` provides the `dispatch` function as well as `height` and `width` as props to the wrapped component.

#### Actions and Async Actions
Actions work in the expected way, defining action constants (`const SET_HEIGHT = 'SET_HEIGHT')`) and action creator functions (`const setHeight = (height) => ({ type: SET_HEIGHT, data: height })`).

We also use a thunks-style middleware which allows for async actions in the form of second order functions, e.g.:

```
const fetchShape = (arguments) => async (dispatch) => {
  dispatch(setLoading(true));
  const result = await asyncMethod(arguments);
  dispatch(setShape(result));
}
```

Any such second order function should work as long as the resulting function takes a dispatch as the only argument.

### Code Coverage (local)

Coverage for the ruby specs:

    $ COVERAGE=true rspec

Code coverage is reported to Code Climate on every CI build so there's a record of trending.

### Using Guard

Run `guard` to automatically listen for file changes and run the appropriate specs:

    $ bundle exec guard

### Using Mailcatcher

    $ gem install mailcatcher
    $ mailcatcher
    $ open http://localhost:1080/

Learn more at [mailcatcher.me](http://mailcatcher.me/). And please don't add mailcatcher to the Gemfile.

### Using ChromeDriver

The ChromeDriver version used in this project is maintained by the [webdrivers](https://github.com/titusfortner/webdrivers) gem.  This is means that the
feature specs are not running against the ChromeDriver installed previously on the machine, such as by Homebrew.

### Headed vs headless Chrome

System specs marked with `js: true` run using headless Chrome by default, in the interest of speed. When writing or troubleshooting specs, you may want to run the normal (i.e. "headed") version of Chrome so you can see what is being rendered and use the Chrome developer tools.

To do so, specify `HEADLESS=false` in your environment when running the specs. For example:

    $ HEADLESS=false bin/rspec spec/system

### Continuous Integration/Deployment with GitLab and Heroku

This project is configured for continuous integration with GitLab, see [.gitlab-ci.yml](.gitlab-ci.yml) for details.

On successful builds, Heroku will trigger a deployment via its
[Dpl support](https://docs.gitlab.com/ee/ci/examples/deployment/).

### Landing Page

The landing page is a static site built in a different repo that we have as a submodule here. When the landing page repo is updated, we need to be sure to update it here. Do that with:

    $ git submodule init
    $ git submodule update
    $ git submodule foreach git pull origin main
    $ rake landing_page:update

If you use an IDE to git "add" it might not see the submodule update. Be sure to use the command git commit -a to ensure it finds it.

### Importing Organizations from Food Rescue Robot

The rake task `robot:import_region` was created to help organizations transition from
[Food Rescue Robot](https://github.com/boulder-food-rescue/food-rescue-robot) to Rootable. This
task copies over a Region's users, sites, and schedules to a Rootable instance.

To import an organization, first load a database dump from Food Rescue Robot into a Postgres
instance that Rootable can connect to. Make note of the DB's connection string, you'll set this
as the `ROBOT_DATABASE_URL` environment variable when running the import.

Next, determine the Region ID of the Organization you are going to import. One way to do this is
by fetching the region in a Rails console like this:

```
  $ rails c
  > Robot::Region.where("name like '%Boulder%'")
  => [#<Robot::Region id: 1, …>, …]
```

Next, choose a cutover date: this is the date after which the organization will begin using
Rootable. Shifts will be scheduled starting one day after this date. You'll pass this in as
`CUTOVER_DATE` in the `yyyy-mm-dd` format.

Finally, run the import:
```
rails robot:import_region CUTOVER_DATE=2023-01-01 REGION_ID=1
```

This will take a moment, but you should see some notes on what's imported as the data is
  transferred.

#### Limitations

Due to time constraints, there are some limitations that should be noted:

* The import is not idempotent. Meaning, a new region will be created each time
  `robot:import_region` is run.
* Pickup/dropoff logs from volunteers will not be created. Only volunteers, locations, and
  shifts are imported.
* Any existing Rootable users (matched by email address) will be removed from their current
  organization and added to the newly created organization. When an existing user is found, their
  organization membership and role will be updated, but additional things like name, timezone,
  and phone number, will not be updated. You will see a note when existing users are encountered
  during the import.
* Since Rootable does not currently support assigning admins to shifts, any Food Rescue Robot
  admins will be imported as volunteers if they are currently assigned shifts.

# Server Environments

### Hosting

Acceptance and Production are hosted on Heroku under Beck's account.

### Environment Variables

Several common features and operational parameters can be set using environment variables.

**Required for deployment**

* `DATABASE_URL` - URL of the PostgreSQL database; e.g. `postgres://user:password@host:port/database`
* `NODE_ENV` - Set to `production` for all deployment environments
* `RACK_ENV` - Set to `production` for all deployment environments
* `RAILS_ENV` - Set to `production` for all deployment environments
* `SECRET_KEY_BASE` - Secret key base for verifying signed cookies. Should be 30+ random characters and secret!
* `SMTP_HOST` - SMTP server address
* `SMTP_PORT` - SMTP server port
* `SMTP_AUTHENTICATION` - SMTP authentication type, "plain" or "login"
* `SMTP_USERNAME` - Username for SMTP authentication
* `SMTP_PASSWORD` - Password for SMTP authentication

**Optional**

* `ASSET_HOST` - Load assets from this host (e.g. CDN) (default: none).
* `BASIC_AUTH_PASSWORD` - Enable basic auth with this password.
* `BASIC_AUTH_USER` - Set a basic auth username (not required, password enables basic auth).
* `CANONICAL_HOSTNAME` - Canonical hostname for this application. Other incoming requests will be redirected to this hostname. Also used by mailers to generate full URLs.
* `DB_POOL` - Number of DB connections per pool (i.e. per worker) (default: RAILS_MAX_THREADS or 5).
* `FORCE_SSL` - Require SSL for all requests, redirecting if necessary (default: false).
* `PORT` - Port to listen on (default: 3000).
* `RACK_TIMEOUT_SERVICE_TIMEOUT` - Terminate requests that take longer than this time (default: 15s).
* `RAILS_LOG_TO_STDOUT` - Log to standard out, good for Heroku (default: false).
* `RAILS_MAX_THREADS` - Threads per worker (default: 5).
* `RAILS_MIN_THREADS` - Threads per worker (default: 5).
* `RAILS_SERVE_STATIC_FILES` - Serve static assets, good for Heroku (default: false).
* `WEB_CONCURRENCY` - Number of puma workers to spawn (default: 1).



Generated with [Raygun](https://github.com/carbonfive/raygun).
