class ShiftsService
  def self.create(shift_params, organization, time_zone)
    zone = ActiveSupport::TimeZone.new(time_zone)
    shift = Shift.new shift_params
    shift.shift_events.each do |shift_event|
      shift_event.event.title = shift.name
      shift_event.event.starts_at = zone.local_to_utc(shift_event.starts_at)
      shift_event.event.ends_at = zone.local_to_utc(shift_event.ends_at)
      shift_event.event.ownable = organization
    end

    if shift.save
      Result.ok(shift)
    else
      Result.error(shift)
    end
  end
end
