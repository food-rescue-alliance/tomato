class EventsService
  def self.first_occurrence(uid)
    Finder.first_occurrence(uid)
  end

  def self.first_occurrence_for_event(event)
    Finder.first_occurrence_for_event(event)
  end

  def self.find_occurrence(uid, starts_at, eventable = ShiftEvent)
    Finder.find_occurrence(uid, starts_at, eventable)
  end

  def self.find_next_occurrence(uid, starts_at)
    Finder.find_next_occurrence(uid, starts_at)
  end

  def self.all_occurrences(owner, eventable_type, range)
    Finder.all_occurrences(owner, eventable_type, range)
  end

  def self.all_user_occurrences(user, range)
    Finder.all_user_occurrences(user, range)
  end

  def self.all_open_user_occurrences(user, range)
    Finder.all_open_user_occurrences(user, range)
  end

  def self.update(occurrence, eventable_attributes, frequency_attributes = {}, eventable = ShiftEvent)
    Updater.update(occurrence, eventable_attributes, frequency_attributes, eventable)
  end

  def self.update_into_future(occurrence, eventable_attributes, frequency_attributes = {}, eventable = ShiftEvent)
    Updater.update_into_future(occurrence, eventable_attributes, frequency_attributes, eventable)
  end

  def self.delete(occurrence, eventable = ShiftEvent)
    Updater.delete(occurrence, eventable)
  end

  def self.delete_into_future(occurrence, eventable = ShiftEvent)
    Updater.delete_into_future(occurrence, eventable)
  end

  def self.post_occurrence_details(occurrence, eventable_attributes, eventable = ShiftEvent)
    Updater.post_occurrence_details(occurrence, eventable_attributes, eventable)
  end
end
