class RruleStr
  attr_reader :rrule, :starts_at

  DAYS = {
    MO: "Monday",
    TU: "Tuesday",
    WE: "Wednesday",
    TH: "Thursday",
    FR: "Friday",
    SA: "Saturday",
    SU: "Sunday"
  }

  def initialize(rrule: "", starts_at: nil)
    @rrule = rrule || ""
    @starts_at = starts_at
  end

  def to_sentence
    return "" if recurrence_options.blank?

    [
      "Repeats",
      occurrences,
      month_days,
      frequency,
      until_end
    ].compact.join(" ")
  end

  private

  def recurrence_options
    @recurrence_options ||= rrule.split(";").map { |pair| pair.split("=") }.to_h
  end

  def occurrences
    return unless recurrence_options["BYDAY"]

    ordinals = [nil, "first", "second", "third", "fourth", "fifth", "last"]
    position = ordinals[recurrence_options["BYSETPOS"].to_i]

    position.nil? ? "on #{days}" : "on the #{position} #{days}"
  end

  def month_days
    return unless recurrence_options["BYMONTHDAY"] && recurrence_options["FREQ"] != "YEARLY"

    by_month_days = recurrence_options["BYMONTHDAY"]
      .split(",")
      .map { |day| day.to_i.ordinalize }
      .to_sentence

    "on the #{by_month_days}"
  end

  def frequency
    case recurrence_options["FREQ"]
    when "DAILY"
      daily_interval
    when "WEEKLY"
      weekly_interval
    when "MONTHLY"
      monthly_interval
    when "YEARLY"
      yearly_interval
    end
  end

  def until_end
    if recurrence_options["UNTIL"]
      date = Time.zone.parse(recurrence_options["UNTIL"]).strftime("%A %b %e")
      "until #{date}"
    elsif recurrence_options["COUNT"]
      "#{recurrence_options["COUNT"]} times"
    end
  end

  def daily_interval
    case recurrence_options["INTERVAL"]
    when "1"
      "every day"
    when "2"
      "every other day"
    else
      "every #{recurrence_options["INTERVAL"]} days"
    end
  end

  def weekly_interval
    case recurrence_options["INTERVAL"]
      when "1"
        "every week"
      when "2"
        "every other week"
      else
        "every #{recurrence_options["INTERVAL"]} weeks"
    end
  end

  def monthly_interval
    case recurrence_options["INTERVAL"]
    when "1"
      "of every month"
    when "2"
      "of every other month"
    else
      "every #{recurrence_options["INTERVAL"]} months"
    end
  end

  def yearly_interval
    "on #{starts_at.strftime("%B")} #{starts_at.day.ordinalize} of every year"
  end

  def days
    case recurrence_options["FREQ"]
    when "MONTHLY"
      monthly_day_str
    when "WEEKLY"
      weekly_day_str
    else
      full_day_names.to_sentence
    end
  end

  def by_days
    recurrence_options["BYDAY"].split(",")
  end

  def full_day_names
    by_days.map do |day|
      if recurrence_options["BYSETPOS"]
        DAYS[day.to_sym]
      else
        DAYS[day.to_sym].pluralize
      end
    end
  end

  def monthly_day_str
    case full_day_names
    when %w[Sunday Monday Tuesday Wednesday Thursday Friday Saturday]
      "day"
    when %w[Monday Tuesday Wednesday Thursday Friday]
      "weekday"
    when %w[Sunday Saturday]
      "weekend day"
    else
      full_day_names.to_sentence(two_words_connector: " or ", last_word_connector: " or ")
    end
  end

  def weekly_day_str
    case full_day_names
    when %w[Sundays Mondays Tuesdays Wednesdays Thursdays Fridays Saturdays]
      "all days"
    when %w[Mondays Tuesdays Wednesdays Thursdays Fridays]
      "weekdays"
    when %w[Sundays Saturdays]
      "weekends"
    else
      full_day_names.to_sentence
    end
  end
end
