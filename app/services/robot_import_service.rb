class RobotImportService
  def self.import_region(id, logger: Rails.logger, cutoff_date: Time.zone.today.to_s)
    new(id, logger, Date.parse(cutoff_date)).import!
  end

  def initialize(region_id, logger, cutoff_date)
    @region_id = region_id
    @logger = logger
    @cutoff_date = cutoff_date
  end

  def import!
    Robot::RobotRecord.transaction do
      @organization = Organization.create!(OrganizationParams.build(region))
      logger.info("Importing volunteers for #{organization.name}:")
      invite_volunteers!
      logger.info("All volunteers imported!")
      logger.info("Importing locations for #{organization.name}:")
      import_locations!
      logger.info("All locations imported!")
      logger.info("Importing shifts for #{organization.name}:")
      import_shifts!
      logger.info("All shifts imported!")
    end
    logger.info("✅ Organization '#{organization.name}' (Robot ID ##{region.id}, " \
                "Rootable ID ##{organization.id}) imported!")
    organization
  end

  private

  attr_reader :region_id, :organization, :logger, :cutoff_date

  def region
    @region ||= Robot::Region.find(region_id)
  end

  def invite_preexisting_user!(volunteer, organization)
    preexisting_user = User.find_by(email: volunteer.email)
    return false if preexisting_user.blank?

    current_orgs = preexisting_user.organizations.map { |o| "'#{o.name} (##{o.id})'" }
    logger.info("    ⚠️  There is already a user present with that email address" \
                " '#{volunteer.email}' (##{volunteer.id}).")
    logger.info("       Removing them from their currently assigned " \
                "#{"organization".pluralize(current_orgs.count)} #{current_orgs.join ", "}.")

    admin = volunteer.region_admin?(region)
    has_shifts = volunteer.has_shifts?(region)

    if admin && has_shifts
      Rails.logger.info("    ⚠️  User '#{volunteer.email}' (Robot ID ##{volunteer.id}) is an admin," \
                        " but will be imported as a volunteer because they have active shifts.")
    end
    preexisting_user.update!(organizations: [organization],
                             roles: (admin && !has_shifts) ? User::ORGANIZATION_ADMINISTRATOR : User::VOLUNTEER)

    logger.info("    ✅ Preexisting user '#{volunteer.email}' (Robot ID ##{volunteer.id}, "\
                " Rootable ID ##{preexisting_user.id}) added to '#{organization.name}'")
    true
  end

  def invite_new_user!(volunteer, organization, region)
    user = User.invite!(UserParams.build(volunteer, organization, region))

    if user.errors.present?
      raise <<~ERR
        ❌ Problem importing User '#{volunteer.email}' (Robot ID ##{volunteer.id}): #{user.errors.full_messages}.
        Rolling back the import.
      ERR
    end

    logger.info("    ✅ User '#{volunteer.email}' (Robot ID ##{volunteer.id}, Rootable ID ##{user.id}) imported!")
  end

  def invite_volunteers!
    region.volunteers.each do |volunteer|
      logger.info("  ➡️  Importing volunteer '#{volunteer.email}'")
      invite_preexisting_user!(volunteer, organization) || invite_new_user!(volunteer, organization, region)
    end
  end

  def import_locations!
    logger.info("Importing sites:")
    region.locations.each do |location|
      logger.info("  ➡️  Importing location '#{location.name}'")
      location.name = "#{location.name} (#{location.id})" if organization.sites.find_by(name: location.name).present?
      location.name = location.id if location.name.blank?
      site = Site.create!(SiteParams.build(location).merge({ organization_id: organization.id }))
      logger.info("    ✅ Site '#{site.name}' (Robot ID ##{location.id}, Rootable ID ##{site.id}) imported!")
    end
  end

  def create_shift!(schedule_chain)
    params = ShiftParams.build(organization: organization, schedule_chain: schedule_chain,
                               cutoff_date: cutoff_date)
    return nil if params.nil?

    shift_result = ShiftsService.create(params, organization, organization.time_zone)
    raise "Something went wrong: #{shift_result.error}" unless shift_result.ok?

    shift_result.value
  end

  def update_occurrences_from_logs!(schedule_chain, shift)
    recurring_volunteer_ids = shift.shift_events.first.user_ids
    recurring_site_ids = shift.shift_events.first.tasks.pluck(:site_id)

    schedule_chain.logs
      .includes(:volunteers)
      .includes(:recipients)
      .where(when: ((cutoff_date)..))
      .group_by(&:when)
      .each do |date, logs|
      occurrence = EventFinder.occurrence_on_date(shift, date.strftime("%F"))

      next if occurrence.nil?

      update_volunteers_from_logs!(recurring_volunteer_ids, logs, occurrence, date)
      update_tasks_from_logs!(recurring_site_ids, logs, occurrence, date)
    end
  end

  def import_shifts!
    region.schedule_chains.includes(:logs).each do |schedule_chain|
      logger.info("  ➡️  Importing Schedule Chain ##{schedule_chain.id}")
      shift = create_shift!(schedule_chain)
      next if shift.nil?

      update_occurrences_from_logs!(schedule_chain, shift)

      logger.info("    ✅ Shift '#{shift.name}' (Robot ID ##{schedule_chain.id}, "\
                  "Rootable ID ##{shift.id}) imported!")
    end
  end

  def update_volunteers_from_logs!(recurring_volunteer_ids, logs, occurrence, date)
    user_ids_from_logs = organization.volunteers.where(email: logs.flat_map(&:volunteers).pluck(:email).uniq).ids

    return if user_ids_from_logs.sort == recurring_volunteer_ids.sort

    logger.info("    🧍 Updated users found on #{date}.")
    EventsService.update(occurrence, { user_ids: user_ids_from_logs },
                         { starts_at: occurrence.starts_at, ends_at: occurrence.ends_at })
  end

  def update_tasks_from_logs!(recurring_site_ids, logs, occurrence, date)
    donor_ids_from_logs = organization.sites.where(robot_location_id: logs.pluck(:donor_id)).ids
    recipient_ids_from_logs = organization.sites.where(robot_location_id: logs.flat_map(&:recipient_ids)).ids

    return if recurring_site_ids.uniq.sort == (donor_ids_from_logs + recipient_ids_from_logs).uniq.sort

    logger.info("    📝 Updated tasks found on #{date}.")

    pickup_task_ids = Task.pickup.where(site_id: donor_ids_from_logs).ids
    dropoff_task_ids = Task.drop_off.where(site_id: recipient_ids_from_logs).ids

    EventsService.update(
      occurrence,
      { ordered_task_ids: pickup_task_ids + dropoff_task_ids },
      { starts_at: occurrence.starts_at, ends_at: occurrence.ends_at }
    )
  end
end
