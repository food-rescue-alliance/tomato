class ExportService
  COLUMNS = %i[
    id
    date
    item_types
    item_weights
    item_descriptions
    total_weight
    donor
    recipients
    volunteers
    transport
    hours_spent
  ]

  def self.export(organization:, start_date:, end_date:)
    new(organization: organization, start_date: start_date, end_date: end_date).export
  end

  def initialize(organization:, start_date:, end_date:)
    @organization = organization
    @date_range = (
      start_date.in_time_zone(organization.time_zone).beginning_of_day..
      end_date.in_time_zone(organization.time_zone).end_of_day
    )
  end

  def export
    shift_events.flat_map { |shift_event| to_row(shift_event) }
  end

  private

  attr_reader :organization, :date_range

  def to_row(shift_event)
    return [] if has_no_logs_or_notes?(shift_event)
    return [pickupless_row(shift_event)] if has_no_pickup_tasks?(shift_event)

    pickup_tasks(shift_event).map do |pickup|
      {
        date: shift_event.starts_at.strftime("%F"),
          transport: shift_event.shift_event_note&.transportation_type&.humanize,
          volunteers: shift_event.users.pluck(:full_name).sort.join(":"),
          donor: pickup.site.name,
          recipients: recipients(shift_event).join(":"),
          item_types: item_types(shift_event, pickup.id).join(":"),
          item_weights: item_weights(shift_event, pickup.id).join(":"),
          item_descriptions: item_descriptions(shift_event, pickup.id).join(":"),
          total_weight: item_weights(shift_event, pickup.id).sum,
          hours_spent: shift_event.shift_event_note&.hours_spent,
          id: shift_event.id
      }
    end
  end

  def has_no_logs_or_notes?(shift_event)
    current_task_logs(shift_event).empty? && shift_event.shift_event_note.blank?
  end

  def has_no_pickup_tasks?(shift_event)
    pickup_tasks(shift_event).empty?
  end

  def current_task_logs(shift_event)
    shift_event.task_logs.filter { |task_log| shift_event.task_ids.include?(task_log.task_id) }
  end

  def pickupless_row(shift_event)
    {
      date: shift_event.starts_at.strftime("%F"),
        transport: shift_event.shift_event_note&.transportation_type&.humanize,
        volunteers: shift_event.users.pluck(:full_name).sort.join(":"),
        hours_spent: shift_event.shift_event_note&.hours_spent,
        id: shift_event.id,
        recipients: recipients(shift_event).join(":")
    }
  end

  def pickup_tasks(shift_event)
    shift_event.tasks.select(&:pickup?)
  end

  def recipients(shift_event)
    shift_event.tasks.filter_map do |task|
      task.site.name if task.drop_off?
    end
  end

  def logs(shift_event, task_id)
    logs = shift_event.task_logs.select { |log| log.task_id == task_id }
    logs.sort_by { |log| log.food_taxonomy.name }
  end

  def item_types(shift_event, task_id)
    logs(shift_event, task_id).map { |log| log.food_taxonomy.name }
  end

  def item_weights(shift_event, task_id)
    logs(shift_event, task_id).map(&:weight)
  end

  def item_descriptions(shift_event, task_id)
    logs(shift_event, task_id).map(&:notes)
  end

  def shift_events
    @shift_events ||= ShiftEvent.includes(:event, :users, :tasks, :shift_event_note, :task_logs,
                                          task_logs: [:task]).where(events: { ownable: organization,
starts_at: date_range }).order(:starts_at, :"shift_event_tasks.order")
    @shift_events
  end
end
