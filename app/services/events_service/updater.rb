class EventsService
  class Updater
    # == update
    #
    # Updates an occurrence's eventable attributes. Will create a single event for occurrence if it resulted from a
    # recurrence. Will also update the UNTIL recurrence option to slice the occurrence from the recurrence.
    #
    # NOTE: slicing behavior only works for recurrences that are monthly or smaller. Yearly recurrences are not
    # supported. If desired, a more sophisticated behavior will need to be implemented to properly determine the
    # +ends_at+ occurrence cutoff for the timeline.
    #
    def self.update(occurrence, eventable_attributes, frequency_attributes = {}, eventable = ShiftEvent)
      event = Finder.find_occurrence(occurrence.uid, occurrence.starts_at, eventable)
      if event.single?
        ActiveRecord::Base.transaction do
          update_event(event, frequency_attributes, eventable_attributes)
        end
        event
      else
        new_event = Event.create
        ActiveRecord::Base.transaction do
          new_event.assign_attributes({
            **event.attributes.symbolize_keys.except(:id, :created_at, :updated_at, :recurrence_options),
                                        eventable: event.eventable.dup.tap do |e|
                                                     e.assign_attributes event.eventable.dup_nested_attributes
                                                   end
                                      })
          frequency_attributes["rrule"] = nil
          update_event(new_event, frequency_attributes, eventable_attributes)
          event.exdates << event.starts_at
          event.save!
        end
        new_event
      end
    end

    def self.update_into_future(occurrence, eventable_attributes, frequency_attributes = {}, eventable = ShiftEvent)
      change_exdates(frequency_attributes["starts_at"], occurrence, eventable)
      timeline = EventOccurrenceTimeline.new(
        events: Event.series(occurrence.uid, occurrence.starts_at, eventable),
        starts_at: occurrence.starts_at,
        ends_at: occurrence.starts_at + 1.day
      )

      previous_event, this_event = timeline.slice occurrence.uid, occurrence.starts_at
      future_events = get_future_events(occurrence, eventable)

      added_users, removed_users = get_added_or_removed_users(this_event, eventable_attributes)
      added_tasks, removed_tasks = get_added_or_removed_tasks(this_event, eventable_attributes)

      ActiveRecord::Base.transaction do
        previous_event.save! if previous_event.present?

        update_event(this_event, frequency_attributes, eventable_attributes)

        future_events, conflicts = merge_users_attribute_changes(future_events, added_users, removed_users)
        future_events.each(&:save!)

        mail_alerts(User.find(added_users), this_event, conflicts)

        future_events = merge_tasks_attribute_changes(future_events, added_tasks, removed_tasks)
        future_events.each(&:save!)

        this_event
      end
    end

    def self.delete(occurrence, eventable = ShiftEvent)
      event = Finder.find_occurrence(occurrence.uid, occurrence.starts_at, eventable)

      event.exdates << event.starts_at

      event.save!
    end

    def self.post_occurrence_details(occurrence, eventable_attributes, eventable = ShiftEvent)
      timeline = EventOccurrenceTimeline.new(
        events: Event.series(occurrence.uid, occurrence.starts_at, eventable),
        starts_at: occurrence.starts_at,
        ends_at: occurrence.starts_at + 1.month
      )
      previous_event, this_event, next_event = timeline.partition occurrence.uid, occurrence.starts_at

      ActiveRecord::Base.transaction do
        previous_event.save! if previous_event.present?

        this_event.recurrence_options = nil unless this_event.single?
        update_event(this_event, {}, eventable_attributes)

        next_event.save! if next_event.present?
      end

      this_event
    end

    def self.delete_into_future(occurrence, eventable = ShiftEvent)
      timeline = EventOccurrenceTimeline.new(
        events: Event.series(occurrence.uid, occurrence.starts_at, eventable),
        starts_at: occurrence.starts_at,
        ends_at: occurrence.starts_at + 1.month
      )
      previous_event, this_event = timeline.slice occurrence.uid, occurrence.starts_at

      ActiveRecord::Base.transaction do
        previous_event.save! if previous_event.present?

        this_event.destroy!
      end
    end

    class << self
      private

      def get_future_events(occurrence, eventable)
        candidate_events = Event.series(occurrence.uid, occurrence.starts_at, eventable)

        extended_timeline = EventOccurrenceTimeline.new(
          events: candidate_events,
          starts_at: occurrence.starts_at,
          ends_at: occurrence.starts_at + 1.year
        )

        candidate_events.filter_map do |future_event|
          next if future_event == occurrence.event
          next future_event if occurrence.starts_at <= future_event.starts_at

          # If we get here, i.e. when `occurrence.starts_at > future_event.starts_at`, the "future_event" is a
          # recurrence that spans the date we're updating. In this case, we need to slice this future event at
          # its next occurrence, as to not return past occurrences of the event.
          next_recurrence = extended_timeline.find_next_recurrence(future_event, after: occurrence.starts_at)
          next if next_recurrence.nil?

          old_future_event, new_future_event = extended_timeline.slice future_event.uid, next_recurrence.starts_at
          old_future_event.save!
          new_future_event
        end
      end

      def mail_alerts(added_users, this_event, conflicts)
        return unless added_users.count.positive? && conflicts.count.positive?

        ApplicationMailer.with(
          organization: this_event.ownable,
          volunteers: added_users,
          conflicts: conflicts
        ).volunteer_overflow_flag_email.deliver_now
      end

      def change_exdates(starts_at, occurrence, eventable)
        if occurrence.exdates.empty? || starts_at.blank? ||
           (occurrence.starts_at.utc.strftime("%H%M%S%N") == starts_at.utc.strftime("%H%M%S%N"))
          return
        end

        event = Finder.find_occurrence(occurrence.uid, occurrence.starts_at, eventable)
        event.exdates = event.exdates.map do |e|
          e.change({ hour: starts_at.hour, min: starts_at.min })
        end
        event.save!
      end

      def merge_users_attribute_changes(events, added_users, removed_users)
        conflicts = []
        events = events.map do |event|
          users = event.eventable.user_ids.map(&:to_s)
          if users.count.positive? && added_users.count.positive?
            first_occurrence = Finder.first_occurrence_for_event(event)
            conflicts.push(first_occurrence)
          end
          users += added_users
          users -= removed_users
          users = users.uniq
          event.eventable.assign_attributes(user_ids: users)
          event
        end
        [events, conflicts]
      end

      def merge_tasks_attribute_changes(events, added_tasks, removed_tasks)
        events.map do |event|
          tasks = event.eventable.task_ids.map(&:to_s)
          tasks += added_tasks
          tasks -= removed_tasks
          tasks = tasks.uniq
          event.eventable.ordered_task_ids = tasks
          event
        end
      end

      def diff_ids(first, second)
        first ||= []
        second ||= []

        diff = first.map(&:to_s) - second.map(&:to_s)
        diff.reject(&:empty?)
      end

      def get_added_or_removed_users(this_event, eventable_attributes)
        added_users = diff_ids(eventable_attributes["user_ids"] || eventable_attributes[:user_ids],
                               this_event.eventable.user_ids)
        removed_users = diff_ids(this_event.eventable.user_ids,
                                 eventable_attributes["user_ids"] || eventable_attributes[:user_ids])

        [added_users, removed_users]
      end

      def get_added_or_removed_tasks(this_event, eventable_attributes)
        added_tasks = diff_ids(eventable_attributes["task_ids"] || eventable_attributes[:task_ids],
                               this_event.eventable.task_ids)
        removed_tasks = diff_ids(this_event.eventable.task_ids,
                                 eventable_attributes["task_ids"] || eventable_attributes[:task_ids])
        [added_tasks, removed_tasks]
      end

      def rename_task_ids(attrs)
        return attrs unless attrs.has_key?(:task_ids)

        { ordered_task_ids: attrs[:task_ids] }.merge(attrs.except(:task_ids))
      end

      def update_event(event, frequency_attributes, eventable_attributes)
        event.eventable.assign_attributes(rename_task_ids(eventable_attributes))
        event.eventable.save!
        event.assign_attributes frequency_attributes
        event.save!
      end
    end
  end
end
