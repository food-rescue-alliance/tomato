class EventsService
  class EventOccurrenceTimeline
    def initialize(events:, starts_at:, ends_at:)
      @ordered_events = []
      @occurrence_timeline = {}
      event_stack = []

      return if starts_at.blank? || ends_at.blank?

      events.sort_by(&:starts_at).reverse_each do |event|
        EventOccurrenceNode.from_event(
          event: event,
          next_event: event_stack.empty? ? nil : event_stack.last,
          starts_at: starts_at,
          ends_at: ends_at
        ).each do |occurrence|
          @occurrence_timeline["#{event.uid}-#{occurrence.starts_at.iso8601}"] = occurrence
        end
        event_stack << event
      end

      @ordered_events = event_stack.reverse
    end

    def first(uid)
      first_key = @occurrence_timeline.keys.filter { |k| k.start_with? uid }.min

      return nil if first_key.blank?

      @occurrence_timeline[first_key]
    end

    def fetch(uid, starts_at)
      @occurrence_timeline["#{uid}-#{starts_at.iso8601}"]
    end

    def fetch_next(uid, starts_at)
      keys = @occurrence_timeline
        .keys
        .filter { |k| k.start_with? uid }
        .sort
      this_occurrence_index = keys.find_index "#{uid}-#{starts_at.iso8601}"
      next_occurrence_index = this_occurrence_index + 1

      return nil if next_occurrence_index >= keys.length

      @occurrence_timeline[keys[next_occurrence_index]]
    end

    def partition(uid, starts_at)
      previous_event, this_event = slice uid, starts_at
      next_occurrence = fetch_next uid, starts_at

      if next_occurrence.nil?
        next_event = nil
        cutoff = this_event.ends_at
      else
        _, next_event = slice uid, next_occurrence.starts_at
        cutoff = next_event.starts_at - 1.minute
      end

      this_event.recurrence_options["UNTIL"] = cutoff.to_s if this_event.recurrence?
      [previous_event, this_event, next_event]
    end

    def slice(event_uid, starts_at)
      occurrence = fetch event_uid, starts_at

      return [] if occurrence.nil?

      event = occurrence.event.id ? Event.find(occurrence.event.id) : occurrence.event

      return [nil, event] if occurrence.head?

      new_event = dup_event(occurrence, event)

      if event.recurrence?
        cutoff = occurrence.starts_at - 1.minute
        event.recurrence_options["UNTIL"] = cutoff.to_s
      end

      [event, new_event]
    end

    def occurrences
      @occurrence_timeline
        .values
        .sort_by(&:starts_at)
    end

    def find_next_recurrence(event, after:)
      occurrences.find { |o| o.starts_at > after && o.event.id == event.id }
    end

    def dup_event(occurrence, event)
      new_event = event.dup
      new_event.assign_attributes(
        starts_at: occurrence.starts_at,
        ends_at: occurrence.ends_at,
        eventable: event.eventable.dup
      )
      new_event.eventable.assign_attributes event.eventable.dup_nested_attributes
      new_event
    end

    class EventOccurrenceNode < EventOccurrence
      attr_reader :next_event

      def initialize(event:, next_event:, starts_at:)
        super(
          event: event,
          starts_at: starts_at.utc,
          ends_at: if event.recurrence?
                     starts_at.change(hour: event.ends_at.utc.hour, min: event.ends_at.utc.min)
                   else
                     event.ends_at.utc
                   end
        )
        @next_event = next_event
      end

      def self.from_event(event:, next_event:, starts_at:, ends_at:)
        if event.recurrence?
          time_zone = ActiveSupport::TimeZone.new(event.ownable.time_zone)
          local_starts_at = time_zone.at(starts_at)
          local_ends_at = time_zone.at(ends_at)
          rule = RRule::Rule.new(event.rrule,
                                 dtstart: event.starts_at,
                                 tzid: time_zone.tzinfo.identifier,
                                 exdate: event.exdates)

          occurrences = rule.between(local_starts_at, local_ends_at)
          occurrences.map do |occurrence_starts_at|
            new(event: event, next_event: next_event, starts_at: occurrence_starts_at.utc)
          end
        else
          return [] unless (starts_at..ends_at).overlaps?(event.starts_at..event.ends_at)

          [new(event: event, next_event: next_event, starts_at: event.starts_at)]
        end
      end
    end

    private_constant :EventOccurrenceNode
  end
end
