class EventsService
  class Finder
    def self.first_occurrence(uid)
      return nil if uid.blank?

      events = Event.where(uid: uid).order("starts_at")
      starts_at = events.first.starts_at

      timeline = EventOccurrenceTimeline.new(
        events: events,
        starts_at: starts_at,
        ends_at: starts_at + 1.year
      )

      timeline.first uid
    end

    def self.first_occurrence_for_event(event)
      return nil if event.blank?

      events = [event]
      starts_at = event.starts_at

      timeline = EventOccurrenceTimeline.new(
        events: events,
        starts_at: starts_at,
        ends_at: starts_at + 1.year
      )

      timeline.first event.uid
    end

    def self.find_occurrence(uid, starts_at, eventable = ShiftEvent)
      return nil if uid.blank? || starts_at.blank?

      timeline = EventOccurrenceTimeline.new(
        events: Event.series(uid, starts_at, eventable),
        starts_at: starts_at,
        ends_at: starts_at
      )

      timeline.fetch uid, starts_at
    end

    def self.find_next_occurrence(uid, starts_at)
      return nil if uid.blank?

      events = Event.where(uid: uid).order("starts_at")

      timeline = EventOccurrenceTimeline.new(
        events: events,
        starts_at: starts_at,
        ends_at: starts_at + 1.year
      )

      occurrence = timeline.occurrences
        .sort_by(&:starts_at)
        .find { |o| o.starts_at >= starts_at }

      return nil if occurrence.nil?

      timeline.fetch uid, occurrence.starts_at
    end

    def self.all_occurrences(owner, eventable_type, range)
      events = Event.all_by(owner, eventable_type, range.first)
      timeline = EventOccurrenceTimeline.new(
        events: events,
        starts_at: range.first,
        ends_at: range.end
      )

      timeline.occurrences
    end

    def self.all_user_occurrences(user, range)
      events = ShiftEvent
        .joins(:users)
        .joins(:event)
        .where(users: { id: user })
        .merge(Event.on_or_after(range.first))
        .map(&:event)

      timeline = EventOccurrenceTimeline.new(
        events: events,
        starts_at: range.first,
        ends_at: range.end
      )

      timeline.occurrences
    end

    def self.all_open_user_occurrences(user, range)
      recurring_events = []
      one_time_events = []

      user.organizations.each do |organization|
        open_org_events = ShiftEvent
          .joins(:event)
          .where("events.ownable_id = ? AND events.ownable_type = ?", organization.id, Organization.name)
          .left_outer_joins(:users).where(users: { id: nil })
          .merge(Event.on_or_after(range.first))
          .map(&:event)

        recurring_open_org_events = open_org_events.select { |event| event.recurrence? == true }
        one_time_open_org_events = open_org_events.select { |event| event.recurrence? == false }

        recurring_events += recurring_open_org_events
        one_time_events += one_time_open_org_events
      end

      recurring_timeline = EventOccurrenceTimeline.new(
        events: recurring_events,
        starts_at: range.first,
        ends_at: range.end
      )

      one_time_timeline = EventOccurrenceTimeline.new(
        events: one_time_events,
        starts_at: range.first,
        ends_at: range.end
      )

      { recurring: recurring_timeline.occurrences, one_time: one_time_timeline.occurrences }
    end
  end
end
