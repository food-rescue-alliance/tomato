class AbsenceService
  def self.all_affected_shifts(user, absence, organization)
    range = beginning_of_day(absence.starts_at,
                             organization.time_zone)..end_of_day(absence.ends_at, organization.time_zone)
    EventsService.all_user_occurrences(user, range)
      .select { |shift_event| shift_event_belongs_to_org?(shift_event, organization) }
  end

  def self.remove_user_from_shifts(user, absence, organization)
    all_affected_shifts(user, absence, organization).each do |occurrence|
      EventsService.update(occurrence, { user_ids: occurrence.shift_event.user_ids - [user.id] },
                           { starts_at: occurrence.starts_at, ends_at: occurrence.ends_at })
    end
  end

  def self.send_scheduled_email(user, absence, organization)
    return if organization.internal_alerts_email.blank?

    shifts = AbsenceService.all_affected_shifts(user, absence, organization).map do |shift_event|
      {
        title: shift_event.event.title,
        starts_at: Time.zone.parse(shift_event.starts_at.to_s).strftime("%D")
      }
    end

    return if shifts.empty?

    AbsenceMailer.with(
      full_name: user.full_name,
      email: organization.internal_alerts_email,
      selected_dates: {
        start_date: absence.starts_at.strftime("%D"),
        end_date: absence.ends_at.strftime("%D")
      },
      affected_shifts: shifts
    ).scheduled_email.deliver_now
  end

  def self.send_updated_email(user, old_absence, updated_absence, organization)
    return if organization.internal_alerts_email.blank?

    AbsenceMailer.with(
      full_name: user.full_name,
      email: organization.internal_alerts_email,
      old_dates: {
        start_date: old_absence[:starts_at].strftime("%D"),
        end_date: old_absence[:ends_at].strftime("%D")
      },
      updated_dates: {
        start_date: Time.find_zone(organization.time_zone).parse(updated_absence[:starts_at]).strftime("%D"),
        end_date: Time.find_zone(organization.time_zone).parse(updated_absence[:ends_at]).strftime("%D")
      }
    ).updated_email.deliver_now
  end

  def self.send_volunteer_updated_email(user, organization)
    AbsenceMailer.with(
      email: user.email,
      organization_name: organization.name
    ).volunteer_updated_email.deliver_now
  end

  def self.send_canceled_email(user, absence, organization)
    return if organization.internal_alerts_email.blank?

    AbsenceMailer.with(
      full_name: user.full_name,
      email: organization.internal_alerts_email,
      selected_dates: {
        start_date: absence.starts_at.strftime("%D"),
        end_date: absence.ends_at.strftime("%D")
      }
    ).canceled_email.deliver_now
  end

  def self.send_volunteer_canceled_email(user, organization)
    AbsenceMailer.with(
      email: user.email,
      organization_name: organization.name
    ).volunteer_canceled_email.deliver_now
  end

  def self.beginning_of_day(raw_date, time_zone)
    Time.find_zone(time_zone).parse(raw_date.to_s).beginning_of_day
  end

  def self.end_of_day(raw_date, time_zone)
    Time.find_zone(time_zone).parse(raw_date.to_s).end_of_day
  end

  def self.shift_event_belongs_to_org?(shift_event, organization)
    shift_event.event.ownable_type == Organization.name &&
      shift_event.event.ownable_id == organization.id
  end
end
