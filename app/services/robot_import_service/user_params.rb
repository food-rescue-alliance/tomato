class RobotImportService
  module UserParams
    def self.build(volunteer, organization, region)
      admin = volunteer.region_admin?(region)
      has_shifts = volunteer.has_shifts?(region)

      if admin && has_shifts
        Rails.logger.info("User '#{volunteer.email}' (Robot ID ##{volunteer.id}) is an admin," \
                          " but will be imported as a volunteer because they have active shifts.")
      end

      {
        skip_invitation: true, # Do not send an invite email to the imported users.
        email: volunteer.email,
        organization_ids: [organization.id],
        roles: (admin && !has_shifts) ? User::ORGANIZATION_ADMINISTRATOR : User::VOLUNTEER,
        time_zone: organization.time_zone,
        full_name: volunteer.name,
        phone: cleaned_phone(volunteer.phone),
        absences: volunteer.absences.map do |absence|
          Absence.new(starts_at: absence.start_date, ends_at: absence.stop_date)
        end
      }
    end

    class << self
      private

      def cleaned_phone(phone)
        return nil if phone.blank?

        cleaned_phone = phone&.tr("^0-9", "")
        return nil if cleaned_phone.length < 10

        cleaned_phone
      end
    end
  end
end
