class RobotImportService
  module AddressParams
    def self.build(address_str)
      # Addresses are not consistent in Robot
      # So we simply save them on one line for now.
      {
        street_one: address_str.presence || "-",
          city: "-",
          state: "-",
          zip: "-"
      }
    end
  end
end
