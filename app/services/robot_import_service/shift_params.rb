class RobotImportService
  class ShiftParams
    def self.build(organization:, schedule_chain:, cutoff_date:)
      new(organization: organization, schedule_chain: schedule_chain, cutoff_date: cutoff_date).params
    end

    def initialize(organization:, schedule_chain:, cutoff_date:)
      @organization = organization
      @schedule_chain = schedule_chain
      @cutoff_date = cutoff_date
    end

    def params
      return nil if should_not_be_imported?

      {
        "name" => name,
        "shift_events_attributes" =>
          [
            {
              "event_attributes" => {
                "starts_at" => starts_at,
                "ends_at" => ends_at,
                "rrule" => rrule
              },
              "admin_notes" => schedule_chain.admin_notes,
              "public_notes" => schedule_chain.public_notes,
              "shift_event_tasks_attributes" => task_ids.each_with_index.map do |task_id, i|
                                                  { task_id: task_id, order: i }
                                                end,
              "user_ids" => user_ids
            }
          ]
      }
    end

    private

    attr_reader :organization, :schedule_chain, :cutoff_date

    def task_ids
      sites = organization.sites.includes(:tasks)
      num_tasks = schedule_chain.schedules.count
      schedule_chain.schedules.order(:position).each_with_index.flat_map { |s, i|
        site = sites.find_by(robot_location_id: s.location_id)
        next if site.nil?

        site.tasks.where(task_type: get_task_type(site, i, num_tasks)).ids
      }.compact
    end

    def get_task_type(site, task_index, num_tasks)
      # If it's not a hub, then let the system decide.
      return %i[pickup drop_off] unless site.hub?

      # Hubs can be marked as pick up and/or drop off with this logic:
      #  - hub is first task => pick up
      #  - hub in middle => pick up + drop off
      #  - hub is last task => drop off
      return [:pickup] if task_index.zero?
      return [:drop_off] if task_index == num_tasks - 1

      %i[pickup drop_off]
    end

    def user_ids
      return [] if schedule_chain.volunteers.empty?

      organization.volunteers.where(email: schedule_chain.volunteers.pluck(:email)).ids
    end

    def should_not_be_imported?
      required_attrs = %i[detailed_start_time detailed_stop_time detailed_date]
      return true if required_attrs.any? { |attr| schedule_chain.send(attr).blank? }
      return true if schedule_chain.detailed_date < cutoff_date && schedule_chain.frequency != "weekly"

      false
    end

    def name
      "Shift ##{schedule_chain.id}"
    end

    def day_of_week
      %i[sunday monday tuesday wednesday thursday friday saturday][schedule_chain.day_of_week || 0]
    end

    def date_of_first_occurrence
      if schedule_chain.frequency == "weekly"
        cutoff_date.next_occurring(day_of_week)
      else
        schedule_chain.detailed_date
      end
    end

    def build_initial_time(time)
      # NOTE: Currently, the ShiftService assumes all times come in with no time zone.
      # The time zone/offset is ignored, and the time is shifted to UTC using the region's
      # time zone. So we leave it off here, and instantiate a time with no UTC offset.
      # And output it in a string like: 'YYYY-MM-DD HH-MM'
      Time.new( # rubocop:disable Rails/TimeZone
        date_of_first_occurrence.year,
        date_of_first_occurrence.month,
        date_of_first_occurrence.day,
        time.hour,
        time.min
      ).strftime("%F %R")
    end

    def starts_at
      build_initial_time(schedule_chain.detailed_start_time)
    end

    def ends_at
      build_initial_time(schedule_chain.detailed_stop_time)
    end

    def rrule
      return nil if schedule_chain.frequency != "weekly"

      day = day_of_week.to_s.upcase[0...2]
      "FREQ=WEEKLY;BYDAY=#{day};INTERVAL=1"
    end
  end
end
