class RobotImportService
  module EventFinder
    def self.occurrence_on_date(shift, date)
      shift_event = shift.shift_events.first
      raise "No shift events for shift #{shift.id}" if shift_event.nil?

      all_day = date.in_time_zone(shift_event.event.ownable.time_zone).all_day
      events = shift.shift_events.joins(:event).merge(Event.on_or_after(all_day.first)).map(&:event)

      EventsService::EventOccurrenceTimeline.new(events: events, starts_at: all_day.first,
                                                 ends_at: all_day.end).occurrences.first
    end
  end
end
