class RobotImportService
  module OrganizationParams
    def self.build(region)
      {
        name: region.name,
          tagline: region.tagline,
          email: region.volunteer_coordinator_email,
          phone: region.phone,
          federal_tax_id: region.tax_id,
          time_zone: lookup_time_zone(region.time_zone),
          internal_alerts_email: region.volunteer_coordinator_email
      }
    end

    class << self
      private

      KNOWN_ZONE_MAPPING = {
        "central" => "America/Chicago",
            "CST" => "America/Chicago",
            "Eastern " => "America/New_York",
            "Eastern Standard Time" => "America/New_York",
            "EST" => "America/New_York",
            "MST" => "America/Denver",
            "Oceania/Brisbane" => "Australia/Brisbane",
            "pacific standard" => "America/Los_Angeles",
            "PST" => "America/Los_Angeles",
            "US/Central" => "America/Chicago",
            "US/Mountain" => "America/Denver"
      }

      def lookup_time_zone(zone_name)
        return "Etc/UTC" if zone_name.blank?
        return zone_name if ActiveSupport::TimeZone.all.map { |tz| tz.tzinfo.identifier }.include?(zone_name)

        KNOWN_ZONE_MAPPING.fetch(zone_name, "Etc/UTC")
      end
    end
  end
end
