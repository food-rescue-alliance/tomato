class RobotImportService
  module SiteParams
    TYPES = %w[Donor Recipient Hub]
    SITEABLE_BY_LOCATION_TYPE = {
      0 => Recipient, # (recipient)
      1 => Donor, # (donor)
      2 => Hub, # (hub)
      3 => Donor, # (seller)
      4 => Recipient # (buyer)
    }

    def self.build(location)
      siteable_class = SITEABLE_BY_LOCATION_TYPE[location.location_type]
      {
        robot_location_id: location.id,
        name: location.name,
        siteable: siteable_class.new,
        lat: location.lat,
        lng: location.lng,
        detailed_hours_json: location.detailed_hours_json,
        admin_notes: admin_notes(location),
        receipt_key: location.receipt_key,
        website: location.website,
        instructions: instructions(location),
        address: Address.new(AddressParams.build(location.address))
      }
    end

    class << self
      private

      def admin_notes(location)
        <<~STR
          #{location.admin_notes.present? ? "#{location.admin_notes}\n\n" : ""}
          #{location.hours.present? ? "Hours\n-----------------------\n\n#{location.hours}\n\n" : ""}
          Contact Info
          -----------------------

          Contact:
          #{location.contact}

          Phone:
          #{location.phone}

          Email:
          #{location.email}

          Twitter Handle:
          #{location.twitter_handle}
        STR
      end

      def instructions(location)
        <<~STR
          #{location.public_notes.present? ? "#{location.public_notes}\n\n" : ""}
          Pickup/Drop Logistics
          -----------------------

          Onsite Contact_Info:
          #{location.onsite_contact_info}

          Entry Info:
          #{location.entry_info}

          Equipment Storage Info:
          #{location.equipment_storage_info}

          Food Storage Info:
          #{location.food_storage_info}

          Exit Info:
          #{location.exit_info}
        STR
      end
    end
  end
end
