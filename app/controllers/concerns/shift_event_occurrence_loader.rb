module ShiftEventOccurrenceLoader
  extend ActiveSupport::Concern

  def find_occurrence
    id_param = params.key?(:id) ? params[:id] : params[:shift_event_occurrence_id]
    uid, _, starts_at = id_param.rpartition "-"

    return Result.new(nil, :bad_request) if uid.blank? || starts_at.blank?

    occurrence = EventsService.find_occurrence uid, Time.zone.at(starts_at.to_i).utc, ShiftEvent

    return Result.new(nil, :not_found) if occurrence.nil?

    Result.new occurrence
  end

  def load_occurrence
    result = find_occurrence

    if result.ok?
      @occurrence = result.value
      @organization = @occurrence.ownable
    else
      skip_authorization
      head result.error
    end
  end
end
