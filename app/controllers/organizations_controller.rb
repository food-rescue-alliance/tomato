class OrganizationsController < ApplicationController
  before_action :authenticate_user!
  before_action :organization, only: %i[show edit update destroy]

  def index
    authorize Organization
    @organizations = policy_scope(Organization).all
  end

  def show
    authorize @organization

    @starts_at = Time.iso8601(query_params["starts_at"])
    @ends_at = @starts_at + 7.days - 1.minute
    range = @starts_at..@ends_at

    @occurrences = EventsService.all_occurrences @organization, ShiftEvent, range
  end

  def new
    authorize Organization

    @organization = Organization.new
  end

  def edit
    authorize @organization
  end

  def create
    authorize Organization

    @organization = Organization.new(organization_params)

    if @organization.save
      redirect_to @organization, notice: "Organization was successfully created."
    else
      render :new
    end
  end

  def update
    authorize @organization

    if @organization.update(organization_params)
      redirect_to edit_organization_path(@organization), notice: "Organization was successfully updated."
    else
      render :edit
    end
  end

  def destroy
    authorize Organization

    @organization.destroy
    redirect_to organizations_url, notice: "Organization was successfully destroyed."
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def organization
    @organization = Organization.find(params[:id])
  end

  # Only allow a trusted parameter "allow list" through.
  def organization_params
    params.require(:organization).permit(
      :name,
      :tagline,
      :phone,
      :email,
      :internal_alerts_email,
      :federal_tax_id,
      :time_zone,
      address_attributes: %i[id street_one street_two city state zip _destroy]
    )
  end

  def query_params
    defaults = {
      "starts_at" => Time.current.midnight.iso8601
    }
    permitted_params = params.permit(:id, :starts_at)
    defaults
      .merge(permitted_params)
  end
end
