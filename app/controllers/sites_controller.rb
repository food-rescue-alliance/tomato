class SitesController < ApplicationController
  before_action :authenticate_user!
  before_action :organization
  before_action :site, only: %i[show edit update]

  def index
    authorize @organization, policy_class: SitePolicy

    @siteable_type = siteable_type
    @sites = @organization.sites.send(@siteable_type.pluralize.downcase)

    # always show hubs
    @sites += @organization.sites.hubs unless @siteable_type.pluralize.downcase == "hubs"
  end

  def new
    authorize @organization, policy_class: SitePolicy

    @siteable_type = siteable_type
    @site = Site.new
  end

  def create
    authorize @organization, policy_class: SitePolicy

    @site = Site.create(new_site_params.merge(organization: @organization))

    if @site.persisted?
      redirect_to organization_site_path(@organization, @site),
                  notice: "#{@site.siteable_name.capitalize} was successfully created."
    else
      render :new
    end
  end

  def show
    authorize @organization, policy_class: SitePolicy
  end

  def edit
    authorize @organization, policy_class: SitePolicy
  end

  def update
    authorize @organization, policy_class: SitePolicy

    if @site.update(edit_site_params)
      redirect_to organization_site_path(@organization, @site),
                  notice: "#{@site.siteable_name.capitalize} was successfully updated."
    else
      render :edit
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def site
    @site = Site.find(params[:id])
  end

  def organization
    @organization = Organization.find(params[:organization_id])
  end

  def site_params
    unformatted_site_params = params.require(:site)
      .permit(:name,
              :siteable_type,
              :instructions,
              :admin_notes,
              :website,
              address_attributes: %i[id street_one street_two city state zip _destroy])
    unformatted_site_params.merge(
      siteable: unformatted_site_params.delete(:siteable_type)&.classify&.safe_constantize&.new
    )
  end

  def new_site_params
    address_attrs = site_params[:address_attributes]
    return site_params if address_attrs.select { |_k, value| value.present? }.present?

    site_params.except(:address_attributes)
  end

  def edit_site_params
    site_params = updateable_site_params
    address_attr_params = site_params[:address_attributes]
    return site_params if address_attr_params.select { |_k, value| value.present? }.present?

    existing_site_address = @site.address
    return site_params.except(:address_attributes) if existing_site_address.nil?

    site_params.merge(address_attributes: { id: existing_site_address.id, _destroy: true })
  end

  def updateable_site_params
    site_params.except(:siteable)
  end

  def siteable_type
    return default_index_siteable_type unless Site.siteable_types.include?(params[:siteable_type]&.capitalize)

    params[:siteable_type].capitalize
  end

  def default_index_siteable_type
    "Donor"
  end
end
