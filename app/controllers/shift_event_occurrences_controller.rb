class ShiftEventOccurrencesController < ApplicationController
  before_action :authenticate_user!

  include ShiftEventOccurrenceLoader
  before_action :load_occurrence, only: %i[show edit update destroy]
  before_action :authorize_resources

  def index
    if params[:user_id] == "me"
      skip_authorization

      range = (Time.zone.now.midnight)..(1.week.from_now.midnight)
      @occurrences = EventsService.all_user_occurrences current_user, range
    else
      @occurrences = []
    end
  end

  def show
    @food_taxonomies = FoodTaxonomy.all
  end

  def edit
    @tasks = if edit_tasks?
               Task.find(edit_shift_event_params[:task_ids].compact_blank)
             else
               @occurrence.shift_event.tasks
             end

    @available_tasks = Task
      .joins(:site)
      .where(sites: { organization_id: @occurrence.ownable.id })
      .reject { |t| @tasks.include?(t) }

    @users = if edit_users?
               User.where(id: edit_shift_event_params[:user_ids])
             else
               @occurrence.shift_event.users
             end
    @future_events_present = EventsService.find_next_occurrence(@occurrence.uid, @occurrence.starts_at + 1).present?
    @available_users = @occurrence.ownable.volunteers.reject { |u| @users.include?(u) }
  end

  def update
    if update_shift_event_params.key?(:range) && update_shift_event_params[:range] == "THISANDFUTURE"
      EventsService.update_into_future(
        @occurrence,
        shift_event_params,
        shift_frequency_params
      )
    else
      EventsService.update(
        @occurrence,
        shift_event_params,
        shift_frequency_params
      )
    end

    redirect_to updated_path, notice: "Shift was successfully updated."
  end

  def destroy
    if @occurrence.single?
      @occurrence.destroy
    elsif update_shift_event_params.key?(:range) && update_shift_event_params[:range] == "THISANDFUTURE"
      EventsService.delete_into_future(@occurrence)
    else
      EventsService.delete(@occurrence)
    end

    redirect_to organization_path(@occurrence.ownable.id), notice: "Shift was successfully destroyed."
  end

  private

  def authorize_resources
    authorize @occurrence.event if @occurrence
  end

  def updated_path
    occurrence = EventsService.find_next_occurrence(@occurrence.uid, shift_frequency_params["starts_at"])
    shift_event_occurrence_path(occurrence.id)
  end

  def shift_event_params
    params
      .require(:shift_event)
      .permit(user_ids: [], task_ids: [])
      .tap do |shift_event_params|
        shift_event_params.require(%i[user_ids task_ids])
      end
  end

  def shift_hidden_params
    params
      .permit(%i[hidden_date hidden_start hidden_end hidden_rrule])
  end

  def shift_frequency_params
    {
      "starts_at" => Time.zone.parse("#{shift_hidden_params[:hidden_date]} #{shift_hidden_params[:hidden_start]}"),
      "ends_at" => Time.zone.parse("#{shift_hidden_params[:hidden_date]} #{shift_hidden_params[:hidden_end]}"),
      "rrule" => shift_hidden_params[:hidden_rrule],
      "title" => params.fetch(:event, {}).permit(:title)[:title] || @occurrence.title
    }
  end

  def edit_shift_event_params
    params
      .fetch(:shift_event, {})
      .permit(user_ids: [], task_ids: [])
  end

  def edit_tasks?
    edit_shift_event_params.key?(:task_ids)
  end

  def edit_users?
    edit_shift_event_params.key?(:user_ids)
  end

  def update_shift_event_params
    params.permit(:range)
  end
end
