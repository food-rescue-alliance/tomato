class CurrentUsersController < ApplicationController
  before_action :load_organization

  def edit
    authorize current_user, policy_class: CurrentUserPolicy

    @user = current_user
  end

  def update
    authorize current_user, policy_class: CurrentUserPolicy

    @user = current_user
    if @user.update(user_params)
      redirect_to(
        edit_current_user_path(organization_id: params[:organization_id]),
        notice: "Successfully edited profile!"
      )
    else
      render :edit
    end
  end

  private

  def user_params
    params.require(:user).permit(
      :full_name,
      :email,
      :time_zone
    )
  end

  def load_organization
    @organization = Organization.find(params[:organization_id]) if params[:organization_id]
  end
end
