class CsvExportersController < ApplicationController
  before_action :authenticate_user!
  before_action :organization

  def index
    authorize @organization, :show?
  end

  def create
    authorize @organization, :show?
    rows = ExportService.export(organization: @organization, start_date: export_params[:start_date],
                                end_date: export_params[:end_date])
    columns = ExportService::COLUMNS
    csv = CSV.generate(write_headers: true, headers: columns.map { |c| c.to_s.humanize.downcase }) do |data|
      rows.each { |row| data << row.values_at(*columns) }
    end

    send_data csv, filename: "#{@organization.name}-#{export_params[:start_date]}-to-#{export_params[:end_date]}.csv"
  end

  private

  def export_params
    params.require(:export).permit(:start_date, :end_date)
  end

  def organization
    @organization = Organization.find(params[:organization_id])
  end
end
