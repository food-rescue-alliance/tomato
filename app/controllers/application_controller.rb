class ApplicationController < ActionController::Base
  layout :layout_by_resource
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  include Pundit::Authorization

  after_action :verify_authorized, unless: :devise_controller?

  around_action :apply_time_zone, if: :current_user

  rescue_from Pundit::NotAuthorizedError, with: :render_internal_server_error
  rescue_from ActiveRecord::RecordInvalid, with: :render_unprocessable_entity_response

  private

  def apply_time_zone(&block)
    Time.use_zone(current_user.time_zone, &block)
  end

  def layout_by_resource
    "application" if devise_controller? && controller_name == "organization_invitations"
  end

  def render_unprocessable_entity_response(exception)
    render json: exception.record.errors, status: :unprocessable_entity
  end

  def render_not_found
    render file: Rails.public_path.join("404.html"), status: :not_found, layout: false
  end

  def render_internal_server_error(message)
    logger.error(message)
    render file: Rails.public_path.join("500.html"), status: :internal_server_error, layout: false
  end
end
