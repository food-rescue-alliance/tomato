class ShiftsController < ApplicationController
  before_action :authenticate_user!
  before_action :organization

  def index
    authorize @organization, :edit?

    if params[:shift_category] == "one_time"
      @shift_category = params[:shift_category]
      @shifts = @organization.events.on_or_after(Time.current.midnight).nonrecurring.shift_events
    else
      @shift_category = "recurring"
      @shifts = @organization.events.on_or_after(Time.current.midnight).recurring.shift_events
    end
  end

  def new
    authorize @organization, :edit?
  end

  private

  def organization
    return if params[:user_id]

    @organization = Organization.find(params[:organization_id])
  end

  def shift_params
    params.require(:shift)
      .permit(:name, shift_events_attributes: [{ event_attributes: %i[title starts_at ends_at rrule], users: [] }])
  end
end
