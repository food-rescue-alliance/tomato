class PagesController < ApplicationController
  before_action :authenticate_user!, only: %i[volunteer_root]

  def root
    skip_authorization

    return render file: Rails.public_path.join("landing.html"), layout: false if current_user.blank?

    return render :volunteer_root, layout: false if current_user.volunteer?

    redirect_to admin_dashboard_path
  end

  def volunteer_root
    skip_authorization

    render :volunteer_root, layout: false
  end

  def volunteer_dashboard_path
    :volunteer_root
  end

  def admin_dashboard_path
    if current_user.administrates_multiple_orgs?
      organizations_path
    elsif current_user.org_admin?
      organization_path(current_user.organizations.first)
    else
      root_path
    end
  end
end
