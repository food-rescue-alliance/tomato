class ReceiptsController < ApplicationController
  before_action :authenticate_user!
  before_action :organization

  skip_before_action :verify_authenticity_token

  def create
    authorize @organization, policy_class: SitePolicy

    @receipt = Receipt.new(
      starts_at: receipt_params[:starts_at],
      ends_at: receipt_params[:ends_at],
      organization_id: params[:organization_id],
      donor_id: receipt_params[:site_id]
    )

    render template: "receipts/create", layout: false
  rescue ActiveRecord::RecordNotFound
    redirect("Donor not found.")
  rescue Date::Error
    redirect("Invalid date range.")
  end

  def new
    authorize @organization, :index?, policy_class: SitePolicy
    @donors = @organization.donors
    @selected_site_id = params[:site_id].to_i
  end

  private

  def redirect(notice)
    redirect_to new_organization_receipt_path(params[:organization_id]), notice: notice
  end

  def organization
    @organization = Organization.find(params[:organization_id])
  end

  def receipt_params
    params.permit(:site_id, :starts_at, :ends_at)
  end
end
