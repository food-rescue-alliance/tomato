class OrganizationInvitationsController < Devise::InvitationsController
  before_action :authenticate_user!
  before_action :configure_permitted_parameters, if: :devise_controller?

  def new
    @organization = Organization.find(params[:id])
    authorize @organization, policy_class: OrganizationInvitationPolicy

    @organization_id = params[:id]
    super
  end

  def create
    @organization = Organization.find(params[:id])
    authorize @organization, policy_class: OrganizationInvitationPolicy

    @organization_id = params[:id]
    self.resource = User.invite!(
      email: resource_params[:email],
      organization_ids: [@organization_id],
      roles: resource_params[:roles].to_i,
      time_zone: @organization.time_zone
    )
    resource_invited = resource.errors.empty?

    if resource_invited
      redirect_to organization_users_path(
        @organization, role: resource.roles.downcase.pluralize
      ), notice: "User was invited successfully."
    else
      render :new
    end
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:invite, keys: [:roles])
    devise_parameter_sanitizer.permit(:accept_invitation, keys: %i[full_name time_zone])
  end
end
