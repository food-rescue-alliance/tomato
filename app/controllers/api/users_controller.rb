module Api
  class UsersController < ApiController
    before_action :authenticate_user!
    skip_before_action :verify_authenticity_token

    def show
      raise Pundit::NotAuthorizedError unless volunteer?

      skip_authorization

      @user = current_user
    end

    def update
      raise Pundit::NotAuthorizedError unless volunteer?

      skip_authorization

      @user = current_user
      @user.update!(volunteer_params)
    end

    private

    def volunteer_params
      params.require(:user).permit(
        :phone,
        :full_name,
        :email,
        :time_zone,
        address_attributes: %i[id street_one street_two city state zip _destroy]
      )
    end

    def volunteer?
      params[:id] == "me"
    end
  end
end
