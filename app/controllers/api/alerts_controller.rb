module Api
  class AlertsController < ApiController
    before_action :authenticate_user!

    def index
      skip_authorization

      raise Pundit::NotAuthorizedError unless params[:user_id] == "me"

      range = (1.year.ago)..(Time.current)
      occurrences = EventsService.all_user_occurrences current_user, range

      @alerts = occurrences
        .filter { |o| o.ends_at.past? && o.eventable.missing_task_log? }
        .map do |occurrence|
          Alert.new(occurrence_id: occurrence.id,
                    message: "Log report for #{occurrence.event.title}, #{occurrence.starts_at.strftime("%A %b %e")}")
        end
      @alerts = @alerts.reverse
    end
  end
end
