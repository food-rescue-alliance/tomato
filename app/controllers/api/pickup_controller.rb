module Api
  class PickupController < ApiController
    before_action :authenticate_user!

    include ShiftEventOccurrenceLoader
    before_action :load_occurrence, only: %i[pickup]
    skip_before_action :verify_authenticity_token

    def index
      raise Pundit::NotAuthorizedError unless params[:user_id] == "me"

      skip_authorization

      range = (Time.zone.now)..(2.months.from_now)

      @shifts = EventsService.all_open_user_occurrences current_user, range
    end

    def pickup
      raise Pundit::NotAuthorizedError unless params[:user_id] == "me"

      skip_authorization

      update_params = pickup_params.merge(user_ids: @occurrence.shift_event.user_ids + [current_user.id])
      update_params = update_params.except(:shift_event_id)
      update_params = update_params.except(:id)

      updated_event = if range_paramss.key?(:range) && range_paramss[:range] == "THISANDFUTURE"
                        EventsService.update_into_future(
                          @occurrence,
                          update_params
                        )
                      else
                        EventsService.update(
                          @occurrence,
                          update_params
                        )
                      end

      @updated_occurrence = EventsService.find_occurrence updated_event.uid, @occurrence.starts_at
    end

    private

    def pickup_params
      params.permit(:id)
    end

    def range_paramss
      params.permit(:range)
    end
  end
end
