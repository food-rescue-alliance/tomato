module Api
  class ShiftEventOccurrencesController < ApiController
    before_action :authenticate_user!

    include ShiftEventOccurrenceLoader
    before_action :load_occurrence, only: %i[show]
    before_action :authorize_resources

    def index
      skip_authorization

      range = (1.week.ago)..(1.month.from_now)
      @occurrences = EventsService.all_user_occurrences current_user, range
    end

    def show
      @food_taxonomies = FoodTaxonomy.all
    end

    private

    def authorize_resources
      authorize @occurrence.event if @occurrence
    end
  end
end
