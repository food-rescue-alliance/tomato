module Api
  class ApiController < ApplicationController
    rescue_from Pundit::NotAuthorizedError, with: :render_forbidden
    def render_forbidden
      render json: { error: "Not authorized" }, status: :forbidden
    end
  end
end
