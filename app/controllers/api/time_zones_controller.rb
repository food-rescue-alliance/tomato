module Api
  class TimeZonesController < ApplicationController
    before_action :authenticate_user!
    skip_before_action :verify_authenticity_token

    def index
      skip_authorization

      @time_zones = helpers.time_zones_with_us_zones_first.map { |tz| { id: tz.tzinfo.identifier, name: tz.name } }
    end
  end
end
