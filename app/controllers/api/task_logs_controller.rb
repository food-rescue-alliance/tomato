module Api
  class TaskLogsController < ApiController
    skip_before_action :verify_authenticity_token

    before_action :authenticate_user!

    include ShiftEventOccurrenceLoader
    before_action :load_occurrence
    before_action :authorize_resources

    def create
      task_log_attributes = task_log_params.merge(
        user_id: current_user.id,
        shift_event_id: @occurrence.shift_event.id
      )
      event = EventsService.update(
        @occurrence,
        {
          task_logs_attributes: [task_log_attributes]
        }
      )

      @task_log = event.eventable.task_logs.last

      if @task_log.errors.any?
        head :unprocessable_entity
      elsif request.accept.start_with? "text/html"
        redirect_to shift_event_occurrence_path(@occurrence.id), notice: "Log was successfully added."
      else
        response.status = :created
      end
    end

    private

    def authorize_resources
      authorize @occurrence.event, :update?
    end

    def task_log_params
      params.permit(:shift_event_occurrence_id)
      params.require(:task_log).permit(:task_id, :weight, :temperature, :notes, :food_taxonomy_id)
    end
  end
end
