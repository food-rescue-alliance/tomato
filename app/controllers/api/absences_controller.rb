module Api
  class AbsenceTooSoonError < StandardError
  end

  class AbsencesController < ApiController
    before_action :authenticate_user!
    before_action :only_allow_me!
    skip_before_action :verify_authenticity_token
    rescue_from AbsenceTooSoonError, with: :absence_too_soon_response

    def index
      @absences = current_user.absences
    end

    def create
      raise AbsenceTooSoonError if absence_params[:starts_at] < Absence::ABSENCE_LEAD_TIME.from_now.beginning_of_day

      absence = Absence.create!(absence_params.merge({ user_id: current_user.id }))

      current_user.organizations.each do |organization|
        AbsenceService.send_scheduled_email(current_user, absence, organization)
        AbsenceService.remove_user_from_shifts(current_user, absence, organization)
      end

      @absences = current_user.absences.reload
      render :create, status: :created
    end

    def update
      raise AbsenceTooSoonError if absence_params[:starts_at] < Absence::ABSENCE_LEAD_TIME.from_now.beginning_of_day

      absence = current_user.absences.find(params[:id])

      raise AbsenceTooSoonError if absence.starts_at < Absence::ABSENCE_LEAD_TIME.from_now.beginning_of_day

      old_absence = {
        starts_at: absence.starts_at,
        ends_at: absence.ends_at
      }

      updated_absence = {
        starts_at: absence_params[:starts_at],
        ends_at: absence_params[:ends_at]
      }

      absence.update!(absence_params)

      current_user.organizations.each do |organization|
        AbsenceService.send_updated_email(
          current_user,
          old_absence,
          updated_absence,
          organization
        )
        AbsenceService.send_volunteer_updated_email(current_user, organization)
      end

      @absences = current_user.absences.reload
    end

    def destroy
      absence = current_user.absences.find(params[:id])

      raise AbsenceTooSoonError if absence.starts_at < Absence::ABSENCE_LEAD_TIME.from_now.beginning_of_day

      absence.destroy!

      current_user.organizations.each do |organization|
        AbsenceService.send_canceled_email(current_user, absence, organization)
        AbsenceService.send_volunteer_canceled_email(current_user, organization)
      end

      @absences = current_user.absences.reload
    end

    private

    def only_allow_me!
      skip_authorization

      raise Pundit::NotAuthorizedError unless params[:user_id] == "me"
    end

    def absence_params
      params.require(:absence).permit(
        :starts_at,
        :ends_at
      )
    end

    def absence_too_soon_response
      render json: { error: "absences.tooSoon" }, status: :unprocessable_entity
    end
  end
end
