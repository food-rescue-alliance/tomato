module Api
  class ShiftsController < ApiController
    layout "api/application"
    before_action :authenticate_user!
    before_action :organization
    skip_before_action :verify_authenticity_token

    def index
      authorize @organization, :edit?

      if params[:shift_category] == "one_time"
        @shift_category = params[:shift_category]
        @shifts = @organization.events.nonrecurring.shift_events
      else
        @shift_category = "recurring"
        @shifts = @organization.events.recurring.shift_events
      end
    end

    def new
      authorize @organization, :edit?

      @shift = Shift.new
      @shift.shift_events.build(event: Event.new, users: [])
    end

    def create
      authorize @organization, :edit?
      shift_result = ShiftsService.create(shift_params, organization, current_user.time_zone)

      if shift_result.ok?
        uid = shift_result.value.shift_events.first.uid
        first_occurrence = EventsService.first_occurrence(uid)
        render status: :created, json: {
          data: {
            link: shift_event_occurrence_path(first_occurrence&.id)
          }
        }
      else
        render status: :bad_request, json: {
          errors: [shift_result.error]
        }
      end
    end

    private

    def organization
      return if params[:user_id]

      @organization = Organization.find(params[:organization_id])
    end

    def shift_params
      params.require(:shift)
        .permit(:name, shift_events_attributes: [{ event_attributes: %i[title starts_at ends_at rrule task_ids],
          users: [], task_ids: [] }])
    end
  end
end
