module Api
  class FoodTaxonomiesController < ApiController
    skip_before_action :verify_authenticity_token

    before_action :authenticate_user!

    def index
      skip_authorization
      @food_taxonomies = FoodTaxonomy.all
    end
  end
end
