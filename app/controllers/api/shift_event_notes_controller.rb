module Api
  class ShiftEventNotesController < ApiController
    class InvalidTransportationTypeError < StandardError; end

    skip_before_action :verify_authenticity_token
    before_action :authenticate_user!

    include ShiftEventOccurrenceLoader
    before_action :load_occurrence

    def show
      authorize @occurrence.event
      @note = @occurrence.shift_event.shift_event_note || ShiftEventNote.new
      render json: {
        transportation_type: @note.transportation_type,
        hours_spent: @note.hours_spent
      }
    end

    def create
      authorize @occurrence.event, :update?
      if invalid_transportation_type?(note_params)
        render json: { error: "Invalid transportation type." }, status: :unprocessable_entity
        return
      end

      shift_event_note_attributes = note_params.merge(
        shift_event_id: @occurrence.shift_event.id
      )

      event = EventsService.post_occurrence_details(
        @occurrence,
        {
          shift_event_note_attributes: shift_event_note_attributes
        }
      )

      note = event.eventable.shift_event_note

      if note.errors.any?
        head :unprocessable_entity
      else
        head :created
      end
    end

    private

    def invalid_transportation_type?(note_params)
      note_params[:transportation_type].present? &&
        ShiftEventNote.transportation_types.exclude?(note_params[:transportation_type])
    end

    def note_params
      params.require(:shift_event_note).permit(:transportation_type, :hours_spent)
    end
  end
end
