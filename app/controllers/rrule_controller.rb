class RruleController < ApplicationController
  before_action :authenticate_user!

  def index
    skip_authorization

    rrule_str = RruleStr.new(rrule: params[:rrule], starts_at: starts_at)

    render json: {
      sentence: rrule_str.to_sentence
    }
  end

  private

  def starts_at
    return unless params[:starts_at]

    Time.zone.parse(params[:starts_at])
  end
end
