class UsersController < ApplicationController
  before_action :authenticate_user!
  before_action :organization

  def index
    authorize @organization, :show?

    @role = params[:role].presence || "volunteers"
    @users = @role.eql?("organization_administrators") ? @organization.org_admins : @organization.volunteers
  end

  def show
    authorize @organization

    @user = User.find(params[:id])
  end

  def resend_invite
    authorize @organization, :create?, policy_class: OrganizationInvitationPolicy

    @user = User.find(params[:id])
    @user.invite!
    redirect_to organization_users_path(@organization), notice: "Invitation Resent!"
  end

  private

  def organization
    @organization = Organization.find(params[:organization_id])
  end
end
