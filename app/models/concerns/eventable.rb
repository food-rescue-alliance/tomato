module Eventable
  extend ActiveSupport::Concern

  included do
    has_one :event, as: :eventable, touch: true, dependent: :destroy

    validates_associated :event

    delegate_missing_to :event
  end
end
