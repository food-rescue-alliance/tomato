module Siteable
  extend ActiveSupport::Concern

  included do
    has_one :site, as: :siteable, touch: true, dependent: :destroy
  end
end
