# == Event Occurrence
#
# Actual instance of when an +Event+ happens. This class exists primarily in support of recurring
# +Event+s but is also intended to be used homogeneously, with one-time +Event+s, for scheduling purposes.
#
# How does this differ from the +Event+ model?
#
# +Event+ can model recurrence with one instance and row in the events table. +EventOccurrence+
# models each occurrence of a recurring +Event+ and is not database-backed. Instead, +EventOccurrence+s
# are derived from +Event+ data and its recurrence rule.
#
# Homogenous usage of +EventOccurrence+ with single and recurring +Event+s
#
# +EventOccurrence+ can also derive from single +Event+s (no recurrence). This is for two reasons.
# One, for consistency when talking about concrete events and two, to fit into the vernacular of Scheduling.
class EventOccurrence
  attr_reader :starts_at, :ends_at, :event

  delegate_missing_to :event

  def initialize(starts_at:, ends_at:, event:)
    @starts_at = starts_at
    @ends_at = ends_at
    @event = event
  end

  # == head
  #
  # An occurrence is a head if the underlying +Event+ is either:
  #
  # * A single event
  # * The first occurrence of a recurrence
  #
  def head?
    event.single? || starts_at == event.starts_at
  end

  def id
    "#{event.uid}-#{starts_at.to_i}"
  end

  def attributes
    event.attributes.merge({ starts_at: starts_at, ends_at: ends_at })
  end
end
