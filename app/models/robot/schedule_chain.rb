module Robot
  class ScheduleChain < RobotRecord
    default_scope { where(active: true) }
    belongs_to :region

    has_many :schedule_volunteers, dependent: :destroy
    has_many :volunteers, -> { where("schedule_volunteers.active" => true) }, through: :schedule_volunteers

    has_many :schedules, dependent: :destroy
    has_many :locations, through: :schedules

    has_many :logs, dependent: :destroy
  end
end
