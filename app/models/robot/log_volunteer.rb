module Robot
  class LogVolunteer < RobotRecord
    belongs_to :log
    belongs_to :volunteer

    accepts_nested_attributes_for :volunteer
  end
end
