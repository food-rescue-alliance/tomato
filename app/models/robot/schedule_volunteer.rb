module Robot
  class ScheduleVolunteer < RobotRecord
    belongs_to :schedule_chain
    belongs_to :volunteer
  end
end
