module Robot
  class Volunteer < RobotRecord
    default_scope { where(active: true, is_disabled: [false, nil]) }

    has_many :absences, dependent: :destroy
    has_many :assignments, dependent: :destroy
    has_many :regions, through: :assignments
    has_many :schedule_volunteers, dependent: :destroy
    has_many :schedule_chains, -> { where("schedule_volunteers.active" => true) }, through: :schedule_volunteers

    def region_admin?(region)
      assignments.where(admin: true, region_id: region.id).present?
    end

    def has_shifts?(region)
      schedule_chains.where(region_id: region.id, active: true).present?
    end
  end
end
