module Robot
  class Location < RobotRecord
    LOCATION_TYPES = {
      0 => "Recipient",
      1 => "Donor",
      2 => "Hub",
      3 => "Seller",
      4 => "Buyer"
    }
    PICKUP_LOCATION_TYPES = [1, 2, 3]
    DROP_LOCATION_TYPES   = [0, 2, 4]

    belongs_to :region
  end
end
