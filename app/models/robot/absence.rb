module Robot
  class Absence < RobotRecord
    belongs_to :volunteer
  end
end
