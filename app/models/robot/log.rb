module Robot
  class Log < RobotRecord
    belongs_to :schedule_chain
    belongs_to :donor, class_name: "Location"
    belongs_to :region

    has_many :log_volunteers, dependent: :destroy
    has_many :volunteers, -> { where("log_volunteers.active" => true) }, through: :log_volunteers

    has_many :log_recipients, dependent: :destroy
    has_many :recipients, through: :log_recipients

    has_and_belongs_to_many :absences

    accepts_nested_attributes_for :log_recipients
    accepts_nested_attributes_for :log_volunteers
    accepts_nested_attributes_for :schedule_chain

    validates :when, presence: true
  end
end
