module Robot
  class Schedule < RobotRecord
    belongs_to :location
    belongs_to :schedule_chain
    has_many :schedule_parts, dependent: :destroy
    has_many :food_types, through: :schedule_parts
  end
end
