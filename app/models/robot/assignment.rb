module Robot
  class Assignment < RobotRecord
    belongs_to :volunteer
    belongs_to :region
  end
end
