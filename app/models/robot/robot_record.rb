module Robot
  class RobotRecord < ApplicationRecord
    self.abstract_class = true

    connects_to database: { reading: :robot, writing: :robot }
  end
end
