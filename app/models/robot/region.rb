module Robot
  class Region < RobotRecord
    has_many :assignments, dependent: :destroy
    has_many :volunteers, through: :assignments
    has_many :locations, -> { where(active: true) }, dependent: :destroy, inverse_of: :region
    has_many :schedule_chains, dependent: :destroy
  end
end
