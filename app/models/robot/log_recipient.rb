module Robot
  class LogRecipient < RobotRecord
    belongs_to :log
    belongs_to :recipient, class_name: "Location"

    accepts_nested_attributes_for :recipient
  end
end
