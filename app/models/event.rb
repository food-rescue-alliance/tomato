# == Schema Information
#
# Table name: events
#
#  created_at         :datetime         not null
#  ends_at            :datetime         not null
#  eventable_id       :bigint           not null
#  eventable_type     :string           not null
#  exdates            :datetime         default([]), is an Array
#  id                 :bigint           not null, primary key
#  ownable_id         :bigint
#  ownable_type       :string
#  recurrence_options :jsonb
#  starts_at          :datetime         not null
#  title              :string           not null
#  uid                :string
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_events_on_eventable_id_and_eventable_type                 (eventable_id,eventable_type)
#  index_events_on_ownable_id_and_ownable_type_and_eventable_type  (ownable_id,ownable_type,eventable_type)
#  index_events_on_ownable_type_and_ownable_id                     (ownable_type,ownable_id)
#  index_events_on_recurrence_options                              (recurrence_options) USING gin
#  index_events_on_uid                                             (uid)
#
class Event < ApplicationRecord
  delegated_type :eventable, types: %w[ShiftEvent]

  belongs_to :ownable, polymorphic: true

  validate :validate_recurrence_options, if: proc { |event| event.recurrence_options.present? }, on: :save
  validates :title, :starts_at, :ends_at, presence: true

  before_create :build_uid, if: proc { |event| event.uid.blank? }

  scope :recurring, -> { where.not(recurrence_options: nil) }
  scope :nonrecurring, -> { where(recurrence_options: nil) }

  scope :between_recurrence, ->(starts_at) {
    where.not(recurrence_options: nil)
      .where("starts_at <= ?", starts_at)
      .where("recurrence_options->>'UNTIL' IS NULL OR recurrence_options->>'UNTIL' >= ?", starts_at)
  }

  scope :all_by, ->(owner, eventable_type, starts_at) {
    where(eventable_type: eventable_type.name, ownable: owner)
      .includes(:eventable)
      .on_or_after(starts_at)
  }

  def recurrence?
    recurrence_options.present?
  end

  def single?
    !recurrence?
  end

  def rrule=(rrule)
    return if rrule.blank?

    return unless validate_recurrence_options rrule

    self.recurrence_options = rrule.split(";").to_h do |key_value|
      key, value = key_value.split("=")

      if key.casecmp("UNTIL").zero?
        [key, Time.zone.parse(value)]
      else
        [key, value]
      end
    end
  end

  def rrule
    return nil if recurrence_options.blank?

    recurrence_options
      .map { |k, v|
        if k.casecmp("UNTIL").zero?
          time = Time.zone.parse(v)
          v = Icalendar::Values::DateTime.new(time, tzid: "UTC").value_ical
        end
        "#{k}=#{v}"
      }
      .join ";"
  end

  def rrule_str
    RruleStr.new(rrule: rrule, starts_at: starts_at).to_sentence
  end

  def validate_recurrence_options(param = nil)
    RRule::Rule.new(param || rrule)
    true
  rescue StandardError => e
    errors.add(:rrule, e.message)
    false
  end

  def self.on_or_after(starts_at)
    merge(Event.between_recurrence(starts_at).or(where("starts_at >= ?", starts_at)))
      .order("starts_at")
  end

  def self.series(uid, starts_at, eventable)
    Event
      .includes(:ownable, :eventable)
      .where(uid: uid, eventable_type: eventable.name)
      .merge(Event.on_or_after(starts_at))
      .order("starts_at")
  end

  def build_uid
    self.uid = SecureRandom.uuid
  end
end
