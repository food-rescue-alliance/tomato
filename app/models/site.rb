# == Schema Information
#
# Table name: sites
#
#  admin_notes         :text
#  created_at          :datetime         not null
#  detailed_hours_json :text
#  id                  :bigint           not null, primary key
#  instructions        :string           default(""), not null
#  lat                 :decimal(, )
#  lng                 :decimal(, )
#  name                :string           not null
#  organization_id     :bigint
#  receipt_key         :string
#  robot_location_id   :integer
#  siteable_id         :bigint
#  siteable_type       :string
#  updated_at          :datetime         not null
#  website             :string
#
# Indexes
#
#  index_sites_on_organization_id                (organization_id)
#  index_sites_on_organization_id_and_name       (organization_id,name) UNIQUE
#  index_sites_on_robot_location_id              (robot_location_id)
#  index_sites_on_siteable_id_and_siteable_type  (siteable_id,siteable_type)
#
require "csv"
class Site < ApplicationRecord
  TYPES = %w[Donor Recipient Hub]

  delegated_type :siteable, types: TYPES, dependent: :destroy

  belongs_to :organization
  has_many :tasks, dependent: :destroy
  has_one :address, as: :addressable, dependent: :destroy
  accepts_nested_attributes_for :address, allow_destroy: true

  validates :name, presence: true, uniqueness: { scope: :organization_id }
  validates_associated :siteable

  before_create :build_task_attributes

  def self.siteable_types
    TYPES
  end

  def build_task_attributes
    return if siteable.blank?

    tasks.build(siteable.build_task_attributes)
  end
end
