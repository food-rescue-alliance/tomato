# == Schema Information
#
# Table name: tasks
#
#  created_at :datetime         not null
#  id         :bigint           not null, primary key
#  site_id    :bigint
#  task_type  :integer
#  updated_at :datetime         not null
#
# Indexes
#
#  index_tasks_on_site_id    (site_id)
#  index_tasks_on_task_type  (task_type)
#
class Task < ApplicationRecord
  belongs_to :site

  enum task_type: { pickup: 0, drop_off: 1 }

  def description
    return "new task" if task_type.blank? || site.blank?

    "#{task_type.titleize} at #{site.name}"
  end
end
