class Alert
  attr_reader :occurrence_id, :message

  def initialize(occurrence_id:, message:)
    @occurrence_id = occurrence_id
    @message = message
  end
end
