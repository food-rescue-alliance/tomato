# == Schema Information
#
# Table name: task_logs
#
#  created_at       :datetime         not null
#  food_taxonomy_id :integer          default(-1), not null
#  id               :bigint           not null, primary key
#  notes            :text
#  shift_event_id   :bigint           not null
#  task_id          :bigint           not null
#  updated_at       :datetime         not null
#  user_id          :bigint           not null
#  weight           :integer
#  temperature      :integer
#
# Indexes
#
#  index_task_logs_on_shift_event_id  (shift_event_id)
#
require "csv"
class TaskLog < ApplicationRecord
  belongs_to :shift_event
  belongs_to :task
  belongs_to :user
  belongs_to :food_taxonomy

  validate :shift_event_task
  validates :weight, presence: true

  def shift_event_task
    return if shift_event.tasks.include?(task)
    return if shift_event.shift_event_tasks.any? { |shift_event_task| shift_event_task.task_id == task&.id }

    errors.add(:task, "The task (#{task_id}) you're attempting to log is not part of shift event #{shift_event_id}")
  end
end
