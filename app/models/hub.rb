# == Schema Information
#
# Table name: hubs
#
#  created_at :datetime         not null
#  id         :bigint           not null, primary key
#  updated_at :datetime         not null
#
class Hub < ApplicationRecord
  include Siteable

  def build_task_attributes
    [
      { task_type: :pickup },
      { task_type: :drop_off }
    ]
  end
end
