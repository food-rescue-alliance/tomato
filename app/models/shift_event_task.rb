# == Schema Information
#
# Table name: shift_event_tasks
#
#  created_at     :datetime         not null
#  id             :bigint           not null, primary key
#  order          :integer          default(0)
#  shift_event_id :bigint           not null
#  task_id        :bigint           not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_shift_event_tasks_on_order           (order)
#  index_shift_event_tasks_on_shift_event_id  (shift_event_id)
#  index_shift_event_tasks_on_task_id         (task_id)
#
class ShiftEventTask < ApplicationRecord
  default_scope { order(:order) }
  belongs_to :shift_event
  belongs_to :task
end
