# == Schema Information
#
# Table name: shift_events
#
#  admin_notes  :text
#  created_at   :datetime         not null
#  id           :bigint           not null, primary key
#  public_notes :text
#  shift_id     :bigint           not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_shift_events_on_shift_id  (shift_id)
#
require "csv"
class ShiftEvent < ApplicationRecord
  include Eventable

  belongs_to :shift
  has_and_belongs_to_many :users
  has_many :shift_event_tasks, dependent: :destroy
  has_many :tasks, through: :shift_event_tasks
  has_one :shift_event_note, dependent: :destroy
  has_many :task_logs, dependent: :destroy

  validate :users_are_volunteers,
           :users_belong_to_organization,
           :user_is_unique_for_shift,
           on: :save

  accepts_nested_attributes_for :event, :users, :tasks, :shift_event_tasks, :task_logs, :shift_event_note

  delegate :include?, to: :users

  def volunteers
    users.volunteer
  end

  def organization
    return event.ownable if event.ownable_type == Organization.name

    nil
  end

  def missing_task_log?
    task_ids = tasks.pluck :id
    logged_task_ids = task_logs.pluck(:task_id).uniq
    missing_log_ids = task_ids - logged_task_ids

    !missing_log_ids.empty?
  end

  def missing_attributes?
    users.empty? || incomplete_tasks?
  end

  def incomplete_tasks?
    tasks.empty?
  end

  def dup_nested_attributes
    {
      user_ids: user_ids.dup,
      ordered_task_ids: task_ids.dup,
      task_logs: task_logs.map(&:dup)
    }
  end

  def ordered_task_ids=(task_ids)
    self.shift_event_tasks = task_ids.compact_blank.each_with_index.map do |task_id, order|
      ShiftEventTask.new({ task_id: task_id, order: order })
    end
  end

  private

  def users_are_volunteers
    users.each do |u|
      errors.add(:users, "must be a volunteer") unless u.volunteer?
    end
  end

  def users_belong_to_organization
    users.each do |u|
      errors.add(:users, "must belong to the organization") unless u.organizations.exists?(organization.id)
    end
  end

  def user_is_unique_for_shift
    errors.add(:users, "user already exists on this shift") unless users.uniq == users
  end
end
