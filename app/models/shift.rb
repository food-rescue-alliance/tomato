# == Schema Information
#
# Table name: shifts
#
#  created_at :datetime         not null
#  id         :bigint           not null, primary key
#  name       :string           not null
#  updated_at :datetime         not null
#
class Shift < ApplicationRecord
  has_many :shift_events, dependent: :destroy

  accepts_nested_attributes_for :shift_events

  scope :by_organization, ->(organization) do
    joins(shift_events: [:event])
      .where("events.ownable_id = ? AND events.ownable_type = ?", organization.id, Organization.name)
      .distinct
  end

  validates :name, presence: true

  def events
    shift_events
      .joins(:event)
      .includes(:event)
  end
end
