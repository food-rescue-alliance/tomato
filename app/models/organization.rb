# == Schema Information
#
# Table name: organizations
#
#  created_at            :datetime         not null
#  email                 :string
#  federal_tax_id        :string
#  id                    :bigint           not null, primary key
#  internal_alerts_email :string
#  name                  :string           not null
#  phone                 :string
#  tagline               :string
#  time_zone             :string           default("Etc/UTC"), not null
#  updated_at            :datetime         not null
#
class Organization < ApplicationRecord
  validates :name, presence: true
  validates :time_zone, inclusion: {
    in: ActiveSupport::TimeZone.all.map { |tz| tz.tzinfo.identifier },
    message: "Invalid time zone"
  }

  has_many :events, as: :ownable, dependent: :destroy
  has_many :sites, dependent: :destroy
  has_many :tasks, through: :sites
  has_and_belongs_to_many :users
  has_one :address, as: :addressable, dependent: :destroy
  accepts_nested_attributes_for :address, allow_destroy: true

  def volunteers
    users.volunteer
  end

  def org_admins
    users.org_admin
  end

  def shifts
    Shift.all.by_organization self
  end

  def donors
    sites.where(siteable_type: "Donor")
  end
end
