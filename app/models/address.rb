# == Schema Information
#
# Table name: addresses
#
#  addressable_id   :bigint
#  addressable_type :string
#  city             :string           not null
#  created_at       :datetime         not null
#  id               :bigint           not null, primary key
#  state            :string           not null
#  street_one       :string           not null
#  street_two       :string
#  updated_at       :datetime         not null
#  zip              :string           not null
#
# Indexes
#
#  index_addresses_on_addressable_type_and_addressable_id  (addressable_type,addressable_id)
#
class Address < ApplicationRecord
  belongs_to :addressable, polymorphic: true

  validates :street_one, :city, :state, :zip, presence: true

  def to_s
    formatted_address = street_one.to_s
    formatted_address << (street_two.present? ? " #{street_two}" : "")
    formatted_address << ", #{state}, #{city} #{zip}"
  end
end
