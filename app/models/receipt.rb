# == Event Occurrence
class Receipt
  attr_reader :starts_at, :ends_at, :organization, :donor, :task_logs, :created_at

  def initialize(starts_at:, ends_at:, organization_id:, donor_id:)
    @starts_at = Time.zone.parse(starts_at)
    @ends_at = Time.zone.parse(ends_at)
    @organization = Organization.find(organization_id)
    @donor = @organization.donors.find donor_id
    range = (@starts_at..@ends_at)
    @task_logs = TaskLog.where(created_at: range).joins(:task).where(tasks: { site_id: @donor.id })
    @created_at = Time.current
  end

  def total
    @task_logs.sum(&:weight)
  end

  def filename
    "#{@organization.name.gsub(/[^0-9A-z.\-]/, "_")}-#{@donor.name.gsub(/[^0-9A-z.\-]/, "_")}-receipt"
  end

  def address
    return if @organization.address.blank?

    address_street = @organization.address.street_one.to_s +
                     (@organization.address.street_two.present? ? " #{@organization.address.street_two}" : "").to_s
    "#{address_street}, #{@organization.address.city}, #{@organization.address.state} #{@organization.address.zip}"
  end
end
