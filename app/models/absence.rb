# == Schema Information
#
# Table name: absences
#
#  created_at :datetime         not null
#  ends_at    :date
#  id         :bigint           not null, primary key
#  starts_at  :date             not null
#  updated_at :datetime         not null
#  user_id    :bigint           not null
#
# Indexes
#
#  index_absences_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
class Absence < ApplicationRecord
  default_scope { order(:starts_at, :ends_at) }
  belongs_to :user
  validates :starts_at, :ends_at, presence: true
  validate :valid_date_range?

  ABSENCE_LEAD_TIME = 7.days

  def valid_date_range?
    return if [ends_at.blank?, starts_at.blank?].any?

    errors.add(:ends_at, "must be after starts_at") if ends_at < starts_at
  end

  def editable?
    starts_at >= 7.days.from_now.beginning_of_day
  end
end
