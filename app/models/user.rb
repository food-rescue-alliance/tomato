# == Schema Information
#
# Table name: users
#
#  created_at             :datetime         not null
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  full_name              :string           default(""), not null
#  id                     :bigint           not null, primary key
#  invitation_accepted_at :datetime
#  invitation_created_at  :datetime
#  invitation_limit       :integer
#  invitation_sent_at     :datetime
#  invitation_token       :string
#  invitations_count      :integer          default(0)
#  invited_by_id          :bigint
#  invited_by_type        :string
#  phone                  :string
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  roles                  :integer          default(NULL), not null
#  time_zone              :string           default("Etc/UTC"), not null
#  updated_at             :datetime         not null
#
# Indexes
#
#  index_users_on_email                              (email) UNIQUE
#  index_users_on_invitation_token                   (invitation_token) UNIQUE
#  index_users_on_invitations_count                  (invitations_count)
#  index_users_on_invited_by_id                      (invited_by_id)
#  index_users_on_invited_by_type_and_invited_by_id  (invited_by_type,invited_by_id)
#  index_users_on_reset_password_token               (reset_password_token) UNIQUE
#

# Indexes
#
#  index_users_on_email                              (email) UNIQUE
#  index_users_on_invitation_token                   (invitation_token) UNIQUE
#  index_users_on_invitations_count                  (invitations_count)
#  index_users_on_invited_by_id                      (invited_by_id)
#  index_users_on_invited_by_type_and_invited_by_id  (invited_by_type,invited_by_id)
#  index_users_on_reset_password_token               (reset_password_token) UNIQUE
#
require "csv"
class User < ApplicationRecord
  VOLUNTEER = 0
  OPERATIONS = 1
  ORGANIZATION_ADMINISTRATOR = 2

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable, :registerable, and :omniauthable
  devise :invitable, :database_authenticatable, :rememberable, :validatable, :recoverable

  scope :volunteer, -> { where(roles: VOLUNTEER) }

  scope :org_admin, -> { where(roles: ORGANIZATION_ADMINISTRATOR) }

  enum roles: {
    VOLUNTEER: VOLUNTEER,
    OPERATIONS: OPERATIONS,
    ORGANIZATION_ADMINISTRATOR: ORGANIZATION_ADMINISTRATOR
  }

  validates :roles, inclusion: { in: roles.keys }
  validates :email, presence: true
  validate :validate_email
  validates :phone, format: /[0-9]{10}/, allow_blank: true

  def validate_email
    Mail::Address.new(email)
    true
  rescue Mail::Field::ParseError
    errors.add(:email, "Must use a valid email address")
    false
  end

  has_many :absences, dependent: :destroy
  has_and_belongs_to_many :organizations
  has_and_belongs_to_many :shift_events
  has_one :address, as: :addressable, dependent: :destroy
  accepts_nested_attributes_for :address, allow_destroy: true

  def ops_coordinator?
    OPERATIONS?
  end

  def org_admin?(organization = nil)
    ORGANIZATION_ADMINISTRATOR? && (organization.nil? ? true : organizations.include?(organization))
  end

  def volunteer?(organization = nil)
    VOLUNTEER? && (organization.nil? ? true : organizations.include?(organization))
  end

  def full_name_or_email
    full_name.presence || email
  end

  def administrates_multiple_orgs?
    ops_coordinator? || (org_admin? && organizations.length > 1)
  end

  def time_zone_offset
    ActiveSupport::TimeZone.new(time_zone).formatted_offset
  end

  def self.role_from_string(str)
    case str
    when "organization_administrators"
      User::ORGANIZATION_ADMINISTRATOR
    when "volunteers"
      User::VOLUNTEER
    end
  end
end
