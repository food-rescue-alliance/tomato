# == Schema Information
#
# Table name: donors
#
#  created_at :datetime         not null
#  id         :bigint           not null, primary key
#  updated_at :datetime         not null
#
class Donor < ApplicationRecord
  include Siteable

  def build_task_attributes
    { task_type: :pickup }
  end
end
