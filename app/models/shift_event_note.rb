# == Schema Information
#
# Table name: shift_event_notes
#
#  created_at          :datetime         not null
#  hours_spent         :decimal(8, 2)
#  id                  :bigint           not null, primary key
#  shift_event_id      :bigint           not null
#  transportation_type :string
#  updated_at          :datetime         not null
#
# Indexes
#
#  index_shift_event_notes_on_shift_event_id  (shift_event_id)
#
# Foreign Keys
#
#  fk_rails_...  (shift_event_id => shift_events.id)
#
class ShiftEventNote < ApplicationRecord
  belongs_to :shift_event

  enum transportation_type: { walk: "walk", bike: "bike", car: "car" }
end
