json.array! @time_zones do |time_zone|
  json.id time_zone[:id]
  json.name time_zone[:name]
end
