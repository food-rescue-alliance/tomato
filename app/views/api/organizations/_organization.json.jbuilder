json.id organization.id
json.name organization.name
json.tagline organization.tagline
json.email organization.email
json.phone organization.phone
json.federal_tax_id organization.federal_tax_id
json.time_zone organization.time_zone
json.sites organization.sites, partial: "api/sites/site", as: :site
