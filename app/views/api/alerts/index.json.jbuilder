json.array! @alerts do |alert|
  json.occurrence_id alert.occurrence_id
  json.message alert.message
end
