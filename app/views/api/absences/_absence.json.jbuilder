json.id absence.id
json.starts_at absence.starts_at.to_fs(:iso8601)
json.ends_at absence.ends_at.to_fs(:iso8601)
json.editable absence.editable?
