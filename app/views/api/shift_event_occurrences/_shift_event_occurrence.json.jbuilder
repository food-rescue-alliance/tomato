json.call(occurrence, :id, :uid, :title)

json.id occurrence.id
json.startsAt occurrence.starts_at.iso8601
json.endsAt occurrence.ends_at.iso8601
json.organizationId occurrence.ownable_id
json.shiftId occurrence.shift_event.shift_id
json.shiftEventId occurrence.shift_event.id
json.flagged occurrence.ends_at.past? && occurrence.eventable.missing_task_log?
json.rrule occurrence.event.rrule_str

json.tasks do
  json.array! occurrence.shift_event.tasks do |task|
    json.call(task, :id, :description)
    json.site do
      json.id task.site.id
      json.type task.site.siteable_type
      json.address task.site.address
      json.instructions task.site.instructions
      json.name task.site.name
    end
  end
end

json.users do
  json.array! occurrence.shift_event.users do |user|
    json.call(user, :id, :email)
    json.fullName user.full_name
  end
end

json.task_logs do
  json.array! occurrence.shift_event.task_logs do |task_log|
    json.call(task_log, :notes, :weight, :temperature)
    json.id task_log.id
    json.userId task_log.user_id
    json.taskId task_log.task_id
    json.foodTaxonomyId task_log.food_taxonomy_id
  end
end
