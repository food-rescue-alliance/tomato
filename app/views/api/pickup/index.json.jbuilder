json.recurring do
  json.array!(@shifts[:recurring],
              partial: "api/shift_event_occurrences/shift_event_occurrence",
              as: :occurrence)
end

json.one_time do
  json.array!(@shifts[:one_time],
              partial: "api/shift_event_occurrences/shift_event_occurrence",
              as: :occurrence)
end
