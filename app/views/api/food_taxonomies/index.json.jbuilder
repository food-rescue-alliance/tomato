json.array! @food_taxonomies do |food_taxonomy|
  json.id food_taxonomy.id
  json.name food_taxonomy.name
end
