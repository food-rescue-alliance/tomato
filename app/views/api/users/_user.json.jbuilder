json.extract! user, :id, :phone, :full_name, :email, :time_zone, :roles, :time_zone_offset
if user.address.present?
  json.address do
    json.partial! user.address, as: :address
  end
end
