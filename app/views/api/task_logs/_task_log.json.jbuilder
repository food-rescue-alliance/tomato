json.call(task_log, :id, :notes, :weight, :temperature)
json.taskId task_log.task_id
json.shiftEventId task_log.shift_event_id
json.userId task_log.user_id
json.foodTaxonomyId task_log.food_taxonomy_id
