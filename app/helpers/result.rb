Result = Struct.new(:value, :error) do
  def self.ok(value)
    new(value, nil)
  end

  def ok?
    value.present?
  end

  def self.error(error)
    new(nil, error)
  end

  def error?
    !ok?
  end
end
