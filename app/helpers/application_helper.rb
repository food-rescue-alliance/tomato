module ApplicationHelper
  # action name to use for the primary submit button on scaffold-created CRUD forms
  def btn_action_prefix
    case action_name
      when "new", "create"
        "Create"
      when "edit", "update"
        "Update"
    end
  end

  def controller?(*controller)
    controller.include?(params[:controller])
  end

  def time_zones_with_us_zones_first
    us_zones = [
      "Pacific Time (US & Canada)",
      "Mountain Time (US & Canada)",
      "Central Time (US & Canada)",
      "Eastern Time (US & Canada)"
    ].map { |name| ActiveSupport::TimeZone[name] }

    (us_zones + ActiveSupport::TimeZone.all).uniq
  end
end
