import { Frequency, rrule } from '../packs/admin_dashboard/rrule';

describe('rrule()', () => {
  const startDate = "2021-08-25"
  const endDate = "2021-08-27"
  const getUntilDate = (value) => {
    const [year, month, day] = value.split("-")
      .map(v => parseInt(v))

    return new Date(year, month - 1, day).toISOString().replace(/[-:]/g, ""); // months are 0-indexed
  }

  test('returns a proper rrule string for daily', () => {
    expect(rrule({ frequency: Frequency.daily, startDate: startDate })).toEqual("FREQ=DAILY;INTERVAL=1")
  })

  test('returns a proper rrule string for weekly', () => {
    expect(rrule({ frequency: Frequency.weekly, startDate: startDate })).toEqual("FREQ=WEEKLY;BYDAY=WE;INTERVAL=1")
  })

  test('returns a proper rrule string for monthly', () => {
    expect(rrule({ frequency: Frequency.monthly, startDate: startDate })).toEqual("FREQ=MONTHLY;BYDAY=WE;BYSETPOS=4;INTERVAL=1")
  })

  test('returns a proper rrule string for yearly', () => {
    expect(rrule({ frequency: Frequency.yearly, startDate: startDate })).toEqual("FREQ=YEARLY;BYMONTH=8;BYMONTHDAY=25;INTERVAL=1")
  })

  test('properly sets the until counter', () => {
    expect(rrule({ frequency: Frequency.daily, count: 1, startDate: startDate })).toEqual("FREQ=DAILY;INTERVAL=1;COUNT=1")
  })

  test('properly sets the until date', () => {
    expect(rrule({ frequency: Frequency.daily, until: endDate, startDate: startDate })).toEqual(`FREQ=DAILY;INTERVAL=1;UNTIL=${getUntilDate(endDate)}`)
  })

  test('returns a weekday only recurrence', () => {
    expect(rrule({ frequency: Frequency.weekday, startDate: startDate })).toEqual("FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR;INTERVAL=1")
  })

  test('returns a custom monthly recurrence', () => {
    expect(rrule({ frequency: Frequency.monthly, startDate: startDate, byDays: 'MO,TU', bySetPos: 2, interval: 2 })).toEqual("FREQ=MONTHLY;BYDAY=MO,TU;BYSETPOS=2;INTERVAL=2")
  })

  test('returns a custom monthly recurrence for each 1st and 15th', () => {
    expect(rrule({ frequency: Frequency.monthly, startDate: startDate, byMonthDays: '1,15', interval: 1 })).toEqual("FREQ=MONTHLY;BYMONTHDAY=1,15;INTERVAL=1")
  })
})
