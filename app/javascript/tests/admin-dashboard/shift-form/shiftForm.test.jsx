import React from 'react';
import {
  fireEvent, render,
} from '@testing-library/react';
import moment from 'moment-timezone';
import * as actions from '../../../packs/admin_dashboard/shift_form/actions';
import { ShiftForm } from '../../../packs/admin_dashboard/shift_form/ShiftForm';

jest.mock('../../../packs/admin_dashboard/shift_form/actions');

describe('ShiftForm', () => {
  const mockDispatch = jest.fn();
  actions.fetchOrgAndInitForm = () => 'fetchOrgAndInitForm';
  actions.submitFormData = () => 'submitFormData';

  const DefaultForm = (
    <ShiftForm
      id="1"
      startDate={moment().format('YYYY-MM-DD')}
      startTime={moment().format('HH:mm')}
      endTime={moment().format('HH:mm')}
      rrule=""
      customText="Custom..."
      tasks={[]}
      dispatch={mockDispatch}
    />
  );

  it('loads shift defaults on mount', async () => {
    render(DefaultForm);
    expect(mockDispatch)
      .toHaveBeenCalledWith('fetchOrgAndInitForm');
  });

  it('renders the frequency form', async () => {
    const page = render(DefaultForm);
    const freq = await page.findByLabelText('Frequency');
    expect(freq).toBeTruthy();
  });

  it('goes to the previous page when cancel is clicked', async () => {
    delete window.history;

    window.history = {
      back: jest.fn(),
    };
    const form = render(DefaultForm);

    fireEvent.click(form.getByRole('button', { name: 'Cancel' }));

    expect(window.history.back).toHaveBeenCalled();
  });

  it('submits the form', async () => {
    const page = render(DefaultForm);
    mockDispatch.mockClear();
    const form = await page.findByRole('form');
    fireEvent.submit(form);

    expect(mockDispatch)
      .toHaveBeenCalledWith('submitFormData');
  });
});
