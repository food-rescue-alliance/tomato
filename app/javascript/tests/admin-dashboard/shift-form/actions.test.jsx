import moment from 'moment-timezone';
import axios from '../../../packs/volunteer_dashboard/axios';
import * as actions from '../../../packs/admin_dashboard/shift_form/actions';
import { calculateStartTime } from '../../../packs/admin_dashboard/shift_form/actions';

jest.mock('../../../packs/volunteer_dashboard/axios');

const dispatch = jest.fn();

describe('actions', () => {
  beforeEach(() => {
    dispatch.mockClear();
  });

  describe('fetchOrgAndInitForm', () => {
    describe('successful request', () => {
      it('sets the defaults correctly from the response', async () => {
        let start = calculateStartTime('UTC');
        const end = moment(start).add(2, 'hour').format('HH:mm');
        start = start.format('HH:mm');
        axios.get.mockResolvedValue({
          data: {
            data: {
              organization: {
                timeZone: 'UTC',
                sites: [
                  {
                    tasks: [
                      {
                        id: 1,
                        description: 'task 1',
                      },
                      {
                        id: 2,
                        description: 'task 2',
                      },
                    ],
                  },
                  {
                    tasks: [
                      {
                        id: 3,
                        description: 'task 3',
                      },
                      {
                        id: 4,
                        description: 'task 4',
                      },
                    ],
                  },
                ],
              },
            },
          },
        });

        await actions.fetchOrgAndInitForm('1')(dispatch);

        expect(dispatch)
          .toHaveBeenCalledTimes(3);
        expect(dispatch)
          .toHaveBeenCalledWith({
            type: actions.INITIALIZE_FORM,
            data: {
              tasks: [
                { id: 1, description: 'task 1' }, { id: 2, description: 'task 2' },
                { id: 3, description: 'task 3' }, { id: 4, description: 'task 4' },
              ],
              endTime: end,
              startTime: start,
            },
          });
      });
    });
  });
  describe('fetchCustomText', () => {
    describe('successful request', () => {
      it('sets the custom text from the response', async () => {
        const sentence = 'Every day';
        axios.get.mockResolvedValue({
          data: {
            sentence,
          },
        });

        await actions.fetchCustomText('rrule', 'date')(dispatch);

        expect(dispatch)
          .toHaveBeenCalledTimes(1);
        expect(dispatch)
          .toHaveBeenCalledWith({
            type: actions.SET_CUSTOM_TEXT,
            data: sentence,
          });
      });
    });
  });
  describe('submitFormData', () => {
    describe('successful post', () => {
      it('dispatches the success event', async () => {
        axios.post.mockResolvedValue({
          data: 'Success',
        });

        await actions.submitFormData({})(dispatch);

        expect(dispatch)
          .toHaveBeenCalledTimes(3);
        expect(dispatch)
          .toHaveBeenCalledWith({
            type: actions.FORM_SUCCESS,
            data: 'Success',
          });
      });
    });
  });
});
