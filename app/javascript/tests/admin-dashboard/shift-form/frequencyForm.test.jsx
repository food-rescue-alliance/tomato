import React from 'react';
import {
  fireEvent, render, within,
} from '@testing-library/react';
import moment from 'moment-timezone';
import * as actions from '../../../packs/admin_dashboard/shift_form/actions';
import { FrequencyForm } from '../../../packs/admin_dashboard/shift_form/FrequencyForm';

jest.mock('../../../packs/admin_dashboard/shift_form/actions');

describe('FrequencyForm', () => {
  const mockDispatch = jest.fn();
  actions.updateBasicForm = () => 'updateBasicForm';
  actions.calculateRrule = () => 'calculateRrule';
  actions.customFrequency = () => 'customFrequency';

  const DefaultForm = (
    <FrequencyForm
      startDate={moment().format('YYYY-MM-DD')}
      startTime={moment().format('HH:mm')}
      endTime={moment().format('HH:mm')}
      rrule=""
      customText="Custom..."
      dispatch={mockDispatch}
    />
  );

  it('recalculates rrule when a new frequency is selected', async () => {
    const form = render(DefaultForm);
    mockDispatch.mockClear();
    fireEvent.mouseDown(form.getByLabelText('Frequency'));

    const listbox = within(form.getByRole('listbox'));

    fireEvent.click(listbox.getByText(/Daily/i));
    expect(mockDispatch)
      .toHaveBeenCalledWith('updateBasicForm');
    expect(mockDispatch)
      .toHaveBeenCalledWith('calculateRrule');
  });

  it('sends data to the hidden fields when used', async () => {
    const date = moment();
    const page = render((
      <>
        <input id="hidden-date" type="hidden" data-testid="hidden-date" />
        <input id="hidden-start" type="hidden" data-testid="hidden-start" />
        <input id="hidden-end" type="hidden" data-testid="hidden-end" />
        <input id="hidden-rrule" type="hidden" data-testid="hidden-rrule" />
        <FrequencyForm
          startDate={date.format('YYYY-MM-DD')}
          startTime={date.format('HH:mm')}
          endTime={date.format('HH:mm')}
          rrule="rrule"
          customText="Custom..."
          dispatch={mockDispatch}
          hiddenFields
        />
      </>
    ));
    const hiddenDate = await page.findByTestId('hidden-date');
    const hiddenStart = await page.findByTestId('hidden-start');
    const hiddenEnd = await page.findByTestId('hidden-end');
    const hiddenRrule = await page.findByTestId('hidden-rrule');
    expect(hiddenDate.getAttribute('value')).toEqual(date.format('YYYY-MM-DD'));
    expect(hiddenStart.getAttribute('value')).toEqual(date.format('HH:mm'));
    expect(hiddenEnd.getAttribute('value')).toEqual(date.format('HH:mm'));
    expect(hiddenRrule.getAttribute('value')).toEqual('rrule');
  });

  it('does not send data to the hidden fields by default', async () => {
    const date = moment();
    const page = render((
      <>
        <input id="hidden-date" type="hidden" data-testid="hidden-date" />
        <input id="hidden-start" type="hidden" data-testid="hidden-start" />
        <input id="hidden-end" type="hidden" data-testid="hidden-end" />
        <input id="hidden-rrule" type="hidden" data-testid="hidden-rrule" />
        <FrequencyForm
          startDate={date.format('YYYY-MM-DD')}
          startTime={date.format('HH:mm')}
          endTime={date.format('HH:mm')}
          rrule="rrule"
          customText="Custom..."
          dispatch={mockDispatch}
        />
      </>
    ));
    const hiddenDate = await page.findByTestId('hidden-date');
    const hiddenStart = await page.findByTestId('hidden-start');
    const hiddenEnd = await page.findByTestId('hidden-end');
    const hiddenRrule = await page.findByTestId('hidden-rrule');
    expect(hiddenDate.getAttribute('value')).toEqual(null);
    expect(hiddenStart.getAttribute('value')).toEqual(null);
    expect(hiddenEnd.getAttribute('value')).toEqual(null);
    expect(hiddenRrule.getAttribute('value')).toEqual(null);
  });

  it('opens custom dialog when custom frequency is selected', async () => {
    const form = render(DefaultForm);
    mockDispatch.mockClear();
    fireEvent.mouseDown(form.getByLabelText('Frequency'));
    const listbox = within(form.getByRole('listbox'));
    fireEvent.click(listbox.getByText('Custom...'));
    const dialog = await form.findByRole('dialog');

    expect(mockDispatch)
      .not.toHaveBeenCalledWith('updateBasicForm');
    expect(mockDispatch)
      .not.toHaveBeenCalledWith('calculateRrule');
    expect(dialog).toBeTruthy();
  });

  it('submits a custom frequency', async () => {
    const form = render(DefaultForm);
    mockDispatch.mockClear();
    fireEvent.mouseDown(form.getByLabelText('Frequency'));
    const listbox = within(form.getByRole('listbox'));
    fireEvent.click(listbox.getByText('Custom...'));
    const dialog = within(await form.findByRole('dialog'));
    fireEvent.click(dialog.getByRole('button', { name: 'Submit' }));

    expect(mockDispatch)
      .toHaveBeenCalledWith('customFrequency');
    expect(mockDispatch)
      .toHaveBeenCalledWith('calculateRrule');
  });
});
