import moment from 'moment-timezone';
import {
  reducer,
  defaultState,
  FrequencyTypes, SubmitTypes, RepeatTypes
} from '../../../packs/admin_dashboard/shift_form/reducer';
import * as types from '../../../packs/admin_dashboard/shift_form/actions';

describe('reducer', () => {
  it('should handle LOADING', () => {
    expect(
      reducer(defaultState, {
        type: types.LOADING,
        data: true,
      }).loading,
    ).toEqual(true);

    expect(
      reducer(defaultState, {
        type: types.LOADING,
        data: false,
      }).loading,
    ).toEqual(false);
  });

  it('should handle SET_DATE', () => {
    const date = moment().format('YYYY-MM-DD');
    const state = reducer(defaultState, {
      type: types.SET_DATE,
      data: date,
    });

    expect(state.startDate)
      .toEqual(date);
  });

  it('should handle SET_START', () => {
    const start = moment().format('HH:mm');
    const state = reducer(defaultState, {
      type: types.SET_START,
      data: start,
    });

    expect(state.startTime)
      .toEqual(start);
  });

  it('should handle SET_END', () => {
    const end = moment().format('HH:mm');
    const state = reducer(defaultState, {
      type: types.SET_END,
      data: end,
    });

    expect(state.endTime)
      .toEqual(end);
  });

  it('should handle SET_CUSTOM_TEXT', () => {
    const text = 'Custom...';
    const state = reducer(defaultState, {
      type: types.SET_CUSTOM_TEXT,
      data: text,
    });

    expect(state.customText)
      .toEqual(text);
  });

  it('should handle INITIALIZE_FORM', () => {
    let start = moment();
    const end = moment(start).add(2, 'hours').format('HH:mm');
    start = start.format('HH:mm');
    const data = {
      startTime: start,
      endTime: end,
      tasks: [{ description: 'task', id: 1 }],
    };
    const state = reducer(defaultState, {
      type: types.INITIALIZE_FORM,
      data,
    });

    expect(state.startTime)
      .toEqual(data.startTime);
    expect(state.endTime)
      .toEqual(data.endTime);
    expect(state.tasks)
      .toEqual(data.tasks);
  });

  it('should handle CUSTOM_FREQUENCY', () => {
    const daily = {
      frequency: FrequencyTypes.daily,
    };
    const weekly = {
      frequency: FrequencyTypes.weekly,
    };
    const monthlyOn = {
      frequency: FrequencyTypes.monthly,
      customMonthType: 'on',
    };
    const monthlyEach = {
      frequency: FrequencyTypes.monthly,
      customMonthType: 'each',
    };
    const dailyState = reducer(defaultState, {
      type: types.CUSTOM_FREQUENCY,
      data: daily,
    });
    const weeklyState = reducer(defaultState, {
      type: types.CUSTOM_FREQUENCY,
      data: weekly,
    });
    const monthlyOnState = reducer(defaultState, {
      type: types.CUSTOM_FREQUENCY,
      data: monthlyOn,
    });
    const monthlyEachState = reducer(defaultState, {
      type: types.CUSTOM_FREQUENCY,
      data: monthlyEach,
    });

    expect(dailyState.frequency).toEqual(daily.frequency);
    expect(dailyState.submitType).toEqual(SubmitTypes.customDaily);
    expect(weeklyState.frequency).toEqual(weekly.frequency);
    expect(weeklyState.submitType).toEqual(SubmitTypes.customWeekly);
    expect(monthlyOnState.frequency).toEqual(monthlyOnState.frequency);
    expect(monthlyOnState.submitType).toEqual(SubmitTypes.customMonthlyWeek);
    expect(monthlyEachState.frequency).toEqual(monthlyEachState.frequency);
    expect(monthlyEachState.submitType).toEqual(SubmitTypes.customMonthlyDate);
  });

  it('should handle UPDATE_BASIC_FORM', () => {
    const data = {
      frequency: FrequencyTypes.daily,
    };
    const state = reducer(defaultState, {
      type: types.UPDATE_BASIC_FORM,
      data,
    });

    expect(state.frequency).toEqual(data.frequency);
    expect(state.submitType).toEqual(SubmitTypes.basic);
  });

  it('should handle CALCULATE_RRULE', () => {
    const state = reducer({
      ...defaultState,
      repeatType: RepeatTypes.forever,
    }, {
      type: types.CALCULATE_RRULE,
    });

    expect(state.rrule).toEqual('FREQ=DAILY;INTERVAL=1');
  });

  it('should handle FORM_SUCCESS', () => {
    delete window.location;

    window.location = {
      replace: jest.fn(),
    };
    const expectedURL = 'test/url';
    reducer(defaultState, {
      type: types.FORM_SUCCESS,
      data: { data: { link: expectedURL } },
    });

    expect(window.location.replace).toHaveBeenCalledWith(expectedURL);
  });
});
