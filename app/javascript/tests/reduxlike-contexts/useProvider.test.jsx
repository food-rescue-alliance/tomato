import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import PropTypes from 'prop-types';
import useProvider from '../../packs/reduxlike-contexts/useProvider';

const TestComponent = ({ foo, user, dispatch }) => {
  const testDispatch = () => {
    dispatch({ type: 'SET_USER', data: { id: 5 } });
  };

  const testDispatchThunk = () => {
    dispatch((otherDispatch) => {
      otherDispatch({ type: 'SET_USER', data: { id: 10 } });
    });
  };
  return (
    <div role="document">
      {foo}
      {user && user.id}
      <button type="button" onClick={testDispatch}>button</button>
      <button type="button" onClick={testDispatchThunk}>thunk</button>
    </div>
  );
};

TestComponent.propTypes = {
  foo: PropTypes.string.isRequired,
  user: PropTypes.shape({
    id: PropTypes.number,
  }),
  dispatch: PropTypes.func.isRequired,
};

TestComponent.defaultProps = {
  user: {},
};

const StateContext = React.createContext();
const DispatchContext = React.createContext();

const reducer = (state, action) => {
  switch (action.type) {
    case 'SET_USER': {
      return { ...state, user: action.data };
    }
    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
};

const defaultState = {};

const [StateProvider, connect] = useProvider(StateContext, DispatchContext, reducer, defaultState);

describe('connect()', () => {
  test('returns a component with state and dispatch passed in as props', () => {
    const ConnectedComponent = connect(TestComponent);

    render(<StateProvider><ConnectedComponent foo="bar" /></StateProvider>);

    expect(screen.getByRole('document')).toHaveTextContent('bar');
    expect(screen.getByRole('document')).not.toHaveTextContent('5');

    fireEvent.click(screen.getByText('button'));

    expect(screen.getByRole('document')).toHaveTextContent('5');

    fireEvent.click(screen.getByText('thunk'));

    expect(screen.getByRole('document')).toHaveTextContent('10');
  });
});
