import { reducer, defaultState } from '../../../../packs/volunteer_dashboard/domains/shift_details/reducer';
import * as types from '../../../../packs/volunteer_dashboard/domains/shift_details/actions';

describe('reducer', () => {
  it('should handle LOADING', () => {
    expect(
      reducer(defaultState, {
        type: types.LOADING,
        data: true,
      }).loading,
    ).toEqual(true);

    expect(
      reducer(defaultState, {
        type: types.LOADING,
        data: false,
      }).loading,
    ).toEqual(false);
  });

  it('should handle SET_SHIFT', () => {
    const dateTime = new Date().toISOString();
    const state = reducer(defaultState, {
      type: types.SET_SHIFT,
      data: {
        id: '123abc',
        startsAt: dateTime,
        endsAt: dateTime,
        title: 'My Shift',
        tasks: [
          {
            id: 1,
            description: 'pickup at market',
            site: { id: 6 },
          },
          {
            id: 2,
            description: 'dropoff at charity',
            site: { id: 7 },
          },
        ],
        taskLogs: [
          {
            taskId: 2, weight: '5', notes: 'applesauce', temperature: '',
          },
        ],
        users: [{ id: 'user-id' }],
      },
    });

    expect(state.loading).toEqual(false);
    expect(state.shift).toEqual({
      id: '123abc',
      startsAt: dateTime,
      endsAt: dateTime,
      title: 'My Shift',
      tasks: [
        {
          id: 1,
          description: 'pickup at market',
          site: { id: 6 },
        },
        {
          id: 2,
          description: 'dropoff at charity',
          site: { id: 7 },
        },
      ],
      users: [{ id: 'user-id' }],
    });

    const key1 = state.taskLogs[1][0].key;
    const key2 = state.taskLogs[2][1].key;

    expect(state.taskLogs).toEqual([
      undefined,
      [{
        notes: '', weight: '', key: key1, temperature: '',
      }],
      [
        {
          taskId: 2,
          weight: '5',
          notes: 'applesauce',
          temperature: '',
        },
        {
          weight: '', notes: '', key: key2, temperature: '',
        },
      ],
    ]);
  });

  it('should handle FOOD_TAXONOMIES_LOADING', () => {
    const state = reducer(defaultState, {
      type: types.FOOD_TAXONOMIES_LOADING,
    });

    expect(state.foodTaxonomies.loading).toEqual(true);
  });

  it('should handle FOOD_TAXONOMIES_SUCCESS', () => {
    const state = reducer({ ...defaultState, food_taxonomies: { loading: true } }, {
      type: types.FOOD_TAXONOMIES_SUCCESS,
      data: 'food_taxonomies_data',
    });

    expect(state.foodTaxonomies.loading).toEqual(false);
    expect(state.foodTaxonomies.values).toEqual('food_taxonomies_data');
  });

  it('should handle FOOD_TAXONOMIES_ERROR', () => {
    const state = reducer(defaultState, {
      type: types.FOOD_TAXONOMIES_ERROR,
    });

    expect(state.foodTaxonomies.error).toEqual(true);
  });

  it('should handle SET_LOG', () => {
    const dateTime = new Date().toISOString();

    const startState = {
      ...defaultState,
      shift: {
        id: '123abc',
        startsAt: dateTime,
        endsAt: dateTime,
        title: 'My Shift',
        tasks: [
          {
            id: 1,
            description: 'pickup at market',
          },
          {
            id: 2,
            description: 'dropoff at charity',
          },
        ],
      },
      taskLogs: [
        undefined,
        [{ weight: '1', notes: '', temperature: '' }],
        [
          {
            taskId: 2,
            weight: '5',
            notes: 'applesauce',
            temperature: '',
          },
          { notes: '', weight: '', temperature: '' },
        ],
      ],
    };

    const state = reducer(startState, {
      type: types.SET_LOG,
      data: {
        taskId: 2,
        log: { weight: '10', notes: 'new note', temperature: '' },
      },
    });

    const { key } = state.taskLogs[2][2];

    expect(state.taskLogs).toEqual([
      undefined,
      [
        { notes: '', weight: '1', temperature: '' },
      ], [
        {
          notes: 'applesauce', taskId: 2, weight: '5', temperature: '',
        },
        {
          notes: 'new note', weight: '10', temperature: '',
        },
        {
          notes: '', weight: '', key, temperature: '',
        },
      ],
    ]);
  });

  it('should handle SET_LOG_ERROR', () => {
    const dateTime = new Date().toISOString();

    const startState = {
      ...defaultState,
      shift: {
        id: '123abc',
        startsAt: dateTime,
        endsAt: dateTime,
        title: 'My Shift',
        tasks: [
          {
            id: 1,
            description: 'pickup at market',
          },
          {
            id: 2,
            description: 'dropoff at charity',
          },
        ],
      },
      taskLogs: [
        undefined,
        [{ weight: '1', notes: '' }],
        [
          {
            taskId: 2,
            weight: '5',
            notes: 'applesauce',
          },
          { notes: '', weight: '' },
        ],
      ],
    };

    const state = reducer(startState, {
      type: types.SET_LOG_ERROR,
      data: 2,
    });

    expect(state.taskLogs).toEqual([
      undefined,
      [
        { notes: '', weight: '1' },
      ],
      [
        { notes: 'applesauce', taskId: 2, weight: '5' },
        { error: true, notes: '', weight: '' },
      ],
    ]);
  });

  it('should handle PICKUP_SHIFT_SUBMITTED', () => {
    const startState = { ...defaultState, error: 'some error message', shift: { id: 1 } };
    const state = reducer(startState, {
      type: types.PICKUP_SHIFT_SUBMITTED,
    });

    expect(state.error).toEqual(null);
    expect(state.shift).toEqual({ id: 1, loading: true });
  });

  it('should handle PICKUP_SHIFT_SUCCESS', () => {
    const startState = { ...defaultState, shift: { id: 1 } };
    const state = reducer(startState, {
      type: types.PICKUP_SHIFT_SUCCESS,
    });

    expect(state.shift).toEqual({ id: 1, loading: false });
  });

  it('should handle PICKUP_SHIFT_ERROR', () => {
    const startState = { ...defaultState, shift: { id: 1 } };
    const state = reducer(startState, {
      type: types.PICKUP_SHIFT_ERROR,
    });

    expect(state.shift).toEqual({ id: 1, loading: false });
  });

  it('should handle ERROR', () => {
    const state = reducer(defaultState, {
      type: types.ERROR,
      data: {
        status: 500,
      },
    });

    expect(state.loading).toEqual(false);
    expect(state.error).toEqual({ status: 500 });
  });
});
