import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { MemoryRouter } from 'react-router-dom';
import { ShiftDetailsComponent } from '../../../../packs/volunteer_dashboard/domains/shift_details/ShiftDetails';
import { ShiftStateProvider } from '../../../../packs/volunteer_dashboard/domains/shift_details/context';
import * as actions from '../../../../packs/volunteer_dashboard/domains/shift_details/actions';

jest.mock('../../../../packs/volunteer_dashboard/domains/shift_details/actions');

describe('ShiftDetails', () => {
  const mockDispatch = jest.fn();
  actions.fetchShiftDetails = () => 'fetchShiftDetails';
  actions.pickupShift = () => 'pickupShift';
  actions.fetchFoodTaxonomies = () => 'fetchFoodTaxonomies';
  actions.fetchShiftEventNotes = () => 'fetchShiftEventNotes';
  actions.updateShiftNote = jest.fn()

  it('loads shift details on mount', async () => {
    render(<ShiftDetailsComponent id="123-123" loading dispatch={mockDispatch} />);
    expect(mockDispatch).toHaveBeenCalledTimes(3);
    expect(mockDispatch).toHaveBeenCalledWith('fetchShiftDetails');
    expect(mockDispatch).toHaveBeenCalledWith('fetchFoodTaxonomies');
    expect(mockDispatch).toHaveBeenCalledWith('fetchShiftEventNotes');
  });

  it('displays loading when shift details are loading', async () => {
    render(<ShiftDetailsComponent id="123-123" dispatch={mockDispatch} loading />);
    expect(screen.getByTestId('loading')).toBeTruthy();
  });

  it('displays error when failed to fetch shift details', async () => {
    render(<ShiftDetailsComponent
      id="123-123"
      dispatch={mockDispatch}
      loading={false}
      error={'someError'}
    />);
    expect(screen.getByTestId('error')).toBeTruthy();
  });

  it('displays a message when no tasks are present on shift', async () => {
    const shift = {
      id: 'abcd1234',
      title: 'My Shift',
      startsAt: new Date().toISOString(),
      endsAt: new Date().toISOString(),
      users: [{}],
    };

    render(
      <MemoryRouter>
        <ShiftDetailsComponent
          dispatch={mockDispatch}
          id="123-123"
          loading={false}
          shift={shift}
          user={{timeZoneOffset: "-07:00"}}
        />
      </MemoryRouter>,
    );
    expect(screen.getByText('shiftDetails.noTasks')).toBeTruthy();
  });

  it('navigates between shift tasks', async () => {
    const taskLogs = [
      undefined,
      [
        {
          weight: '',
          notes: '',
        },
      ],
      [
        {
          weight: '10',
          notes: 'applesauce',
        },
        {
          weight: '',
          notes: '',
        },
      ],
      [
        {
          weight: '',
          notes: '',
        },
      ],
      [
        {
          weight: '',
          notes: '',
        },
      ],
    ];
    const tasks = [
      {
        id: 1,
        description: 'pickup at market',
        site: {
          type: 'Donor',
          name: 'market',
        },
      },
      {
        id: 2,
        description: 'pickup at restaurant',
        site: {
          type: 'Donor',
          name: 'restaurant',
        },
      },
      {
        id: 3,
        description: 'dropoff at shelter',
        site: {
          type: 'Recipient',
          name: 'shelter',
        },
      },
      {
        id: 4,
        description: 'dropoff at library',
        site: {
          type: 'Recipient',
          name: 'library',
        },
      },
    ];

    const shift = {
      id: 'abcd1234',
      title: 'My Shift',
      startsAt: new Date().toISOString(),
      endsAt: new Date().toISOString(),
      tasks,
      taskLogs,
      users: [{}],
    };

    render(
      <MemoryRouter>
        <ShiftStateProvider>
          <ShiftDetailsComponent
            dispatch={mockDispatch}
            id="123-123"
            loading={false}
            shift={shift}
            taskLogs={taskLogs}
            user={{timeZoneOffset: "-07:00"}}
          />
        </ShiftStateProvider>
      </MemoryRouter>,
    );
    expect(screen.getByText('shiftDetails.pickup')).toBeTruthy();
    expect(screen.getByText('market')).toBeTruthy();

    fireEvent.click(screen.getByTitle('next-task'));

    expect(screen.getByText('shiftDetails.pickup')).toBeTruthy();
    expect(screen.getByText('restaurant')).toBeTruthy();

    fireEvent.click(screen.getByTitle('next-task'));

    expect(screen.getByText('shiftDetails.dropoff')).toBeTruthy();
    expect(screen.getByText('shelter')).toBeTruthy();

    fireEvent.click(screen.getByTitle('next-task'));

    expect(screen.getByText('shiftDetails.dropoff')).toBeTruthy();
    expect(screen.getByText('library')).toBeTruthy();

    fireEvent.click(screen.getByTitle('previous-task'));

    expect(screen.getByText('shiftDetails.dropoff')).toBeTruthy();
    expect(screen.getByText('shelter')).toBeTruthy();
  });

  it('displays shift details', () => {
    const shift = {
      id: 'abcd1234',
      title: 'My Shift',
      startsAt: new Date().toISOString(),
      endsAt: new Date().toISOString(),
      users: [{}],
      tasks: [{
        id: 1,
        description: 'pickup at market',
        site: {
          type: 'Donor',
          name: 'market',
        },
      }],
    };

    render(
      <MemoryRouter>
        <ShiftStateProvider>
          <ShiftDetailsComponent
            dispatch={mockDispatch}
            id="123-123"
            loading={false}
            shift={shift}
            user={{timeZoneOffset: "-07:00"}}
          />
        </ShiftStateProvider>
      </MemoryRouter>,
    );

    expect(screen.getByText('My Shift')).toBeTruthy();
    expect(screen.queryByText('shiftDetails.pickupButton')).toBeFalsy();
    expect(screen.getByText('shiftDetails.logReport')).toBeTruthy();
  });

  it('allows user to select a transportation type', () => {
    const shift = {
      id: 'abcd1234',
      title: 'My Shift',
      startsAt: new Date().toISOString(),
      endsAt: new Date().toISOString(),
      users: [{}],
    };

    render(<MemoryRouter>
      <ShiftDetailsComponent
        dispatch={mockDispatch}
        id="123-123"
        loading={false}
        shift={shift}
        user={{timeZoneOffset: "-07:00"}}
      />
    </MemoryRouter>,);

    fireEvent.click(screen.getByLabelText('car'));
    expect(actions.updateShiftNote).toHaveBeenCalledWith("abcd1234",  {transportationType: "car"});
  })
  it('allows user to enter their hours spent', () => {
    const shift = {
      id: 'abcd1234',
      title: 'My Shift',
      startsAt: new Date().toISOString(),
      endsAt: new Date().toISOString(),
      users: [{}],
    };

    render(<MemoryRouter>
      <ShiftDetailsComponent
        dispatch={mockDispatch}
        id="123-123"
        loading={false}
        shift={shift}
        user={{timeZoneOffset: "-07:00"}}
      />
    </MemoryRouter>,);

    const hoursSpentInput = screen.getByLabelText('shiftDetails.hoursSpentLabel')
    fireEvent.click(hoursSpentInput);
    fireEvent.change(hoursSpentInput, {target: {value: '23'}})
    fireEvent.blur(hoursSpentInput);
    expect(actions.updateShiftNote).toHaveBeenCalledWith("abcd1234",  {hoursSpent: "23"});

  })

  it('allows user to pickup an open shift', () => {
    const shift = {
      id: 'abcd1234',
      title: 'My Shift',
      startsAt: new Date().toISOString(),
      endsAt: new Date().toISOString(),
      loading: false,
      users: [],
      tasks: [{
        id: 1,
        description: 'pickup at market',
        site: {
          type: 'Donor',
          name: 'market',
        },
      }],
    };

    const page = render(
      <MemoryRouter>
        <ShiftStateProvider>
          <ShiftDetailsComponent
            dispatch={mockDispatch}
            id="123-123"
            loading={false}
            shift={shift}
            user={{timeZoneOffset: "-07:00"}}
          />
        </ShiftStateProvider>
      </MemoryRouter>,
    );

    expect(screen.getByText('My Shift')).toBeTruthy();
    expect(screen.getByText('shiftDetails.pickupButton')).toBeTruthy();
    expect(screen.queryByText('shiftDetails.logReport')).toBeFalsy();

    fireEvent.click(screen.getByText('shiftDetails.pickupButton'));
    fireEvent.click(screen.getByText('pickupShifts.oneTime'));

    expect(mockDispatch).toHaveBeenCalledWith('pickupShift');
  });

  it('disables pickup button when shift is loading', () => {
    const shift = {
      id: 'abcd1234',
      title: 'My Shift',
      startsAt: new Date().toISOString(),
      endsAt: new Date().toISOString(),
      users: [],
      tasks: [{
        id: 1,
        description: 'pickup at market',
        site: {
          type: 'Donor',
          name: 'market',
        },
      }],
      loading: true,
    };

    const page = render(
      <MemoryRouter>
        <ShiftStateProvider>
          <ShiftDetailsComponent
            dispatch={mockDispatch}
            id="123-123"
            loading={false}
            shift={shift}
            user={{timeZoneOffset: "-07:00"}}
          />
        </ShiftStateProvider>
      </MemoryRouter>,
    );

    expect(page.getByText('shiftDetails.pickupButton').closest('button')).toBeDisabled();
  });

  it('displays errors', () => {
    const shift = {
      id: 'abcd1234',
      title: 'My Shift',
      startsAt: new Date().toISOString(),
      endsAt: new Date().toISOString(),
      users: [],
      tasks: [{
        id: 1,
        description: 'pickup at market',
        site: {
          type: 'Donor',
          name: 'market',
        },
      }],
    };

    const page = render(
      <MemoryRouter>
        <ShiftStateProvider>
          <ShiftDetailsComponent
            dispatch={mockDispatch}
            id="123-123"
            loading={false}
            shift={shift}
            error="error message"
            user={{timeZoneOffset: "-07:00"}}
          />
        </ShiftStateProvider>
      </MemoryRouter>,
    );

    expect(page.getByText('error message')).toBeTruthy();
    expect(page.getByText('shiftDetails.pickupButton').closest('button')).not.toBeDisabled();
  });
});
