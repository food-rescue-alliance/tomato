import axios from '../../../../packs/volunteer_dashboard/axios';
import * as actions from '../../../../packs/volunteer_dashboard/domains/shift_details/actions';

jest.mock('../../../../packs/volunteer_dashboard/axios');

const dispatch = jest.fn();

describe('actions', () => {
  beforeEach(() => {
    dispatch.mockClear();
  });

  describe('fetchShiftDetails', () => {
    describe('successful request', () => {
      it('sets the shift details correctly from the response', async () => {
        axios.get.mockResolvedValueOnce({ data: { users: [{ id: 1 }] } });

        await actions.fetchShiftDetails(1)(dispatch);

        expect(dispatch).toHaveBeenCalledTimes(2);
        expect(dispatch).toHaveBeenCalledWith({ type: actions.LOADING, data: true });
        expect(dispatch).toHaveBeenCalledWith({
          type: actions.SET_SHIFT,
          data: { users: [{ id: 1 }] },
        });
      });
    });

    describe('request for shift details is a failure', () => {
      it('dispatches an error action', async () => {
        axios.get.mockImplementationOnce(() => Promise.reject(new Error('Request Failed!')));

        await actions.fetchShiftDetails('123-123')(dispatch);

        expect(dispatch).toHaveBeenCalledTimes(2);
        expect(dispatch).toHaveBeenCalledWith({ type: actions.LOADING, data: true });
        expect(dispatch).toHaveBeenCalledWith({ type: actions.ERROR, data: 'shiftDetails.loadShiftError' });
      });
    });
  });

  describe('fetchFoodTaxonomies', () => {
    describe('successful request', () => {
      it('sets the food taxonomies correctly from the response', async () => {
        axios.get.mockResolvedValueOnce({ data: 'food_taxonomies' });

        await actions.fetchFoodTaxonomies()(dispatch);

        expect(dispatch).toHaveBeenCalledTimes(2);
        expect(dispatch).toHaveBeenCalledWith({ type: actions.FOOD_TAXONOMIES_LOADING });
        expect(dispatch).toHaveBeenCalledWith({
          type: actions.FOOD_TAXONOMIES_SUCCESS,
          data: 'food_taxonomies',
        });
      });
    });

    describe('request for shift details is a failure', () => {
      it('dispatches an error action', async () => {
        axios.get.mockImplementationOnce(() => Promise.reject(new Error('Request Failed!')));

        await actions.fetchFoodTaxonomies()(dispatch);

        expect(dispatch).toHaveBeenCalledTimes(2);
        expect(dispatch).toHaveBeenCalledWith({ type: actions.FOOD_TAXONOMIES_LOADING });
        expect(dispatch).toHaveBeenCalledWith({ type: actions.FOOD_TAXONOMIES_ERROR });
      });
    });
  });

  describe('updateShiftLog', () => {
    describe('successful request', () => {
      it('updates the shift logs correctly', async () => {
        axios.post.mockResolvedValueOnce({ data: { id: 'new-id' } });

        await actions.updateShiftLog('123-123', 1, '10', 'applesauce')(dispatch);

        expect(dispatch).toHaveBeenCalledTimes(1);
        expect(dispatch).toHaveBeenCalledWith({
          type: actions.SET_LOG,
          data: { taskId: 1, log: { id: 'new-id' } },
        });
      });
    });

    describe('request for shift details is a failure', () => {
      it('dispatches an error action', async () => {
        axios.post.mockImplementationOnce(() => Promise.reject(new Error('Request Failed!')));

        await actions.updateShiftLog('123-123', 1, '10', 'applesauce')(dispatch);

        expect(dispatch).toHaveBeenCalledTimes(1);
        expect(dispatch).toHaveBeenCalledWith({
          type: actions.SET_LOG_ERROR,
          data: 1,
        });
      });
    });
  });

  describe('pickupShift', () => {
    describe('successful request', () => {
      it('updates the shift details', async () => {
        axios.post.mockResolvedValueOnce({ data: { id: 'new-id' } });

        await actions.pickupShift('123-123')(dispatch);

        expect(dispatch).toHaveBeenCalledTimes(3);
        expect(dispatch).toHaveBeenCalledWith({ type: actions.PICKUP_SHIFT_SUBMITTED });
        expect(dispatch).toHaveBeenCalledWith({
          type: actions.SET_SHIFT,
          data: { id: 'new-id' },
        });
        expect(dispatch).toHaveBeenCalledWith({ type: actions.PICKUP_SHIFT_SUCCESS });
      });
    });

    describe('request to pickup shift is a failure', () => {
      it('dispatches an error action', async () => {
        axios.post.mockImplementationOnce(() => Promise.reject(new Error('Request Failed!')));

        await actions.pickupShift('123-123')(dispatch);

        expect(dispatch).toHaveBeenCalledTimes(3);
        expect(dispatch).toHaveBeenCalledWith({ type: actions.PICKUP_SHIFT_SUBMITTED });
        expect(dispatch).toHaveBeenCalledWith({
          type: actions.ERROR,
          data: 'shiftDetails.pickupShiftError',
        });
        expect(dispatch).toHaveBeenCalledWith({ type: actions.PICKUP_SHIFT_ERROR });
      });
    });
  });
});
