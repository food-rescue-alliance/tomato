import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Loading from '../../../../packs/volunteer_dashboard/domains/global/Loading';

describe('Loading', () => {
  it('displays a full page loading modal', async () => {
    render(<Loading />);
    expect(screen.queryByTestId('inlineLoading')).toBeFalsy();
  });

  it('displays an inline page loading indicator when inline is true', async () => {
    render(<Loading inline />);
    expect(screen.getByTestId('inlineLoading')).toBeTruthy();
  });
});
