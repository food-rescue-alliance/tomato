import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import HardRedirect from '../../../../packs/volunteer_dashboard/domains/global/HardRedirect';

describe('HardRedirect', () => {
  // mocking window.location
  const { location } = window;
  beforeAll(() => {
    delete window.location;
    window.location = {
      href: '',
    };
  });
  afterAll(() => {
    window.location = location;
  });

  it('redirects to the given url', async () => {
    render(<HardRedirect to="url1" />);
    expect(window.location.href).toBe('url1');
  });
});
