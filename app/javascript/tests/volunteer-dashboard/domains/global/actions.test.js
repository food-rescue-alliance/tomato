import axios from '../../../../packs/volunteer_dashboard/axios';
import * as actions from '../../../../packs/volunteer_dashboard/domains/global/actions';

jest.mock('../../../../packs/volunteer_dashboard/axios');

const dispatch = jest.fn();

describe('actions', () => {
  beforeEach(() => {
    dispatch.mockClear();
  });

  describe('fetchUser', () => {
    describe('successful request', () => {
      it('sets the user correctly from the response', async () => {
        axios.get.mockResolvedValue({ data: 'name' });

        await actions.fetchUser()(dispatch);

        expect(dispatch).toHaveBeenCalledTimes(2);
        expect(dispatch).toHaveBeenCalledWith({ type: actions.LOADING, data: true });
        expect(dispatch).toHaveBeenCalledWith({ type: actions.SET_USER, data: 'name' });
      });
    });

    describe('request for user is a failure', () => {
      it('dispatches an error action', async () => {
        axios.get.mockImplementationOnce(() => Promise.reject(new Error('Request Failed!')));

        await actions.fetchUser()(dispatch);

        expect(dispatch).toHaveBeenCalledTimes(2);
        expect(dispatch).toHaveBeenCalledWith({ type: actions.LOADING, data: true });
        expect(dispatch).toHaveBeenCalledWith({ type: actions.ERROR, data: new Error('Request Failed!') });
      });
    });
  });

  describe('updateUser', () => {
    describe('successful request', () => {
      it('updates the user correctly', async () => {
        axios.put = jest.fn();
        axios.put.mockResolvedValue({ data: {full_name: 'UPDATE RESPONSE'} });

        const address = {
          streetOne: '123 Sesame St',
          streetTwo: 'Apt 1',
          city: 'New York',
          state: 'NY',
          zip: '12345',
        };

        await actions.updateUser('Inigo Montoya', 'imontoya@gmail.com', '(555)555-5555', address)(dispatch);

        expect(axios.put).toHaveBeenCalledWith('/api/users/me.json', {
          full_name: 'Inigo Montoya',
          email: 'imontoya@gmail.com',
          phone: '5555555555',
          address_attributes: {
            street_one: '123 Sesame St',
            street_two: 'Apt 1',
            city: 'New York',
            state: 'NY',
            zip: '12345',
          },
        });

        expect(dispatch).toHaveBeenCalledTimes(2);
        expect(dispatch).toHaveBeenCalledWith({
          type: actions.SET_USER,
          data: { loading: true, error: undefined },
        });
        expect(dispatch).toHaveBeenCalledWith({
          type: actions.SET_USER,
          data: {
            loading: false,
            full_name: 'UPDATE RESPONSE',
          },
        });
      });
    });

    describe('request to update user is a failure', () => {
      it('dispatches an error action', async () => {
        const address = {
          streetOne: '',
          streetTwo: '',
          city: '',
          state: '',
          zip: '',
        };

        const error = new Error();
        error.response = { data: 'Request Failed!' };

        axios.put.mockImplementationOnce(() => Promise.reject(error));

        await actions.updateUser('Inigo Montoya', 'imontoya@gmail.com', '(555)555-5555', address)(dispatch);

        expect(dispatch).toHaveBeenCalledTimes(2);
        expect(dispatch).toHaveBeenCalledWith({
          type: actions.SET_USER,
          data: { loading: true, error: undefined },
        });
        expect(dispatch).toHaveBeenCalledWith({
          type: actions.SET_USER,
          data: { loading: false, error: 'Request Failed!' },
        });
      });
    });
  });

  describe('logout', () => {
    describe('successful request', () => {
      it('sets the user to blank', async () => {
        axios.get.mockResolvedValue({});

        await actions.logout()(dispatch);

        expect(dispatch).toHaveBeenCalledTimes(1);
        expect(dispatch).toHaveBeenCalledWith({ type: actions.LOGOUT });
      });
    });

    describe('request for logout is a failure', () => {
      it('dispatches an error action', async () => {
        axios.get.mockImplementationOnce(() => Promise.reject(new Error('Request Failed!')));

        await actions.logout()(dispatch);

        expect(dispatch).toHaveBeenCalledTimes(1);
        expect(dispatch).toHaveBeenCalledWith({ type: actions.ERROR, data: new Error('Request Failed!') });
      });
    });
  });
});
