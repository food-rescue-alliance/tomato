import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import StatusButton from '../../../../packs/volunteer_dashboard/domains/global/StatusButton';

describe('StatusButton', () => {
  it('displays a normal button', async () => {
    render(<StatusButton />);
    expect(screen.queryByTestId('status-button')).toBeTruthy();
  });

  it('displays a success button', async () => {
    render(<StatusButton status="success" />);
    expect(screen.queryByTestId('status-button-success')).toBeTruthy();
  });

  it('displays a loading button', async () => {
    render(<StatusButton status="loading" />);
    expect(screen.queryByTestId('status-button-loading')).toBeTruthy();
  });
});
