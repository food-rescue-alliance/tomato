import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import TextField from '../../../../packs/volunteer_dashboard/domains/global/TextField';

describe('TextField', () => {
  it('is a controlled input', async () => {
    const mockOnChange = jest.fn();
    const textField = render(<TextField onChange={mockOnChange} />);
    const input = textField.getByRole('textbox');
    fireEvent.change(input, { target: { value: '23' } });
    expect(mockOnChange).toHaveBeenCalledTimes(1);
    expect(mockOnChange).toHaveBeenCalledWith('23');
  });
});
