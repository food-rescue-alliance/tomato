import { reducer, defaultState } from '../../../../packs/volunteer_dashboard/domains/global/reducer';
import * as types from '../../../../packs/volunteer_dashboard/domains/global/actions';

describe('reducer', () => {
  it('should handle LOADING', () => {
    expect(
      reducer(defaultState, {
        type: types.LOADING,
        data: true,
      }).loading,
    ).toEqual(true);

    expect(
      reducer(defaultState, {
        type: types.LOADING,
        data: false,
      }).loading,
    ).toEqual(false);
  });

  it('should handle SET_USER', () => {
    const state = reducer(defaultState, {
      type: types.SET_USER,
      data: {
        id: 5,
        fullName: 'Ruth',
      },
    });

    expect(state.loading).toEqual(false);
    expect(state.user).toEqual({ id: 5, fullName: 'Ruth' });
  });

  it('should handle ERROR', () => {
    const state = reducer(defaultState, {
      type: types.ERROR,
      error: {
        status: 500,
      },
    });

    expect(state.loading).toEqual(false);
    expect(state.error).toEqual({ status: 500 });
  });

  it('should handle LOGOUT', () => {
    const startingState = {
      ...defaultState,
      user: {
        id: 5,
        fullName: 'Ruth',
      },
    };
    const state = reducer(startingState, {
      type: types.LOGOUT,
    });

    expect(state.user).toEqual({});
  });
});
