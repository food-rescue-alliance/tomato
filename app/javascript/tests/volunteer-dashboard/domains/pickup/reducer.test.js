import { reducer, defaultState } from '../../../../packs/volunteer_dashboard/domains/pickup_shift/reducer';
import * as types from '../../../../packs/volunteer_dashboard/domains/pickup_shift/actions';

describe('reducer', () => {
  it('should handle LOADING', () => {
    expect(
      reducer(defaultState, {
        type: types.LOADING,
        data: true,
      }).loading,
    ).toEqual(true);

    expect(
      reducer(defaultState, {
        type: types.LOADING,
        data: false,
      }).loading,
    ).toEqual(false);
  });

  it('should handle SET_PICKUP_SHIFTS', () => {
    const state = reducer(defaultState, {
      type: types.SET_PICKUP_SHIFTS,
      data: [{ id: 1 }, { id: 2 }],
    });

    expect(state.loading).toEqual(false);
    expect(state.openShifts).toEqual([{ id: 1 }, { id: 2 }]);
  });

  it('should handle ERROR', () => {
    const state = reducer(defaultState, {
      type: types.ERROR,
      data: {
        status: 500,
      },
    });

    expect(state.loading).toEqual(false);
    expect(state.error).toEqual({ status: 500 });
  });
});
