import axios from '../../../../packs/volunteer_dashboard/axios';
import * as actions from '../../../../packs/volunteer_dashboard/domains/pickup_shift/actions';

jest.mock('../../../../packs/volunteer_dashboard/axios');

const dispatch = jest.fn();

describe('actions', () => {
  beforeEach(() => {
    dispatch.mockClear();
  });

  describe('fetchOpenShifts', () => {
    describe('successful request', () => {
      it('sets the open shifts correctly from the response', async () => {
        axios.get.mockResolvedValue({ data: [{ id: 1 }] });

        await actions.fetchOpenShifts()(dispatch);

        expect(dispatch).toHaveBeenCalledTimes(2);
        expect(dispatch).toHaveBeenCalledWith({ type: actions.LOADING, data: true });
        expect(dispatch).toHaveBeenCalledWith({
          type: actions.SET_PICKUP_SHIFTS,
          data: [{ id: 1 }],
        });
      });
    });

    describe('request for user is a failure', () => {
      it('dispatches an error action', async () => {
        axios.get.mockImplementationOnce(() => Promise.reject(new Error('Request Failed!')));

        await actions.fetchOpenShifts()(dispatch);

        expect(dispatch).toHaveBeenCalledTimes(2);
        expect(dispatch).toHaveBeenCalledWith({ type: actions.LOADING, data: true });
        expect(dispatch).toHaveBeenCalledWith({ type: actions.ERROR, data: new Error('Request Failed!') });
      });
    });
  });
});
