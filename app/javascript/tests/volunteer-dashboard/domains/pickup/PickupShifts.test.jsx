import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { MemoryRouter } from 'react-router-dom';
import { PickupShiftsComponent as PickupShifts } from '../../../../packs/volunteer_dashboard/domains/pickup_shift/PickupShifts';
import * as actions from '../../../../packs/volunteer_dashboard/domains/pickup_shift/actions';

jest.mock('../../../../packs/volunteer_dashboard/domains/pickup_shift/actions');

describe('PickupShifts', () => {
  it('loads available shifts on mount', async () => {
    const mockDispatch = jest.fn();
    actions.fetchOpenShifts = () => 'fetchOpenShifts';

    render(
      <MemoryRouter>
        <PickupShifts dispatch={mockDispatch} loading />
      </MemoryRouter>,
    );

    expect(mockDispatch).toHaveBeenCalledTimes(1);
    expect(mockDispatch).toHaveBeenCalledWith('fetchOpenShifts');
  });

  it('displays available shifts and navigates between recurring and one time shifts', async () => {
    const mockDispatch = jest.fn();
    const dateTime = new Date().toISOString();
    const oneTimeShifts = [
      {
        id: 'shift1',
        startsAt: dateTime,
        endsAt: dateTime,
        title: 'Shift 1',
        users: [],
        tasks: [],
      },
      {
        id: 'shift2',
        startsAt: dateTime,
        endsAt: dateTime,
        title: 'Shift 2',
        users: [],
        tasks: [],
      },
    ];
    const recurringShifts = [
      {
        id: 'shift3-1',
        startsAt: dateTime,
        endsAt: dateTime,
        title: 'Shift 3',
        users: [],
        tasks: [],
      },
      {
        id: 'shift3-2',
        startsAt: dateTime,
        endsAt: dateTime,
        title: 'Shift 3',
        users: [],
        tasks: [],
      },
    ];
    const shifts = {
      recurring: recurringShifts,
      oneTime: oneTimeShifts,
    };

    const page = render(
      <MemoryRouter>
        <PickupShifts dispatch={mockDispatch} loading={false} openShifts={shifts} user={{timeZoneOffset: "-07:00"}}/>
      </MemoryRouter>,
    );

    expect(page.getByText('pickupShifts.title')).toBeTruthy();

    expect(page.getAllByText('Shift 3').length).toEqual(2);
    expect(page.queryByText('Shift 1')).toBeFalsy();
    expect(page.queryByText('Shift 2')).toBeFalsy();

    fireEvent.click(page.getByText('global.oneTime'));

    expect(page.getByText('Shift 1')).toBeTruthy();
    expect(page.getByText('Shift 2')).toBeTruthy();
    expect(page.queryByText('Shift 3')).toBeFalsy();

    fireEvent.click(page.getByText('global.recurring'));

    expect(page.getAllByText('Shift 3').length).toEqual(2);
    expect(page.queryByText('Shift 1')).toBeFalsy();
    expect(page.queryByText('Shift 2')).toBeFalsy();
  });

  it('displays loading', async () => {
    const page = render(<MemoryRouter><PickupShifts dispatch={jest.fn()} loading /></MemoryRouter>);
    expect(page.getByTestId('loading')).toBeTruthy();
  });
});
