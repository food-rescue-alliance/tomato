import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { UserDetailsComponent } from '../../../../packs/volunteer_dashboard/domains/settings/UserDetails';

describe('UserDetails', () => {
  it('calls onEdit()', async () => {
    const onEdit = jest.fn();
    const user = {
      id: 1,
      fullName: 'Inigo Montoya',
      email: 'imontoya@gmail.com',
    };

    const details = render(<UserDetailsComponent
      onEdit={onEdit}
      user={user}
    />);

    const edit = details.getByText('global.edit');

    fireEvent.click(edit);
    expect(onEdit).toHaveBeenCalledTimes(1);
  });
});
