import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { UserSettingsFormComponent } from '../../../../packs/volunteer_dashboard/domains/settings/UserSettingsForm';
import * as actions from '../../../../packs/volunteer_dashboard/domains/global/actions';

jest.mock('../../../../packs/volunteer_dashboard/domains/global/actions');

describe('Settings', () => {
  it('displays user settings and handles updates', async () => {
    const dispatch = jest.fn();
    const onCancel = jest.fn();
    const onSuccess = jest.fn();
    const onError = jest.fn();
    const user = {
      id: 1,
      fullName: 'Inigo Montoya',
      email: 'imontoya@gmail.com',
      timeZone: 'America/Chicago',
    };
    const updatedUser = {
      id: 1,
      fullName: 'Inga',
      email: 'inga@gmail.com',
      phone: '5555555555',
      timeZone: 'America/New_York',
    };
    const errorUser = {
      id: 1,
      fullName: 'Inga',
      email: 'inga@gmail.com',
      phone: '5555555555',
      error: { phone: 'invalid phone number' },
    };
    const timeZones = {
      loading: false,
      values: [
        {
          id: 'America/New_York',
          name: 'Eastern Time (US & Canada)'
        },
        {
          id: 'America/Chicago',
          name: 'Central Time (US & Canada)'
        },
      ],
    };

    const form = render(<UserSettingsFormComponent
      dispatch={dispatch}
      onCancel={onCancel}
      onSuccess={onSuccess}
      onError={onError}
      user={user}
      timeZones={timeZones}
    />);

    const nameInput = form.getByLabelText('settings.fullName');
    const emailInput = form.getByLabelText('settings.email');
    const phoneInput = form.getByLabelText('settings.phone');
    const streetOneInput = form.getByLabelText('settings.streetOne');
    const streetTwoInput = form.getByLabelText('settings.streetTwo');
    const cityInput = form.getByLabelText('settings.city');
    const stateInput = form.getByLabelText('settings.state');
    const zipInput = form.getByLabelText('settings.zip');
    const timeZone = form.getByLabelText('settings.timeZone');

    expect(nameInput.value).toBe('Inigo Montoya');
    expect(emailInput.value).toBe('imontoya@gmail.com');
    expect(phoneInput.value).toBe('');
    expect(streetOneInput.value).toBe('');
    expect(streetTwoInput.value).toBe('');
    expect(cityInput.value).toBe('');
    expect(stateInput.value).toBe('');
    expect(zipInput.value).toBe('');
    expect(timeZone.innerHTML).toContain('Central Time');

    fireEvent.change(nameInput, { target: { value: 'Inga' } });
    fireEvent.change(emailInput, { target: { value: 'inga@gmail.com' } });
    fireEvent.change(phoneInput, { target: { value: '5555555555' } });
    fireEvent.change(streetOneInput, { target: { value: '123 Sesame st' } });
    fireEvent.change(streetTwoInput, { target: { value: 'Apt 1' } });
    fireEvent.change(cityInput, { target: { value: 'New York' } });
    fireEvent.change(stateInput, { target: { value: 'NY' } });
    fireEvent.change(zipInput, { target: { value: '12345' } });
    fireEvent.mouseDown(timeZone);
    fireEvent.click(form.getAllByRole('option')[0]);

    expect(nameInput.value).toBe('Inga');
    expect(emailInput.value).toBe('inga@gmail.com');
    expect(phoneInput.value).toBe('(555) 555-5555');
    expect(streetOneInput.value).toBe('123 Sesame st');
    expect(streetTwoInput.value).toBe('Apt 1');
    expect(cityInput.value).toBe('New York');
    expect(stateInput.value).toBe('NY');
    expect(zipInput.value).toBe('12345');
    expect(timeZone.innerHTML).toContain('Eastern Time')

    const cancel = form.getByText('global.cancel');
    const submit = form.getByText('global.submit');

    fireEvent.click(cancel);
    expect(onCancel).toHaveBeenCalledTimes(1);

    const mockUpdateUser = jest.fn(() => 'updateUser');
    actions.updateUser = mockUpdateUser;
    fireEvent.click(submit);
    expect(mockUpdateUser).toHaveBeenCalledTimes(1);
    expect(mockUpdateUser).toHaveBeenCalledWith('Inga', 'inga@gmail.com', '(555) 555-5555', {
      streetOne: '123 Sesame st', streetTwo: 'Apt 1', city: 'New York', state: 'NY', zip: '12345',
    }, 'America/New_York');
    expect(dispatch).toHaveBeenCalledTimes(1);
    expect(dispatch).toHaveBeenCalledWith('updateUser');

    expect(onSuccess).toHaveBeenCalledTimes(0);

    const { rerender } = form;
    rerender(<UserSettingsFormComponent
      dispatch={dispatch}
      onCancel={onCancel}
      onSuccess={onSuccess}
      onError={onError}
      user={updatedUser}
      timeZones={timeZones}
    />);

    expect(onSuccess).toHaveBeenCalledTimes(1);

    fireEvent.click(submit);
    rerender(<UserSettingsFormComponent
      dispatch={dispatch}
      onCancel={onCancel}
      onSuccess={onSuccess}
      onError={onError}
      user={errorUser}
      timeZones={timeZones}
    />);

    expect(onError).toHaveBeenCalled();
  });
});
