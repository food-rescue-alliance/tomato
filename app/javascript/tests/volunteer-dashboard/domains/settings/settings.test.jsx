/* eslint-disable react/prop-types */

import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { SettingsComponent } from '../../../../packs/volunteer_dashboard/domains/settings/Settings';
import { AbsenceStateProvider } from '../../../../packs/volunteer_dashboard/domains/absences/context';
import { SnackbarProvider } from 'notistack';

jest.mock('../../../../packs/volunteer_dashboard/domains/settings/UserDetails', () => ({
  __esModule: true,
  default: ({ onEdit }) => (
    <div>
      User Details Component
      <button onClick={() => onEdit()} type="button">Edit</button>
    </div>
  ),
}));

jest.mock('../../../../packs/volunteer_dashboard/domains/settings/UserSettingsForm', () => ({
  __esModule: true,
  default: ({ onCancel, onSuccess, onError }) => (
    <div>
      User Settings Form Component
      <button onClick={() => onCancel()} type="button">Cancel</button>
      <button onClick={() => onSuccess('Success message')} type="button">Success</button>
      <button onClick={() => onError('Error message')} type="button">Error</button>
    </div>
  ),
}));

describe('UserDetails', () => {
  it('displays alerts', async () => {
    const dispatch = jest.fn();

    const page = render(
      <AbsenceStateProvider>
        <SnackbarProvider>
          <SettingsComponent dispatch={dispatch} />
        </SnackbarProvider>
      </AbsenceStateProvider>
    );

    expect(page.getByText('User Details Component')).toBeTruthy();

    fireEvent.click(page.getByText('Edit'));

    expect(page.getByText('User Settings Form Component')).toBeTruthy();

    const success = page.getByText('Success');
    fireEvent.click(success);

    expect(page.getByText('User Details Component')).toBeTruthy();
    expect(page.getByText('Success message')).toBeTruthy();

    fireEvent.click(page.getByText('Edit'));

    expect(page.getByText('User Settings Form Component')).toBeTruthy();
    expect(page.queryByText('Success message')).toBeFalsy();

    const error = page.getByText('Error');
    fireEvent.click(error);

    expect(page.getByText('User Settings Form Component')).toBeTruthy();
    expect(page.getByText('Error message')).toBeTruthy();

    const cancel = page.getByText('Cancel');
    fireEvent.click(cancel);

    expect(page.getByText('User Details Component')).toBeTruthy();
    expect(page.queryByText('Error message')).toBeFalsy();

    fireEvent.click(page.getByText('Edit'));

    expect(page.getByText('User Settings Form Component')).toBeTruthy();
    expect(page.queryByText('Error message')).toBeFalsy();
  });
});
