import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { MemoryRouter } from 'react-router-dom';
import Shifts from '../../../../packs/volunteer_dashboard/domains/dashboard/Shifts';

describe('Shifts', () => {
  it('displays upcoming shifts', async () => {
    const dateTime = new Date().toISOString();
    const shifts = [
      {
        id: 'shift1',
        startsAt: dateTime,
        endsAt: dateTime,
        title: 'Shift 1',
        users: [{ id: 1 }],
        tasks: [],
      },
      {
        id: 'shift2',
        startsAt: dateTime,
        endsAt: dateTime,
        title: 'Shift 2',
        users: [{ id: 1 }],
        tasks: [],
      },
    ];
    const page = render(<MemoryRouter><Shifts loading={false} shifts={shifts} user={{timeZoneOffset: "-07:00"}} /></MemoryRouter>);
    expect(page.getAllByText('shiftDetails.logReport').length).toBeGreaterThan(0);
    expect(page.queryAllByText('shiftDetails.pickupButton').length).toBe(0);
  });

  it('displays pickup shifts', async () => {
    const dateTime = new Date().toISOString();
    const shifts = [
      {
        id: 'shift1',
        startsAt: dateTime,
        endsAt: dateTime,
        title: 'Shift 1',
        users: [],
        tasks: [],
      },
      {
        id: 'shift2',
        startsAt: dateTime,
        endsAt: dateTime,
        title: 'Shift 2',
        users: [],
        tasks: [],
      },
    ];
    const page = render(<MemoryRouter><Shifts loading={false} shifts={shifts} user={{timeZoneOffset: "-07:00"}} /></MemoryRouter>);
    expect(page.getAllByText('shiftDetails.pickupButton').length).toBeGreaterThan(0);
    expect(page.queryAllByText('shiftDetails.logReport').length).toBe(0);
  });

  it('displays loading', async () => {
    const page = render(<MemoryRouter><Shifts loading /></MemoryRouter>);
    expect(page.getByTestId('loading')).toBeTruthy();
  });
});
