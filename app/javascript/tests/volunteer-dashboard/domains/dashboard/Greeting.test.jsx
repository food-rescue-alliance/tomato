import { greeting } from '../../../../packs/volunteer_dashboard/domains/dashboard/Greeting';

describe('greeting()', () => {
  test('returns morning when time is before 10am', () => {
    const result = greeting(new Date('January 1, 2020 9:00'));
    expect(result).toEqual('dashboard.goodMorning');
  });

  test('returns afternoon when time is before 8pm', () => {
    const result = greeting(new Date('January 1, 2020 19:00'));
    expect(result).toEqual('dashboard.goodAfternoon');
  });

  test('returns evening when time is after 8pm', () => {
    const result = greeting(new Date('January 1, 2020 20:00'));
    expect(result).toEqual('dashboard.goodEvening');
  });
});
