import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { MemoryRouter } from 'react-router-dom';
import Alerts from '../../../../packs/volunteer_dashboard/domains/dashboard/Alerts';

describe('Alerts', () => {
  it('displays alerts', async () => {
    const alerts = [
      { message: 'alert message 1', occurrenceId: '1' },
      { message: 'alert message 2', occurrenceId: '2' },
    ];
    render(<MemoryRouter><Alerts loading={false} alerts={alerts} /></MemoryRouter>);
    expect(screen.getByText('alert message 1')).toBeTruthy();
    expect(screen.getByText('alert message 2')).toBeTruthy();
  });

  it('displays loading', async () => {
    render(<Alerts loading alerts={[]} />);
    expect(screen.getByTestId('loading')).toBeTruthy();
  });

  it('displays errors', async () => {
    render(<Alerts error loading={false} alerts={[]} />);
    expect(screen.getByTestId('error')).toBeTruthy();
  });
});
