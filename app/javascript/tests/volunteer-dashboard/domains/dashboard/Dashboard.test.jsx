import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { DashboardComponent } from '../../../../packs/volunteer_dashboard/domains/dashboard/Dashboard';
import * as actions from '../../../../packs/volunteer_dashboard/domains/dashboard/actions';

jest.mock('../../../../packs/volunteer_dashboard/domains/dashboard/actions');
jest.mock('../../../../packs/volunteer_dashboard/domains/dashboard/Greeting', () => ({
  __esModule: true,
  default: () => (
    <div>
      greeting component
    </div>
  ),
}));

describe('Dashboard', () => {
  const mockDispatch = jest.fn();
  actions.fetchShifts = () => 'fetchShifts';
  actions.fetchAlerts = () => 'fetchAlerts';

  it('loads shifts and alerts on mount', async () => {
    render(<DashboardComponent dispatch={mockDispatch} user={{ full_name: '' }} />);
    expect(mockDispatch).toHaveBeenCalledTimes(2);
    expect(mockDispatch).toHaveBeenCalledWith('fetchShifts');
    expect(mockDispatch).toHaveBeenCalledWith('fetchAlerts');
  });
});
