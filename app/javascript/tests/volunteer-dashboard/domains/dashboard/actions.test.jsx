import axios from '../../../../packs/volunteer_dashboard/axios';
import * as actions from '../../../../packs/volunteer_dashboard/domains/dashboard/actions';

jest.mock('../../../../packs/volunteer_dashboard/axios');

const dispatch = jest.fn();

describe('actions', () => {
  beforeEach(() => {
    dispatch.mockClear();
  });

  describe('fetchShifts', () => {
    describe('successful request', () => {
      it('sets the shifts correctly from the response', async () => {
        axios.get.mockResolvedValue({ data: [{ id: 1 }] });

        await actions.fetchShifts()(dispatch);

        expect(dispatch).toHaveBeenCalledTimes(2);
        expect(dispatch).toHaveBeenCalledWith({
          type: actions.SET_SHIFTS,
          data: { loading: true, shifts: [] },
        });
        expect(dispatch).toHaveBeenCalledWith({
          type: actions.SET_SHIFTS,
          data: { loading: false, shifts: [{ id: 1 }] },
        });
      });
    });

    describe('request for user is a failure', () => {
      it('dispatches an error action', async () => {
        axios.get.mockImplementationOnce(() => Promise.reject(new Error('Request Failed!')));

        await actions.fetchShifts()(dispatch);

        expect(dispatch).toHaveBeenCalledTimes(2);
        expect(dispatch).toHaveBeenCalledWith({
          type: actions.SET_SHIFTS,
          data: { loading: true, shifts: [] },
        });
        expect(dispatch).toHaveBeenCalledWith({ type: actions.ERROR, data: new Error('Request Failed!') });
      });
    });
  });

  describe('fetchAlerts', () => {
    describe('successful request', () => {
      it('sets the alerts correctly from the response', async () => {
        axios.get.mockResolvedValue({ data: [{ occurrenceId: 1, message: 'message 1' }] });

        await actions.fetchAlerts()(dispatch);

        expect(dispatch).toHaveBeenCalledTimes(2);
        expect(dispatch).toHaveBeenCalledWith({
          type: actions.SET_ALERTS,
          data: { loading: true },
        });
        expect(dispatch).toHaveBeenCalledWith({
          type: actions.SET_ALERTS,
          data: { loading: false, alerts: [{ occurrenceId: 1, message: 'message 1' }] },
        });
      });
    });

    describe('request for alerts is a failure', () => {
      it('dispatches an error action', async () => {
        const error = new Error();
        error.response = { data: 'Request Failed!' };
        axios.get.mockImplementationOnce(() => Promise.reject(error));

        await actions.fetchAlerts()(dispatch);

        expect(dispatch).toHaveBeenCalledTimes(2);
        expect(dispatch).toHaveBeenCalledWith({
          type: actions.SET_ALERTS,
          data: { loading: true },
        });
        expect(dispatch).toHaveBeenCalledWith({
          type: actions.SET_ALERTS,
          data: {
            error: 'Request Failed!',
            loading: false,
          },
        });
      });
    });
  });
});
