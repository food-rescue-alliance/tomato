export const parseDateInTz = (dateString) => {
  const date = new Date(dateString);
  return new Date(date.valueOf() + date.getTimezoneOffset() * 60 * 1000);
};
