import * as React from 'react';
import PropTypes from 'prop-types';
import applyMiddleware from './middleware';

const useProvider = (
  StateContext,
  DispatchContext,
  reducer,
  defaultState,
) => {
  const ReturnedProvider = ({ children }) => {
    const [state, dispatch] = React.useReducer(reducer, defaultState);
    const enhancedDispatch = applyMiddleware(dispatch);
    return (
      <StateContext.Provider value={state}>
        <DispatchContext.Provider value={enhancedDispatch}>
          {children}
        </DispatchContext.Provider>
      </StateContext.Provider>
    );
  };

  ReturnedProvider.propTypes = {
    children: PropTypes.node.isRequired,
  };

  const useState = () => {
    const context = React.useContext(StateContext);
    if (context === undefined) {
      throw new Error('useState must be used within an appropriate Provider');
    }
    return context;
  };

  const useDispatch = () => {
    const context = React.useContext(DispatchContext);
    if (context === undefined) {
      throw new Error('useDispatch must be used within an appropriate Provider');
    }
    return context;
  };

  const connect = (Component) => (props) => {
    const connectState = useState();
    const connectDispatch = useDispatch();

    // eslint-disable-next-line react/jsx-props-no-spreading
    return (<Component {...props} {...connectState} dispatch={connectDispatch} />);
  };

  return [ReturnedProvider, connect];
};

export default useProvider;
