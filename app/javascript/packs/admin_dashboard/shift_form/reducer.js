import moment from 'moment-timezone';
import { rrule } from '../rrule';
import * as types from './actions';

export const FrequencyTypes = {
  daily: 'DAILY',
  weekly: 'WEEKLY',
  monthly: 'MONTHLY',
  yearly: 'YEARLY',
  weekday: 'weekday',
};

export const SubmitTypes = {
  basic: 'basic',
  customDaily: 'customDaily',
  customWeekly: 'customWeekly',
  customMonthlyWeek: 'customMonthlyWeek',
  customMonthlyDate: 'customMonthlyDate',
};

export const RepeatTypes = {
  none: 'none',
  forever: 'forever',
  endsOn: 'endsOn',
  endsAfter: 'endsAfter',
};

export const WeekdaySort = {
  SU: 1, MO: 2, TU: 3, WE: 4, TH: 5, FR: 6, SA: 7,
};

export const defaultState = {
  loading: true,
  frequency: FrequencyTypes.daily,
  submitType: SubmitTypes.basic,
  repeatType: RepeatTypes.none,
  startDate: moment().format('YYYY-MM-DD'),
  startTime: moment().format('HH:mm'),
  endTime: moment().format('HH:mm'),
  customText: 'Custom...',
  rrule: '',
  interval: 1,
  byDays: '',
  byMonthDays: '',
  bySetPos: '',
  endsOn: '',
  endsAfter: 1,
  tasks: [],
};

export const reducer = (state, action) => {
  switch (action.type) {
    case types.LOADING: {
      return { ...state, loading: action.data };
    }
    case types.ERROR: {
      return { ...state, error: action.data };
    }
    case types.SET_DATE: {
      return { ...state, startDate: action.data };
    }
    case types.SET_START: {
      return { ...state, startTime: action.data };
    }
    case types.SET_END: {
      return { ...state, endTime: action.data };
    }
    case types.SET_RRULE: {
      return { ...state, rrule: action.data };
    }
    case types.SET_CUSTOM_TEXT: {
      return { ...state, customText: action.data };
    }
    case types.INITIALIZE_FORM: {
      return { ...state, ...action.data };
    }
    case types.CUSTOM_FREQUENCY: {
      const data = {
        ...action.data,
      };
      switch (action.data.frequency) {
        case FrequencyTypes.daily:
          data.submitType = SubmitTypes.customDaily;
          break;
        case FrequencyTypes.weekly:
          data.submitType = SubmitTypes.customWeekly;
          break;
        case FrequencyTypes.monthly:
          if (action.data.customMonthType === 'on') {
            data.submitType = SubmitTypes.customMonthlyWeek;
          } else {
            data.submitType = SubmitTypes.customMonthlyDate;
          }
          break;
        default:
          break;
      }
      return { ...state, ...data };
    }
    case types.UPDATE_BASIC_FORM: {
      const data = {
        submitType: SubmitTypes.basic,
        ...action.data,
      };
      return { ...state, ...data };
    }
    case types.CALCULATE_RRULE: {
      if (state.repeatType === RepeatTypes.none) {
        return { ...state, rrule: '' };
      }
      const rruleParams = { frequency: state.frequency, startDate: state.startDate };
      switch (state.submitType) {
        case SubmitTypes.customDaily:
          rruleParams.interval = state.interval;
          break;
        case SubmitTypes.customWeekly:
          rruleParams.interval = state.interval;
          rruleParams.byDays = state.byDays;
          break;
        case SubmitTypes.customMonthlyWeek:
          rruleParams.interval = state.interval;
          rruleParams.byDays = state.byDays;
          rruleParams.bySetPos = state.bySetPos;
          break;
        case SubmitTypes.customMonthlyDate:
          rruleParams.interval = state.interval;
          rruleParams.byMonthDays = state.byMonthDays;
          break;
        default:
          break;
      }
      switch (state.repeatType) {
        case RepeatTypes.endsOn:
          rruleParams.until = state.endsOn;
          break;
        case RepeatTypes.endsAfter:
          rruleParams.count = state.endsAfter;
          break;
        default:
          break;
      }
      const rruleString = rrule(rruleParams);
      return { ...state, rrule: rruleString };
    }
    case types.FORM_SUCCESS: {
      // TODO replace with react-navigation
      window.location.replace(action.data.data.link);
      return { ...defaultState };
    }
    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
};
