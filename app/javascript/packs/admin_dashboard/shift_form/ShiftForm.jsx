import React, { useState, useEffect } from 'react';
import { adaptV4Theme } from '@mui/material/styles';
import {
  Button,
  Grid,
  ThemeProvider,
  StyledEngineProvider,
  createTheme,
  MenuItem,
  InputAdornment,
  IconButton,
  TextField as MuiTextField,
} from '@mui/material';
import PropTypes from 'prop-types';
import { withStyles } from '@mui/styles';
import { Cancel } from '@mui/icons-material';
import TextField from '../../volunteer_dashboard/domains/global/TextField';
import theme from '../../volunteer_dashboard/domains/global/theme.json';
import * as actions from './actions';
import { connectShift } from './context';
import { FrequencyForm } from './FrequencyForm';

const StyledInput = withStyles({
  root: {
    background: 'transparent',
    '& input': { background: 'white' },
  },
})(TextField);

export const ShiftForm = ({
  id,
  startDate,
  startTime,
  endTime,
  rrule,
  customText,
  tasks,
  dispatch,
}) => {
  useEffect(() => {
    dispatch(actions.fetchOrgAndInitForm(id));
  }, [id]);
  const [selectedTasks, setSelectedTasks] = useState([]);
  const [name, setName] = useState('');
  const onCancel = () => {
    // TODO replace with react-navigation
    window.history.back();
  };
  const onFormSubmit = (e) => {
    e.preventDefault();
    const data = {
      id,
      name,
      shiftDate: startDate,
      startTime,
      endTime,
      rrule,
      task_ids: selectedTasks.map((t) => t.id),
    };
    dispatch(actions.submitFormData(data));
  };

  return (
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={createTheme(adaptV4Theme(theme))}>
        <form aria-label="form" onSubmit={onFormSubmit}>
          <Grid container spacing={3}>
            <Grid item xs={8}>
              <StyledInput
                error={false}
                id="name"
                label="Shift Name"
                variant="outlined"
                required
                helperText="*Required"
                value={name}
                onChange={setName}
              />
            </Grid>
            <Grid item xs={4} />
            <FrequencyForm
              rrule={rrule}
              dispatch={dispatch}
              customText={customText}
              endTime={endTime}
              startTime={startTime}
              startDate={startDate}
            />
            {selectedTasks.map((task) => (
              <React.Fragment key={task.id}>
                <Grid item xs={8}>
                  <MuiTextField
                    id={task.description}
                    variant="outlined"
                    value={task.description}
                    label="Task"
                    fullWidth
                    InputProps={{
                      readOnly: true,
                      endAdornment: (
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="remove task"
                            edge="end"
                            onClick={() => {
                              const ind = selectedTasks.findIndex((t) => t.id === task.id);
                              selectedTasks.splice(ind, 1);
                              setSelectedTasks([...selectedTasks]);
                            }}
                            size="large">
                            <Cancel />
                          </IconButton>
                        </InputAdornment>
                      ),
                    }}
                  />
                </Grid>
                <Grid item xs={4} />
              </React.Fragment>
            ))}
            <Grid item xs={8}>
              <TextField
                id="addTask"
                variant="outlined"
                value="Add a task"
                onChange={(v) => setSelectedTasks([...selectedTasks, v])}
                select
              >
                <MenuItem value="Add a task">Add a task</MenuItem>
                {
                  tasks.filter((task) => !selectedTasks.includes(task))
                    .map((task) => <MenuItem key={task.id} value={task}>{task.description}</MenuItem>)
                }
              </TextField>
            </Grid>
            <Grid item xs={4} />
            <Grid item xs={4}>
              <Button
                type="submit"
                variant="contained"
                color="primary"
                disableElevation
                disabled={!name}
              >
                Submit
              </Button>
              <Button
                onClick={onCancel}
                disableElevation
                style={{ backgroundColor: 'transparent', marginLeft: '0.5em' }}
              >
                Cancel
              </Button>
            </Grid>
            <Grid item xs={8} />
          </Grid>
        </form>
      </ThemeProvider>
    </StyledEngineProvider>
  );
};

ShiftForm.propTypes = {
  id: PropTypes.string.isRequired,
  dispatch: PropTypes.func.isRequired,
  startDate: PropTypes.string.isRequired,
  startTime: PropTypes.string.isRequired,
  endTime: PropTypes.string.isRequired,
  rrule: PropTypes.string.isRequired,
  customText: PropTypes.string.isRequired,
  tasks: PropTypes.instanceOf(Array),
};

ShiftForm.defaultProps = {
  tasks: [],
};

export default connectShift(ShiftForm);
