import PropTypes from 'prop-types';
import { adaptV4Theme } from '@mui/material/styles';
import React from 'react';
import { createTheme, Grid, ThemeProvider, StyledEngineProvider } from '@mui/material';
import { ShiftStateProvider } from './context';
import FrequencyForm from './FrequencyForm';
import theme from '../../volunteer_dashboard/domains/global/theme.json';

const FrequencyFormComponent = ({
  date,
  start,
  end,
  rrule,
}) => (
  <ShiftStateProvider>
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={createTheme(adaptV4Theme(theme))}>
        <Grid container spacing={3}>
          <FrequencyForm
            startingValues={{
              date, start, end, rrule,
            }}
            hiddenFields
          />
        </Grid>
      </ThemeProvider>
    </StyledEngineProvider>
  </ShiftStateProvider>
);

FrequencyFormComponent.propTypes = {
  date: PropTypes.string.isRequired,
  start: PropTypes.string.isRequired,
  end: PropTypes.string.isRequired,
  rrule: PropTypes.string.isRequired,
};

export default FrequencyFormComponent;
