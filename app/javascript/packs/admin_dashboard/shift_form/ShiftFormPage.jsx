import React from 'react';
import PropTypes from 'prop-types';
import { ShiftStateProvider } from './context';
import ShiftForm from './ShiftForm';

const ShiftFormPage = ({ organization }) => (
  <ShiftStateProvider>
    <ShiftForm id={organization} />
  </ShiftStateProvider>
);

ShiftFormPage.propTypes = {
  organization: PropTypes.string.isRequired,
};

export default ShiftFormPage;
