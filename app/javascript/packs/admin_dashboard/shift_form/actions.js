import moment from 'moment-timezone';
import axios from '../../volunteer_dashboard/axios';

// ACTIONS
export const LOADING = 'LOADING';
export const INITIALIZE_FORM = 'INITIALIZE_FORM';
export const UPDATE_BASIC_FORM = 'UPDATE_BASIC_FORM';
export const CUSTOM_FREQUENCY = 'CUSTOM_FREQUENCY';
export const SET_DATE = 'SET_DATE';
export const SET_START = 'SET_START';
export const SET_END = 'SET_END';
export const SET_RRULE = 'SET_RRULE';
export const SET_CUSTOM_TEXT = 'SET_CUSTOM_TEXT';
export const CALCULATE_RRULE = 'CALCULATE_RRULE';
export const FORM_SUCCESS = 'FORM_SUCCESS';
export const ERROR = 'ERROR';

// ACTION CREATORS
export const setLoading = (loading) => ({ type: LOADING, data: loading });
export const setCustomText = (text) => ({ type: SET_CUSTOM_TEXT, data: text });
export const setDate = (date) => ({ type: SET_DATE, data: date });
export const setStart = (start) => ({ type: SET_START, data: start });
export const setEnd = (end) => ({ type: SET_END, data: end });
export const setRrule = (rrule) => ({ type: SET_RRULE, data: rrule });
export const updateBasicForm = (data) => ({ type: UPDATE_BASIC_FORM, data });
export const customFrequency = (data) => ({ type: CUSTOM_FREQUENCY, data });
export const calculateRrule = () => ({ type: CALCULATE_RRULE });
const initializeForm = (shift) => ({
  type: INITIALIZE_FORM,
  data: shift,
});
const formSuccess = (shift) => ({
  type: FORM_SUCCESS,
  data: shift,
});
const setError = (err) => ({ type: ERROR, data: err });

// HELPERS
export const calculateStartTime = (offset) => {
  const t = moment().tz(offset);
  if (t.minutes() <= 30) {
    t.set('minute', 30);
  } else {
    t.set('minute', 0);
    t.add(1, 'hour');
  }
  return t;
};

// ASYNC
export const fetchOrgAndInitForm = (id) => async (dispatch) => {
  dispatch(setLoading(true));
  try {
    const orgResponse = await axios.get(`/api/organizations/${id}/shifts/new.json`);
    const start = calculateStartTime(orgResponse.data.data.organization.timeZone);
    const tasks = orgResponse.data.data.organization.sites.reduce((taskList, site) => {
      site.tasks.forEach((task) => taskList.push(task));
      return taskList;
    }, []);
    dispatch(initializeForm({
      startTime: start.format('HH:mm'),
      endTime: moment(start).add(2, 'hour').format('HH:mm'),
      tasks,
    }));
  } catch (err) {
    dispatch(setError('shiftForm.initializeFormError'));
  }
  dispatch(setLoading(false));
};

export const fetchCustomText = (rrule, shiftDate) => async (dispatch) => {
  try {
    const resp = await axios.get('/rrule.json', { params: { rrule, starts_at: shiftDate } });
    dispatch(setCustomText(resp.data.sentence));
  } catch (err) {
    dispatch(setError('shiftForm.fetchCustomTextError'));
  }
};

export const submitFormData = ({
  id,
  name,
  shiftDate,
  startTime,
  endTime,
  rrule,
  // eslint-disable-next-line camelcase
  task_ids,
}) => async (dispatch) => {
  dispatch(setLoading(true));
  try {
    const shift = {
      name,
      shift_events_attributes: {
        0: {
          event_attributes: {
            starts_at: `${shiftDate} ${startTime}`,
            ends_at: `${shiftDate} ${endTime}`,
            rrule,
          },
          task_ids,
        },
      },
    };
    const postResp = await axios.post(`/api/organizations/${id}/shifts.json`, { shift });
    dispatch(formSuccess(postResp.data));
  } catch (err) {
    dispatch(setError('shiftForm.submitFormError'));
  }
  dispatch(setLoading(false));
};
