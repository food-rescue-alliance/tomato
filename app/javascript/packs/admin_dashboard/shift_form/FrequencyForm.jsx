import React, { useEffect, useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import {
  Button, Dialog,
  DialogActions,
  DialogContent,
  DialogTitle, FormControlLabel,
  FormLabel,
  Grid,
  MenuItem, Radio,
  RadioGroup,
} from '@mui/material';
import { withStyles } from '@mui/styles';
import moment from 'moment-timezone';
import TextField from '../../volunteer_dashboard/domains/global/TextField';
import { FrequencyTypes, RepeatTypes, WeekdaySort } from './reducer';
import * as actions from './actions';
import { getDayOfWeek, getDayOfYearFormatted, getOrdinalWeekOfMonth } from '../rrule';
import { connectShift } from './context';

const basicFreqOptions = {
  none: 'none',
  custom: 'custom',
  ...FrequencyTypes,
};
const customMonthOptions = {
  on: 'on the',
  each: 'each',
};
const customMonthOnIntervalOptions = {
  first: 1,
  second: 2,
  third: 3,
  fourth: 4,
  fifth: 5,
  last: -1,
};
const StyledRadio = withStyles({
  root: {
    '&$checked': {
      color: '#274653',
    },
    marginLeft: '8px',
  },
  checked: {},
})(Radio);
const StyledDialog = withStyles({
  paper: {
    padding: 20,
  },
})(Dialog);
export const FrequencyForm = ({
  startDate,
  startTime,
  endTime,
  rrule,
  customText,
  dispatch,
  hiddenFields,
  startingValues,
}) => {
  const [showDialog, setShowDialog] = useState(false);
  const [basicFrequency, setBasicFrequency] = useState(startingValues?.rrule
    ? basicFreqOptions.custom : basicFreqOptions.none);
  const [basicEnds, setBasicEnds] = useState(RepeatTypes.forever);
  const [endsOn, setEndsOn] = useState(moment().format('YYYY-MM-DD'));
  const [endsAfter, setEndsAfter] = useState(0);
  const [customFrequency, setCustomFrequency] = useState(FrequencyTypes.daily);
  const [customInterval, setCustomInterval] = useState(1);
  const [monthlyType, setMonthlyType] = useState(customMonthOptions.on);
  const [monthInterval, setMonthInterval] = useState(customMonthOnIntervalOptions.first);
  const [weekByDays, setWeekByDays] = useState([]);
  const [monthOnThe, setMonthOnThe] = useState('SU,MO,TU,WE,TH,FR,SA');
  const [monthDays, setMonthDays] = useState([]);
  const [customEnds, setCustomEnds] = useState(RepeatTypes.forever);
  const [customEndsOn, setCustomEndsOn] = useState(moment().format('YYYY-MM-DD'));
  const [customEndsAfter, setCustomEndsAfter] = useState(0);
  const showRepeat = basicFrequency !== basicFreqOptions.none
    && basicFrequency !== basicFreqOptions.custom;
  useEffect(() => {
    if (!startingValues) {
      return;
    }
    dispatch(actions.setDate(startingValues.date));
    dispatch(actions.setStart(startingValues.start));
    dispatch(actions.setEnd(startingValues.end));
    dispatch(actions.setRrule(startingValues.rrule));
  }, []);
  useEffect(() => {
    if (basicFrequency === basicFreqOptions.custom) {
      return;
    }
    const data = {
      frequency: basicFrequency,
      repeatType: basicFrequency === basicFreqOptions.none ? RepeatTypes.none : basicEnds,
      endsOn,
      endsAfter,
    };
    dispatch(actions.updateBasicForm(data));
    dispatch(actions.calculateRrule());
  }, [basicFrequency, basicEnds, endsOn, endsAfter]);
  useEffect(() => {
    if (rrule && basicFrequency === basicFreqOptions.custom) {
      dispatch(actions.fetchCustomText(rrule));
    } else {
      dispatch(actions.setCustomText('Custom...'));
    }
  }, [rrule]);
  useEffect(() => {
    if (!hiddenFields) {
      return;
    }
    document.getElementById('hidden-date').value = startDate;
    document.getElementById('hidden-start').value = startTime;
    document.getElementById('hidden-end').value = endTime;
    document.getElementById('hidden-rrule').value = rrule;
  }, [startDate, startTime, endTime, rrule]);
  const submitCustom = () => {
    const byDays = customFrequency === FrequencyTypes.monthly
      ? monthOnThe
      : weekByDays.sort((a, b) => WeekdaySort[a] - WeekdaySort[b]).join(',');
    const byMonthDays = monthDays.sort((a, b) => a - b);
    const data = {
      customMonthType: monthlyType,
      frequency: customFrequency,
      repeatType: customEnds,
      interval: customInterval,
      bySetPos: monthInterval,
      byDays,
      byMonthDays,
      endsOn: customEndsOn,
      endsAfter: customEndsAfter,
    };
    dispatch(actions.customFrequency(data));
    dispatch(actions.calculateRrule());
    setShowDialog(false);
  };
  const cancelCustom = () => {
    setShowDialog(false);
  };
  const openCustom = () => {
    setShowDialog(true);
  };
  const dateChange = (v) => {
    dispatch(actions.setDate(v));
    if (basicFrequency !== basicFreqOptions.custom) {
      dispatch(actions.calculateRrule());
    }
  };
  const handleWeeklyChecks = (val) => {
    const index = weekByDays.indexOf(val);
    if (index > -1) {
      weekByDays.splice(index, 1);
    } else {
      weekByDays.push(val);
    }
    setWeekByDays([...weekByDays]);
  };
  const handleMonthlyChecks = (val) => {
    const index = monthDays.indexOf(val);
    if (index > -1) {
      monthDays.splice(index, 1);
    } else {
      monthDays.push(val);
    }
    setMonthDays([...monthDays]);
  };
  const monthRows = useMemo(() => {
    const arr = [];
    for (let i = 1; i < 32; i += 1) {
      arr.push(
        <label htmlFor={`${i}`} className="picker__container" key={i}>
          <input
            id={`${i}`}
            name={`${i}`}
            type="checkbox"
            value={i}
            onChange={() => handleMonthlyChecks(`${i}`)}
            checked={monthDays.includes(`${i}`)}
          />
          <span className="picker__indicator">{i}</span>
        </label>,
      );
    }
    return arr;
  }, [monthDays]);
  const weekRows = useMemo(() => {
    const arr = [];
    ['SU', 'MO', 'TU', 'WE', 'TH', 'FR', 'SA'].forEach((val) => {
      arr.push(
        <label htmlFor={val} className="picker__container" key={val}>
          <input
            id={val}
            name={val}
            type="checkbox"
            value={val}
            onChange={() => handleWeeklyChecks(val)}
            checked={weekByDays.includes(val)}
          />
          <span className="picker__indicator">{val.charAt(0)}</span>
        </label>,
      );
    });
    return arr;
  }, [weekByDays]);
  return (
    <>
      <StyledDialog open={showDialog} onClose={cancelCustom} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Custom Recurrence</DialogTitle>
        <DialogContent>
          <Grid container spacing={2}>
            <Grid item xs={3} style={{ alignSelf: 'center' }}>
              Repeat every
            </Grid>
            <Grid item xs={3}>
              <TextField
                id="interval"
                type="number"
                variant="outlined"
                value={customInterval}
                onChange={setCustomInterval}
              />
            </Grid>
            <Grid item xs={4}>
              <TextField
                id="customFrequency"
                variant="outlined"
                value={customFrequency}
                onChange={setCustomFrequency}
                select
              >
                <MenuItem value={FrequencyTypes.daily}>day(s)</MenuItem>
                <MenuItem value={FrequencyTypes.weekly}>week(s)</MenuItem>
                <MenuItem value={FrequencyTypes.monthly}>month(s)</MenuItem>
              </TextField>
            </Grid>
            <Grid item xs={1} />
            {
              customFrequency === FrequencyTypes.weekly && (
                <>
                  <Grid item xs={12} style={{ alignSelf: 'center' }}>
                    Repeat on
                  </Grid>
                  <Grid item xs={1} />
                  <Grid item xs={11} className="picker picker__week">
                    {weekRows}
                  </Grid>
                </>
              )
            }
            {
              customFrequency === FrequencyTypes.monthly && (
                <>
                  <Grid item xs={1} />
                  <Grid item xs={2} style={{ alignSelf: 'center' }}>
                    Repeat
                  </Grid>
                  <Grid item xs={4}>
                    <TextField
                      id="monthlyType"
                      variant="outlined"
                      value={monthlyType}
                      onChange={setMonthlyType}
                      select
                    >
                      <MenuItem value={customMonthOptions.on}>{customMonthOptions.on}</MenuItem>
                      <MenuItem value={customMonthOptions.each}>{customMonthOptions.each}</MenuItem>
                    </TextField>
                  </Grid>
                  <Grid item xs={5} />
                  {
                    monthlyType === customMonthOptions.on
                      ? (
                        <>
                          <Grid item xs={3}>
                            <TextField
                              id="monthOnInterval"
                              variant="outlined"
                              value={monthInterval}
                              onChange={setMonthInterval}
                              select
                            >
                              <MenuItem
                                value={customMonthOnIntervalOptions.first}
                              >
                                first
                              </MenuItem>
                              <MenuItem
                                value={customMonthOnIntervalOptions.second}
                              >
                                second
                              </MenuItem>
                              <MenuItem
                                value={customMonthOnIntervalOptions.third}
                              >
                                third
                              </MenuItem>
                              <MenuItem
                                value={customMonthOnIntervalOptions.fourth}
                              >
                                fourth
                              </MenuItem>
                              <MenuItem
                                value={customMonthOnIntervalOptions.fifth}
                              >
                                fifth
                              </MenuItem>
                              <MenuItem
                                value={customMonthOnIntervalOptions.last}
                              >
                                last
                              </MenuItem>
                            </TextField>
                          </Grid>
                          <Grid item xs={1} />
                          <Grid item xs={4}>
                            <TextField
                              id="byDays"
                              variant="outlined"
                              value={monthOnThe}
                              onChange={setMonthOnThe}
                              select
                            >
                              <MenuItem value="SU,MO,TU,WE,TH,FR,SA">day</MenuItem>
                              <MenuItem value="MO,TU,WE,TH,FR">weekday</MenuItem>
                              <MenuItem value="SU,SA">weekend day</MenuItem>
                              <MenuItem value="SU">Sunday</MenuItem>
                              <MenuItem value="MO">Monday</MenuItem>
                              <MenuItem value="TU">Tuesday</MenuItem>
                              <MenuItem value="WE">Wednesday</MenuItem>
                              <MenuItem value="TH">Thursday</MenuItem>
                              <MenuItem value="FR">Friday</MenuItem>
                              <MenuItem value="SA">Saturday</MenuItem>
                            </TextField>
                          </Grid>
                          <Grid item xs={4} />
                        </>
                      )
                      : (
                        <>
                          <Grid item xs={2} />
                          <div className="picker picker__month">
                            {monthRows}
                          </div>
                        </>
                      )
                  }
                </>
              )
            }
            <Grid item xs={12}>
              <FormLabel style={{ color: '#274653' }} component="legend">Ends</FormLabel>
            </Grid>
            <RadioGroup aria-label="ends" name="ends1" value={customEnds} onChange={(_, v) => setCustomEnds(v)}>
              <FormControlLabel value="forever" control={<StyledRadio />} label="Never" />
              <Grid container spacing={2}>
                <Grid item xs={12} />
                <Grid item xs={4}>
                  <FormControlLabel value={RepeatTypes.endsOn} control={<StyledRadio />} label="On" />
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    id="endsOn"
                    type="date"
                    variant="outlined"
                    value={customEndsOn}
                    onChange={setCustomEndsOn}
                    disabled={customEnds !== RepeatTypes.endsOn}
                  />
                </Grid>
              </Grid>
              <Grid container spacing={2}>
                <Grid item xs={12} />
                <Grid item xs={4}>
                  <FormControlLabel value={RepeatTypes.endsAfter} control={<StyledRadio />} label="After" />
                </Grid>
                <Grid item xs={3}>
                  <TextField
                    id="endsAfter"
                    type="number"
                    variant="outlined"
                    value={customEndsAfter}
                    onChange={setCustomEndsAfter}
                    disabled={customEnds !== RepeatTypes.endsAfter}
                  />
                </Grid>
                <Grid item xs={3} style={{ alignSelf: 'center' }}>
                  occurrence(s)
                </Grid>
              </Grid>
            </RadioGroup>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Grid container spacing={2}>
            <Grid item xs={12} />
            <Grid item xs={8}>
              <Button
                onClick={submitCustom}
                variant="contained"
                color="primary"
                disableElevation
                style={{ marginLeft: '15px' }}
              >
                Submit
              </Button>
              <Button
                onClick={cancelCustom}
                disableElevation
                style={{ backgroundColor: 'transparent', marginLeft: '0.5em' }}
              >
                Cancel
              </Button>
            </Grid>
            <Grid item xs={4} />
            <Grid item xs={12} />
          </Grid>
        </DialogActions>
      </StyledDialog>
      <Grid item xs={3}>
        <TextField
          id="date"
          label="Date"
          type="date"
          variant="outlined"
          value={startDate}
          onChange={dateChange}
        />
      </Grid>
      <Grid item xs={2}>
        <TextField
          id="start"
          variant="outlined"
          type="time"
          value={startTime}
          onChange={(v) => dispatch(actions.setStart(v))}
        />
      </Grid>
      <Grid item xs={1} style={{ textAlign: 'center', alignSelf: 'center' }}>
        to
      </Grid>
      <Grid item xs={2}>
        <TextField
          id="end"
          variant="outlined"
          type="time"
          value={endTime}
          onChange={(v) => dispatch(actions.setEnd(v))}
        />
      </Grid>
      <Grid item xs={4} />
      <Grid item xs={8}>
        <TextField
          id="mdc-select--rrule-frequency-select"
          label="Frequency"
          variant="outlined"
          value={basicFrequency}
          onChange={setBasicFrequency}
          select
        >
          <MenuItem className="mdc-list-item" value={basicFreqOptions.none}>Does not repeat</MenuItem>
          <MenuItem className="mdc-list-item" value={basicFreqOptions.daily}>Daily</MenuItem>
          <MenuItem className="mdc-list-item" value={basicFreqOptions.weekly}>{`Weekly on ${getDayOfWeek(startDate)}`}</MenuItem>
          <MenuItem className="mdc-list-item" value={basicFreqOptions.monthly}>{`Monthly on the ${getOrdinalWeekOfMonth(startDate)} ${getDayOfWeek(startDate)}`}</MenuItem>
          <MenuItem className="mdc-list-item" value={basicFreqOptions.yearly}>{`Annually on ${getDayOfYearFormatted(startDate)}`}</MenuItem>
          <MenuItem className="mdc-list-item" value={basicFreqOptions.weekday}>Every Weekday (Monday to Friday)</MenuItem>
          <MenuItem onClick={openCustom} className="mdc-list-item" value={basicFreqOptions.custom}>{customText}</MenuItem>
        </TextField>
      </Grid>
      <Grid item xs={4} />
      {
          showRepeat && (
            <>
              <Grid item xs={8}>
                <TextField
                  id="until"
                  label="Until"
                  variant="outlined"
                  value={basicEnds}
                  onChange={setBasicEnds}
                  select
                >
                  <MenuItem value={RepeatTypes.forever}>Repeats forever</MenuItem>
                  <MenuItem value={RepeatTypes.endsOn}>Until date</MenuItem>
                  <MenuItem value={RepeatTypes.endsAfter}># of shifts</MenuItem>
                </TextField>
              </Grid>
              <Grid item xs={4} />
            </>
          )
        }
      {
          (basicEnds !== RepeatTypes.none && basicEnds !== RepeatTypes.forever) && (
            <>
              <Grid item xs={8}>
                {
                  (showRepeat && basicEnds === RepeatTypes.endsOn) && (
                    <TextField
                      id="until"
                      label="Date"
                      type="date"
                      variant="outlined"
                      value={endsOn}
                      onChange={setEndsOn}
                    />
                  )
                }
                {
                  (showRepeat && basicEnds === RepeatTypes.endsAfter) && (
                    <TextField
                      id="count"
                      label="Number"
                      type="number"
                      variant="outlined"
                      value={endsAfter}
                      onChange={setEndsAfter}
                    />
                  )
                }
              </Grid>
              <Grid item xs={4} />
            </>
          )
        }
    </>
  );
};

FrequencyForm.propTypes = {
  dispatch: PropTypes.func.isRequired,
  startDate: PropTypes.string.isRequired,
  startTime: PropTypes.string.isRequired,
  endTime: PropTypes.string.isRequired,
  rrule: PropTypes.string.isRequired,
  customText: PropTypes.string.isRequired,
  hiddenFields: PropTypes.bool,
  startingValues: PropTypes.shape({
    date: PropTypes.string,
    start: PropTypes.string,
    end: PropTypes.string,
    rrule: PropTypes.string,
  }),
};

FrequencyForm.defaultProps = {
  hiddenFields: false,
  startingValues: null,
};

export default connectShift(FrequencyForm);
