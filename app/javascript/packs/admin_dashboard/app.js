import React from 'react';
import { createRoot } from 'react-dom/client';
import { isNull } from 'lodash';
import ShiftFormPage from './shift_form/ShiftFormPage';
import FrequencyFormComponent from './shift_form/FrequencyFormComponent';

const e = React.createElement;

document.addEventListener('DOMContentLoaded', () => {
  const domContainer = document.querySelector('#shift-form-react');
  if (isNull(domContainer)) { return }
  const organization = domContainer.dataset.organizationId;
  createRoot(domContainer).render(e(ShiftFormPage, { organization }))
});

document.addEventListener('DOMContentLoaded', () => {
  const domContainer = document.querySelector('#frequency-form-react');
  if (isNull(domContainer)) { return }
  const organization = domContainer.dataset.organizationId;
  const {
    date, start, end, rrule,
  } = domContainer.dataset;
  createRoot(domContainer).render(e(FrequencyFormComponent, {
    organization, date, start, end, rrule,
  }))
});
