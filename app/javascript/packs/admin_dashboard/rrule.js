import _ from 'lodash';

const Frequency = {
  daily: 'DAILY',
  weekly: 'WEEKLY',
  monthly: 'MONTHLY',
  yearly: 'YEARLY',
  weekday: 'weekday',
};

const parseDate = (value) => {
  const [year, month, day] = value.split('-')
    .map((v) => parseInt(v, 10));

  return new Date(year, month - 1, day); // months are 0-indexed
};

const dayOfWeek = (date) => {
  const daysOfWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  const weekDay = date.getDay();
  return daysOfWeek[weekDay];
};

const weekOfMonth = (date) => (date.getDate() % 7 === 0
  ? date.getDate() / 7 : Math.floor(date.getDate() / 7) + 1);

const nth = function (num) {
  if (num > 3 && num < 21) return 'th';
  switch (num % 10) {
    case 1: return 'st';
    case 2: return 'nd';
    case 3: return 'rd';
    default: return 'th';
  }
};

const dayCode = (day) => day.toUpperCase().substring(0, 2);
const monthNumFrom1 = (date) => date.getMonth() + 1;
const formatDayOfYear = (date) => `${date.toLocaleDateString('en-us', { month: 'long', day: 'numeric' })}${nth(date.getDate())}`;

const ordinalize = (digit) => digit + nth(digit);

const getDayOfWeek = _.flow(
  parseDate,
  dayOfWeek,
);

const getDayCode = _.flow(
  getDayOfWeek,
  dayCode,
);

const getDayOfYearFormatted = _.flow(
  parseDate,
  formatDayOfYear,
);

const getWeekOfMonth = _.flow(
  parseDate,
  weekOfMonth,
);

const getMonth = _.flow(
  parseDate,
  monthNumFrom1,
);

const getOrdinalWeekOfMonth = _.flow(
  getWeekOfMonth,
  ordinalize,
);

const rrule = ({
  frequency, startDate, count, until, byDays, byMonthDays, bySetPos, interval = 1,
}) => {
  let result = [`FREQ=${frequency}`];
  const byDay = byDays || getDayCode(startDate);
  const realBySetPos = bySetPos || getWeekOfMonth(startDate);
  const useMonthlyDay = !!byMonthDays;
  const byMonthDay = byMonthDays || parseDate(startDate).getDate();

  switch (frequency) {
    case Frequency.weekday: {
      result = [`FREQ=${Frequency.weekly}`, 'BYDAY=MO,TU,WE,TH,FR'];
      break;
    }
    case Frequency.weekly: {
      result.push(`BYDAY=${byDay}`);
      break;
    }
    case Frequency.monthly: {
      if (useMonthlyDay) {
        result.push(`BYMONTHDAY=${byMonthDay}`);
      } else {
        result.push(`BYDAY=${byDay}`);
        result.push(`BYSETPOS=${realBySetPos}`);
      }
      break;
    }
    case Frequency.yearly: {
      const month = getMonth(startDate);
      result.push(`BYMONTH=${month}`);
      result.push(`BYMONTHDAY=${byMonthDay}`);
      break;
    }
    default:
      break;
  }

  result.push(`INTERVAL=${interval}`);

  if (count) {
    result.push(`COUNT=${count}`);
  }

  if (until) {
    const untilDate = parseDate(until)
      .toISOString()
      .replace(/[-:]/g, '');
    result.push(`UNTIL=${untilDate}`);
  }

  return result.join(';');
};

export {
  Frequency, rrule, getDayOfWeek, getOrdinalWeekOfMonth, getDayOfYearFormatted,
};
