window.onload = function() {// add dropdown select listeners
  const selects = [].map.call(document.querySelectorAll('.mdc-select'), function(el) {return new mdc.select.MDCSelect(el);});
  selects.map((select) => {
    select.listen('MDCSelect:change', () => {
      select.root.querySelector('input').value = select.value
      select.root.querySelector('input').onchange({target: {value: select.value}})
    });
  })

  // add textfield listeners
  const textfields = [].map.call(document.querySelectorAll('.mdc-text-field'), function(el) {return new mdc.textField.MDCTextField(el);});

  // add button listeners
  const buttons = [].map.call(document.querySelectorAll('.mdc-button'), function(el) {return new mdc.ripple.MDCRipple(el);});

  // add menu listeners
  const menus = [].map.call(document.querySelectorAll('.mdc-menu'), function(el) { return new mdc.menu.MDCMenu(el); })
  menus.map((menu) => {
    menu.open = false;
    const toggle = document.querySelector(`#${menu.root.id}-toggle`)
    if (toggle) {toggle.onclick = () => {
      menu.open = true;
    }}
  })

  // add tab listeners
  const tabBars = new mdc.tabBar.MDCTabBar(document.querySelector('.mdc-tab-bar'));
}
