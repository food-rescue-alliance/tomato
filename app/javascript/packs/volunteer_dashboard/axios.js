import axios from 'axios';
import { camelizeKeys } from 'humps';

const api = axios.create();

api.interceptors.response.use((response) => {
  if (response.data) {
    response.data = camelizeKeys(response.data);
  }
  return response;
}, (err) => Promise.reject(camelizeKeys(err)));

export default api;
