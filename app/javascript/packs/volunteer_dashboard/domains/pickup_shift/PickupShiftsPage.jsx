import React from 'react';
import { PickupShiftStateProvider } from './context';
import PickupShifts from './PickupShifts';

const PickupShiftPage = () => (
  <PickupShiftStateProvider>
    <PickupShifts />
  </PickupShiftStateProvider>
);

export default PickupShiftPage;
