import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { Grid } from '@mui/material';
import { Tabs, Tab } from '../global/Tabs';
import { shiftType } from '../../types';
import { connectPickupShift } from './context';
import { fetchOpenShifts } from './actions';
import Shifts from '../dashboard/Shifts';
import Header from '../global/Header';
import Loading from '../global/Loading';
import Error from '../global/Error';
import { connect } from '../global/context';

export const PickupShiftsComponent = ({
  dispatch, loading, openShifts, error, user,
}) => {
  const { t } = useTranslation();

  const [tabValue, setTabValue] = useState(0);

  useEffect(() => {
    dispatch(fetchOpenShifts());
  }, []);

  if (error) return <Error />;

  if (loading) return <Loading />;

  return (
    <div>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Header>{t('pickupShifts.title')}</Header>
        </Grid>
        <Grid item xs={12}>
          <Tabs
            value={tabValue}
            onChange={(event, newValue) => { setTabValue(newValue); }}
            indicatorColor="primary"
            textColor="primary"
            style={{ width: '100%' }}
          >
            <Tab label={t('global.recurring')} />
            <Tab label={t('global.oneTime')} />
          </Tabs>
        </Grid>
        <Grid item xs={12}>
          <Shifts
            loading={loading}
            shifts={tabValue === 0 ? openShifts.recurring : openShifts.oneTime}
            user={user}
          />
        </Grid>
      </Grid>
    </div>
  );
};

PickupShiftsComponent.propTypes = {
  dispatch: PropTypes.func.isRequired,
  loading: PropTypes.bool,
  user: PropTypes.shape({
    timeZoneOffset: PropTypes.string,
  }),
  openShifts: PropTypes.shape({
    recurring: PropTypes.arrayOf(shiftType),
    oneTime: PropTypes.arrayOf(shiftType),
  }),
  error: PropTypes.string,
};

PickupShiftsComponent.defaultProps = {
  openShifts: {
    recurring: [],
    oneTime: [],
  },
  user: {},
  loading: true,
  error: null,
};

export default connect(connectPickupShift(PickupShiftsComponent));
