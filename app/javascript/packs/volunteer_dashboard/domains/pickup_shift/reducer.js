import * as types from './actions';

export const defaultState = {
  loading: true,
  openShifts: {
    recurring: [],
    oneTime: [],
  },
};

export const reducer = (state, action) => {
  switch (action.type) {
    case types.LOADING: {
      return { ...state, loading: action.data };
    }
    case types.ERROR: {
      return { ...state, loading: false, error: action.data };
    }
    case types.SET_PICKUP_SHIFTS: {
      return { ...state, loading: false, openShifts: action.data };
    }
    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
};
