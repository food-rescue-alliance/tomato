import * as React from 'react';
import { defaultState, reducer } from './reducer';
import useProvider from '../../../reduxlike-contexts/useProvider';

const PickupShiftStateContext = React.createContext();
const PickupShiftDispatchContext = React.createContext();

const [PickupShiftStateProvider, connectPickupShift] = useProvider(
  PickupShiftStateContext,
  PickupShiftDispatchContext,
  reducer,
  defaultState,
);

export { PickupShiftStateProvider, connectPickupShift };
