import axios from '../../axios';

// ACTIONS
export const ERROR = 'ERROR';
export const LOADING = 'LOADING';
export const SET_PICKUP_SHIFTS = 'SET_PICKUP_SHIFTS';

// ACTION CREATORS
const setLoading = (loading) => ({ type: LOADING, data: loading });
const setError = (err) => ({ type: ERROR, data: err });
const setPickupShifts = (shifts) => ({ type: SET_PICKUP_SHIFTS, data: shifts });

// ASYNC
export const fetchOpenShifts = () => async (dispatch) => {
  dispatch(setLoading(true));
  try {
    const response = await axios.get('/api/users/me/shift_event_occurrences/pickup.json');
    dispatch(setPickupShifts(response.data));
  } catch (err) {
    dispatch(setError(err));
  }
};
