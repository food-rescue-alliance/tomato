import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Grid } from '@mui/material';
import { alertType, shiftType } from '../../types';
import { connectDashboard } from './context';
import { fetchShifts, fetchAlerts } from './actions';
import Shifts from './Shifts';
import Greeting from './Greeting';
import Alerts from './Alerts';
import { connect } from '../global/context';

export const DashboardComponent = ({
  dispatch, shifts, alerts, user,
}) => {
  useEffect(() => {
    dispatch(fetchShifts());
    dispatch(fetchAlerts());
  }, []);

  return (
    <Grid container spacing={3}>
      <Grid item xs={12}>
        <Greeting />
      </Grid>
      <Grid item xs={12}>
        <Alerts alerts={alerts.alerts} loading={alerts.loading} />
      </Grid>
      <Grid item xs={12}>
        <Shifts loading={shifts.loading} shifts={shifts.shifts} user={user} />
      </Grid>
    </Grid>
  );
};

DashboardComponent.propTypes = {
  dispatch: PropTypes.func.isRequired,
  user: PropTypes.shape({
    timeZoneOffset: PropTypes.string,
  }),
  shifts: PropTypes.shape({
    loading: PropTypes.bool,
    shifts: PropTypes.arrayOf(shiftType),
  }),
  alerts: PropTypes.shape({
    alerts: PropTypes.arrayOf(alertType),
    loading: PropTypes.bool,
  }),
};

DashboardComponent.defaultProps = {
  shifts: {
    loading: true,
    shifts: [],
  },
  user: {},
  alerts: {
    loading: true,
    alerts: [],
  },
};

export default connect(connectDashboard(DashboardComponent));
