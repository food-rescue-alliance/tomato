import * as React from 'react';
import { defaultState, reducer } from './reducer';
import useProvider from '../../../reduxlike-contexts/useProvider';

const DashboardStateContext = React.createContext();
const DashboardDispatchContext = React.createContext();

const [DashboardProvider, connectDashboard] = useProvider(
  DashboardStateContext,
  DashboardDispatchContext,
  reducer,
  defaultState,
);

export { DashboardProvider, connectDashboard };
