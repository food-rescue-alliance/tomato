import * as types from './actions';

export const defaultState = {
  shifts: {
    loading: true,
    shifts: [],
  },
  alerts: {
    loading: true,
    alerts: [],
  },
};

export const reducer = (state, action) => {
  switch (action.type) {
    case types.SET_SHIFTS: {
      return { ...state, shifts: action.data, loading: false };
    }
    case types.SET_ALERTS: {
      return { ...state, alerts: action.data };
    }
    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
};
