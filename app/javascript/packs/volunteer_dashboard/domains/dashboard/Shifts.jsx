import React from 'react';
import PropTypes from 'prop-types';
import {
  Card,
  Grid,
  Button,
} from '@mui/material';
import { makeStyles } from '@mui/styles';
import { Link } from 'react-router-dom';
import { formatInTimeZone } from 'date-fns-tz';
import { useTranslation } from 'react-i18next';
import { shiftType } from '../../types';
import Loading from '../global/Loading';

const useStyles = makeStyles((theme) => ({
  dayheader: {
    ...theme.typography?.h2,
    color: '#274653',
  },
  dayheadercontainer: {
    padding: '20px 24px !important',
    width: '94px',
    textAlign: 'right',
    paddingRight: '5px !important',
  },
  date: {
    color: '#3A7D44',
    fontWeight: 'bold',
  },
  title: {
    color: '#6A798B',
  },
  shiftdetails: {
    padding: '28px 12px !important',
    maxWidth: 'calc(100% - 94px)',
  },
  shiftLink: {
    textDecoration: 'none',
  },
  alert: {
    '&::before': {
      content: '""',
      position: 'absolute',
      background: '#F3AE29',
      top: '0',
      bottom: '0',
      left: '0',
      width: '5px',
    },
    position: 'relative',
  },
}));

const Shift = ({ shift, user }) => {
  const { t } = useTranslation();
  const classes = useStyles();

  const isPickup = shift.users.length === 0;

  return (
    <Grid item xs={12}>
      <Card className={shift.flagged ? classes.alert : ''}>
        <Grid container spacing={3} component={Link} to={`/shifts/${shift.id}`} className={classes.shiftLink}>
          <Grid item className={classes.dayheadercontainer}>
            <span className={classes.dayheader}>{formatInTimeZone(new Date(shift.startsAt), user.timeZoneOffset, 'd')}</span>
          </Grid>
          <Grid item className={classes.shiftdetails}>
            <div className={classes.date}>
              {formatInTimeZone(new Date(shift.startsAt), user.timeZoneOffset, 'EEEE, MMM d').toUpperCase()}
              {' '}
            </div>
            <div className={classes.date}>
              {formatInTimeZone(new Date(shift.startsAt), user.timeZoneOffset, "h':'mm")}
              -
              {formatInTimeZone(new Date(shift.endsAt), user.timeZoneOffset, "h':'mma")}
            </div>
            <div className={classes.title}>
              <div>{shift.title}</div>
              {isPickup && (
              <div>
                {shift.tasks.map((task) => <div key={`task-${shift.id}-${task.id}`}>{task.description}</div>)}
              </div>
              )}
              <div>
                {shift.rrule && shift.rrule}
              </div>
            </div>
            <br />
            <div>
              <Button>{isPickup ? t('shiftDetails.pickupButton') : t('shiftDetails.logReport')}</Button>
            </div>
          </Grid>
        </Grid>
      </Card>
    </Grid>
  );
};

Shift.propTypes = {
  shift: shiftType.isRequired,
  user: PropTypes.shape({ timeZoneOffset: PropTypes.string }),
};

Shift.defaultProps = {
  user: {},
};

const Shifts = ({ loading, shifts, user }) => {
  if (loading) {
    return (
      <div>
        <Loading inline />
      </div>
    );
  }
  return (
    <div>
      <Grid container spacing={1}>
        {shifts[0] ? shifts.map((shift) => (
          <Shift shift={shift} key={shift.id} user={user} />
        )) : (
          <Grid item xs={12}>
            <Card>
              No upcoming shifts.
            </Card>
          </Grid>
        )}
      </Grid>
    </div>
  );
};

Shifts.propTypes = {
  loading: PropTypes.bool,
  user: PropTypes.shape({ timeZoneOffset: PropTypes.string }),
  shifts: PropTypes.arrayOf(shiftType),
};

Shifts.defaultProps = {
  loading: true,
  shifts: [],
  user: {},
};

export default Shifts;
