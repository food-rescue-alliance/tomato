import axios from '../../axios';

// ACTIONS
export const ERROR = 'ERROR';
export const SET_SHIFTS = 'SET_SHIFTS';
export const SET_ALERTS = 'SET_ALERTS';

// ACTION CREATORS
const setError = (err) => ({ type: ERROR, data: err });
const setShifts = (shifts) => ({ type: SET_SHIFTS, data: shifts });
const setAlerts = (alerts) => ({ type: SET_ALERTS, data: alerts });

// ASYNC
export const fetchShifts = () => async (dispatch) => {
  dispatch(setShifts({ loading: true, shifts: [] }));
  try {
    const response = await axios.get('/api/users/me/shift_event_occurrences.json');
    dispatch(setShifts({ loading: false, shifts: response.data }));
  } catch (err) {
    dispatch(setError(err));
  }
};

export const fetchAlerts = () => async (dispatch) => {
  dispatch(setAlerts({ loading: true }));
  try {
    const response = await axios.get('/api/users/me/alerts.json');
    dispatch(setAlerts({ loading: false, alerts: response.data }));
  } catch (err) {
    dispatch(setAlerts({ loading: false, error: err.response.data }));
  }
};
