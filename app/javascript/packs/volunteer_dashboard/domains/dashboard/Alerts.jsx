import React from 'react';
import PropTypes from 'prop-types';
import {
  Grid,
  Card,
} from '@mui/material';
import { makeStyles } from '@mui/styles';
import { Link } from 'react-router-dom';
import { alertType } from '../../types';
import Loading from '../global/Loading';
import Error from '../global/Error';

const useStyles = makeStyles(() => ({
  alertContainer: {
    display: 'flex',
    flexFlow: 'row nowrap',
    overflow: 'auto',
  },
  alertCard: {
    '&::before': {
      content: '""',
      position: 'absolute',
      background: '#F3AE29',
      top: '0',
      bottom: '0',
      left: '0',
      width: '5px',
    },
    position: 'relative',
    height: '100px',
    width: '200px',
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    color: '#6A798B',
    textDecoration: 'none',
  },
}));

export const AlertsComponent = ({ error, loading, alerts }) => {
  const classes = useStyles();

  if (error) {
    return <Error />;
  }
  if (loading) {
    return <Loading inline />;
  }
  if (alerts.length <= 0) {
    return <div />;
  }
  return (
    <Grid container spacing={3} className={classes.alertContainer}>
      {alerts.map((alert) => (
        <Grid item key={`alert-${alert.occurrenceId}`}>
          <Card className={classes.alertCard} component={Link} to={`/shifts/${alert.occurrenceId}`}>{alert.message}</Card>
        </Grid>
      ))}
    </Grid>
  );
};

AlertsComponent.propTypes = {
  alerts: PropTypes.arrayOf(alertType),
  loading: PropTypes.bool.isRequired,
  error: PropTypes.bool,
};

AlertsComponent.defaultProps = {
  alerts: [],
  error: false,
};

export default AlertsComponent;
