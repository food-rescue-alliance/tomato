import React from 'react';
import { Grid } from '@mui/material';
import { useTranslation } from 'react-i18next';
import { userType } from '../../types';
import { connect } from '../global/context';

export const greeting = (time) => {
  const hours = time.getHours();
  if (hours < 10) { return 'dashboard.goodMorning'; }
  if (hours < 20) { return 'dashboard.goodAfternoon'; }
  return 'dashboard.goodEvening';
};

export const DashboardComponent = ({ user }) => {
  const { t } = useTranslation();

  return (
    <Grid
      container
      direction="column"
      justifyContent="flex-start"
      alignItems="flex-start"
      style={{
        fontSize: '18px',
        color: '#274653',
      }}
    >
      <div>
        <b>{t(greeting(new Date()))}</b>
      </div>
      <div>
        {user.fullName}
        !
        {' '}
      </div>
    </Grid>
  );
};

DashboardComponent.propTypes = {
  user: userType.isRequired,
};

export default connect(DashboardComponent);
