import React from 'react';

import { DashboardProvider } from './context';
import Dashboard from './Dashboard';

export const DashboardPage = () => (
  <DashboardProvider>
    <Dashboard />
  </DashboardProvider>
);

export default DashboardPage;
