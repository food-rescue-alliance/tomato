import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import {
  Box,
  Card,
  CircularProgress,
  Grid,
  Stack,
  Typography,
} from '@mui/material';
import { EditOutlined } from '@mui/icons-material';
import { useTranslation } from 'react-i18next';
import { format } from 'date-fns';
import { useSnackbar } from 'notistack';
import { connectAbsence } from './context';
import {
  fetchAllAbsences,
  createAbsence,
  updateAbsence,
  deleteAbsence,
} from './actions';
import DateRangeForm from './DateRangeForm';
import EditAbsence from './EditAbsence';

const formatAbsenceDateString = ({ startsAt, endsAt }) => {
  const rawStartDate = new Date(startsAt);
  const rawEndDate = new Date(endsAt);

  const startDate = new Date(
    rawStartDate.valueOf() + rawStartDate.getTimezoneOffset() * 60 * 1000,
  );
  const endDate = new Date(
    rawEndDate.valueOf() + rawEndDate.getTimezoneOffset() * 60 * 1000,
  );

  if (startsAt === endsAt) {
    return format(startDate, 'EEEE, MMM d');
  }

  return `${format(startDate, 'EEEE, MMM d')} - ${format(endDate, 'EEEE, MMM d')}`;
};

const Absences = ({
  dispatch, loading, absences, snackbarNotification,
}) => {
  const { t } = useTranslation();
  const { enqueueSnackbar } = useSnackbar();
  const [currentAbsence, setCurrentAbsence] = useState(null);

  const onScheduleAbsence = (date) => {
    dispatch(createAbsence({
      startsAt: date.startDate,
      endsAt: date.endDate,
    }));
  };

  const onEditAbsence = (date) => {
    dispatch(updateAbsence({
      id: currentAbsence.id,
      startsAt: date.startDate,
      endsAt: date.endDate,
    }));
  };

  const onDeleteAbsence = () => {
    dispatch(deleteAbsence(currentAbsence.id));
  };

  useEffect(() => {
    dispatch(fetchAllAbsences());
  }, []);

  useEffect(() => {
    if (snackbarNotification) {
      enqueueSnackbar(
        t(snackbarNotification.messageKey, snackbarNotification.messageOptions),
        snackbarNotification.options,
      );
    }
  }, [snackbarNotification, enqueueSnackbar, t]);

  return (
    <>
      {currentAbsence
        && (
        <EditAbsence
          absence={currentAbsence}
          onClose={() => setCurrentAbsence(null)}
          onSubmit={onEditAbsence}
          onDelete={onDeleteAbsence}
        />
        )}
      <Grid container direction="column" spacing={3}>
        <Grid item mt={1.5}>
          <Typography variant="h5" fontWeight="bold">{t('absences.formTitle')}</Typography>
          <Typography variant="body2" color="grey.600">{t('absences.formSubtitle')}</Typography>
        </Grid>
        <Grid item>
          <DateRangeForm
            submitLabel={t('absences.submitButton')}
            submitHelpText={t('absences.submitButtonHelpText')}
            onSubmit={onScheduleAbsence}
          />
        </Grid>
        {(absences.loading || absences.length > 0) && (
        <>
          <Grid item><hr /></Grid>
          <Grid item>
            <Typography variant="h6" fontWeight="bold">{t('absences.listTitle')}</Typography>
            <Typography variant="body2" color="grey.600">{t('absences.listSubtitle')}</Typography>
          </Grid>
          <Grid item>
            <Stack direction="column" spacing={3}>
              {loading && (
              <Box display="flex" alignItems="center" justifyContent="center">
                <CircularProgress />
              </Box>
              )}
              {absences.map((absence) => (
                <Card
                  key={absence.id}
                  elevation={0}
                  sx={{ border: '1px solid rgba(39, 70, 83, 0.2)' }}
                  onClick={absence.editable ? () => setCurrentAbsence(absence) : undefined}
                >
                  <Box display="flex" alignItems="center" justifyContent="space-between">
                    {formatAbsenceDateString(absence)}
                    {absence.editable && <EditOutlined />}
                  </Box>
                </Card>
              ))}
            </Stack>
          </Grid>
        </>
        )}
      </Grid>
    </>
  );
};

Absences.defaultProps = {
  snackbarNotification: null,
};

Absences.propTypes = {
  dispatch: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  absences: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    startsAt: PropTypes.string.isRequired,
    endsAt: PropTypes.string.isRequired,
  })).isRequired,
  snackbarNotification: PropTypes.shape({
    messageKey: PropTypes.string,
    messageOptions: PropTypes.shape({
      startsAt: PropTypes.string,
    }),
    options: PropTypes.shape({
      variant: PropTypes.string,
    }),
  }),
};

export default connectAbsence(Absences);
