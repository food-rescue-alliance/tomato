import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Stack,
  TextField,
  Typography,
} from '@mui/material';
import { LocalizationProvider, DatePicker } from '@mui/x-date-pickers';
import { addDays, format } from 'date-fns';
import DateFnsAdapter from '@date-io/date-fns';
import HyphenIcon from '../global/icons/HyphenIcon';

const DateRangeForm = ({
  submitLabel, onSubmit, submitHelpText, absence, submitButtonProps,
}) => {
  const [date, setDate] = useState(absence);

  const firstSchedulableDate = addDays(new Date(), 7);

  return (
    <form onSubmit={(e) => {
      onSubmit(date);
      setDate({
        startDate: null,
        endDate: null,
      });
      e.preventDefault();
    }}
    >
      <LocalizationProvider dateAdapter={DateFnsAdapter}>
        <Stack direction="column" spacing={2}>
          <Stack direction="row" spacing={1}>
            <DatePicker
              label="Start Date"
              value={date.startDate}
              defaultCalendarMonth={firstSchedulableDate}
              minDate={firstSchedulableDate}
              maxDate={date.endDate}
              getOpenDialogAriaText={(selected) => (selected ? `Choose start date, selected date is ${format(selected, 'PPP')}` : 'Choose start date')}
              onChange={(value) => setDate({
                ...date,
                startDate: value,
              })}
              // eslint-disable-next-line react/jsx-props-no-spreading
              renderInput={(props) => <TextField {...props} />}
            />
            <HyphenIcon />
            <DatePicker
              label="End Date"
              value={date.endDate}
              defaultCalendarMonth={date.startDate}
              getOpenDialogAriaText={(selected) => (selected ? `Choose end date, selected date is ${format(selected, 'PPP')}` : 'Choose end date')}
              minDate={date.startDate || firstSchedulableDate}
              onChange={(value) => setDate({
                ...date,
                endDate: value,
              })}
            // eslint-disable-next-line react/jsx-props-no-spreading
              renderInput={(props) => <TextField {...props} />}
            />
          </Stack>
          <Stack direction="column" spacing={2}>
            <Stack direction="row">
              <Button
                sx={{ mt: 2 }}
                type="submit"
                variant="contained"
                disabled={date.startDate == null || date.endDate == null}
                // eslint-disable-next-line react/jsx-props-no-spreading
                {...submitButtonProps}
              >
                {submitLabel}
              </Button>
            </Stack>
            {submitHelpText && <Typography variant="body2" fontWeight="medium" color="grey.600">{submitHelpText}</Typography>}
          </Stack>
        </Stack>
      </LocalizationProvider>
    </form>
  );
};

DateRangeForm.defaultProps = {
  submitHelpText: null,
  absence: {
    startDate: null,
    endDate: null,
  },
  submitButtonProps: {},
};

DateRangeForm.propTypes = {
  submitLabel: PropTypes.string.isRequired,
  submitHelpText: PropTypes.string,
  submitButtonProps: PropTypes.shape({}),
  onSubmit: PropTypes.func.isRequired,
  absence: PropTypes.shape({
    startDate: PropTypes.instanceOf(Date),
    endDate: PropTypes.instanceOf(Date),
  }),
};

export default DateRangeForm;
