import { format } from 'date-fns';
import axios from '../../axios';

// ACTIONS
export const LOADING = 'LOADING';
export const ERROR = 'ERROR';
export const SET_ABSENCES = 'SET_ABSENCES';
export const ADD_ABSENCE = 'ADD_ABSENCE';
export const UPDATE_ABSENCE = 'UPDATE_ABSENCE';
export const DELETE_ABSENCE = 'DELETE_ABSENCE';
export const SET_SNACKBAR_NOTIFICATION = 'SET_SNACKBAR_NOTIFICATION';

// ACTION CREATORS
const setLoading = (loading) => ({
  type: LOADING,
  data: loading,
});
const setError = (err) => ({
  type: ERROR,
  data: err,
});
const setAbsences = (absences) => ({
  type: SET_ABSENCES,
  data: absences,
});
const setSnackbarNotification = (notification) => ({
  type: SET_SNACKBAR_NOTIFICATION,
  data: notification,
});

// ASYNC
export const fetchAllAbsences = () => async (dispatch) => {
  dispatch(setLoading(true));
  try {
    const response = await axios.get('/api/users/me/absences.json');
    dispatch(setAbsences(response.data));
  } catch (err) {
    dispatch(setError(err));
    dispatch(setSnackbarNotification({
      messageKey: 'absences.error',
      options: {
        variant: 'error',
      },
    }));
  }
  dispatch(setLoading(false));
};

export const createAbsence = ({ startsAt, endsAt }) => async (dispatch) => {
  dispatch(setLoading(true));
  try {
    const response = await axios.post('api/users/me/absences.json', {
      starts_at: startsAt,
      ends_at: endsAt,
    });
    dispatch(setAbsences(response.data));
    dispatch(setSnackbarNotification({
      messageKey: 'absences.created',
      messageOptions: { startsAt: format(startsAt, 'EEEE, MMM d') },
      options: {
        variant: 'success',
      },
    }));
  } catch (err) {
    dispatch(setError(err));
    dispatch(setSnackbarNotification({
      messageKey: err.response.data.error || 'absences.error',
      options: {
        variant: 'error',
      },
    }));
  }
  dispatch(setLoading(false));
};

export const updateAbsence = ({ id, startsAt, endsAt }) => async (dispatch) => {
  dispatch(setLoading(true));
  try {
    const response = await axios.patch(`api/users/me/absences/${id}.json`, {
      starts_at: startsAt,
      ends_at: endsAt,
    });
    dispatch(setAbsences(response.data));
    dispatch(setSnackbarNotification({
      messageKey: 'absences.edited',
      options: {
        variant: 'success',
      },
    }));
  } catch (err) {
    dispatch(setError(err));
    dispatch(setSnackbarNotification({
      messageKey: err.response.data.error || 'absences.error',
      options: {
        variant: 'error',
      },
    }));
  }
  dispatch(setLoading(false));
};

export const deleteAbsence = (id) => async (dispatch) => {
  dispatch(setLoading(true));
  try {
    const response = await axios.delete(`api/users/me/absences/${id}.json`);
    dispatch(setAbsences(response.data));
    dispatch(setSnackbarNotification({
      messageKey: 'absences.deleted',
      options: {
        variant: 'success',
      },
    }));
  } catch (err) {
    dispatch(setError(err));
    dispatch(setSnackbarNotification({
      messageKey: err.response.data.error || 'absences.error',
      options: {
        variant: 'error',
      },
    }));
  }
  dispatch(setLoading(false));
};
