import React from 'react';
import { reducer, defaultState } from './reducer';
import useProvider from '../../../reduxlike-contexts/useProvider';

const AbsenceStateContext = React.createContext();
const AbsenceDispatchContext = React.createContext();

const [AbsenceStateProvider, connectAbsence] = useProvider(
  AbsenceStateContext,
  AbsenceDispatchContext,
  reducer,
  defaultState,
);

export { AbsenceStateProvider, connectAbsence };
