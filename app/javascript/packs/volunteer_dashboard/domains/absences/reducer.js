import * as types from './actions';

export const defaultState = {
  loading: true,
  absences: [],
  snackbarNotification: null,
};

export const reducer = (state, action) => {
  switch (action.type) {
    case types.LOADING: {
      return { ...state, loading: action.data };
    }
    case types.ERROR: {
      return { ...state, error: action.data };
    }
    case types.SET_ABSENCES: {
      return { ...state, absences: action.data };
    }
    case types.SET_SNACKBAR_NOTIFICATION: {
      return { ...state, snackbarNotification: action.data };
    }
    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
};
