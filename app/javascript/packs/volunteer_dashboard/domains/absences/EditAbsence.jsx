import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import {
  Box,
  Button,
  Dialog,
  IconButton,
  Typography,
} from '@mui/material';
import { Close } from '@mui/icons-material';
import { parseDateInTz } from '../../../utils/parse';
import DateRangeForm from './DateRangeForm';

const EditAbsence = ({
  absence, onClose, onSubmit, onDelete,
}) => {
  const { t } = useTranslation();

  return (
    <Dialog
      open
      onClose={onClose}
    >
      <Box display="flex" justifyContent="flex-end" p={1}>
        <IconButton onClick={onClose}>
          <Close color="secondary" />
        </IconButton>
      </Box>
      <Box pl={3} pb={4} pr={3}>
        <Typography variant="h5" fontWeight="bold">{t('absences.editFormTitle')}</Typography>
        <Typography variant="body2" color="grey.600">{t('absences.submitButtonHelpText')}</Typography>
        <Box mt={4}>
          <DateRangeForm
            absence={{
              startDate: parseDateInTz(absence.startsAt),
              endDate: parseDateInTz(absence.endsAt),
            }}
            submitLabel={t('absences.submitEditButton')}
            onSubmit={(date) => { onSubmit(date); onClose(); }}
            submitButtonProps={{ fullWidth: true }}
          />
        </Box>
        <Box mt={2}>
          <Button fullWidth variant="text" onClick={() => { onDelete(); onClose(); }}>Delete</Button>
        </Box>
      </Box>
    </Dialog>
  );
};

EditAbsence.propTypes = {
  absence: PropTypes.shape({
    startsAt: PropTypes.string,
    endsAt: PropTypes.string,
  }).isRequired,
  onClose: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
};

export default EditAbsence;
