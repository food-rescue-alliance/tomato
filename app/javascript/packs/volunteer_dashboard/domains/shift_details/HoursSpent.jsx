import React, { useState } from 'react';

import PropTypes from 'prop-types';

import {
  TextField as MuiTextField,
} from '@mui/material';
// import TextField from '../global/TextField';

const HOURS_REGEX = /^\d{0,4}(\.\d{0,2})?$/;
const isValid = (hours) => HOURS_REGEX.test(hours);

const HoursSpent = ({ value, label, onChange }) => {
  const [hours, setHours] = useState(value);
  return (
    <MuiTextField
      id="hours-spent"
      label={label}
      type="text"
      variant="outlined"
      defaultValue={hours}
      onChange={(e) => setHours(e.target.value)}
      onBlur={() => { if (isValid(hours)) onChange(hours); }}
      error={hours && !isValid(hours)}
      inputProps={{ inputMode: 'numeric', pattern: HOURS_REGEX.toString() }}
    />
  );
};

HoursSpent.propTypes = {
  value: PropTypes.string,
  label: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

HoursSpent.defaultProps = {
  value: undefined,
};

export default HoursSpent;
