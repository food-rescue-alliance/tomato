import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { Grid } from '@mui/material';
import { connectShift } from './context';
import {
  logType, shiftType, taskType, foodTaxonomyType,
} from '../../types';
import { updateShiftLog } from './actions';
import TaskLog from './TaskLog';
import Loading from '../global/Loading';

const TaskDetails = ({
  dispatch, task, shift, logs, isPickup, foodTaxonomies,
}) => {
  const { t } = useTranslation();

  const onFormSubmit = (weight, temperature, notes, foodTaxonomyId) => {
    dispatch(updateShiftLog(shift.id, task.id, weight, temperature, notes, foodTaxonomyId));
  };

  return (
    <div style={{ margin: '15px', flexShrink: '0' }}>
      <h4>
        <span>
          {task.site.type === 'Donor' && t('shiftDetails.pickup')}
          {task.site.type === 'Recipient' && t('shiftDetails.dropoff')}
        </span>
        <br />
        <span>{task.site.name}</span>
      </h4>
      {task.site.address && (
      <div>
        <div>{task.site.address.streetOne}</div>
        {task.site.address.streetTwo && <div>{task.site.address.streetTwo}</div>}
        <div>
          {task.site.address.city}
          ,
          {' '}
          {task.site.address.state}
          {' '}
          {task.site.address.zip}
        </div>
      </div>
      )}
      <h4>{t('shiftDetails.instructions')}</h4>
      <div style={{ whiteSpace: 'pre-wrap' }}>{task.site.instructions}</div>
      {!isPickup && (
      <div>
        <h4>{t('shiftDetails.logReport')}</h4>
        {foodTaxonomies.loading ? <Loading inline /> : (
          <Grid container spacing={3}>
            {logs.map((log) => (
              <Grid item xs={12} key={log.id ? `log-${log.id}` : `newLog-${log.key}`}>
                <TaskLog
                  onFormSubmit={onFormSubmit}
                  log={log}
                  disabled={!!log.id}
                  foodTaxonomies={foodTaxonomies}
                />
              </Grid>
            ))}
          </Grid>
        )}
      </div>
      )}
    </div>
  );
};

TaskDetails.propTypes = {
  dispatch: PropTypes.func.isRequired,
  task: taskType.isRequired,
  shift: shiftType,
  logs: PropTypes.arrayOf(logType),
  isPickup: PropTypes.bool,
  foodTaxonomies: PropTypes.shape({
    loading: PropTypes.bool,
    error: PropTypes.bool,
    values: PropTypes.arrayOf(foodTaxonomyType),
  }),
};

TaskDetails.defaultProps = {
  shift: {},
  logs: [],
  isPickup: false,
  foodTaxonomies: {
    loading: true,
    values: [],
  },
};

export default connectShift(TaskDetails);
