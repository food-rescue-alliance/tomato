import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import {
  Grid,
  Button,
  MenuItem,
  InputLabel,
  Select,
  FormControl,
  Alert,
} from '@mui/material';

import { useTranslation } from 'react-i18next';
import TextField from '../global/TextField';
import { logType, foodTaxonomyType } from '../../types';

const TaskLogForm = ({
  onFormSubmit, log, disabled, foodTaxonomies,
}) => {
  const { t } = useTranslation();
  const [weight, setWeight] = useState(log.weight);
  const [temperature, setTemperature] = useState(log.temperature);
  const [notes, setNotes] = useState(log.notes);
  const [foodTaxonomy, setTaxonomy] = useState(log.foodTaxonomyId || '');
  const [buttonState, setButtonState] = useState();

  useEffect(() => {
    if (log && log.error) {
      setButtonState();
    }
  }, [log]);

  return (
    <form onSubmit={(e) => {
      e.preventDefault();
      setButtonState('loading');
      onFormSubmit(weight, temperature, notes, foodTaxonomy);
    }}
    >
      <Grid container spacing={3}>
        {log && log.error && (
        <Grid item xs={12}>
          <Alert severity="error">{t('shiftDetails.formError')}</Alert>
        </Grid>
        )}
        <Grid item xs={12}>
          <FormControl variant="outlined" fullWidth>
            <InputLabel id="foodTaxonomyLabel">{t('shiftDetails.foodTaxonomyLabel')}</InputLabel>
            <Select
              variant="outlined"
              disabled={disabled}
              label="Food Taxonomy"
              labelId="foodTaxonomyLabel"
              id="foodTaxonomy"
              value={foodTaxonomy}
              onChange={(e) => setTaxonomy(e.target.value)}
            >
              {foodTaxonomies.values.map((taxonomy) => (
                <MenuItem value={taxonomy.id} key={`food-taxonomy-${taxonomy.id}`}>{taxonomy.name}</MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={6}>
          <TextField
            id="weight"
            label={t('shiftDetails.weightLabel')}
            type="number"
            variant="outlined"
            value={weight}
            onChange={setWeight}
            disabled={disabled}
          />
        </Grid>
        <Grid item xs={6}>
          <TextField
            id="temperature"
            label={t('shiftDetails.temperatureLabel')}
            type="number"
            variant="outlined"
            value={temperature}
            onChange={setTemperature}
            disabled={disabled}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            id="weight"
            label={t('shiftDetails.notesLabel')}
            multiline
            variant="outlined"
            value={notes}
            onChange={setNotes}
            disabled={disabled}
          />
        </Grid>
        { !disabled
                && (
                <Grid item xs={12}>
                  <Button
                    type="submit"
                    variant="contained"
                    color="primary"
                    disableElevation
                    data-testid="status-button"
                    disabled={buttonState === 'loading'}
                  >
                    {t('shiftDetails.saveLog')}
                  </Button>
                </Grid>
                )}
        <hr />
      </Grid>
    </form>
  );
};

TaskLogForm.propTypes = {
  log: logType,
  onFormSubmit: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
  foodTaxonomies: PropTypes.shape({
    loading: PropTypes.bool,
    error: PropTypes.bool,
    values: PropTypes.arrayOf(foodTaxonomyType),
  }).isRequired,
};
TaskLogForm.defaultProps = {
  log: { notes: '', weight: '' },
  disabled: false,
};

export default TaskLogForm;
