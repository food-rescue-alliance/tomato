import React from 'react';
import {
  Grid, IconButton, Typography,
} from '@mui/material';
import { makeStyles } from '@mui/styles';
import PropTypes from 'prop-types';
import { BikeIcon, CarIcon, WalkIcon } from '../global/icons';

const transportationTypes = [['bike', BikeIcon], ['car', CarIcon], ['walk', WalkIcon]];

const useStyles = makeStyles(() => ({
  transportationLabel: {
    fontSize: '18px',
    fontWeight: 'bold',
    paddingBottom: '7px',
  },
  transportationButton: {
    width: '60px',
    height: '45px',
    borderRadius: 7,
  },
  transportationButtonInactive: {
    border: '1.5px solid rgba(39,70,83, 0.3)',
  },
  transportationButtonActive: {
    border: '1.5px solid #274653',
  },
}));

const TransportationType = ({
  label, currentSelected, onSelectTransportationType, isLoading,
}) => {
  const classes = useStyles();
  return (
    <>
      <Typography className={classes.transportationLabel}>{label}</Typography>
      <Grid
        container
        spacing={1}
        alignItems="center"
      >
        {transportationTypes.map(([type, Icon]) => {
          const isSelected = currentSelected === type;
          return (
            <Grid key={type} item>
              <IconButton
                className={`${classes.transportationButton} ${isSelected ? classes.transportationButtonActive : classes.transportationButtonInactive}`}
                aria-label={type}
                aria-current={currentSelected === type ? 'transportation-type' : undefined}
                disabled={isLoading}
                onClick={() => onSelectTransportationType(type)}
                size="large"
              >
                <Icon isSelected={currentSelected === type} style={{ border: '1px solid red' }} />
              </IconButton>
            </Grid>
          );
        })}
      </Grid>
    </>
  );
};

TransportationType.propTypes = {
  label: PropTypes.string.isRequired,
  onSelectTransportationType: PropTypes.func.isRequired,
  currentSelected: PropTypes.string,
  isLoading: PropTypes.bool,
};

TransportationType.defaultProps = {
  currentSelected: null,
  isLoading: false,
};

export default TransportationType;
