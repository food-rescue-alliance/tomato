import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import { Link as RouterLink } from 'react-router-dom';
import {
  Grid,
  Link,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  MobileStepper,
  Alert,
} from '@mui/material';
import { makeStyles } from '@mui/styles';

import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import { useTranslation } from 'react-i18next';
import { formatInTimeZone } from 'date-fns-tz';
import HoursSpent from './HoursSpent';
import { Tabs, Tab } from '../global/Tabs';
import { connectShift } from './context';
import { connect as connectGlobal } from '../global/context';
import {
  fetchShiftDetails, pickupShift, fetchFoodTaxonomies, fetchShiftEventNotes, updateShiftNote,
} from './actions';
import { shiftType, logType } from '../../types';
import TransportationType from './TransportationType';
import TaskDetails from './TaskDetails';
import Loading from '../global/Loading';
import Error from '../global/Error';
import ShiftHeader from './ShiftHeader';
import StatusButton from '../global/StatusButton';
import Header from '../global/Header';

const useStyles = makeStyles(() => ({
  stepperDot: {
    margin: '0 10px',
  },
}));

export const ShiftDetailsComponent = ({
  id, dispatch, shift, taskLogs, loading, error, note, user,
}) => {
  const classes = useStyles();
  const { t } = useTranslation();

  const [tabValue, setTabValue] = useState(0);
  const [selectedTask, setSelectedTask] = useState(0);
  const [showDialog, setShowDialog] = useState(false);
  const selectedTransportationType = note.transportationType ?? '';

  const confirmPickupShift = () => {
    setShowDialog(true);
  };
  const onPickupShift = (range) => {
    dispatch(pickupShift(id, range));
    setShowDialog(false);
  };

  const onSelectTransportationType = (transportationType) => {
    dispatch(updateShiftNote(shift.id, { ...note, transportationType }));
  };

  const onEnterHoursSpent = (hoursSpent) => {
    dispatch(updateShiftNote(shift.id, { ...note, hoursSpent }));
  };

  useEffect(() => {
    dispatch(fetchShiftDetails(id));
    dispatch(fetchShiftEventNotes(id));
    dispatch(fetchFoodTaxonomies());
  }, []);

  if (loading) {
    return (<Loading />);
  }

  if (error && !shift.id) {
    return (<Error />);
  }

  const isPickup = shift.users.length === 0;
  const mapsUrl = new URL('https://www.google.com/maps/dir/?api=1');

  const toAddressString = (address) => {
    if (!address || !address.streetOne) { return ''; }
    let addressStr = address.streetOne;
    if (address.streetTwo) { addressStr += ` ${address.streetTwo}`; }
    addressStr += `, ${address.city}`;
    addressStr += `, ${address.state}`;
    addressStr += ` ${address.zip}`;
    return addressStr;
  };

  if (shift.tasks) {
    const siteAddresses = shift.tasks
      .map((task) => toAddressString(task.site.address))
      .filter(String);
    const origin = siteAddresses.shift();
    const destination = siteAddresses.pop();
    const waypoints = siteAddresses.join('|');
    if (origin) { mapsUrl.searchParams.append('origin', origin); }
    if (waypoints) { mapsUrl.searchParams.append('waypoints', waypoints); }
    if (destination) { mapsUrl.searchParams.append('destination', destination); }
  }

  return (
    <div>
      <Grid container spacing={3}>
        {error && (
        <Grid item xs={12}>
          <Alert severity="error">{t(error)}</Alert>
        </Grid>
        )}
        <Grid item xs={1}>
          <Link
            component={RouterLink}
            to={isPickup ? '/pickup' : '/'}
            color="secondary"
            underline="hover"
          >
            <ArrowBackIosIcon />
          </Link>
        </Grid>
        <Grid item xs={11}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <div>
                <ShiftHeader shift={shift} user={user} />
                {isPickup && (
                <div>
                  {shift.tasks.map((task) => <Header key={`task-${task.id}`}>{task.description}</Header>)}
                  {shift.rrule && (<Header>{shift.rrule}</Header>)}
                  <br />
                  <div>
                    <StatusButton status={shift.loading ? 'loading' : ''} onClick={confirmPickupShift}>
                      {t('shiftDetails.pickupButton')}
                    </StatusButton>
                  </div>
                </div>
                )}
              </div>
            </Grid>
            <Grid item xs={12}>
              {!isPickup && (
                <HoursSpent label={t('shiftDetails.hoursSpentLabel')} value={note.hoursSpent ?? undefined} onChange={onEnterHoursSpent} />
              )}
            </Grid>
            <Grid item xs={12}>
              {!isPickup && (
                <TransportationType
                  label={t('shiftDetails.transportationLabel')}
                  currentSelected={selectedTransportationType}
                  onSelectTransportationType={onSelectTransportationType}
                  isLoading={note.loading}
                />
              )}
            </Grid>
            <Grid item xs={12}>
              <Tabs
                value={tabValue}
                onChange={(event, newValue) => { setTabValue(newValue); }}
                indicatorColor="primary"
                textColor="primary"
                style={{ width: '100%' }}
              >
                <Tab label="Tasks" />
                <Tab label="Map" component="a" href={mapsUrl.href} target="_blank" rel="noopener noreferrer" />
              </Tabs>
            </Grid>
            <Grid item xs={12}>
              {tabValue === 0 && (
              <div>
                <Grid
                  container
                  direction="row"
                  justifyContent="center"
                  alignItems="center"
                >
                  {shift.tasks
                  && (
                  <MobileStepper
                    variant="dots"
                    steps={shift.tasks.length}
                    position="static"
                    classes={{
                      dot: classes.stepperDot,
                    }}
                    activeStep={selectedTask}
                    nextButton={(
                      <Button size="small" onClick={() => { setSelectedTask(selectedTask + 1); }} disabled={selectedTask >= shift.tasks.length - 1}><ArrowForwardIosIcon title="next-task" /></Button>
      )}
                    backButton={(
                      <Button size="small" onClick={() => { setSelectedTask(selectedTask - 1); }} disabled={selectedTask <= 0}><ArrowBackIosIcon title="previous-task" /></Button>

      )}
                  />
                  )}
                </Grid>
                {shift.tasks && shift.tasks.length > 0
                  ? (
                    <TaskDetails
                      task={shift.tasks[selectedTask]}
                      key={shift.tasks[selectedTask].id}
                      logs={taskLogs[shift.tasks[selectedTask].id]}
                      isPickup={isPickup}
                    />
                  )
                  : <div>{t('shiftDetails.noTasks')}</div>}
              </div>
              )}
              {tabValue === 1 && <div>Opening in google maps...</div>}
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <Dialog
        open={showDialog}
        onClose={() => { setShowDialog(false); }}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Pickup recurring shift on
            {' '}
            {formatInTimeZone(new Date(shift.startsAt), user.timeZoneOffset, "EEEE MMM d h':'mm")}
            -
            {formatInTimeZone(new Date(shift.endsAt), user.timeZoneOffset, "h':'mma")}
            ?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Grid
            container
            direction="row"
            justifyContent="space-around"
            alignItems="center"
          >
            <Button onClick={() => { onPickupShift(''); }} color="primary">
              {t('pickupShifts.oneTime')}
            </Button>
            <Button onClick={() => { onPickupShift('THISANDFUTURE'); }} color="primary" autoFocus>
              {t('pickupShifts.recurring')}
            </Button>
          </Grid>
        </DialogActions>
      </Dialog>
    </div>
  );
};

ShiftDetailsComponent.propTypes = {
  id: PropTypes.string.isRequired,
  dispatch: PropTypes.func.isRequired,
  shift: shiftType,
  loading: PropTypes.bool.isRequired,
  error: PropTypes.string,
  note: PropTypes.shape({
    transportationType: PropTypes.string,
    hoursSpent: PropTypes.string,
    loading: PropTypes.bool,
  }),
  taskLogs: PropTypes.arrayOf(
    PropTypes.arrayOf(logType),
  ),
  user: PropTypes.shape({ timeZoneOffset: PropTypes.string }),
};

ShiftDetailsComponent.defaultProps = {
  error: null,
  shift: {},
  taskLogs: [],
  note: {},
  user: {},
};

export default connectGlobal(connectShift(ShiftDetailsComponent));
