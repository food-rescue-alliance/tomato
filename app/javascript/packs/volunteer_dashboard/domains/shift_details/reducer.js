import _ from 'lodash';
import * as types from './actions';

export const defaultState = {
  loading: true,
  note: {},
  shift: {
  },
  taskLogs: [],
  foodTaxonomies: {
    loading: true,
    values: [],
  },
};

export const reducer = (state, action) => {
  switch (action.type) {
    case types.LOADING: {
      return { ...state, loading: action.data };
    }
    case types.SET_SHIFT: {
      const { taskLogs, ...shift } = action.data;

      const sortedTaskLogs = _.reduce(shift.tasks, (result, task) => {
        const returnResult = result;
        const logs = taskLogs.filter((log) => log.taskId === task.id);
        logs.push({
          weight: '',
          temperature: '',
          notes: '',
          key: Math.random(),
        });
        returnResult[task.id] = logs;
        return returnResult;
      }, []);
      return {
        ...state,
        loading: false,
        shift,
        taskLogs: sortedTaskLogs,
      };
    }
    case types.FOOD_TAXONOMIES_LOADING: {
      return {
        ...state,
        foodTaxonomies: {
          loading: true,
        },
      };
    }
    case types.FOOD_TAXONOMIES_SUCCESS: {
      return {
        ...state,
        foodTaxonomies: {
          loading: false,
          values: action.data,
        },
      };
    }
    case types.FOOD_TAXONOMIES_ERROR: {
      return {
        ...state,
        foodTaxonomies: {
          loading: false,
          error: true,
        },
      };
    }
    case types.LOAD_NOTE: {
      return {
        ...state,
        note: {
          ...state.note,
          ...action.data,
          loading: false,
        },
      };
    }
    case types.SET_NOTE_ERROR: {
      return {
        ...state,
        note: {
          loading: false,
          error: true,
        },
      };
    }
    case types.SET_NOTE_LOADING: {
      return {
        ...state,
        note: {
          ...state.note,
          loading: true,
        },
      };
    }
    case types.SET_LOG: {
      const { log, taskId } = action.data;
      const taskLogList = state.taskLogs[taskId];
      taskLogList.pop();
      taskLogList.push(log);
      taskLogList.push({
        weight: '',
        temperature: '',
        notes: '',
        key: Math.random(),
      });

      const { taskLogs } = state;
      taskLogs[taskId] = taskLogList;

      return {
        ...state,
        taskLogs,
      };
    }
    case types.SET_LOG_ERROR: {
      const taskLogList = state.taskLogs[action.data];
      const last = taskLogList.pop();
      taskLogList.push({ ...last, error: true });

      const { taskLogs } = state;
      taskLogs[action.data] = taskLogList;

      return {
        ...state,
        taskLogs,
      };
    }
    case types.PICKUP_SHIFT_SUBMITTED: {
      return { ...state, shift: { ...state.shift, loading: true }, error: null };
    }
    case types.PICKUP_SHIFT_SUCCESS:
    case types.PICKUP_SHIFT_ERROR: {
      return { ...state, shift: { ...state.shift, loading: false } };
    }
    case types.ERROR: {
      return { ...state, loading: false, error: action.data };
    }
    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
};
