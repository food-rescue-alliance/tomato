import axios from '../../axios';

// ACTIONS
export const LOADING = 'LOADING';
export const SET_SHIFT = 'SET_SHIFT';
export const ERROR = 'ERROR';
export const SET_LOG = 'SET_LOG';
export const SET_LOG_ERROR = 'SET_LOG_ERROR';
export const LOG_SUCCESS = 'LOG_SUCCESSS';
export const LOAD_NOTE = 'LOAD_NOTE';
export const SET_NOTE_ERROR = 'SET_NOTE_ERROR';
export const SET_NOTE_LOADING = 'SET_NOTE_LOADING';
export const PICKUP_SHIFT_SUBMITTED = 'PICKUP_SHIFT_SUBMITTED';
export const PICKUP_SHIFT_SUCCESS = 'PICKUP_SHIFT_SUCCESS';
export const PICKUP_SHIFT_ERROR = 'PICKUP_SHIFT_ERROR';
export const FOOD_TAXONOMIES_LOADING = 'FOOD_TAXONOMIES_LOADING';
export const FOOD_TAXONOMIES_SUCCESS = 'FOOD_TAXONOMIES_SUCCESS';
export const FOOD_TAXONOMIES_ERROR = 'FOOD_TAXONOMIES_ERROR';

// ACTION CREATORS
const setLoading = (loading) => ({ type: LOADING, data: loading });
const setShift = (shift) => ({
  type: SET_SHIFT,
  data: shift,
});
const setError = (err) => ({ type: ERROR, data: err });
const setLog = (taskId, log) => ({ type: SET_LOG, data: { taskId, log } });
const setLogError = (taskId) => ({
  type: SET_LOG_ERROR,
  data: taskId,
});
const pickupShiftSubmitted = () => ({ type: PICKUP_SHIFT_SUBMITTED });
const pickupShiftSuccess = () => ({ type: PICKUP_SHIFT_SUCCESS });
const pickupShiftError = () => ({ type: PICKUP_SHIFT_ERROR });
const setFoodTaxonomiesLoading = () => ({ type: FOOD_TAXONOMIES_LOADING });
const setFoodTaxonomiesSuccess = (taxonomies) => ({
  type: FOOD_TAXONOMIES_SUCCESS, data: taxonomies,
});
const setFoodTaxonomiesError = () => ({ type: FOOD_TAXONOMIES_ERROR });

const loadNote = (note) => ({ type: LOAD_NOTE, data: note });
const setNoteError = () => ({ type: SET_NOTE_ERROR });
const setNoteLoading = () => ({ type: SET_NOTE_LOADING });

// ASYNC
export const fetchShiftDetails = (id) => async (dispatch) => {
  dispatch(setLoading(true));
  try {
    const shiftResponse = await axios.get(`/api/shift_event_occurrences/${id}.json`);
    dispatch(setShift(shiftResponse.data));
  } catch (err) {
    dispatch(setError('shiftDetails.loadShiftError'));
  }
};

export const fetchShiftEventNotes = (shiftId) => async (dispatch) => {
  dispatch(setNoteLoading());
  try {
    const response = await axios.get(`/api/shift_event_occurrences/${shiftId}/note.json`);
    dispatch(loadNote(response.data));
  } catch (err) {
    dispatch(setNoteError(shiftId));
  }
};

export const updateShiftNote = (
  shiftId,
  { transportationType, hoursSpent },
) => async (dispatch) => {
  try {
    dispatch(setNoteLoading());
    await axios.post(`/api/shift_event_occurrences/${shiftId}/note.json`, {
      shift_event_occurrence_id: shiftId,
      shift_event_note: {
        transportation_type: transportationType,
        hours_spent: hoursSpent,
      },
    });
    dispatch(loadNote({ transportationType, hoursSpent }));
  } catch (err) {
    dispatch(setNoteError());
  }
};

export const fetchFoodTaxonomies = () => async (dispatch) => {
  dispatch(setFoodTaxonomiesLoading());
  try {
    const response = await axios.get('/api/food_taxonomies.json');
    dispatch(setFoodTaxonomiesSuccess(response.data));
  } catch (err) {
    dispatch(setFoodTaxonomiesError());
  }
};

export const updateShiftLog = (
  occurrenceId,
  taskId,
  weight,
  temperature,
  notes,
  foodTaxonomyId,
) => async (dispatch) => {
  try {
    const response = await axios.post(`/api/shift_event_occurrences/${occurrenceId}/task_logs.json`, {
      weight, temperature, notes, task_id: taskId, food_taxonomy_id: foodTaxonomyId,
    });
    dispatch(setLog(taskId, response.data));
  } catch (err) {
    dispatch(setLogError(taskId));
  }
};

export const pickupShift = (id, range) => async (dispatch) => {
  dispatch(pickupShiftSubmitted());
  try {
    const response = await axios.post(`/api/users/me/shift_event_occurrences/${id}/pickup.json`, { range });
    dispatch(setShift(response.data));
    dispatch(pickupShiftSuccess());
  } catch (err) {
    dispatch(setError('shiftDetails.pickupShiftError'));
    dispatch(pickupShiftError());
  }
};
