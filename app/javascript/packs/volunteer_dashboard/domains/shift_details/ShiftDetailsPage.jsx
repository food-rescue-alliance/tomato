import React from 'react';
import { useParams } from 'react-router-dom';
import { ShiftStateProvider } from './context';
import ShiftDetails from './ShiftDetails';

const ShiftDetailsPage = () => {
  const { id } = useParams();

  return (
    <ShiftStateProvider>
      <ShiftDetails id={id} />
    </ShiftStateProvider>
  );
};

ShiftDetailsPage.propTypes = {
};

export default ShiftDetailsPage;
