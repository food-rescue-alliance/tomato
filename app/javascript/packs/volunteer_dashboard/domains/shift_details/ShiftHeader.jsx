import React from 'react';
import PropTypes from 'prop-types';
import { formatInTimeZone } from 'date-fns-tz';
import { shiftType } from '../../types';

const ShiftHeader = ({ shift, user }) => (
  <div>
    <div style={{ fontSize: '18px', fontWeight: 'bold' }}>{formatInTimeZone(new Date(shift.startsAt), user.timeZoneOffset, 'EEEE MMM d').toUpperCase()}</div>
    <div style={{ fontSize: '18px', fontWeight: 'bold' }}>
      {formatInTimeZone(new Date(shift.startsAt), user.timeZoneOffset, "h':'mm").toUpperCase()}
      -
      {formatInTimeZone(new Date(shift.endsAt), user.timeZoneOffset, "h':'mma")}
    </div>
    <div style={{ fontSize: '18px' }}>{shift.title}</div>
  </div>
);

ShiftHeader.propTypes = {
  shift: shiftType,
  user: PropTypes.shape({
    timeZoneOffset: PropTypes.string,
  }),
};

ShiftHeader.defaultProps = {
  shift: {},
  user: {},
};

export default ShiftHeader;
