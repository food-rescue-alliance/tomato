import * as React from 'react';
import { defaultState, reducer } from './reducer';
import useProvider from '../../../reduxlike-contexts/useProvider';

const ShiftStateContext = React.createContext();
const ShiftDispatchContext = React.createContext();

const [ShiftStateProvider, connectShift] = useProvider(
  ShiftStateContext,
  ShiftDispatchContext,
  reducer,
  defaultState,
);

export { ShiftStateProvider, connectShift };
