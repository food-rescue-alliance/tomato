import React from 'react';
import PropTypes from 'prop-types';
import { logType, foodTaxonomyType } from '../../types';
import TaskLogForm from './TaskLogForm';

const TaskLog = ({
  onFormSubmit, log, disabled, foodTaxonomies,
}) => {
  const foodTaxonomy = foodTaxonomies.values
    .filter((taxonomy) => taxonomy.id === log.foodTaxonomyId)[0];

  if (disabled) {
    return (
      <div>
        <div>
          {foodTaxonomy && `${foodTaxonomy.name} `}
          {log.weight}
          {' '}
          lbs
        </div>
        <div>
          {log.temperature && `${log.temperature}° F`}
        </div>
        <div>{log.notes}</div>
      </div>
    );
  }
  return <TaskLogForm onFormSubmit={onFormSubmit} log={log} foodTaxonomies={foodTaxonomies} />;
};

TaskLog.propTypes = {
  log: logType,
  onFormSubmit: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
  foodTaxonomies: PropTypes.shape({
    loading: PropTypes.bool,
    error: PropTypes.bool,
    values: PropTypes.arrayOf(foodTaxonomyType),
  }).isRequired,
};
TaskLog.defaultProps = {
  log: { notes: '', weight: '', temperature: '' },
  disabled: false,
};

export default TaskLog;
