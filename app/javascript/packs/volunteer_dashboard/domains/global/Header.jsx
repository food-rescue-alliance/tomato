import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles(() => ({
  header: {
    fontSize: '18px',
    fontWeight: 'normal',
    margin: '0',
  },
}));

const Header = ({ children }) => {
  const classes = useStyles();

  return <h1 className={classes.header}>{children}</h1>;
};

Header.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Header;
