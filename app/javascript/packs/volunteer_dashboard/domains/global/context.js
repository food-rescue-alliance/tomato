import * as React from 'react';
import { defaultState, reducer } from './reducer';
import useProvider from '../../../reduxlike-contexts/useProvider';

const StateContext = React.createContext();
const DispatchContext = React.createContext();

const [StateProvider, connect] = useProvider(
  StateContext,
  DispatchContext,
  reducer,
  defaultState,
);

export { StateProvider, connect };
