import MuiTabs from '@mui/material/Tabs';
import MuiTab from '@mui/material/Tab';
import { withStyles } from '@mui/styles';

export const Tabs = withStyles({
  root: {
    borderBottom: '1px solid #A45E35',
  },
  indicator: {
    backgroundColor: '#A45E35',
  },
})(MuiTabs);

export const Tab = withStyles({
  root: {
    fontSize: '18px',
    color: '#274653',
    paddingLeft: '0',
  },
})(MuiTab);
