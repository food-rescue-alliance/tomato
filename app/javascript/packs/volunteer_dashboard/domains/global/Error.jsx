import React from 'react';
import { useTranslation } from 'react-i18next';

const Error = () => {
  const { t } = useTranslation();

  return <div data-testid="error">{t('global.serverError')}</div>;
};

export default Error;
