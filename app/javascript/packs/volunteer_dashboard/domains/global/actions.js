import { decamelizeKeys } from 'humps';
import axios from '../../axios';

// ACTIONS
export const LOADING = 'LOADING';
export const SET_USER = 'SET_USER';
export const LOGOUT = 'LOGOUT';
export const ERROR = 'ERROR';
export const SET_TIME_ZONES_LOADING = 'SET_TIME_ZONES_LOADING';
export const SET_TIME_ZONES = 'SET_TIME_ZONES';

// ACTION CREATORS
export const setUser = (user) => ({ type: SET_USER, data: user });
const setError = (err) => ({ type: ERROR, data: err });
const logoutUser = () => ({ type: LOGOUT });
const setLoading = (loading) => ({ type: LOADING, data: loading });

const setTimeZonesLoading = (loading) => ({ type: SET_TIME_ZONES_LOADING, data: loading });
const setTimeZones = (timeZones) => ({ type: SET_TIME_ZONES, data: timeZones });

// ASYNC
export const fetchUser = () => async (dispatch) => {
  dispatch(setLoading(true));
  try {
    const response = await axios.get('/api/users/me.json');
    dispatch(setUser(response.data));
  } catch (err) {
    dispatch(setError(err));
  }
};

export const fetchTimeZones = () => async (dispatch) => {
  dispatch(setTimeZonesLoading(true));
  try {
    const response = await axios.get('/api/time_zones.json');
    dispatch(setTimeZones(response.data));
  } catch (err) {
    dispatch(setError(err));
  }
};

export const updateUser = (fullName, email, phone, address, timeZone) => async (dispatch) => {
  dispatch(setUser({ loading: true, error: undefined }));
  try {
    const body = {
      full_name: fullName,
      email,
      phone: phone.replace(/\D/g, ''),
      time_zone: timeZone,
    };

    if (address.streetOne || address.streetTwo || address.city || address.state || address.zip) {
      body.address_attributes = decamelizeKeys(address);
    } else {
      body.address_attributes = { id: address.id, _destroy: '1' };
    }

    const userResponse = await axios.put('/api/users/me.json', body);
    dispatch(setUser({
      loading: false, ...userResponse.data,
    }));
  } catch (err) {
    dispatch(setUser({ loading: false, error: err.response.data }));
  }
};

export const logout = () => async (dispatch) => {
  try {
    await axios.get('/auth/logout');
    dispatch(logoutUser());
  } catch (err) {
    dispatch(setError(err));
  }
};
