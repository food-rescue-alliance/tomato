import React from 'react';

const RootableIcon = () => <img src="/rootable-icon-dark.svg" alt="home" />;

export default RootableIcon;
