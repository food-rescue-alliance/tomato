import React from 'react';

const Pickup = () => <img src="/pickup-icon.svg" alt="pickup" />;

export default Pickup;
