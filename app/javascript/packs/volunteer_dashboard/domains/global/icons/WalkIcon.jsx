import React from 'react';
import PropTypes from 'prop-types';

const WalkIcon = ({ isSelected }) => (
  <img src={isSelected ? '/walk-icon.svg' : '/walk-icon-disabled.svg'} alt="walk-icon" />
);

WalkIcon.propTypes = {
  isSelected: PropTypes.bool,
};
WalkIcon.defaultProps = {
  isSelected: false,
};
export default WalkIcon;
