import React from 'react';

const HyphenIcon = () => <img src="/hyphen-icon.svg" width="16" alt="-" />;

export default HyphenIcon;
