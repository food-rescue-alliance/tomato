import React from 'react';

const RootableIcon = () => <img src="/users-icon-dark.svg" alt="user" />;

export default RootableIcon;
