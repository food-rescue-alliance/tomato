import React from 'react';
import PropTypes from 'prop-types';

const BikeIcon = ({ isSelected }) => (
  <img src={isSelected ? '/bike-icon.svg' : '/bike-icon-disabled.svg'} alt="bike-icon" />
);

BikeIcon.propTypes = {
  isSelected: PropTypes.bool,
};

BikeIcon.defaultProps = {
  isSelected: false,
};

export default BikeIcon;
