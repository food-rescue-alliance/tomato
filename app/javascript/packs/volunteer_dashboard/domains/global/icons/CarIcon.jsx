import React from 'react';
import PropTypes from 'prop-types';

const CarIcon = ({ isSelected }) => (
  <img src={isSelected ? '/car-icon.svg' : '/car-icon-disabled.svg'} alt="car-icon" />
);
CarIcon.propTypes = {
  isSelected: PropTypes.bool,
};
CarIcon.defaultProps = {
  isSelected: false,
};
export default CarIcon;
