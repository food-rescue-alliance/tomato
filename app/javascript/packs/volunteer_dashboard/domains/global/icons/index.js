import BikeIcon from './BikeIcon';
import CarIcon from './CarIcon';
import PickupIcon from './PickupIcon';
import RootableIcon from './RootableIcon';
import SettingsIcon from './SettingsIcon';
import WalkIcon from './WalkIcon';

export {
  BikeIcon,
  CarIcon,
  PickupIcon,
  RootableIcon,
  SettingsIcon,
  WalkIcon,
};
