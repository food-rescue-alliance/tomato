import React from 'react';
import PropTypes from 'prop-types';
import {
  Grid,
  CircularProgress,
  CardContent,
} from '@mui/material';

const Loading = ({ inline }) => {
  if (inline) {
    return (
      <Grid container spacing={3} style={{ position: 'relative' }} data-testid="inlineLoading">
        <Grid item xs={12}>
          <CardContent>
            <Loading />
          </CardContent>
        </Grid>
      </Grid>
    );
  }
  return (
    <div>
      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="center"
        style={{
          position: 'absolute', top: 0, left: 0, right: 0, bottom: 0,
        }}
        data-testid="loading"
      >
        <CircularProgress />
      </Grid>
    </div>
  );
};

Loading.propTypes = {
  inline: PropTypes.bool,
};

Loading.defaultProps = {
  inline: false,
};

export default Loading;
