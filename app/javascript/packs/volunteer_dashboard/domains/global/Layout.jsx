import React from 'react';
import { adaptV4Theme } from '@mui/material/styles';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import {
  BottomNavigation,
  BottomNavigationAction,
  Grid,
  Container,
  Box,
  CssBaseline,
  ThemeProvider,
  StyledEngineProvider,
  createTheme,
} from '@mui/material';
import { makeStyles } from '@mui/styles';
import theme from './theme.json';
import RootableIcon from './icons/RootableIcon';
import SettingsIcon from './icons/SettingsIcon';
import PickupIcon from './icons/PickupIcon';

const useStyles = makeStyles(() => ({
  footer: {
    position: 'fixed',
    bottom: 0,
  },
  content: {
    marginBottom: '50px',
  },
  greeting: {
    fontSize: '18px',
    color: '#274653',
  },
}));

const Layout = ({ children }) => {
  const classes = useStyles();
  const [value, setValue] = React.useState();

  return (
    <Container maxWidth="sm" className={classes.body}>
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={createTheme(adaptV4Theme(theme))}>
          <CssBaseline />
          <Grid
            container
            direction="column"
            justifyContent="space-between"
            alignItems="center"
            style={{
              position: 'absolute', top: 0, left: 0, right: 0, bottom: 0,
            }}
          >
            <Box padding="32px 16px" width="100%" className={classes.content}>
              {children}
            </Box>

            <Box boxShadow={3} width="100%" className={classes.footer}>
              <BottomNavigation
                value={value}
                onChange={(event, newValue) => {
                  setValue(newValue);
                }}
              >
                <BottomNavigationAction icon={<RootableIcon />} component={Link} to="/" />
                <BottomNavigationAction icon={<PickupIcon />} component={Link} to="/pickup" />
                <BottomNavigationAction icon={<SettingsIcon />} component={Link} to="/settings" />
              </BottomNavigation>
            </Box>
          </Grid>
        </ThemeProvider>
      </StyledEngineProvider>
    </Container>
  );
};

Layout.propTypes = {
  children: PropTypes.node,
};

Layout.defaultProps = {
  children: (<div />),
};

export default Layout;
