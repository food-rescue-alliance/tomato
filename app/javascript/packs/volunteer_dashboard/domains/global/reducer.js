import * as types from './actions';

export const defaultState = {
  loading: true,
  user: {},
  timeZones: {
    loading: true,
    values: [],
  },
};

export const reducer = (state, action) => {
  switch (action.type) {
    case types.LOADING: {
      return { ...state, loading: action.data };
    }
    case types.SET_USER: {
      const user = { ...state.user, ...action.data };

      return { ...state, loading: false, user };
    }
    case types.SET_TIME_ZONES_LOADING: {
      return { ...state, timeZones: { ...state.timeZones, loading: action.data } };
    }
    case types.SET_TIME_ZONES: {
      return { ...state, timeZones: { ...state.timeZones, loading: false, values: action.data } };
    }
    case types.LOGOUT: {
      return { ...state, user: {} };
    }
    case types.ERROR: {
      return { ...state, loading: false, error: action.error };
    }
    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
};
