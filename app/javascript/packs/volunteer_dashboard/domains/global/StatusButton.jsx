/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import PropTypes from 'prop-types';
import { Button } from '@mui/material';

const StatusButton = ({ status, children, ...otherProps }) => {
  if (status === 'loading') {
    return (
      <Button
        variant="contained"
        disableElevation
        disabled
        data-testid="status-button-loading"
        {...otherProps}
      >
        {children}
      </Button>
    );
  }
  if (status === 'success') {
    return (
      <Button
        variant="contained"
        color="secondary"
        style={{ background: '#4caf50' }}
        disableElevation
        data-testid="status-button-success"
        {...otherProps}
      >
        Success!
      </Button>
    );
  }
  return (
    <Button
      variant="contained"
      color="primary"
      disableElevation
      data-testid="status-button"
      {...otherProps}
    >
      {children}
    </Button>
  );
};

StatusButton.propTypes = {
  status: PropTypes.string,
  children: PropTypes.node,
};

StatusButton.defaultProps = {
  status: '',
  children: 'Submit',

};

export default StatusButton;
