import React from 'react';
import PropTypes from 'prop-types';
import { TextField as MuiTextField } from '@mui/material';

const TextField = ({ onChange, ...otherProps }) => (
  <MuiTextField
    variant="standard"
    fullWidth
    onChange={(e) => { onChange(e.target.value); }}
    // eslint-disable-next-line react/jsx-props-no-spreading
    {...otherProps}
  />
);

TextField.propTypes = {
  onChange: PropTypes.func.isRequired,
};

export default TextField;
