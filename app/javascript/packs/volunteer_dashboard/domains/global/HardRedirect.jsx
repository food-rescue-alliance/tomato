import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

const HardRedirect = ({ to }) => {
  useEffect(() => {
    window.location.href = to;
  });

  return (
    <div />
  );
};

HardRedirect.propTypes = {
  to: PropTypes.string.isRequired,
};

export default HardRedirect;
