import React from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Grid,
} from '@mui/material';
import { format as phoneFormat } from 'phone-formats';
import { useTranslation } from 'react-i18next';
import { userType } from '../../types';
import { connect } from '../global/context';

export const UserDetailsComponent = ({ onEdit, user }) => {
  const { t } = useTranslation();

  return (
    <div>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Button
            onClick={onEdit}
            variant="outlined"
            disableElevation
          >
            { t('global.edit') }
          </Button>
        </Grid>
        <Grid item xs={12}>
          <div>
            <b>{user.fullName}</b>
          </div>
          {user.address && user.address.zip && (
          <div>
            <div>
              {user.address.streetOne}
            </div>
            {user.address.streetTwo && (
            <div>
              {user.address.streetTwo}
            </div>
            )}
            <div>
              {user.address.city}
              ,
              {user.address.state}
              {' '}
              {user.address.zip}
            </div>
          </div>
          )}
          <div>
            {phoneFormat(user.phone)}
          </div>
          <div>
            {user.email}
          </div>
        </Grid>
      </Grid>
    </div>
  );
};

UserDetailsComponent.propTypes = {
  onEdit: PropTypes.func.isRequired,
  user: userType.isRequired,
};

export default connect(UserDetailsComponent);
