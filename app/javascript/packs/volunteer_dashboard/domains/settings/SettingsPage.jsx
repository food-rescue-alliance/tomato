import React from 'react';
import Settings from './Settings';
import { AbsenceStateProvider } from '../absences/context';

const SettingsPage = () => (
  <AbsenceStateProvider>
    <Settings />
  </AbsenceStateProvider>
);

export default SettingsPage;
