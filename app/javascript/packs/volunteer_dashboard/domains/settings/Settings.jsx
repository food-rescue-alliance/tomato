import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  Box,
  Button,
  Grid,
  Alert,
} from '@mui/material';
import { useTranslation } from 'react-i18next';
import { logout, setUser, fetchTimeZones } from '../global/actions';
import { connect } from '../global/context';
import { Tabs, Tab } from '../global/Tabs';
import Absences from '../absences/Absences';
import UserSettingsForm from './UserSettingsForm';
import UserDetails from './UserDetails';

export const SettingsComponent = ({ dispatch }) => {
  const { t } = useTranslation();

  const [editing, setEditing] = useState(false);
  const [alertMessage, setAlertMessage] = useState();
  const onLogout = () => {
    dispatch(logout());
  };

  useEffect(() => dispatch(fetchTimeZones()), []);
  useEffect(() => () => { dispatch(setUser({ error: undefined })); }, []);

  return (
    <Grid container spacing={3} pb={7}>
      {alertMessage && (
        <Grid item xs={12}>
          <Alert severity={alertMessage[0]}>{alertMessage[1]}</Alert>
        </Grid>
      )}
      {editing ? (
        <Grid item xs={12}>
          <UserSettingsForm
            onCancel={() => {
              dispatch(setUser({ error: undefined }));
              setEditing(false); setAlertMessage();
            }}
            onSuccess={(message) => { setEditing(false); setAlertMessage(['success', message]); }}
            onError={(message) => { setAlertMessage(['error', message]); }}
          />
        </Grid>
      ) : (
        <Grid item xs={12}>
          <UserDetails onEdit={() => { setEditing(true); setAlertMessage(); }} />
        </Grid>
      )}
      <Grid item xs={12}>
        <Box mb={2}>
          <Tabs value={0} textColor="primary">
            <Tab label="Availability" />
          </Tabs>
        </Box>
        <Absences />
      </Grid>
      <Grid item xs={12}><hr /></Grid>
      <Grid item xs={12}>
        <Button
          onClick={onLogout}
          variant="outlined"
          disableElevation
        >
          {t('settings.logout')}
        </Button>
      </Grid>
    </Grid>
  );
};

SettingsComponent.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

export default connect(SettingsComponent);
