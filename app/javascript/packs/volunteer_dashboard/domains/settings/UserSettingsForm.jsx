import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Select,
} from '@mui/material';
import { useTranslation } from 'react-i18next';
import { format as phoneFormat } from 'phone-formats';
import { userType, timeZoneType } from '../../types';
import { connect } from '../global/context';
import { updateUser } from '../global/actions';
import TextField from '../global/TextField';
import StatusButton from '../global/StatusButton';

export const UserSettingsFormComponent = ({
  dispatch, onCancel, onSuccess, onError, user, timeZones,
}) => {
  const { t } = useTranslation();
  const [fullName, setFullName] = useState(user.fullName || '');
  const [email, setEmail] = useState(user.email || '');
  const [timeZone, setTimeZone] = useState(user.timeZone || '');
  const [phone, setPhone] = useState(phoneFormat(user.phone) || '');
  const [address, setAddress] = useState(user.address || {
    streetOne: '', streetTwo: '', city: '', state: '', zip: '',
  });
  const [buttonState, setButtonState] = useState();

  const onChangePhone = (val) => {
    setPhone(phoneFormat(val));
  };

  const onFormSubmit = (e) => {
    e.preventDefault();
    setButtonState('loading');
    dispatch(updateUser(fullName, email, phone, address, timeZone));
  };

  useEffect(() => {
    if (user.error) {
      setButtonState();
      onError(t('global.formError'));
    } else if (buttonState === 'loading' && !user.loading) {
      onSuccess(t('settings.settingsUpdateSuccess'));
    }
  }, [user]);

  return (
    <form onSubmit={onFormSubmit}>
      <Grid container spacing={3}>
        <Grid item xs={4}>
          <Button
            onClick={onCancel}
            variant="outlined"
            disableElevation
          >
            {t('global.cancel')}
          </Button>
        </Grid>
        <Grid item xs={8}>
          <StatusButton
            type="submit"
            variant="contained"
            color="primary"
            disableElevation
            status={buttonState}
          >
            {t('global.submit')}
          </StatusButton>
        </Grid>
        <Grid item xs={12}>
          <TextField
            error={user.error && 'fullName' in user.error}
            id="fullName"
            label={t('settings.fullName')}
            variant="outlined"
            value={fullName}
            onChange={setFullName}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            error={user.error && 'address.streetOne' in user.error}
            id="streetOne"
            label={t('settings.streetOne')}
            variant="outlined"
            value={address.streetOne}
            onChange={(val) => { setAddress({ ...address, streetOne: val }); }}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            error={user.error && 'address.streetTwo' in user.error}
            id="streetTwo"
            label={t('settings.streetTwo')}
            variant="outlined"
            value={address.streetTwo}
            onChange={(val) => { setAddress({ ...address, streetTwo: val }); }}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            error={user.error && 'address.city' in user.error}
            id="city"
            label={t('settings.city')}
            variant="outlined"
            value={address.city}
            onChange={(val) => { setAddress({ ...address, city: val }); }}
          />
        </Grid>
        <Grid item xs={5}>
          <TextField
            error={user.error && 'address.state' in user.error}
            id="state"
            label={t('settings.state')}
            variant="outlined"
            value={address.state}
            onChange={(val) => { setAddress({ ...address, state: val }); }}
          />
        </Grid>
        <Grid item xs={7}>
          <TextField
            error={user.error && 'address.zip' in user.error}
            id="zip"
            label={t('settings.zip')}
            variant="outlined"
            value={address.zip}
            onChange={(val) => { setAddress({ ...address, zip: val }); }}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            error={user.error && 'phone' in user.error}
            id="phone"
            label={t('settings.phone')}
            variant="outlined"
            value={phone}
            onChange={onChangePhone}
          />
        </Grid>
        <Grid item xs={12}>
          <FormControl variant="outlined" fullWidth>
            <InputLabel id="timeZoneLabel">{t('settings.timeZone')}</InputLabel>
            <Select
              error={user.error && 'timeZone' in user.error}
              disabled={timeZones.loading}
              id="timeZone"
              labelId="timeZoneLabel"
              label={t('settings.timeZone')}
              variant="outlined"
              value={timeZone}
              onChange={(e) => setTimeZone(e.target.value)}
            >
              {timeZones.values.map((tz) => (
                <MenuItem key={tz.name} value={tz.id}>{tz.name}</MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          <TextField
            error={user.error && 'email' in user.error}
            id="email"
            label={t('settings.email')}
            variant="outlined"
            value={email}
            onChange={setEmail}
          />
        </Grid>
      </Grid>
    </form>
  );
};

UserSettingsFormComponent.propTypes = {
  dispatch: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired,
  onError: PropTypes.func.isRequired,
  user: userType.isRequired,
  timeZones: timeZoneType.isRequired,
};

export default connect(UserSettingsFormComponent);
