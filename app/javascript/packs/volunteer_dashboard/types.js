import PropTypes from 'prop-types';

export const addressType = PropTypes.shape({
  streetOne: PropTypes.string,
  streetTwo: PropTypes.string,
  city: PropTypes.string,
  state: PropTypes.string,
  zip: PropTypes.string,
});

export const userType = PropTypes.shape({
  id: PropTypes.number,
  fullName: PropTypes.string,
  email: PropTypes.string,
  phone: PropTypes.string,
  address: addressType,
});

export const timeZoneType = PropTypes.shape({
  loading: PropTypes.bool,
  values: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
  })),
});

export const foodTaxonomyType = PropTypes.shape({
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  name: PropTypes.string,
});

export const logType = PropTypes.shape({
  weight: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  temperature: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  notes: PropTypes.string,
  error: PropTypes.bool,
  loading: PropTypes.bool,
  foodTaxonomyId: PropTypes.number,
});

export const siteType = PropTypes.shape({
  name: PropTypes.string,
  siteType: PropTypes.string,
  instructions: PropTypes.string,
  addresss: addressType,
});

export const taskType = PropTypes.shape({
  id: PropTypes.number,
  description: PropTypes.string,
  site: siteType,
  log: logType,
});

export const shiftType = PropTypes.shape({
  id: PropTypes.string,
  startsAt: PropTypes.string,
  endsAt: PropTypes.string,
  organizationId: PropTypes.number,
  shiftId: PropTypes.number,
  shiftEventId: PropTypes.number,
  flagged: PropTypes.boolean,
  tasks: PropTypes.arrayOf(taskType),
});

export const alertType = PropTypes.shape({
  id: PropTypes.number,
  type: PropTypes.string,
  record: PropTypes.string,
  message: PropTypes.string,
});
