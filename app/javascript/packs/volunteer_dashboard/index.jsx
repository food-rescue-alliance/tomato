// Run this example by adding <%= javascript_pack_tag 'index' %> to the head of your layout file,
// like app/views/layouts/application.html.erb. All it does is render <div>Hello React</div> at
// the bottom of the page.

import React from 'react';
import { createRoot } from 'react-dom/client';
import { I18nextProvider } from 'react-i18next';
import App from './app';
import i18n from './i18n';
import { StateProvider } from './domains/global/context';

document.addEventListener('DOMContentLoaded', () => {
  createRoot(document.body.appendChild(document.createElement('div'))).render(
    <StateProvider>
      <I18nextProvider i18n={i18n}>
        <App name="React" />
      </I18nextProvider>
    </StateProvider>,
  );
});
