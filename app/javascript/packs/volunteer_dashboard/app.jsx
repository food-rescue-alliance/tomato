import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  BrowserRouter as Router,
  Routes as ReactRouterRoutes,
  Route,
} from 'react-router-dom';
import { SnackbarProvider } from 'notistack';
import { userType } from './types';
import { connect } from './domains/global/context';
import { fetchUser } from './domains/global/actions';
import Layout from './domains/global/Layout';
import Settings from './domains/settings/SettingsPage';
import HardRedirect from './domains/global/HardRedirect';
import Loading from './domains/global/Loading';
import Dashboard from './domains/dashboard/DashboardPage';
import ShiftDetails from './domains/shift_details/ShiftDetailsPage';
import Error from './domains/global/Error';
import PickupShifts from './domains/pickup_shift/PickupShiftsPage';

const NoMatch = () => (<div>404</div>);

const RedirectToLogin = () => (
  <div>
    Redirecting to login...
    <HardRedirect to="/auth/login" />
  </div>
);

const Routes = () => (
  <ReactRouterRoutes>
    <Route path="/" element={<Dashboard />} />
    <Route path="/settings" element={<Settings />} />
    <Route path="/shifts/:id" element={<ShiftDetails />} />
    <Route path="/pickup" element={<PickupShifts />} />
    <Route path="*" element={<NoMatch />} />
  </ReactRouterRoutes>
);

const App = ({
  user,
  loading,
  error,
  dispatch,
}) => {
  useEffect(() => { dispatch(fetchUser()); }, []);

  const protectedRender = () => {
    if (loading === true) {
      return <Loading />;
    }

    if (error) {
      return <Error />;
    }

    if (loading === false && !user.id) {
      return <RedirectToLogin />;
    }

    return <Layout><Routes /></Layout>;
  };

  return (
    <Router>
      <SnackbarProvider maxSnack={2} autoHideDuration={5000}>
        {protectedRender()}
      </SnackbarProvider>
    </Router>
  );
};

App.propTypes = {
  user: userType,
  loading: PropTypes.bool,
  error: PropTypes.shape({
    status: PropTypes.number,
  }),
  dispatch: PropTypes.func.isRequired,
};

App.defaultProps = {
  error: null,
  user: null,
  loading: true,
};

export default connect(App);
