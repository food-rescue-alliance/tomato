class OrganizationPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      if user.ops_coordinator?
        scope.all
      elsif user.org_admin?
        scope.joins(:users).where users: { id: user }
      end
    end
  end

  def index?
    user.administrates_multiple_orgs?
  end

  def show?
    user.ops_coordinator? || (user.org_admin? && user.organizations.exists?(record.id))
  end

  def new?
    user.ops_coordinator?
  end

  def edit?
    user.ops_coordinator? || (user.org_admin? && user.organizations.exists?(record.id))
  end

  def create?
    user.ops_coordinator?
  end

  def update?
    user.ops_coordinator? || (user.org_admin? && user.organizations.exists?(record.id))
  end

  def destroy?
    user.ops_coordinator?
  end
end
