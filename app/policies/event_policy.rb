class EventPolicy < ApplicationPolicy
  def index?
    user.ops_coordinator? || user.org_admin?(record.ownable)
  end

  def show?
    user.ops_coordinator? || user.org_admin?(record.ownable) || user.volunteer?(record.ownable)
  end

  def update?
    user.ops_coordinator? || user.org_admin?(record.ownable) ||
      (record.shift_event? && record.shift_event.include?(user))
  end

  def destroy?
    user.ops_coordinator? || user.org_admin?(record.ownable) ||
      (record.shift_event? && record.shift_event.include?(user))
  end
end
