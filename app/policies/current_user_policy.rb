class CurrentUserPolicy < ApplicationPolicy
  def edit?
    user.ops_coordinator? || user.org_admin?
  end

  def update?
    user.ops_coordinator? || user.org_admin?
  end
end
