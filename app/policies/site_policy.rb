class SitePolicy < ApplicationPolicy
  def index?
    user.ops_coordinator? || (user.org_admin? && user.organizations.exists?(record.id))
  end

  def new?
    user.ops_coordinator? || (user.org_admin? && user.organizations.exists?(record.id))
  end

  def create?
    user.ops_coordinator? || (user.org_admin? && user.organizations.exists?(record.id))
  end

  def show?
    user.ops_coordinator? || (user.org_admin? && user.organizations.exists?(record.id))
  end

  def edit?
    user.ops_coordinator? || (user.org_admin? && user.organizations.exists?(record.id))
  end

  def update?
    user.ops_coordinator? || (user.org_admin? && user.organizations.exists?(record.id))
  end
end
