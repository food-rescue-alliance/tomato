class OrganizationInvitationPolicy < ApplicationPolicy
  def new?
    user.ops_coordinator? || (user.org_admin? && user.organizations.exists?(record.id))
  end

  def create?
    user.ops_coordinator? || (user.org_admin? && user.organizations.exists?(record.id))
  end
end
