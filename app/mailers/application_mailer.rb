class ApplicationMailer < ActionMailer::Base
  default from: "Food Rescue Alliance <software@foodrescuealliance.org>"
  layout "mailer"

  def volunteer_overflow_flag_email
    mail(to: params[:organization].internal_alerts_email,
         body: email_body(params[:volunteers], params[:conflicts], params[:organization]),
         content_type: "text/html",
         subject: "[ROOTABLE] Multiple volunteers were assigned to the same shift.")
  end

  def email_body(volunteers, conflicts, organization)
    time_zone = ActiveSupport::TimeZone.new(organization.time_zone)

    body = "The volunteer(s) #{volunteers.map(&:full_name_or_email).to_sentence} "
    body += "were added to the following shifts which were already assigned:

"

    conflicts.each do |conflict|
      body += "#{conflict.title} on #{time_zone.at(conflict.starts_at).strftime("%A %b %e at %l:%M%p")} "
      body += "with #{conflict.eventable.volunteers.map(&:full_name_or_email).to_sentence}
#{shift_event_occurrence_url(conflict.id)}

"
    end

    body += "Please verify that volunteer staffing for these shifts is correct."
  end
end
