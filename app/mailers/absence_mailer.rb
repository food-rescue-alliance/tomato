class AbsenceMailer < ApplicationMailer
  def scheduled_email
    start_date = params[:selected_dates][:start_date]
    end_date = params[:selected_dates][:end_date]
    @date_range = start_date == end_date ? start_date : "#{start_date} to #{end_date}"
    @name = params[:full_name]
    @shifts = params[:affected_shifts]
    unique_shift_names = @shifts.pluck(:title).uniq

    mail(
      to: params[:email],
      subject: "[Absence Scheduled] Impacting #{unique_shift_names.join(", ")}."
    )
  end

  def updated_email
    old_start_date = params[:old_dates][:start_date]
    old_end_date = params[:old_dates][:end_date]
    updated_start_date = params[:updated_dates][:start_date]
    updated_end_date = params[:updated_dates][:end_date]
    @name = params[:full_name]
    @old_date_range = old_start_date == old_end_date ? old_start_date : "#{old_start_date} - #{old_end_date}"
    @updated_date_range =
      updated_start_date == updated_end_date ? updated_start_date : "#{updated_start_date} - #{updated_end_date}"

    mail(
      to: params[:email],
      subject: "[Absence Edited] #{@name}"
    )
  end

  def volunteer_updated_email
    @organization_name = params[:organization_name]

    mail(
      to: params[:email],
      subject: "[Action Needed] You edited a scheduled absence with #{@organization_name}"
    )
  end

  def canceled_email
    start_date = params[:selected_dates][:start_date]
    end_date = params[:selected_dates][:end_date]
    @name = params[:full_name]
    @date_range = start_date == end_date ? start_date : "#{start_date} - #{end_date}"

    mail(
      to: params[:email],
      subject: "[Absence Deleted] #{@name}"
    )
  end

  def volunteer_canceled_email
    @organization_name = params[:organization_name]

    mail(
      to: params[:email],
      subject: "[Action Needed] You deleted a scheduled absence with #{@organization_name}"
    )
  end
end
