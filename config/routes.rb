Rails.application.routes.draw do
  namespace :api do
    resources :food_taxonomies, only: %i[index]
    resources :time_zones, only: %i[index]
    resources :shift_event_occurrences, only: %i[index show] do
      resources :task_logs, only: %i[create]
      resource :shift_event_note, path: "note", only: %i[show create]
    end
    resources :users, only: %i[show update] do
      resources :absences, only: %i[index create update destroy]
      resources :alerts, only: %i[index]
      resources :shift_event_occurrences, only: %i[index] do
        post "/pickup", to: "pickup#pickup", as: :pickup_shift
      end
      get "shift_event_occurrences/pickup", to: "pickup#index", as: :list_pickup_shifts
    end
    resources :organizations do
      resources :shifts
    end
  end

  resource :current_user, only: %i[edit update]
  resources :users, only: %i[show update]
  resources :organizations do
    resources :shifts
    resources :sites
    resources :shift_event_occurrences
    resources :users, only: %i[index show]
    resources :receipts, only: %i[new create]
    resources :csv_exporters, only: %i[create index]
  end

  resources :shift_event_occurrences, only: %i[show edit update destroy] do
    resources :task_logs, only: %i[create]
  end

  devise_for :users, path: "auth", path_names: { sign_in: "login", sign_out: "logout" }, skip: [:invitation]
  devise_scope :user do
    get "/organizations/:id/invite", to: "organization_invitations#new", as: :new_user_invitation
    post "/organizations/:id/invite", to: "organization_invitations#create", as: :create_user_invitation
    post "/organizations/:organization_id/users/:id/resend_invite", to: "users#resend_invite",
      as: :resend_user_invitation
    get "/auth/invitation/accept", to: "organization_invitations#edit", as: :accept_user_invitation
    put "/auth/invitation", to: "organization_invitations#update", as: :user_invitation
  end

  get "/rrule", to: "rrule#index"

  root to: "pages#root"

  match "*path", to: "pages#volunteer_root", via: :all
end
