# Acceptance Criteria
1. As a `Butterfly`, I can fly


# Implementation Details
1. No bugs please


# Tasks
- [ ] Code
- [ ] Test

/label priority::pending
/label type::feature
