// setupJest.js
global.fetch = () => {
  throw 'DO NOT CALL fetch() FROM TESTS';
};
global.Request = () => {
  throw 'DO NOT CALL Request FROM TESTS';
};

// Mock react-i18next
jest.mock('react-i18next', () => ({
  // this mock makes sure any components using the translate hook can use it without a warning being shown
  useTranslation: () => {
    return {
      t: (str) => str,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    };
  },
}));