namespace :robot do
  desc "Imports a region from the robot"
  task import_region: :environment do
    if ENV["REGION_ID"].blank?
      raise "ERROR: No ID provided. Please provide a region ID: 'REGION_ID=123 rails robot:import_region'"
    end
    if ENV["ROBOT_DATABASE_URL"].blank?
      raise "ERROR: No Robot DB specified. Please ensure ROBOT_DATABASE_URL is configured in the environment."
    end

    if ENV["CUTOVER_DATE"].blank?
      raise "ERROR: No cutover date specified, i.e. the day this region will start using Rootable. " \
            "Please provide a date: 'CUTOVER_DATE=#{Time.zone.today} rails robot:import_region'"
    end
    RobotImportService.import_region(ENV["REGION_ID"].to_i, logger: ActiveSupport::Logger.new($stdout),
cutoff_date: ENV["CUTOVER_DATE"])
  end
end
