namespace :seed_tasks do
  desc "Renames the Prepared Food food taxonomies to distinguish them"
  task rename_prepared_foods_food_taxonomy: :environment do
    FoodTaxonomy.find_by(feeding_america_id: "E70047", name: "Prepared Food").update(name: "Prepared Meals (frozen)")
    FoodTaxonomy
      .find_by(feeding_america_id: "E70529", name: "Prepared Food")
      .update(name: "Prepared Meals (refrigerated)")
  end
end
