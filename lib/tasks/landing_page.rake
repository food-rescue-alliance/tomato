namespace :landing_page do
  desc "Update the landing page"
  task update: :environment do
    landing_pate_path = Rails.public_path
    submodule_path = Rails.public_path.join("rootable-landing-page/public")

    `git submodule update`
    `cp -r #{submodule_path}/* #{landing_pate_path}`
    `mv #{landing_pate_path}/index.html #{landing_pate_path}/landing.html`
  end
end
