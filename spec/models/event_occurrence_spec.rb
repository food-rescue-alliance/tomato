require "rails_helper"

RSpec.describe EventOccurrence, type: :model do
  describe "event" do
    it "returns event attributes" do
      starts_at = Time.current
      ends_at = starts_at + 1.day
      event = build :event

      subject = EventOccurrence.new starts_at: starts_at, ends_at: ends_at, event: event

      expect(subject.uid).to eq(event.uid)
      expect(subject.starts_at).to eq(starts_at)
      expect(subject.ends_at).to eq(ends_at)
      expect(subject.title).to eq(event.title)
      expect(subject.starts_at).to_not eq(event.starts_at)
      expect(subject.ends_at).to_not eq(event.ends_at)
      expect(subject.eventable).to eq(event.eventable)
      expect(subject.ownable).to eq(event.ownable)
    end
  end

  describe "head?" do
    it "returns true given single event" do
      event = build :event, :single
      starts_at = event.starts_at
      ends_at = starts_at + 1.day

      subject = EventOccurrence.new starts_at: starts_at, ends_at: ends_at, event: event

      expect(subject.head?).to be(true)
    end

    it "returns true given recurring event at exact start time" do
      event = build :event, :weekly_on_mondays
      starts_at = event.starts_at
      ends_at = starts_at + 1.day

      subject = EventOccurrence.new starts_at: starts_at, ends_at: ends_at, event: event

      expect(subject.head?).to be(true)
    end

    it "returns true given a single event" do
      event = build :event, :single
      starts_at = Time.current
      ends_at = starts_at + 1.day

      subject = EventOccurrence.new starts_at: starts_at, ends_at: ends_at, event: event

      expect(subject.head?).to be(true)
    end

    it "returns false given a single event" do
      event = build :event, :weekly_on_mondays
      second_monday_start = Time.iso8601 "2020-10-12T03:00:00Z"
      second_monday_end = Time.iso8601 "2020-10-12T05:00:00Z"

      subject = EventOccurrence.new starts_at: second_monday_start, ends_at: second_monday_end, event: event

      expect(subject.head?).to be(false)
    end
  end

  describe "id" do
    it "returns unique identifier given a different start time" do
      event = build :event, uid: Faker::Lorem.words.join("-")

      occurrence = EventOccurrence.new starts_at: event.starts_at, ends_at: event.ends_at, event: event
      subject = EventOccurrence.new starts_at: Time.zone.now, ends_at: event.ends_at, event: event

      expect(subject.id).not_to eq(occurrence.id)
    end
  end
end
