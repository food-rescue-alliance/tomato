require "rails_helper"

describe Robot::Volunteer do
  describe "#region_admin?" do
    let(:volunteer) { create(:robot_volunteer) }
    let(:region) { create(:robot_region) }
    subject(:region_admin?) { volunteer.region_admin?(region) }

    context "when the user is an admin in that region" do
      before { create(:robot_assignment, admin: true, volunteer: volunteer, region: region) }
      it { is_expected.to be_truthy }
    end

    context "when the user is not assigned to the region" do
      it { is_expected.to be_falsy }
    end

    context "when the user is not an admin in that region" do
      before { create(:robot_assignment, volunteer: volunteer, region: region) }
      it { is_expected.to be_falsy }
    end
  end

  describe "#has_shifts?" do
    let(:region) { create(:robot_region) }
    let(:volunteer) { create(:robot_volunteer, regions: [region]) }
    let(:schedule_chain) { create(:robot_schedule_chain, region_id: region.id, active: true) }
    let!(:schedule_volunteer) do
      create(:robot_schedule_volunteer, schedule_chain_id: schedule_chain.id, volunteer_id: volunteer.id, active: true)
    end
    subject(:region_admin?) { volunteer.has_shifts?(region) }
    context "when the chain and assignment are active" do
      it { is_expected.to be_truthy }
    end
    context "when the chain is inactive" do
      before { schedule_chain.update!(active: false) }
      it { is_expected.to be_falsy }
    end
    context "when the assignment (i.e. schedule_volunteer) is inactive" do
      before { schedule_volunteer.update!(active: false) }
      it { is_expected.to be_falsy }
    end
  end
end
