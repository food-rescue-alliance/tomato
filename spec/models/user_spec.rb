require "rails_helper"

describe User do
  describe "associations" do
    it { is_expected.to have_many(:absences) }
  end
  describe "validate_email" do
    it "returns true for valid email address" do
      user = create(:volunteer, email: "foo@bar.com")

      result = user.validate_email

      expect(result).to eq(true)
    end

    it "returns errors for invalid email address" do
      expect { create(:volunteer, email: "foo") }.to raise_error(ActiveRecord::RecordInvalid)
    end
  end
end
