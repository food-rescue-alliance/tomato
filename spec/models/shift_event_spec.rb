require "rails_helper"

describe ShiftEvent, type: :model do
  describe "event" do
    it "returns event attributes" do
      subject = create :shift_event

      Event.attribute_names.each do |attr|
        expect(subject).to respond_to(attr)
        expect(subject).to respond_to("#{attr}=")
      end
    end
  end

  describe "tasks" do
    it "preserves the order" do
      subject = create :shift_event

      subject.ownable.sites = (build_list(:donor, 3) + build_list(:recipient, 3)).shuffle
      subject.update(ordered_task_ids: subject.ownable.tasks.ids.reverse)
      expect(subject.reload.task_ids).to eql(subject.ownable.tasks.ids.reverse)
    end
  end

  describe "missing_task_log?" do
    it "returns true when missing task logs" do
      subject = create(
        :shift_event,
        :with_tasks
      )

      expect(subject.missing_task_log?).to eq(true)
    end

    it "returns false when task logs are present" do
      subject = create(
        :shift_event,
        :with_tasks
      )
      create :task_log, shift_event: subject

      expect(subject.missing_task_log?).to eq(false)
    end
  end
end
