require "rails_helper"

describe Site do
  describe "validations" do
    subject { create(:site) }

    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_uniqueness_of(:name).scoped_to(:organization_id) }
  end

  describe "associations" do
    it { is_expected.to belong_to(:organization) }
  end
end
