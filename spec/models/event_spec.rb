require "rails_helper"

RSpec.describe Event, type: :model do
  describe ".on_or_after" do
    before do
      @owner = create :organization
    end

    it "returns recurring events starting in the past of given start time" do
      starts_at = Time.iso8601("2020-10-01T05:00:00Z")
      event_from_last_month = create :event, :weekly_on_mondays,
                                     starts_at: starts_at - 1.month,
                                     ownable: @owner
      event_from_last_year = create :event, :weekly_on_mondays,
                                    starts_at: starts_at - 1.year,
                                    ownable: @owner

      results = Event.where(ownable: @owner).on_or_after starts_at

      expect(results.length).to be(2)
      expect(results).to include(event_from_last_month)
      expect(results).to include(event_from_last_year)
    end

    it "returns empty array given recurring events in the past that have ended before start time" do
      starts_at = Time.iso8601("2020-10-05T05:00:00Z")
      create :event, :weekly_on_mondays,
             starts_at: starts_at - 1.month,
             ownable: @owner,
             rrule: "FREQ=WEEKLY;INTERVAL=1;BYDAY=MO;UNTIL=20201004T050000Z"
      create :event, :weekly_on_mondays,
             starts_at: starts_at - 1.year,
             ownable: @owner,
             rrule: "FREQ=WEEKLY;INTERVAL=1;BYDAY=MO;UNTIL=20200905T050000Z"

      results = Event.where(ownable: @owner).on_or_after starts_at

      expect(results).to be_empty
    end

    it "returns events into the future of given start time" do
      starts_at = Time.iso8601("2020-10-05T05:00:00Z")
      create :event, :weekly_on_mondays,
             starts_at: starts_at - 1.year,
             ownable: @owner,
             rrule: "FREQ=WEEKLY;INTERVAL=1;BYDAY=MO;UNTIL=20201004T000000Z"
      event_from_last_month = create :event, :weekly_on_mondays,
                                     starts_at: starts_at - 1.month,
                                     ownable: @owner
      event_week_into_future = create :event, :single,
                                      starts_at: starts_at + 1.week,
                                      ownable: @owner
      event_month_into_future = create :event, :weekly_on_mondays,
                                       starts_at: starts_at + 1.month,
                                       ownable: @owner

      results = Event.where(ownable: @owner).on_or_after starts_at

      expect(results.length).to be(3)
      expect(results).to include(event_from_last_month)
      expect(results).to include(event_week_into_future)
      expect(results).to include(event_month_into_future)
    end

    it "returns events exactly matching given start time" do
      date_time = Time.iso8601("2020-10-01T05:00:00Z")
      single_event = create(:shift, starts_at: date_time).shift_events.first.event

      results = Event.on_or_after date_time

      expect(results.length).to be(1)
      expect(results).to include(single_event)
      expect(results.last.starts_at).to eq(date_time)
    end

    it "returns empty array by default" do
      date_time = Time.iso8601("2020-10-01T05:00:00Z")
      result = Event.on_or_after date_time

      expect(result).to be_empty
    end
  end

  describe ".all_by" do
    it "returns events for given owner" do
      events = create_list :event, 3
      subject = events.first
      create_list :event, 2, ownable: subject.ownable

      results = Event.all_by subject.ownable, subject.eventable.class, 1.month.ago

      expect(subject.ownable).to_not eq(events.last.ownable)
      expect(results.length).to eq(3)
      expect(results.first.eventable.id).to be_a_kind_of(Numeric)
      expect(results[0].ownable).to eq(subject.ownable)
      expect(results[1].ownable).to eq(subject.ownable)
      expect(results[2].ownable).to eq(subject.ownable)
    end

    it "returns an empty array given an owner with no events" do
      owner = create :organization

      results = Event.all_by owner, ShiftEvent, 1.month.ago

      expect(results).to be_empty
    end
  end

  describe "valid?" do
    let(:org) { create(:organization) }

    it "returns false when rrule is not valid" do
      # Given
      invalid_rrule = "FREQ=asdf"
      event = Event.new(
        title: "New Event",
        starts_at: Time.current,
        ends_at: Time.current,
        eventable: User.new,
        rrule: invalid_rrule
      )

      # When
      result = event.valid?
      errors = event.errors

      # Then
      expect(result).to be(false)
      expect(errors).to_not be_empty
    end

    it "returns true when rrule is valid" do
      # Given
      valid_rrule = "FREQ=DAILY"
      event = Event.new(
        title: "New Event",
        starts_at: Time.current,
        ends_at: Time.current,
        eventable: User.new,
        ownable: org,
        rrule: valid_rrule
      )

      # When
      result = event.valid?
      errors = event.errors

      # Then
      expect(result).to be(true)
      expect(errors).to be_empty
    end
  end

  describe "recurrence?" do
    it "returns true" do
      event = Event.new(rrule: "FREQ=DAILY")
      expect(event.recurrence?).to be true
    end

    it "returns false" do
      expect(Event.new.recurrence?).to be false
    end
  end
end
