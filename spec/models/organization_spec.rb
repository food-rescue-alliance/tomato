require "rails_helper"

describe Organization, type: :model do
  describe "shifts" do
    subject { create :organization }

    it "returns an empty array by default" do
      result = subject.shifts

      expect(result).to be_empty
    end

    it "returns shifts for organization given it has shifts" do
      shifts = create_list :shift, Faker::Number.digit,
                           :series,
                           organization: subject

      result = subject.shifts

      expect(result.length).to be(shifts.length)
    end

    it "returns an empty array given other organizations have shifts" do
      other_organization = create :organization
      create :shift, organization: other_organization

      result = subject.shifts

      expect(result).to be_empty
      expect(other_organization.shifts.length).to be(1)
    end
  end

  describe "valid?" do
    subject { Organization.new(name: Faker::Company.name) }

    it "returns true given valid values" do
      subject.time_zone = "Etc/UTC"

      expect(subject.valid?).to be(true)
    end

    it "returns false given invalid time zone" do
      subject.time_zone = Faker::Lorem.word

      expect(subject.valid?).to be(false)
    end
  end
end
