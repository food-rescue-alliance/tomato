require "rails_helper"

describe ShiftEventNote do
  subject { create(:shift_event_note) }

  describe "associations" do
    it { is_expected.to belong_to(:shift_event) }
  end
end
