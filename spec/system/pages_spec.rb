require "rails_helper"

describe "Static Pages" do
  let(:user) { create(:ops_coordinator) }

  it "/ authenticates users", js: true do
    visit root_path
    click_on "Login"

    fill_in "Email", with: user.email
    fill_in "Password", with: "password"
    click_on "Log in"
    click_on "logout-menu-toggle"
    click_link("Logout")
    expect(page).to have_content("LOGIN")
  end
end
