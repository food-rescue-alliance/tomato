require "rails_helper"

describe "Creating an absence", js: true do
  let(:volunteer) { create(:volunteer, organizations: [organization, other_organization]) }
  let(:organization) { create(:organization, :with_internal_alerts_email) }
  let(:other_organization) { create(:organization) }
  let(:impacted_shift) { create(:shift, :single, organization: organization, starts_at: 1.week.from_now) }
  let(:other_shift) { create(:shift, :single, organization: organization, starts_at: 2.days.from_now) }
  let(:impacted_shift_for_other_org) do
    create(:shift, :single, organization: other_organization, starts_at: 15.days.from_now)
  end

  before do
    [impacted_shift, other_shift, impacted_shift_for_other_org].each do |shift|
      shift.shift_events.last.user_ids = [volunteer.id]
    end
  end

  it "lets a volunteer create an absence" do
    sign_in volunteer
    visit root_path

    expect(page).to have_content(volunteer.full_name)
    expect(page).to have_content(impacted_shift.name)
    expect(page).to have_content(other_shift.name)

    click_on("user")

    expect(page).to have_content(volunteer.email)
    find("[aria-label='Choose start date']").click # Needs to match buttons and inputs with this aria-label
    first(".MuiPickersDay-root:not(:disabled)").click
    click_on "OK" if page.has_button?("OK") # If runnning in mobile view, there will be an OK button
    find("[aria-label='Choose end date']").click
    first(".MuiPickersDay-root:not(:disabled)").click
    click_on "OK" if page.has_button?("OK")  # If runnning in mobile view, there will be an OK button
    click_on("Submit Absence")

    human_readable_date_range = 1.week.from_now.strftime("%A, %b %-d")
    expect(page).to have_content(human_readable_date_range)

    raw_email = ActionMailer::Base.deliveries.first
    expect(raw_email.to).to eql([organization.internal_alerts_email])
    expect(raw_email.body.encoded).to include(impacted_shift.name)
    expect(raw_email.body.encoded).not_to include(other_shift.name)
    expect(raw_email.body.encoded).not_to include(impacted_shift_for_other_org.name)
  end
end
