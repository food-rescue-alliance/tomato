require "rails_helper"

describe "Accepting user invitations" do
  before do
    @users = [
      User.invite!(attributes_for(:volunteer)),
      User.invite!(attributes_for(:org_admin))
    ]
  end

  context "when the invited user is a volunteer", js: true do
    let(:user) { User.invite!(attributes_for(:volunteer)) }

    it "logs them in" do
      name = Faker::Name.name
      accept_invitation(user, name)

      expect(page).to have_content(name)
    end
  end

  context "when the invited user is an org admin" do
    let(:user) { User.invite!(attributes_for(:org_admin)) }

    it "shows a success message" do
      accept_invitation(user)
      expect(page).to have_content I18n.t("devise.invitations.updated")
    end
  end

  def accept_invitation(user, name = Faker::Name.name)
    visit accept_user_invitation_path(invitation_token: user.raw_invitation_token)

    expect(page).to have_content I18n.t("devise.invitations.edit.header")

    click_on "Enter"
    # TODO: https://gitlab.com/food-rescue-alliance/tomato/-/issues/88
    # expect(find(class: "mdc-form-field", text: "Full name")).to have_content "can't be blank"
    expect(find(class: "mdc-form-field", text: "Password", match: :prefer_exact)).to have_content "can't be blank"
    expect(page).to have_content "can't be blank"

    fill_in I18n.t("devise.invitations.edit.name"), with: name
    fill_in I18n.t("devise.invitations.edit.password"), with: "password"
    fill_in I18n.t("devise.invitations.edit.password_confirmation"), with: "password"
    click_on I18n.t("devise.invitations.edit.submit_button")
  end
end
