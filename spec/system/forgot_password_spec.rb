require "rails_helper"

describe "Forgot password flow" do
  let(:user) { create(:volunteer) }

  it "sends a user a reset password email" do
    visit root_path
    click_on "Login"
    click_on "Forgot Password"

    fill_in "Email", with: user.email
    click_on "Send Email"

    raw_email = Devise.mailer.deliveries.first.body.raw_source
    reset_token = raw_email.match(/reset_password_token=(.{20})/)[1]

    expect(page).to have_content(
      "You will receive an email with instructions on how to reset your password in a few minutes."
    )

    visit edit_user_password_path(reset_password_token: reset_token)
    expect(page).to have_content "Enter new password"
    fill_in "New password", with: "password"
    fill_in "Confirm your new password", with: "password"
    click_on "Reset Password"

    expect(page).to have_content "Your password has been changed successfully."
  end
end
