module AuthorizationExpectations
  def expect_authorized_for
    expect_unauthorized_for
    visit organization_path organization
    expect(page).to have_content(organization.name)
  end

  def expect_unauthorized_for
    visit organization_path other_organization

    expect(page).to_not have_content(other_organization.name)
    expect(page).to have_content "We're sorry, but something went wrong."

    visit organization_users_path other_organization
    expect(page).to have_content "We're sorry, but something went wrong."

    visit organization_sites_path other_organization
    expect(page).to have_content "We're sorry, but something went wrong."

    visit organization_shifts_path other_organization
    expect(page).to have_content "We're sorry, but something went wrong."
  end
end
