module SingleShiftExpectations
  def expect_single_shift_management
    manage_single_shift
  end

  private

  def manage_single_shift
    click_on "shifts"

    click_on "Add Shift"

    fill_in "Name", with: shift_title2
    find("#addTask").click
    find(class: "MuiMenuItem-root", text: "Pickup at #{donor.name}").click
    click_on "Submit"

    expect(page).to have_content "Pickup at #{donor.name}"

    # delete the event
    click_on "Delete event"
    page.driver.browser.switch_to.alert.accept

    expect(page).to have_content "Shift was successfully destroyed."

    click_on "shifts"

    click_on "Add Shift"

    fill_in "Name", with: shift_title2
    find("#addTask").click
    find(class: "MuiMenuItem-root", text: "Pickup at #{donor.name}").click
    click_on "Submit"

    expect(page).to have_content "Pickup at #{donor.name}"

    # change the date and times
    click_on "Edit"

    fill_in "Name", with: "New Chuck Norris Shift"
    fill_in "Date", with: "01-01-2000"
    time_elems = find_all("[type='time']")
    time_elems.first.set("14:23")
    time_elems.last.set("14:24")

    find("#save-button").click

    expect(page).to have_content "Saturday January 01"
    expect(page).to have_content "2:23PM- 2:24PM"
    expect(page).to have_content "New Chuck Norris Shift"

    # Change the shift to a recurring shift
    click_on "Edit"

    find("#mdc-select--rrule-frequency-select").click
    find(class: "mdc-list-item", text: "Daily").click

    find("#save-button").click

    expect(page).to have_content "Repeats every day"
  end
end
