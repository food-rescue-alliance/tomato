module UserExpectations
  def expect_user_management
    click_on "users"

    expect(page).to have_content "Users"
    expect(page).not_to have_css("#organization.siderail__button--active")
    expect(page).to have_css("#users.siderail__button--active")
    expect(page).not_to have_css("#sites.siderail__button--active")
    expect(page).not_to have_css("#shifts.siderail__button--active")

    invite(user3, :admin)
    invite(volunteer, :volunteer)
    invite(volunteer2, :volunteer)

    resend_invite(volunteer, :volunteer)
  end

  def invite(user, role)
    click_on "Invite User"
    expect(page).to have_content "Send invitation"

    find("#mdc-select--user_roles").click
    find(class: "mdc-list-item", text: role.to_s.capitalize).click

    fill_in "Email", with: user.email
    click_on "Submit"

    expect(page).to have_content "User was invited successfully."

    raw_email = Devise.mailer.deliveries.last.parts.first.body.raw_source
    invitation_token_match = raw_email.match(/invitation_token=(.{20})/)
    expect(raw_email).to include(user.email)
    expect(invitation_token_match.to_a).to_not be_empty

    expect(page).to have_content role.to_s.capitalize
    expect(find("#mdc-tab-#{role.to_s.downcase.pluralize}")["aria-selected"]).to eq("true")
    expect(page).to have_content user.email
  end

  def resend_invite(user, role)
    first("a", text: "RESEND INVITE").click
    expect(page).to have_content "Invitation Resent!"

    expect(user.roles).to eq(role.to_s.upcase)

    raw_email = Devise.mailer.deliveries.last.parts.first.body.raw_source
    invitation_token_match = raw_email.match(/invitation_token=(.{20})/)
    expect(raw_email).to include(user.email)
    expect(invitation_token_match.to_a).to_not be_empty
  end
end
