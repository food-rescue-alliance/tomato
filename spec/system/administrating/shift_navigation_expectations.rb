module ShiftNavigationExpectations
  def expect_shift_navigation
    navigate_between_shifts
  end

  def navigate_between_shifts
    click_on "organization"

    expect(page).to have_text Time.current.midnight.strftime("%A %b %d")

    expect(page).to have_css("#organization.siderail__button--active")
    expect(page).not_to have_css("#volunteers.siderail__button--active")
    expect(page).not_to have_css("#sites.siderail__button--active")
    expect(page).not_to have_css("#shifts.siderail__button--active")

    find("tr", text: shift_title_1, match: :first).click
    expect(page).to have_content shift_title_1

    click_on "organization"

    find("tr", text: shift_title_2, match: :first).click
    expect(page).to have_content shift_title_2

    click_on "organization"

    click_on "next-week"
    expect(page).to have_content shift_title_1
    click_on "next-week"
    expect(page).to have_content shift_title_1
    click_on "previous-week"
    expect(page).to have_content shift_title_1

    click_on "organization"
  end
end
