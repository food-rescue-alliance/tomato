module OrganizationExpectations
  def expect_organization_management
    sign_in user
    visit organization_path(organization.id)

    expect(page).to have_content organization.name
    expect(page).to have_css("#organization.siderail__button--active")
    expect(page).not_to have_css("#volunteers.siderail__button--active")
    expect(page).not_to have_css("#sites.siderail__button--active")
    expect(page).not_to have_css("#shifts.siderail__button--active")
  end
end
