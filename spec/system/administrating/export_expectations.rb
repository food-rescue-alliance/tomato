module ExportExpectations
  def expect_export_management
    click_on "more menu"
    click_on "Export Data"
    click_on "Generate CSV"
    # NOTE: No expectations on the generated CSV, just checking for runtime errors.
  end
end
