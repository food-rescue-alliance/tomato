module ReceiptExpectations
  def expect_site_receipts
    day_range = 7
    receipt_site = Site.find_by(name: donor.name)
    receipt_shift_event = create :shift_event, organization: organization,
      ordered_task_ids: [Task.find_by(site_id: receipt_site.id).id]
    receipt_task_log1 = create :task_log, shift_event: receipt_shift_event,
      task: Task.find_by(site_id: receipt_site.id), created_at: Faker::Time.backward(days: day_range, period: :morning)
    receipt_task_log2 = create :task_log, shift_event: receipt_shift_event,
      task: Task.find_by(site_id: receipt_site.id), created_at: Faker::Time.backward(days: day_range, period: :morning)

    visit new_organization_receipt_path(organization.id)
    expect(page).to have_content "Generate receipts"
    find("#mdc-select--site_id").click
    find(class: "mdc-list-item", text: donor.name).click
    fill_in "Starts at", with: Time.zone.today - (day_range + 1)
    fill_in "Ends at", with: Time.zone.today + 1

    new_window = window_opened_by { click_on "Enter" }
    within_window new_window do
      expect(page).to have_content organization.name
      expect(page).to have_content "Receipt for: #{donor.name}"
      expect(page).to have_content "Date"
      expect(page).to have_content "Description"
      expect(page).to have_content "Log #"
      expect(page).to have_content "Weight (lbs)"

      expect(page).to have_content receipt_task_log1.food_taxonomy.name
      expect(page).to have_content receipt_task_log1.weight

      expect(page).to have_content receipt_task_log2.food_taxonomy.name
      expect(page).to have_content receipt_task_log2.weight

      expect(page).to have_content(receipt_task_log1.weight + receipt_task_log2.weight)
    end
  end
end
