module SiteExpectations
  def expect_site_management
    click_on "sites"

    expect(page).to have_content "Sites"
    expect(page).not_to have_css("#organization.siderail__button--active")
    expect(page).not_to have_css("#volunteers.siderail__button--active")
    expect(page).to have_css("#sites.siderail__button--active")
    expect(page).not_to have_css("#shifts.siderail__button--active")

    click_on "Add Site"

    find("#mdc-select--site-type").click
    find(class: "mdc-list-item", text: "Donor").click
    fill_in "Site Name", with: donor.name
    fill_in "Address 1", with: "Peel Grove"
    fill_in "Address 2", with: "Bunch #2"
    fill_in "City", with: "Chiquita"
    fill_in "State", with: "Cavendish"
    fill_in "Zip Code", with: "22626"
    click_on "Submit"

    expect(page).to have_content donor.name
    expect(page).to have_content "Donor"

    find("#sites.mdc-icon-button").click
    expect(page).to have_content donor.name
    find("tr", text: donor.name).click

    expect(page).to have_content donor.name
    click_on "Edit"

    expect(page).to have_content "Edit"
    fill_in "Name", with: ""
    click_on "Submit"
    expect(page).to have_content "Name can't be blank"

    fill_in "Name", with: donor.name

    click_on "Submit"
    expect(page).to have_content donor.name

    find("#sites.mdc-icon-button").click

    click_on "Add Site"
    find("#mdc-select--site-type").click
    find(class: "mdc-list-item", text: "Recipient").click
    fill_in "Site Name", with: recipient.name
    fill_in "Type instructions here", with: "Make sure to buzz me when you get here"
    click_on "Submit"

    expect(page).to have_content recipient.name

    add_new_address_in_edit(recipient)
    remove_address_in_edit(recipient)

    click_on "sites"
    click_on "Hubs"

    click_on "Add Site"
    find("#mdc-select--site-type").click
    find(class: "mdc-list-item", text: "Hub").click
    fill_in "Site Name", with: hub.name
    fill_in "Address 1", with: Faker::Address.street_address
    fill_in "Address 2", with: Faker::Address.secondary_address
    fill_in "City", with: Faker::Address.city
    fill_in "State", with: Faker::Address.state
    fill_in "Zip Code", with: Faker::Address.zip_code
    click_on "Submit"

    expect(page).to have_content hub.name

    # hubs should appear on every tab
    click_on "sites"
    click_on "Donors"
    expect(page).to have_content hub.name
    click_on "Recipients"
    expect(page).to have_content hub.name
  end

  def remove_address_in_edit(site)
    click_on "sites"
    click_on site.siteable_type.pluralize

    expect(page).to have_content site.name
    find("tr", text: site.name).click
    expect(page).to have_content site.name
    click_on "Edit"
    fill_in "Address 1", with: ""
    fill_in "Address 2", with: ""
    fill_in "City", with: ""
    fill_in "State", with: ""
    fill_in "Zip Code", with: ""

    click_on "Submit"
    expect(page).to have_content site.name
    expect(page).not_to have_content "Address"
  end

  def add_new_address_in_edit(site)
    click_on "sites"
    click_on site.siteable_type.pluralize

    expect(page).to have_content site.name
    find("tr", text: site.name).click
    expect(page).to have_content site.name
    click_on "Edit"
    fill_in "Address 1", with: "Hive Lane"
    fill_in "City", with: "Buzzerly Hills"
    fill_in "State", with: "Nebruzzka"

    click_on "Submit"

    expect(page).to have_content "Address zip can't be blank"
    fill_in "Zip Code", with: "28999"
    click_on "Submit"

    expect(page).to have_content site.name
    expect(page).to have_content "Address"
    expect(page).to have_content "Hive Lane"
  end
end
