# rubocop:disable Metrics/ModuleLength
module ShiftExpectations
  def expect_shift_management
    manage_recurring_shift
  end

  private

  def manage_recurring_shift
    click_on "shifts"

    click_on "Add Shift"

    fill_in "Name", with: shift_title1
    find("#mdc-select--rrule-frequency-select").click
    find(class: "mdc-list-item", text: "Daily").click
    click_on "Submit"

    expect(page).to have_content shift_title1

    delete_individual_event
    delete_this_and_future_events

    click_on shift_title1
    click_on "Edit"

    click_on "ADD TASK"
    find("#mdc-select--task_id-0").click
    within "#mdc-select--task_id-0" do
      find("span", text: "Pickup at #{donor.name}").first(:xpath, ".//..").click
    end

    click_on "ADD VOLUNTEER"
    find("#mdc-select--user_id-0").click
    within "#mdc-select--user_id-0" do
      find("span", text: volunteer2.email).first(:xpath, ".//..").click
    end

    find("#mdc-select--rrule-frequency-select").click
    find(class: "mdc-list-item", text: "Every Weekday (Monday to Friday)").click

    find("#save-button").click
    find_field("This and following events", visible: false).click
    click_on "OK"

    expect(page).to have_content "Pickup at #{donor.name}"
    expect(page).to have_content volunteer2.email
    expect(page).not_to have_content volunteer.email
    expect(page).to have_content "Repeats on weekdays every week"

    remove_voluneer_from_future_shifts

    update_frequency

    find("#mdc-select--food_taxonomy_id").click
    find(class: "mdc-list-item", text: FoodTaxonomy.first.name).click
    fill_in "Weight", with: "1"
    fill_in "Temp (°F)", with: "30"
    fill_in "Notes", with: "a note"

    click_on "Save Log"

    expect(page).to have_content "1 lbs"
    expect(page).to have_content "30° F"
    expect(page).to have_content "a note"
  end

  def delete_individual_event
    click_on "organization"

    expect(page.all("a", text: shift_title1).count).to eq 7

    page.all("a", text: shift_title1)[1].click
    click_on "Delete event"
    click_on "OK"
    page.driver.browser.switch_to.alert.accept

    expect(page).to have_content "Shift was successfully destroyed."
    expect(page.all("a", text: shift_title1).count).to eq 6
  end

  def delete_this_and_future_events
    page.all("a", text: shift_title1)[1].click
    click_on "Delete event"
    find_field("This and following events", visible: false).click
    click_on "OK"
    page.driver.browser.switch_to.alert.accept

    expect(page).to have_content "Shift was successfully destroyed."
    expect(page.all("a", text: shift_title1).count).to eq 1
  end

  def update_frequency
    click_on "Edit"

    fill_in "Name", with: "New shift name"
    fill_in "Date", with: "01-01-2000"
    time_elems = find_all("[type='time']")
    time_elems.first.set("14:23")
    time_elems.last.set("14:24")

    find("#save-button").click
    click_on "OK"

    expect(page).to have_content "Shift was successfully updated."
    expect(page).to have_content "Saturday January 01"
    expect(page).to have_content "2:23PM- 2:24PM"
    expect(page).to have_content "New shift name"
  end

  def remove_voluneer_from_future_shifts
    click_on "organization"
    page.all("a", text: shift_title1)[2].click
    expect(page).to have_content shift_title1

    click_on "Edit"

    all("input[name='shift_event[user_ids][]']").first.set(false)

    find("#save-button").click
    find_field("This and following events", visible: false).click
    click_on "OK"

    expect(page).to have_content "Shift was successfully updated."

    [0, 1].each { |i| expect_shift_to_have_volunteer(shift_title1, i, volunteer2) }
    [2, 3].each { |i| expect_shift_not_to_have_volunteer(shift_title1, i, volunteer2) }
  end

  def expect_shift_not_to_have_volunteer(title, count, volunteer)
    click_on "organization"
    within find_all("td", text: title)[count] do
      click_on title
    end
    expect(page).not_to have_content(volunteer.email)
  end

  def expect_shift_to_have_volunteer(title, count, volunteer)
    click_on "organization"
    within find_all("td", text: title)[count] do
      click_on title
    end
    expect(page).to have_content(volunteer.email)
  end
end
# rubocop:enable Metrics/ModuleLength
