require "rails_helper"
require "system/administrating/authorization_expectations"
require "system/administrating/organization_expectations"
require "system/administrating/site_expectations"
require "system/administrating/user_expectations"
require "system/administrating/receipt_expectations"
require "system/administrating/shift_expectations"
require "system/administrating/single_shift_expectations"
require "system/administrating/export_expectations"

describe "Administrating an Organization" do
  let(:user) { create :org_admin }
  let(:user2) { build :user }
  let(:user3) { build :user }
  let(:organization) { user.organizations.first }
  let(:other_organization) { create :organization }
  let(:shift_title1) { Faker::ChuckNorris.fact }
  let(:shift_title2) { Faker::ChuckNorris.fact }
  let(:donor) { build :donor }
  let(:recipient) { build :recipient }
  let(:hub) { build :hub }
  let(:volunteer) { build :volunteer }
  let(:volunteer2) { build :volunteer }

  include AuthorizationExpectations
  include OrganizationExpectations
  include SiteExpectations
  include UserExpectations
  include ReceiptExpectations
  include ShiftExpectations
  include ExportExpectations
  include SingleShiftExpectations

  it "allows an organization administrator to administrate their organization", js: true do
    expect_organization_management
    expect_site_management
    expect_site_receipts
    expect_user_management
    expect_shift_management
    expect_single_shift_management
    expect_authorized_for
    expect_export_management
  end
end
