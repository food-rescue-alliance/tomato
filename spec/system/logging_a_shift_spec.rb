require "rails_helper"

describe "Logging a shift", js: true do
  let(:volunteer) { create(:volunteer) }
  before do
    create(:shift_event, :single, user_ids: [volunteer.id], organization: volunteer.organizations[0])
  end

  it "lets the volunteer log shift info" do
    visit root_path

    click_on "Login"

    fill_in "Email", with: volunteer.email
    fill_in "Password", with: "password"
    click_on "Log in"

    expect(page).to have_content(volunteer.full_name)

    click_on("Log Report")
    click_on "car"

    # Returning later...
    visit root_path
    click_on("Log Report")
    expect(find(:css, "button[aria-current='transportation-type'][aria-label='car']")).to be_present
  end
end
