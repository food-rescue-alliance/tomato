require "rails_helper"

describe "Organizations" do
  let(:user) { create(:ops_coordinator) }

  it "/ mangages orgnaizations", js: true do
    org_name = "Organization 13"
    sign_in user
    visit root_path

    expect(page).to have_content "Organizations"

    click_on "New Organization"
    expect(page).to have_content "New organization"

    fill_in "Name", with: org_name
    fill_in "Address 1", with: "Peel Grove"
    fill_in "Address 2", with: "Bunch #2"
    fill_in "City", with: "Chiquita"
    fill_in "State", with: "Cavendish"
    fill_in "Zip Code", with: "22626"
    find("#mdc-select--time_zone").click
    within "#mdc-select--time_zone" do
      first("span", text: "Pacific Time (US & Canada)").click
    end

    click_on "Submit"
    expect(page).to have_content "Organization was successfully created"
    expect(page).to have_content org_name

    click_on "logout-menu-toggle"
    click_on "edit-org"
    expect(page).to have_content "Edit Organization"
    fill_in "Name", with: "Organization XIII"
    click_on "Submit"
    expect(page).to have_content "Edit Organization"
    expect(page).to have_xpath("//input[@value='Organization XIII']")

    visit root_path
    expect(page).to have_content "Organization XIII"
    click_on "Destroy"
    page.driver.browser.switch_to.alert.accept

    expect(page).to have_content "Organization was successfully destroyed."
  end
end
