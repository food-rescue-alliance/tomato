require "rails_helper"

describe "Editing profile", js: true do
  let(:admin) { create(:org_admin) }

  it "lets an admin update their email and name" do
    sign_in admin

    visit root_path
    expect(page).to have_title "Rootable"

    click_on "more menu"
    click_on "Edit Profile"

    expect(page).to have_content "Edit Profile"

    fill_in "Email", with: "test-update@example.com"
    fill_in "Name", with: "Test N. Update"
    find("#mdc-select--time_zone").click
    within "#mdc-select--time_zone" do
      first("span", text: "Pacific Time (US & Canada)").click
    end
    click_on "Submit"

    expect(page).to have_content("Successfully edited profile!")
    expect(page).to have_field("Email", with: "test-update@example.com")
    expect(page).to have_field("Name", with: "Test N. Update")
    expect(page).to have_content("Pacific Time (US & Canada")

    click_on "organization"
    expect(page).to have_content(admin.organizations.first.name)
  end
end
