# == Schema Information
#
# Table name: food_taxonomies
#
#  created_at         :datetime         not null
#  details            :string
#  feeding_america_id :string           not null
#  id                 :bigint           not null, primary key
#  name               :string           not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_food_taxonomies_on_feeding_america_id  (feeding_america_id) UNIQUE
#
FactoryBot.define do
  factory :food_taxonomy do
    feeding_america_id { Faker::Alphanumeric.alphanumeric(number: 10) }
    name { Faker::Food.ingredient }
    details { Faker::Food.description }
  end
end
