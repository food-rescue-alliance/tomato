FactoryBot.define do
  factory :site do
    association :organization
    name { Faker::Restaurant.unique.name }
    siteable { Donor.new }

    factory :donor do
      siteable { Donor.new }
      tasks { [association(:task, :pickup)] }
    end

    factory :recipient do
      siteable { Recipient.new }
      tasks { [association(:task, :drop_off)] }
    end

    factory :hub do
      siteable { Hub.new }
      tasks { [association(:task, :pickup), association(:task, :drop_off)] }
    end
  end
end
