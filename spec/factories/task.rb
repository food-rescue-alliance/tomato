FactoryBot.define do
  factory :task do
    site

    trait :pickup do
      task_type { :pickup }
    end

    trait :drop_off do
      task_type { :drop_off }
    end
  end
end
