# == Schema Information
#
# Table name: shifts
#
#  created_at :datetime         not null
#  id         :bigint           not null, primary key
#  name       :string           not null
#  updated_at :datetime         not null
#
FactoryBot.define do
  factory :shift do
    name { Faker::Book.title }

    transient do
      starts_at { 24.hours.from_now }
      ends_at { starts_at + 1.hour }
      organization { association :organization }
    end

    shift_events do
      [association(:shift_event,
                   shift: instance,
                   starts_at: starts_at,
                   ends_at: ends_at,
                   title: name,
                   organization: organization)]
    end

    trait :weekly_on_mondays do
      shift_events do
        [association(:shift_event, :weekly_on_mondays,
                     shift: instance,
                     starts_at: starts_at,
                     ends_at: ends_at,
                     title: name,
                     organization: organization)]
      end
    end

    trait :daily do
      shift_events do
        [association(:shift_event, :daily,
                     shift: instance,
                     starts_at: starts_at,
                     ends_at: ends_at,
                     title: name,
                     organization: organization)]
      end
    end

    trait :single do
      shift_events do
        [association(:shift_event, :single,
                     shift: instance,
                     starts_at: starts_at,
                     ends_at: ends_at,
                     title: name,
                     organization: organization)]
      end
    end

    trait :series do
      shift_events do
        [
          association(:shift_event, :weekly_on_mondays,
                      shift: instance,
                      starts_at: starts_at,
                      ends_at: ends_at,
                      title: name,
                      organization: organization),
          association(:shift_event, :single,
                      shift: instance,
                      starts_at: starts_at + 3.weeks,
                      ends_at: ends_at + 3.weeks,
                      title: name,
                      organization: organization)
        ]
      end
    end
  end
end
