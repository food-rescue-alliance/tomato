# == Schema Information
#
# Table name: organizations
#
#  created_at            :datetime         not null
#  email                 :string
#  federal_tax_id        :string
#  id                    :bigint           not null, primary key
#  internal_alerts_email :string
#  name                  :string           not null
#  phone                 :string
#  tagline               :string
#  time_zone             :string           default("Etc/UTC"), not null
#  updated_at            :datetime         not null
#
FactoryBot.define do
  factory :organization do
    name { Faker::Kpop.ii_groups }

    trait :with_internal_alerts_email do
      internal_alerts_email { Faker::Internet.email }
    end
  end
end
