# == Schema Information
#
# Table name: users
#
#  created_at             :datetime         not null
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  full_name              :string           default(""), not null
#  id                     :bigint           not null, primary key
#  invitation_accepted_at :datetime
#  invitation_created_at  :datetime
#  invitation_limit       :integer
#  invitation_sent_at     :datetime
#  invitation_token       :string
#  invitations_count      :integer          default(0)
#  invited_by_id          :bigint
#  invited_by_type        :string
#  phone                  :string
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  roles                  :integer          default(NULL), not null
#  time_zone              :string           default("Etc/UTC"), not null
#  updated_at             :datetime         not null
#
# Indexes
#
#  index_users_on_email                              (email) UNIQUE
#  index_users_on_invitation_token                   (invitation_token) UNIQUE
#  index_users_on_invitations_count                  (invitations_count)
#  index_users_on_invited_by_id                      (invited_by_id)
#  index_users_on_invited_by_type_and_invited_by_id  (invited_by_type,invited_by_id)
#  index_users_on_reset_password_token               (reset_password_token) UNIQUE
#
FactoryBot.define do
  factory :user do
    email { Faker::Internet.email }
    full_name { Faker::Name.name }
    password { "password" }
  end

  factory :ops_coordinator, class: "User" do
    email { Faker::Internet.email }
    full_name { Faker::Name.name }
    password { "password" }
    roles { 1 }
  end

  factory :org_admin, class: "User" do
    email { Faker::Internet.email }
    full_name { Faker::Name.name }
    password { "password" }
    roles { 2 }
    organizations { [create(:organization)] }
  end

  factory :volunteer, class: "User" do
    email { Faker::Internet.email }
    full_name { Faker::Name.name }
    password { "password" }
    roles { 0 }
    organizations { [create(:organization)] }
  end
end
