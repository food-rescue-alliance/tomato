FactoryBot.define do
  factory :robot_schedule, class: Robot::Schedule do
    sequence(:position, 0)
  end
end
