FactoryBot.define do
  factory :robot_location, class: Robot::Location do
    recip_category { %w[A B C D].sample }
    donor_type do
      %w[Cafeteria Grocer Individual Restaurant Cafe Bakery Other Caterer Market Community Garden Farm].sample
    end
    address do
      [Faker::Address.street_address, Faker::Address.community, Faker::Address.state, Faker::Address.zip].join(", ")
    end
    lat { Faker::Address.latitude }
    lng { Faker::Address.longitude }
    name { Faker::Company.name }
    contact { Faker::Name.name }
    website { Faker::Internet.url }
    admin_notes { Faker::Lorem.sentence(word_count: 3, supplemental: false, random_words_to_add: 10) }
    public_notes { Faker::Lorem.sentence(word_count: 3, supplemental: false, random_words_to_add: 10) }
    hours { Faker::Lorem.sentence }
    twitter_handle { Faker::Twitter.screen_name }
    receipt_key { Faker::Alphanumeric.alphanumeric(number: 8) }
    detailed_hours_json do
      Faker::Json.shallow_json(width: 3, options: { key: "Number.digit", value: "Lorem.sentence" })
    end
    email { Faker::Internet.email }
    phone { Faker::PhoneNumber.phone_number }
    equipment_storage_info { Faker::Lorem.sentence(word_count: 3, supplemental: false, random_words_to_add: 10) }
    food_storage_info { Faker::Lorem.sentence(word_count: 3, supplemental: false, random_words_to_add: 10) }
    entry_info { Faker::Lorem.sentence(word_count: 3, supplemental: false, random_words_to_add: 10) }
    exit_info { Faker::Lorem.sentence(word_count: 3, supplemental: false, random_words_to_add: 10) }
    onsite_contact_info { "#{Faker::Name.name} - #{Faker::PhoneNumber.phone_number}" }
    active { true }

    trait :recipient do
      location_type { 0 }
    end

    trait :donor do
      location_type { 1 }
    end

    trait :hub do
      location_type { 2 }
    end

    trait :seller do
      location_type { 3 }
    end

    trait :buyer do
      location_type { 4 }
    end
  end
end
