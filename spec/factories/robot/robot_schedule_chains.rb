FactoryBot.define do
  factory :robot_schedule_chain, class: Robot::ScheduleChain do
    active { true }
  end
end
