FactoryBot.define do
  factory :robot_assignment, class: Robot::Assignment do
    trait :admin do
      admin { true }
    end
  end
end
