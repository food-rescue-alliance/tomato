FactoryBot.define do
  factory :robot_region, class: Robot::Region do
    lat { Faker::Address.latitude }
    lng { Faker::Address.longitude }
    name { Faker::Company.name }
    website { Faker::Internet.url }
    title { Faker::Company.name }
    phone { Faker::PhoneNumber.phone_number }
    tax_id { Faker::Company.ein }
    weight_unit { "pound" }
    time_zone { Faker::Address.time_zone }
    volunteer_coordinator_email { Faker::Internet.email }
    post_pickup_emails { false }
    unschedule_self { false }
    transient do
      admins { [] }
    end

    after(:create) do |region, evaluator|
      region.assignments += evaluator.admins.map { |admin| build(:robot_assignment, :admin, volunteer: admin) }
    end
  end
end
