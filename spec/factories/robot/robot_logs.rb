FactoryBot.define do
  factory :robot_log, class: Robot::Log do
    self.when { Time.zone.today + ((rand > 0.5 ? -1 : 1) * rand(10)) }
  end
end
