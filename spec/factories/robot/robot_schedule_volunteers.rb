FactoryBot.define do
  factory :robot_schedule_volunteer, class: Robot::ScheduleVolunteer do
    active { true }
  end
end
