FactoryBot.define do
  factory :robot_absence, class: Robot::Absence do
    start_date { 2.days.from_now }
    stop_date { 6.days.from_now }
  end
end
