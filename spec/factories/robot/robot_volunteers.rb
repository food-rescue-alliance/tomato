FactoryBot.define do
  factory :robot_volunteer, class: Robot::Volunteer do
    email { Faker::Internet.email }
    name { Faker::Name.name }
  end
end
