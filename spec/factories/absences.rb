# == Schema Information
#
# Table name: absences
#
#  created_at :datetime         not null
#  ends_at    :date
#  id         :bigint           not null, primary key
#  starts_at  :date             not null
#  updated_at :datetime         not null
#  user_id    :bigint           not null
#
# Indexes
#
#  index_absences_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
FactoryBot.define do
  factory :absence do
    starts_at { 1.day.from_now }
    ends_at { 5.days.from_now }
    user { association :volunteer }
  end
end
