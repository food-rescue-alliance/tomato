FactoryBot.define do
  factory :task_log do
    notes { Faker::Lorem.paragraph }
    weight { Faker::Number.number(digits: 1) }
    temperature { Faker::Number.number(digits: 1) }
    user { create :volunteer }
    shift_event { create :shift_event, :with_tasks }
    task { shift_event.tasks.first }
    food_taxonomy { create(:food_taxonomy) }
  end
end
