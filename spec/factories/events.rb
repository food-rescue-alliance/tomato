# == Schema Information
#
# Table name: events
#
#  created_at         :datetime         not null
#  ends_at            :datetime         not null
#  eventable_id       :bigint           not null
#  eventable_type     :string           not null
#  exdates            :datetime         default([]), is an Array
#  id                 :bigint           not null, primary key
#  ownable_id         :bigint
#  ownable_type       :string
#  recurrence_options :jsonb
#  starts_at          :datetime         not null
#  title              :string           not null
#  uid                :string
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_events_on_eventable_id_and_eventable_type                 (eventable_id,eventable_type)
#  index_events_on_ownable_id_and_ownable_type_and_eventable_type  (ownable_id,ownable_type,eventable_type)
#  index_events_on_ownable_type_and_ownable_id                     (ownable_type,ownable_id)
#  index_events_on_recurrence_options                              (recurrence_options) USING gin
#  index_events_on_uid                                             (uid)
#

FactoryBot.define do
  factory :event do
    title { Faker::Esport.event }
    starts_at { Faker::Time.forward(days: 23, period: :morning) }
    ends_at { Faker::Time.forward(days: 23, period: :afternoon) }
    eventable { create(:shift_event) }
    ownable { create(:organization) }

    trait :weekly_on_mondays do
      rrule { "FREQ=WEEKLY;INTERVAL=1;BYDAY=MO" }
    end

    trait :daily do
      rrule { "FREQ=DAILY;INTERVAL=1;" }
    end

    trait :single do
      rrule { nil }
    end
  end
end
