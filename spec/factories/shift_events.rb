# == Schema Information
#
# Table name: shift_events
#
#  admin_notes  :text
#  created_at   :datetime         not null
#  id           :bigint           not null, primary key
#  public_notes :text
#  shift_id     :bigint           not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_shift_events_on_shift_id  (shift_id)
#
FactoryBot.define do
  factory :shift_event do
    shift

    transient do
      starts_at { Faker::Time.forward(days: 23, period: :morning) }
      ends_at { Faker::Time.forward(days: 23, period: :afternoon) }
      organization { association :organization }
    end

    event do
      association :event,
                  eventable: instance,
                  starts_at: starts_at,
                  ends_at: ends_at,
                  ownable: organization
    end

    trait :single do
      event do
        association :event, :single,
                    starts_at: starts_at,
                    ends_at: ends_at,
                    ownable: organization
      end
    end

    trait :weekly_on_mondays do
      event do
        association :event, :weekly_on_mondays,
                    starts_at: starts_at,
                    ends_at: ends_at,
                    ownable: organization
      end
    end

    trait :daily do
      event do
        association :event, :daily,
                    starts_at: starts_at,
                    ends_at: ends_at,
                    ownable: organization
      end
    end

    trait :with_tasks do
      transient do
        ordered_tasks { [create(:donor).tasks.first] }
        ends_at { Faker::Time.forward(days: 23, period: :afternoon) }
        organization { association :organization }
      end
      shift_event_tasks do
        ordered_tasks.each_with_index.map do |task, i|
          build(:shift_event_task, task: task, order: i)
        end
      end
    end
  end
end
