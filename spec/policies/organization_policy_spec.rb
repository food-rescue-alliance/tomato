require "rails_helper"

RSpec.describe OrganizationPolicy do
  subject { described_class }

  permissions :index? do
    before do
      @organization = create(:organization)
      @organization2 = create(:organization)
      @ops_coordinator = create(:ops_coordinator)
      @org_admin = create(:org_admin, organization_ids: [@organization.id])
      @multiple_orgs_admin = create(:org_admin, organization_ids: [@organization.id, @organization2.id])
      @volunteer = create(:volunteer)
    end

    it "does not grant access if user is a volunteer" do
      expect(subject).not_to permit(@volunteer)
    end

    it "grants access if user is an ops coordinator" do
      expect(subject).to permit(@ops_coordinator)
    end

    it "grants access if user is an org admin with multiple orgs" do
      expect(subject).to permit(@multiple_orgs_admin)
    end

    it "does not grant access if user is an org admin with only 1 org" do
      expect(subject).not_to permit(@org_admin)
    end
  end

  permissions :show?, :edit?, :update? do
    before do
      @organization = create(:organization)
      @ops_coordinator = create(:ops_coordinator)
      @org_admin = create(:org_admin, organization_ids: [@organization.id])
      @wrong_org_admin = create(:org_admin)
      @volunteer = create(:volunteer)
    end

    it "does not grant access if user is a volunteer" do
      expect(subject).not_to permit(@volunteer)
    end

    it "does not grant access if user is an admin for the wrong org" do
      expect(subject).not_to permit(@wrong_org_admin, @organization)
    end

    it "grants access if user is an ops coordinator" do
      expect(subject).to permit(@ops_coordinator)
    end

    it "grants access if user is an admin for the organization" do
      expect(subject).to permit(@org_admin, @organization)
    end
  end

  permissions :new?, :create?, :destroy? do
    before do
      @organization = create(:organization)
      @ops_coordinator = create(:ops_coordinator)
      @org_admin = create(:org_admin, organization_ids: [@organization.id])
      @volunteer = create(:volunteer)
    end

    it "does not grant access if user is a volunteer" do
      expect(subject).not_to permit(@volunteer)
    end

    it "does not grant access if user is an org admin" do
      expect(subject).not_to permit(@org_admin, @organization)
    end

    it "grants access if user is an ops coordinator" do
      expect(subject).to permit(@ops_coordinator)
    end
  end

  permissions ".scope" do
    before do
      @organization1 = create(:organization)
      @organization2 = create(:organization)
      @ops_coordinator = create(:ops_coordinator)
      @org_admin = create(:org_admin, organization_ids: [@organization1.id])
    end

    it "shows all organizations for an ops coordinator" do
      @policy_scope = OrganizationPolicy::Scope.new(@ops_coordinator, Organization).resolve
      expect(@policy_scope).to eq [@organization1, @organization2]
    end

    it "shows only the orgs belonging to the org admin" do
      @policy_scope = OrganizationPolicy::Scope.new(@org_admin, Organization).resolve
      expect(@policy_scope).to eq [@organization1]
    end
  end
end
