require "rails_helper"

RSpec.describe EventPolicy do
  subject { described_class }

  permissions :index?, :show?, :update? do
    before do
      @event = create(:event)
      @organization = @event.ownable
      @ops_coordinator = create(:ops_coordinator)
      @org_admin = create(:org_admin, organization_ids: [@organization.id])
      @wrong_org_admin = create(:org_admin)
      @volunteer = create(:volunteer)
    end

    it "does not grant access if user is a volunteer" do
      expect(subject).not_to permit(@volunteer, @event)
    end

    it "does not grant access if user is an admin for the wrong org" do
      expect(subject).not_to permit(@wrong_org_admin, @event)
    end

    it "grants access if user is an ops coordinator" do
      expect(subject).to permit(@ops_coordinator, @event)
    end

    it "grants access if user is an admin for the organization" do
      expect(subject).to permit(@org_admin, @event)
    end
  end

  permissions :show? do
    before do
      @event = create(:event)
      @organization = @event.ownable
      @volunteer = create(:volunteer, organization_ids: [@organization.id])
      @other_volunteer = create(:volunteer)
    end

    it "grants access if user is a volunteer in the organization" do
      expect(subject).to permit(@volunteer, @event)
    end
    it "doess not grant access if user is not a volunteer in the organization" do
      expect(subject).not_to permit(@other_volunteer, @event)
    end
  end
end
