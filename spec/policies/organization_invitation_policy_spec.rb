require "rails_helper"

RSpec.describe OrganizationInvitationPolicy do
  subject { described_class }

  permissions :new?, :create? do
    before do
      @organization = create(:organization)
      @ops_coordinator = create(:ops_coordinator)
      @org_admin = create(:org_admin, organization_ids: [@organization.id])
      @wrong_org_admin = create(:org_admin)
      @volunteer = create(:volunteer)
    end

    it "does not grant access if user is a volunteer" do
      expect(subject).not_to permit(@volunteer, @organization)
    end

    it "does not grant access if user is an org admin for the wrong organization" do
      expect(subject).not_to permit(@wrong_org_admin, @organization)
    end

    it "grants access if user is an org admin for the organization" do
      expect(subject).to permit(@org_admin, @organization)
    end

    it "grants access if user is an ops coordinator" do
      expect(subject).to permit(@ops_coordinator, @organization)
    end
  end
end
