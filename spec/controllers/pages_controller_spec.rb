require "rails_helper"

describe PagesController do
  describe "GET 'root'" do
    before do
      @ops_coordinator = create(:ops_coordinator)
      @org_admin = create(:org_admin)
      @org1 = create(:organization)
      @org2 = create(:organization)
      @multiple_orgs_admin = create(:org_admin, organizations: [@org1, @org2])
    end

    it "renders the landing page when user is not logged in" do
      get "root"
      expect(response.body).to have_link "Login"
    end

    it "redirects to the org list page when user is an ops coordinator" do
      sign_in @ops_coordinator
      get "root"
      expect(response).to redirect_to organizations_path
    end

    it "redirects to the org list page when user is an org admin with multiple orgs" do
      sign_in @multiple_orgs_admin
      get "root"
      expect(response).to redirect_to organizations_path
    end

    it "redirects to the org details page when user is an org admin with only 1 org" do
      sign_in @org_admin
      get "root"
      expect(response).to redirect_to organization_path(@org_admin.organizations.first)
    end
  end
end
