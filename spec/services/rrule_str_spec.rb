require "spec_helper"

RSpec.describe RruleStr do
  context "with no recurrence" do
    it "returns an empty string" do
      rrule_str = RruleStr.new

      expect(rrule_str.to_sentence).to eq ""
    end
  end

  context "with recurrence" do
    let(:starts_at) { Time.zone.parse("July 11 2021 10:00") }
    let(:ends_at) { "2021-07-15" }
    let(:rrules) do
      {
        "FREQ=DAILY;INTERVAL=1" => "Repeats every day",
        "FREQ=DAILY;INTERVAL=2" => "Repeats every other day",
        "FREQ=DAILY;INTERVAL=3" => "Repeats every 3 days",
        "FREQ=DAILY;INTERVAL=1;COUNT=4" => "Repeats every day 4 times",
        "FREQ=DAILY;INTERVAL=1;UNTIL=#{ends_at}" => "Repeats every day until Thursday Jul 15",
        "FREQ=WEEKLY;BYDAY=TH;INTERVAL=1" => "Repeats on Thursdays every week",
        "FREQ=WEEKLY;BYDAY=WE;INTERVAL=2" => "Repeats on Wednesdays every other week",
        "FREQ=WEEKLY;BYDAY=FR;INTERVAL=3" => "Repeats on Fridays every 3 weeks",
        "FREQ=WEEKLY;BYDAY=TU,TH;INTERVAL=1" => "Repeats on Tuesdays and Thursdays every week",
        "FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR;INTERVAL=1" =>
          "Repeats on weekdays every week",
        "FREQ=WEEKLY;BYDAY=SU,SA;INTERVAL=1" =>
          "Repeats on weekends every week",
        "FREQ=WEEKLY;BYDAY=SU,MO,TU,WE,TH,FR,SA;INTERVAL=1" =>
          "Repeats on all days every week",
        "FREQ=MONTHLY;BYDAY=FR;BYSETPOS=1;INTERVAL=1" => "Repeats on the first Friday of every month",
        "FREQ=MONTHLY;BYDAY=TU;BYSETPOS=2;INTERVAL=2" => "Repeats on the second Tuesday of every other month",
        "FREQ=MONTHLY;BYDAY=TH,SA;BYSETPOS=2;INTERVAL=1" =>
          "Repeats on the second Thursday or Saturday of every month",
        "FREQ=MONTHLY;BYDAY=SU,MO,TU,WE,TH,FR,SA;BYSETPOS=2;INTERVAL=1" => "Repeats on the second day of every month",
        "FREQ=MONTHLY;BYDAY=MO,TU,WE,TH,FR;BYSETPOS=2;INTERVAL=1" => "Repeats on the second weekday of every month",
        "FREQ=MONTHLY;BYDAY=SU,SA;BYSETPOS=-1;INTERVAL=1" => "Repeats on the last weekend day of every month",
        "FREQ=YEARLY;INTERVAL=1" => "Repeats on July 11th of every year",
        "FREQ=MONTHLY;BYMONTHDAY=1,3;INTERVAL=1" => "Repeats on the 1st and 3rd of every month"
      }
    end

    it "matches the expected string" do
      rrules.each do |rrule, str|
        rrule_str = RruleStr.new(rrule: rrule, starts_at: starts_at)

        expect(rrule_str.to_sentence).to eq str
      end
    end
  end
end
