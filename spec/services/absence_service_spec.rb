require "rails_helper"

describe AbsenceService do
  describe ".remove_user_from_shifts" do
    context "when multiple users have scheduled absences" do
      let(:organization) { create(:organization) }
      let(:vol1) { create(:volunteer) }
      let(:vol2) { create(:volunteer) }

      before do
        shift = create(:shift, :daily, organization: organization)
        initial_shift_event = shift.shift_events.first
        initial_shift_event.user_ids = [vol1.id, vol2.id]
      end

      it "handles multiple volunteer absences correctly" do
        absence1 = create(:absence, starts_at: 2.days.from_now, ends_at: 2.days.from_now, user: vol1)
        described_class.remove_user_from_shifts(vol1, absence1, organization)

        absence2 = create(:absence, starts_at: 3.days.from_now, ends_at: 3.days.from_now, user: vol2)
        described_class.remove_user_from_shifts(vol2, absence2, organization)

        # Mirror how the shift_event_occurrence list is generated for admins
        occurrences = EventsService.all_occurrences organization, ShiftEvent,
                                                    1.day.from_now.beginning_of_day..4.days.from_now.end_of_day
        volunteers_for_each_occurrence = occurrences.map { |o| o.event.shift_event.volunteers.to_a }

        expect(volunteers_for_each_occurrence).to eql([
          [vol1, vol2], # No absence: both are assigned.
          [vol2], # volunteer 1 is absent
          [vol1], # volunteer 2 is absent
          [vol1, vol2] # No absence: both are assigned
        ])
      end
    end
  end
end
