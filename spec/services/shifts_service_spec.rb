require "rails_helper"

describe EventsService do
  describe ".create" do
    it "returns ok shift given a name" do
      shift_name = Faker::Lorem.sentence
      organization = create :organization

      result = ShiftsService.create({ name: shift_name }, organization, organization.time_zone)

      expect(result).to be_ok
      expect(result.value).to be_persisted
      expect(result.error).to be_blank
      expect(result.value.name).to eq(shift_name)
    end

    it "returns ok shift in organization timezone given shift event" do
      shift_name = Faker::Lorem.sentence
      organization = create :organization, time_zone: "America/Los_Angeles"
      first_monday_start = "2020-10-05 03:00:00 PM"
      first_monday_end = "2020-10-05 05:00:00 PM"

      result = ShiftsService.create({
                                      name: shift_name,
                                      shift_events_attributes: [
                                        {
                                          event_attributes: {
                                            starts_at: first_monday_start,
                                            ends_at: first_monday_end
                                          }
                                        }
                                      ]
                                    },
                                    organization,
                                    organization.time_zone)

      expect(result).to be_ok
      expect(result.value).to be_persisted
      expect(result.error).to be_blank

      expect(result.value.name).to eq(shift_name)
      expect(result.value.shift_events).not_to be_empty
      starts_at = result.value.shift_events.first.starts_at
      ends_at = result.value.shift_events.first.ends_at
      expect(starts_at).to eq("2020-10-05 22:00:00.000000000 +0000")
      expect(ends_at).to eq("2020-10-05 24:00:00.000000000 +0000")
      expect(starts_at.in_time_zone(organization.time_zone)).to eq("2020-10-05 15:00:00.000000000 -0700")
      expect(ends_at.in_time_zone(organization.time_zone)).to eq("2020-10-05 17:00:00.000000000 -0700")
    end

    it "returns ok shift in other timezone given a time_zone" do
      shift_name = Faker::Lorem.sentence
      organization = create :organization, time_zone: "America/Los_Angeles"
      first_monday_start = "2020-10-05 05:00:00 PM"
      first_monday_end = "2020-10-05 07:00:00 PM"
      time_zone = "America/Chicago"

      result = ShiftsService.create({
                                      name: shift_name,
                                      shift_events_attributes: [
                                        {
                                          event_attributes: {
                                            starts_at: first_monday_start,
                                            ends_at: first_monday_end
                                          }
                                        }
                                      ]
                                    },
                                    organization,
                                    time_zone)

      expect(result).to be_ok
      expect(result.value).to be_persisted
      expect(result.error).to be_blank

      expect(result.value.name).to eq(shift_name)
      expect(result.value.shift_events).not_to be_empty
      starts_at = result.value.shift_events.first.starts_at
      ends_at = result.value.shift_events.first.ends_at
      expect(starts_at).to eq("2020-10-05 22:00:00.000000000 +0000")
      expect(ends_at).to eq("2020-10-05 24:00:00.000000000 +0000")
      expect(starts_at.in_time_zone(organization.time_zone)).to eq("2020-10-05 15:00:00.000000000 -0700")
      expect(ends_at.in_time_zone(organization.time_zone)).to eq("2020-10-05 17:00:00.000000000 -0700")
    end

    it "returns an error" do
      organization = create :organization

      result = ShiftsService.create({}, organization, organization.time_zone)

      expect(result).to be_error
      expect(result.error).not_to be_persisted
      expect(result.error.errors.full_messages).not_to be_empty
    end
  end
end
