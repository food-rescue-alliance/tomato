require "rails_helper"

describe RobotImportService do
  describe ".import_region" do
    let(:region) { create(:robot_region) }
    subject(:import_region!) { described_class.import_region(region.id) }

    it "creates an organization" do
      expect { import_region! }.to change { Organization.count }.by(1)
    end

    it "assigns the correct attributes to the new organization" do
      import_region!
      new_org = Organization.last

      expect(new_org).to have_attributes({
                                           name: region.name,
        tagline: region.tagline,
        email: region.volunteer_coordinator_email,
        phone: region.phone,
        federal_tax_id: region.tax_id,
        time_zone: region.time_zone
                                         })
    end

    describe "time zones" do
      subject(:imported_time_zone) { described_class.import_region(region.id).time_zone }

      context "with a normal time zone" do
        let(:region) { create(:robot_region, time_zone: "America/Denver") }

        it { is_expected.to eq("America/Denver") }
      end

      context "with a bad timezone" do
        let(:region) { create(:robot_region, time_zone: "NOT_REAL") }

        it { is_expected.to eq("Etc/UTC") }
      end

      context "with a blank timezone" do
        let(:region) { create(:robot_region, time_zone: "") }

        it { is_expected.to eq("Etc/UTC") }
      end

      context "when a region has an non-canonical Time Zone ID" do
        [
          ["central", "America/Chicago"],
          ["CST", "America/Chicago"],
          ["Eastern ", "America/New_York"],
          ["Eastern Standard Time", "America/New_York"],
          ["EST", "America/New_York"],
          ["MST", "America/Denver"],
          ["Oceania/Brisbane", "Australia/Brisbane"],
          ["pacific standard", "America/Los_Angeles"],
          ["PST", "America/Los_Angeles"],
          ["US/Central", "America/Chicago"],
          ["US/Mountain", "America/Denver"]
        ].each do |non_canonical, canonical|
          context "given '#{non_canonical}' TZ" do
            let(:region) { create(:robot_region, time_zone: non_canonical) }

            it { is_expected.to eq(canonical) }
          end
        end
      end
    end

    describe "the imported volunteers" do
      context "when there is a volunteer" do
        let(:volunteer) { create(:robot_volunteer) }
        let(:region) { create(:robot_region, volunteers: [volunteer]) }
        it "creates a user for that volunteer" do
          expect { import_region! }.to change { User.count }.by(1)

          expect(User.last).to have_attributes(
            {
              full_name: volunteer.name,
              email: volunteer.email,
              roles: "VOLUNTEER",
              time_zone: region.time_zone
            }
          )
        end

        it "does not send an email" do
          expect { import_region! }.not_to change { ActionMailer::Base.deliveries.count }
        end

        context "and that volunteer already exists in Rootable" do
          let!(:preexisting_user) { create(:volunteer, email: volunteer.email) }

          it "should skip the user" do
            expect { import_region! }.not_to change { User.count }
          end

          it "adds the existing user to the new org" do
            new_organization = import_region!
            expect(preexisting_user.organizations.reload).to contain_exactly(new_organization)
          end

          context "when they are an admin with no shifts" do
            let(:region) { create(:robot_region, admins: [volunteer]) }
            it "marks them as an admin" do
              new_organization = import_region!
              expect(preexisting_user.reload.org_admin?(new_organization)).to be_truthy
            end
          end

          context "when they are an admin with shifts" do
            let(:region) { create(:robot_region, admins: [volunteer]) }
            before do
              create(:robot_schedule_volunteer,
                     schedule_chain: create(:robot_schedule_chain, region: region),

                     volunteer_id: volunteer.id)
            end
            it "marks them as a volunteer" do
              new_organization = import_region!
              expect(preexisting_user.reload.volunteer?(new_organization)).to be_truthy
            end
          end

          it "logs a warning" do
            allow(Rails.logger).to receive(:info)
            old_org = preexisting_user.organizations.first

            import_region!

            expect(Rails.logger).to have_received(:info).with(
              /Removing them from their currently assigned organization '#{Regexp.escape(old_org.name)}/
            )
          end
        end
      end

      context "when there is an admin" do
        let(:admin) { create(:robot_volunteer) }
        let(:region) { create(:robot_region, admins: [admin]) }

        it "creates an admin" do
          expect { import_region! }.to change { User.count }.by(1)
          expect(User.last).to have_attributes(
            {
              email: admin.email,
              roles: "ORGANIZATION_ADMINISTRATOR"
            }
          )
        end

        context "and the admin has some shifts" do
          before do
            create(:robot_schedule_volunteer,
                   schedule_chain_id: create(:robot_schedule_chain, region_id: region.id, day_of_week: 1).id,

                   volunteer_id: admin.id)
          end

          it "creates a volunteer" do
            expect { import_region! }.to change { User.count }.by(1)
            expect(User.last).to have_attributes(
              {
                email: admin.email,
                roles: "VOLUNTEER"
              }
            )
          end

          it "logs a warning" do
            allow(Rails.logger).to receive(:info)

            import_region!

            expect(Rails.logger).to have_received(:info).with(
              "User '#{admin.email}' (Robot ID ##{admin.id}) is an admin," \
              " but will be imported as a volunteer because they have active shifts."
            )
          end
        end
      end

      context "when the user has absences" do
        let(:absence) { build(:robot_absence) }
        let(:volunteer) { create(:robot_volunteer, absences: [absence]) }
        let(:region) { create(:robot_region, volunteers: [volunteer]) }

        it "creates the user's absences" do
          expect { import_region! }.to change { Absence.count }.by(1)

          expect(User.last.absences.first).to have_attributes(
            {
              starts_at: absence.start_date,
              ends_at: absence.stop_date
            }
          )
        end
      end
    end

    describe "the imported locations" do
      let(:location) { build(:robot_location) }
      let(:region) { create(:robot_region, locations: [location]) }

      subject(:import_site!) { described_class.import_region(region.id).sites.first }

      it "creates the corresponding site" do
        expect { import_site! }.to change { Site.count }.by(1)
      end

      context "with multiple locations with the same name" do
        let(:locations) { [build(:robot_location, name: "a"), build(:robot_location, name: "a")] }
        let(:region) { create(:robot_region, locations: locations) }
        it "creates both sites" do
          expect { import_site! }.to change { Site.count }.by(2)
        end
      end
      context "with a location with a blank name" do
        let(:location) { build(:robot_location, name: "") }

        it "creates the corresponding site" do
          expect { import_site! }.to change { Site.count }.by(1)
        end
      end

      it "saves the structured data" do
        site = import_site!
        expect(site.robot_location_id).to eql(location.id)
        expect(site.name).to eql(location.name)
        expect(site.lat).to eql(location.lat)
        expect(site.lng).to eql(location.lng)
        expect(site.detailed_hours_json).to eql(location.detailed_hours_json)
        expect(site.receipt_key).to eql(location.receipt_key)
        expect(site.website).to eql(location.website)
      end

      it "saves some of the misc. fields to admin_notes" do
        site = import_site!
        expect(site.admin_notes).to include(location.admin_notes)

        expect(site.admin_notes).to include("Contact Info")
        expect(site.admin_notes).to include("Contact:\n#{location.contact}")
        expect(site.admin_notes).to include("Phone:\n#{location.phone}")
        expect(site.admin_notes).to include("Email:\n#{location.email}")
        expect(site.admin_notes).to include("Twitter Handle:\n#{location.twitter_handle}")

        expect(site.admin_notes).to include("Hours")
        expect(site.admin_notes).to include(location.hours)
      end

      it "saves some of the misc. fields to instructions" do
        site = import_site!
        expect(site.instructions).to include(location.public_notes.to_s)

        expect(site.instructions).to include("Entry Info:\n#{location.entry_info}")
        expect(site.instructions).to include("Equipment Storage Info:\n#{location.equipment_storage_info}")
        expect(site.instructions).to include("Food Storage Info:\n#{location.food_storage_info}")
        expect(site.instructions).to include("Onsite Contact_Info:\n#{location.onsite_contact_info}")
        expect(site.instructions).to include("Exit Info:\n#{location.exit_info}")
      end

      describe "#address" do
        let(:location) { build(:robot_location, address: "123 Fake Street, Boulder, CO, 80301") }

        subject(:address) { described_class.import_region(region.id).sites.first.address }
        it {
          is_expected.to have_attributes({
                                           street_one: "123 Fake Street, Boulder, CO, 80301",
         city: "-",
         state: "-",
         zip: "-"
                                         })
        }

        context "with a blank address" do
          let(:location) { build(:robot_location, address: "") }

          it {
            is_expected.to have_attributes({
                                             street_one: "-",
           city: "-",
           state: "-",
           zip: "-"
                                           })
          }
        end
      end

      describe "#siteable_type" do
        [
          [:recipient, "Recipient"],
          [:donor, "Donor"],
          [:hub, "Hub"],
          [:seller, "Donor"],
          [:buyer, "Recipient"]
        ].each do |location_type, siteable_type|
          context "given location_type: #{location_type}" do
            subject(:import_region!) { described_class.import_region(region.id).sites.first.siteable_type }
            let(:location) { build(:robot_location, location_type) }

            it { is_expected.to eql(siteable_type) }
          end
        end
      end
    end

    describe "the imported shifts" do
      context "given an organization in Mountain Time" do
        let(:region) { create(:robot_region, time_zone: "America/Denver") }

        let!(:schedule_chain) do
          create(:robot_schedule_chain,
                 detailed_start_time: "09:00",
                 detailed_stop_time: "10:00",
                 detailed_date: "2023-01-02",
                 region: region,
                 frequency: "weekly",
                 day_of_week: 1)
        end
        let(:locations) do
          [build(:robot_location, :seller, region: region, name: "pickup location"),
           build(:robot_location, :buyer, region: region, name: "dropoff location")]
        end
        let(:cutoff_date) { "2023-01-01" }
        subject(:import_shifts!) { described_class.import_region(region.id, cutoff_date: cutoff_date).shifts }

        before do
          schedule_chain.locations = locations
        end

        it "creates a shift with the right time" do
          event = import_shifts!.first.shift_events.first.event
          expect(event.starts_at.in_time_zone(region.time_zone).strftime("%T")).to eql("09:00:00")
          expect(event.ends_at.in_time_zone(region.time_zone).strftime("%T")).to eql("10:00:00")
        end
      end

      context "given a weekly schedule_chain" do
        let!(:schedule_chain) do
          create(:robot_schedule_chain,
                 detailed_start_time: "13:00",
                 detailed_stop_time: "14:00",
                 detailed_date: "2022-06-06",
                 region: region,
                 frequency: "weekly",
                 day_of_week: 1,
                volunteers: volunteers)
        end

        let(:volunteers) { create_list(:robot_volunteer, 3, regions: [region]) }
        let(:locations) do
          [build(:robot_location, :seller, region: region, name: "pickup location"),
           build(:robot_location, :buyer, region: region, name: "dropoff location 1"),
           build(:robot_location, :buyer, region: region, name: "dropoff location 2")]
        end
        let(:cutoff_date) { "2022-11-12" }
        subject(:import_shifts!) { described_class.import_region(region.id, cutoff_date: cutoff_date).shifts }

        before do
          schedule_chain.locations = locations
        end

        it "imports the shift" do
          expect { import_shifts! }.to change { Shift.count }.by(1)
        end

        it "defaults to repeating on sundays when day_of_week is nil" do
          schedule_chain.update!(day_of_week: nil)
          shift = import_shifts!.last
          expect(
            shift.shift_events.first.rrule
          ).to eql(
            "FREQ=WEEKLY;BYDAY=SU;INTERVAL=1"
          )
        end

        it "creates a shift starting on the cutoff date" do
          shift = import_shifts!.last
          event = shift.shift_events.first.event
          expect(
            event.starts_at.in_time_zone(region.time_zone).strftime("%F")
          ).to eql(
            "2022-11-14" # 11/14 is the first Monday to follow the cutoff: 11/12
          )
        end

        it "creates a shift with the right recurrence rule" do
          shift = import_shifts!.last
          expect(
            shift.shift_events.first.rrule
          ).to eql(
            "FREQ=WEEKLY;BYDAY=MO;INTERVAL=1"
          )
        end

        it "assigns the correct users to the task" do
          shift = import_shifts!.last
          shift_event = shift.shift_events.first
          expect(
            shift_event.users.pluck(:email)
          ).to match_array(volunteers.pluck(:email))
        end

        it "creates a shift with the correct tasks" do
          shift = import_shifts!.last
          shift_event = shift.shift_events.first
          expect(
            shift_event.tasks.map { |t| [t.site.name, t.site.siteable_type] }
          ).to eq([["pickup location", "Donor"], ["dropoff location 1", "Recipient"],
                   ["dropoff location 2", "Recipient"]])
        end

        context "when theres a hub in the middle" do
          let(:locations) do
            [build(:robot_location, :seller, region: region, name: "A"),
             build(:robot_location, :hub, region: region, name: "B"),
             build(:robot_location, :buyer, region: region, name: "C")]
          end
          it "creates a pickup and drop off at the hub" do
            shift = import_shifts!.last
            shift_event = shift.shift_events.first
            expect(
              shift_event.tasks.map { |t| [t.site.name, t.task_type] }
            ).to eq([%w[A pickup], %w[B pickup], %w[B drop_off], %w[C drop_off]])
          end
        end
        context "when theres a hub at the beginning" do
          let(:locations) do
            [build(:robot_location, :hub, region: region, name: "A"),
             build(:robot_location, :seller, region: region, name: "B"),
             build(:robot_location, :recipient, region: region, name: "C")]
          end
          it "creates a pickup at the hub" do
            shift = import_shifts!.last
            shift_event = shift.shift_events.first
            expect(
              shift_event.tasks.map { |t| [t.site.name, t.task_type] }
            ).to eq([%w[A pickup], %w[B pickup], %w[C drop_off]])
          end
        end
        context "when theres a hub at the end" do
          let(:locations) do
            [build(:robot_location, :donor, region: region, name: "A"),
             build(:robot_location, :buyer, region: region, name: "B"),
             build(:robot_location, :hub, region: region, name: "C")]
          end
          it "creates a drop off at the hub" do
            shift = import_shifts!.last
            shift_event = shift.shift_events.first
            expect(
              shift_event.tasks.map { |t| [t.site.name, t.task_type] }
            ).to eq([%w[A pickup], %w[B drop_off], %w[C drop_off]])
          end
        end
      end

      context "given a weekly schedule_chain with logs" do
        let!(:schedule_chain) do
          create(:robot_schedule_chain,
                 detailed_start_time: "13:00",
                 detailed_stop_time: "14:00",
                 detailed_date: "2022-06-06",
                 frequency: "weekly",
                 day_of_week: 1,
                 region: region,
                 locations: locations,
                volunteers: volunteers)
        end

        let(:volunteers) { create_list(:robot_volunteer, 3, regions: [region]) }
        let(:locations) do
          [build(:robot_location, :seller, region: region, name: "pickup location"),
           build(:robot_location, :buyer, region: region, name: "dropoff location 1"),
           build(:robot_location, :buyer, region: region, name: "dropoff location 2")]
        end
        let(:cutoff_date) { "2022-11-12" }
        subject(:import_shifts!) { described_class.import_region(region.id, cutoff_date: cutoff_date).shifts.first }
        let(:log_volunteers) { volunteers }

        let!(:robot_log) do
          create(:robot_log,
                 schedule_chain_id: schedule_chain.id,
                 donor: locations.first,
                 region: region,
                 num_volunteers: log_volunteers.count,
                 volunteers: log_volunteers,
                 recipients: locations[1..],
                 when: "2022-12-12")
        end

        context "when a volunteer is missing in a log entry" do
          let(:log_volunteers) { volunteers[1..] }
          it "creates an event for the log date" do
            expect(import_shifts!.shift_events.count).to eql(2)
          end

          it "marks the volunteer as absent on the log date" do
            expect(
              import_shifts!.shift_events.last.volunteers.pluck(:email)
            ).to match_array(
              log_volunteers.pluck(:email)
            )
          end
        end

        context "when an additional volunteer has picked up the shift" do
          let(:log_volunteers) { volunteers + [create(:robot_volunteer, regions: [region])] }
          it "creates an event for the log date" do
            expect(import_shifts!.shift_events.count).to eql(2)
          end

          it "marks the volunteer as absent on the log date" do
            expect(
              import_shifts!.shift_events.last.volunteers.pluck(:email)
            ).to match_array(
              log_volunteers.pluck(:email).sort
            )
          end
        end

        context "when there has been no change to the volunteers" do
          let(:log_volunteers) { volunteers }
          it "does not create an event for the log date" do
            expect(import_shifts!.shift_events.count).to eql(1)
          end
        end
        context "when there have been changes to the schedule" do
          let!(:robot_log)  do
            create(:robot_log,
                   schedule_chain_id: schedule_chain.id,
                   donor: create(:robot_location, :donor, region: region, name: "alt pickup"),
                   region: region,
                   num_volunteers: log_volunteers.count,
                   volunteers: log_volunteers,
                   recipients: [create(:robot_location, :buyer, region: region, name: "alt dropoff 1"),
                                create(:robot_location, :recipient, region: region, name: "alt dropoff 2")],
                   when: "2022-12-12")
          end

          let(:log_volunteers) { volunteers }
          it "creates an additional event" do
            expect(import_shifts!.shift_events.count).to eql(2)
          end

          it "updates that event's task ids, but not necessarily in order" do
            donor, *recipients = import_shifts!.shift_events.last.tasks.map(&:site).pluck(:name)
            expect(donor).to eql("alt pickup") # pickups always come first
            expect(recipients).to match_array([ # but tasks of like kind may not be ordered because logs have no ordinal
              "alt dropoff 1",
              "alt dropoff 2"
            ])
          end
        end
      end

      context "given a one-time schedule_chain" do
        let!(:schedule_chain) do
          create(:robot_schedule_chain,
                 detailed_start_time: "10:00",
                 detailed_stop_time: "11:00",
                 detailed_date: "2022-3-14",
                 region: region,
                 frequency: "one-time")
        end

        subject(:import_shifts!) { described_class.import_region(region.id, cutoff_date: cutoff_date).shifts }

        let(:locations) do
          [build(:robot_location, :seller, region: region, name: "pickup location"),
           build(:robot_location, :buyer, region: region, name: "dropoff location")]
        end

        before do
          schedule_chain.locations = locations
        end

        context "scheduled after the cutoff" do
          let(:cutoff_date) { "2022-3-13" }

          it "imports the shift" do
            expect { import_shifts! }.to change { Shift.count }.by(1)
          end

          it "creates a shift with no recurrence" do
            shift = import_shifts!.last
            event = shift.shift_events.first.event

            expect(event.rrule).to be_nil
          end

          it "creates a shift on the scheduled day" do
            shift = import_shifts!.last
            event = shift.shift_events.first.event

            expect(
              event.starts_at.in_time_zone(region.time_zone).strftime("%F")
            ).to eql(
              schedule_chain.detailed_date.strftime("%F")
            )
          end
        end

        context "scheduled before the cutoff" do
          let(:cutoff_date) { "2022-3-15" }

          it "doesn't create a shift" do
            expect { import_shifts! }.not_to change { Shift.count }
          end
        end
      end
    end
  end
end
