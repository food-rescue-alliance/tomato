require "rails_helper"

RSpec.describe EventsService::EventOccurrenceTimeline do
  before do
    @month_start = Time.iso8601 "2020-10-01T05:00:00Z"
    @month_end = Time.iso8601 "2020-10-31T06:00:00Z"

    @organization = build :organization

    @first_monday_start = Time.iso8601 "2020-10-05T03:00:00Z"
    @first_monday_end = Time.iso8601 "2020-10-05T05:00:00Z"
    @second_monday_start = Time.iso8601 "2020-10-12T03:00:00Z"
    @second_monday_end = Time.iso8601 "2020-10-12T05:00:00Z"
    @third_monday_start = Time.iso8601 "2020-10-19T03:00:00Z"
    @third_monday_end = Time.iso8601 "2020-10-19T05:00:00Z"
    @fourth_monday_start = Time.iso8601 "2020-10-26T03:00:00Z"
    @forth_monday_end = Time.iso8601 "2020-10-26T05:00:00Z"

    @first_tuesday_start = Time.iso8601 "2020-10-06T03:00:00Z"
    @first_tuesday_end = Time.iso8601 "2020-10-06T05:00:00Z"
    @first_wednesday_start = Time.iso8601 "2020-10-07T03:00:00Z"
    @first_wednesday_end = Time.iso8601 "2020-10-07T05:00:00Z"
  end

  describe ".new" do
    it "returns an instance given empty array" do
      result = described_class.new events: [], starts_at: nil, ends_at: nil

      expect(result).to be_present
    end

    context "with an recurring event" do
      let(:event) do
        build :event,
              :weekly_on_mondays,
              starts_at: @first_monday_start,
              ends_at: @first_monday_end
      end

      it "has a month of occurrences when given events" do
        timeline = described_class.new(
          events: [event],
          starts_at: @month_start,
          ends_at: @month_end
        )

        expect(timeline.occurrences.length).to eq 4
      end
    end
  end

  describe "fetch" do
    it "returns nil given no events" do
      event = build :event
      subject = described_class.new(
        events: [],
        starts_at: Time.iso8601("2020-11-01T06:00:00Z"),
        ends_at: Time.iso8601("2020-11-30T06:00:00Z")
      )

      result = subject.fetch event.uid, event.starts_at

      expect(result).to be_nil
    end

    it "returns nil given occurrence doesn't follow recurrence" do
      event = build :event,
                    :weekly_on_mondays,
                    starts_at: @first_monday_start,
                    ends_at: @first_monday_end

      subject = described_class.new(
        events: [event],
        starts_at: @month_start,
        ends_at: @month_end
      )

      result = subject.fetch(event.uid, event.starts_at + 1.day)

      expect(result).to be_nil
    end

    it "returns occurrence given an id following a recurrence" do
      event = build :event,
                    :weekly_on_mondays,
                    starts_at: @first_monday_start,
                    ends_at: @first_monday_end

      subject = described_class.new(
        events: [event],
        starts_at: @month_start,
        ends_at: @month_end
      )

      result = subject.fetch(event.uid, @third_monday_start)

      expect(result).to be_a_kind_of(EventOccurrence)
      expect(result.starts_at).to eq(@third_monday_start)
    end

    it "returns occurrence that begins outside of range but ends within start" do
      starts_at = Time.iso8601("2020-10-04T02:00:00Z")
      event = build :event,
                    :single,
                    starts_at: starts_at,
                    ends_at: @first_monday_start

      subject = described_class.new(
        events: [event],
        starts_at: @first_monday_start,
        ends_at: @third_monday_end
      )

      result = subject.fetch(event.uid, starts_at)

      expect(result).to_not be_nil
      expect(result.starts_at).to eq(event.starts_at)
      expect(result.ends_at).to eq(event.ends_at)
    end
  end

  describe "slice" do
    it "returns an empty array given no events" do
      event = build :event
      subject = described_class.new(
        events: [],
        starts_at: @month_start,
        ends_at: @month_end
      )

      result = subject.slice event.uid, event.starts_at

      expect(result).to be_empty
    end

    it "returns an empty array given an occurrence that doesn't follow timeline" do
      event = build :event, :weekly_on_mondays,
                    starts_at: @first_monday_start,
                    ends_at: @first_monday_end,
                    ownable: @organization
      subject = described_class.new(
        events: [event],
        starts_at: @month_start,
        ends_at: @month_end
      )

      result = subject.slice event.uid, @first_tuesday_start

      expect(result).to be_empty
    end

    it "returns one single event given an occurrence that matches an event time" do
      single_event = build :event, :single,
                           starts_at: @first_wednesday_start,
                           ends_at: @first_wednesday_end,
                           ownable: @organization
      weekly_event = build :event, :weekly_on_mondays,
                           starts_at: @first_monday_start,
                           ends_at: @first_monday_end,
                           ownable: @organization
      subject = described_class.new(
        events: [single_event, weekly_event],
        starts_at: @month_start,
        ends_at: @month_end
      )

      result = subject.slice single_event.uid, single_event.starts_at

      expect(result.first).to be_nil
      expect(result.last).to eq(single_event)
      expect(result.last.recurrence_options).to be(nil)
    end

    it "returns one recurring event given an occurrence that matches an event time" do
      single_event = build :event, :single,
                           starts_at: @first_wednesday_start,
                           ends_at: @first_wednesday_end,
                           ownable: @organization
      weekly_event = build :event, :weekly_on_mondays,
                           starts_at: @first_monday_start,
                           ends_at: @first_monday_end,
                           ownable: @organization
      subject = described_class.new(
        events: [single_event, weekly_event],
        starts_at: @month_start,
        ends_at: @month_end
      )

      result = subject.slice weekly_event.uid, weekly_event.starts_at

      expect(result.first).to be_nil
      expect(result.last).to eq(weekly_event)
      expect(result.last.recurrence_options).to_not have_key("UNTIL")
    end

    it "returns a cut event and a new following eventable given an occurrence of a recurrence" do
      weekly_event = build :event, :weekly_on_mondays,
                           starts_at: @first_monday_start,
                           ends_at: @first_monday_end,
                           ownable: @organization
      subject = described_class.new(
        events: [weekly_event],
        starts_at: @month_start,
        ends_at: @month_end
      )

      result = subject.slice weekly_event.uid, @third_monday_start

      expect(result.first).to eq(weekly_event)
      expect(result.first.recurrence_options).to have_key("UNTIL")
      expect(Time.zone.parse(result.first.recurrence_options["UNTIL"])).to be_before(result.last.starts_at)
      expect(result.last).to be_a_kind_of(Event)
      expect(result.last).to_not be_persisted
      expect(result.last.title).to eq(weekly_event.title)
      expect(result.last.recurrence_options).to_not have_key("UNTIL")
    end
  end

  describe "occurrences" do
    it "returns empty array given no events" do
      schedule_starts_at = Time.iso8601("2020-10-05T00:00:00Z")
      schedule_ends_at = Time.iso8601("2020-10-31T24:00:00Z")
      subject = described_class.new events: [], starts_at: schedule_starts_at, ends_at: schedule_ends_at

      result = subject.occurrences

      expect(result).to be_empty
    end

    it "returns empty array given past event is out of range" do
      halloween_start = "2020-10-31T06:00:00Z"
      halloween_end = "2020-10-31T08:30:00Z"
      event = build :event, starts_at: halloween_start, ends_at: halloween_end
      subject = described_class.new(
        events: [event],
        starts_at: Time.iso8601("2020-11-01T06:00:00Z"),
        ends_at: Time.iso8601("2020-11-30T06:00:00Z")
      )

      result = subject.occurrences

      expect(result).to be_empty
    end

    it "returns empty array given future event is out of range" do
      halloween_start = "2020-10-31T06:00:00Z"
      halloween_end = "2020-10-31T08:30:00Z"
      event = build :event, starts_at: halloween_start, ends_at: halloween_end
      subject = described_class.new(
        events: [event],
        starts_at: Time.iso8601("2020-10-01T06:00:00Z"),
        ends_at: Time.iso8601("2020-10-30T06:00:00Z")
      )

      result = subject.occurrences

      expect(result).to be_empty
    end

    it "returns empty array when missing parts of schedule range" do
      halloween_start = "2020-10-31T06:00:00Z"
      halloween_end = "2020-10-31T08:30:00Z"
      event = build :event, starts_at: halloween_start, ends_at: halloween_end
      subject = described_class.new(
        events: [event],
        starts_at: nil,
        ends_at: nil
      )

      result = subject.occurrences

      expect(result).to be_empty
    end

    it "returns occurrences according to timeline start time, not event start time" do
      event = build :event,
                    :weekly_on_mondays,
                    starts_at: Time.iso8601("2020-10-05T03:00:00Z"),
                    ends_at: Time.iso8601("2020-10-05T05:00:00Z")

      subject = described_class.new(
        events: [event],
        starts_at: @third_monday_start,
        ends_at: @forth_monday_end
      )

      results = subject.occurrences

      expect(results.length).to eq(2)
      expect(results[0].starts_at).to eq(@third_monday_start)
      expect(results[0].ends_at).to eq(@third_monday_end)
      expect(results[1].starts_at).to eq(@fourth_monday_start)
      expect(results[1].ends_at).to eq(@forth_monday_end)
    end

    it "returns occurrences for recurring and single events" do
      halloween_start = "2020-10-31T06:00:00Z"
      halloween_end = "2020-10-31T08:30:00Z"
      halloween_event = build :event, starts_at: halloween_start, ends_at: halloween_end
      weekly_event = build :event,
                           :weekly_on_mondays,
                           starts_at: Time.iso8601("2020-09-28T03:00:00Z"),
                           ends_at: Time.iso8601("2020-09-28T05:00:00Z")

      schedule_starts_at = Time.iso8601("2020-10-05T00:00:00Z")
      schedule_ends_at = Time.iso8601("2020-10-31T24:00:00Z")

      subject = described_class.new(
        events: [halloween_event, weekly_event],
        starts_at: schedule_starts_at,
        ends_at: schedule_ends_at
      )

      result = subject.occurrences

      expect(result.size).to eq(5)
      expect(result.first.starts_at).to eq(Time.iso8601("2020-10-05T03:00:00Z"))
      expect(result.last.starts_at.iso8601).to eq(halloween_start)
      expect(result.last.ends_at.iso8601).to eq(halloween_end)
    end
  end
end
