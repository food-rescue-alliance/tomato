require "rails_helper"

describe RobotImportService::EventFinder do
  describe ".occurrence_on_date" do
    before do
      @weekly_event_starts_at = Time.iso8601("2020-10-19T03:00:00Z")
      @weekly_event_ends_at = Time.iso8601("2020-10-19T05:00:00Z")
      @organization = create :organization
    end

    it "returns a shift event occurrence on the requisite date" do
      shift = create :shift, :weekly_on_mondays,
                     organization: @organization,
                     starts_at: @weekly_event_starts_at,
                     ends_at: @weekly_event_ends_at
      result = described_class.occurrence_on_date(shift, "2020-10-26")
      expect(result).to have_attributes({
                                          starts_at: Time.iso8601("2020-10-26T03:00:00Z"),
        ends_at: Time.iso8601("2020-10-26T05:00:00Z")
                                        })
    end
    it "doesn't return anything if the event doesn't occurr on the date" do
      shift = create :shift, :weekly_on_mondays,
                     organization: @organization,
                     starts_at: @weekly_event_starts_at,
                     ends_at: @weekly_event_ends_at
      result = described_class.occurrence_on_date(shift, "2020-10-27")
      expect(result).to be_nil
    end
  end
end
