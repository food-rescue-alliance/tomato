require "rails_helper"

describe EventsService do
  describe ".all_occurrences" do
    before do
      @third_monday_start = "2020-10-19T03:00:00Z"
      @third_monday_end = "2020-10-19T05:00:00Z"
      @fourth_monday_start = "2020-10-26T03:00:00Z"
      @fourth_monday_end = "2020-10-26T05:00:00Z"
      @weekly_event_starts_at = Time.iso8601(@third_monday_start)
      @weekly_event_ends_at = Time.iso8601(@third_monday_end)
      @organization = create :organization
    end

    it "returns an empty array" do
      other_organization = create :organization

      result = EventsService.all_occurrences(
        other_organization,
        ShiftEvent,
        @weekly_event_starts_at..@weekly_event_ends_at
      )

      expect(result).to be_empty
    end

    it "returns all shift event occurrences for an organization" do
      single_oct_10_start = "2020-10-10T08:45:00Z"
      single_oct_10_end = "2020-10-10T09:30:00Z"
      create :shift, :weekly_on_mondays,
             organization: @organization,
             starts_at: @weekly_event_starts_at,
             ends_at: @weekly_event_ends_at
      other_organization = create :organization
      create :shift, :single,
             organization: other_organization,
             starts_at: single_oct_10_start,
             ends_at: single_oct_10_end
      date_range = Time.iso8601("2020-10-01T00:00:00Z")..Time.iso8601("2020-10-31T24:00:00Z")

      result_for_organization = EventsService.all_occurrences(
        @organization,
        ShiftEvent,
        date_range
      )
      result_for_other_organization = EventsService.all_occurrences(
        other_organization,
        ShiftEvent,
        date_range
      )

      expect(result_for_organization.length).to eq(2)
      expect(result_for_other_organization.length).to eq(1)
    end

    it "returns events for single and recurring events within range" do
      create :shift, :weekly_on_mondays,
             organization: @organization,
             starts_at: @weekly_event_starts_at,
             ends_at: @weekly_event_ends_at

      single_oct_10_start = "2020-10-10T08:45:00Z"
      single_oct_10_end = "2020-10-10T09:30:00Z"
      create :shift, :single,
             organization: @organization,
             starts_at: single_oct_10_start,
             ends_at: single_oct_10_end

      single_oct_25_start = "2020-10-25T08:45:00Z"
      single_oct_25_end = "2020-10-25T09:30:00Z"
      create :shift, :single,
             organization: @organization,
             starts_at: single_oct_25_start,
             ends_at: single_oct_25_end
      single_nov_01_start = "2020-11-01T08:45:00Z"
      single_nov_01_end = "2020-11-01T09:30:00Z"
      create :shift, :single,
             organization: @organization,
             starts_at: single_nov_01_start,
             ends_at: single_nov_01_end
      date_range = Time.iso8601("2020-10-25T00:00:00Z")..Time.iso8601("2020-10-31T24:00:00Z")

      result = EventsService.all_occurrences(
        @organization,
        ShiftEvent,
        date_range
      )
      result_array = result.to_a

      expect(result.size).to eq(2)

      expect(result_array[0].starts_at.iso8601).to eq(single_oct_25_start)
      expect(result_array[0].ends_at.iso8601).to eq(single_oct_25_end)

      expect(result_array[1].starts_at.iso8601).to eq(@fourth_monday_start)
      expect(result_array[1].ends_at.iso8601).to eq(@fourth_monday_end)
    end

    it "returns event occurrences from an event that repeats forever and start earlier than schedule range" do
      create :shift, :weekly_on_mondays,
             organization: @organization,
             starts_at: @weekly_event_starts_at,
             ends_at: @weekly_event_ends_at

      date_range = Time.iso8601("2020-11-02T00:00:00Z")..Time.iso8601("2020-11-30T24:00:00Z")

      result = EventsService.all_occurrences(
        @organization,
        ShiftEvent,
        date_range
      )

      expect(result.size).to eq(5)
    end
  end

  describe ".all_user_occurrences" do
    before do
      @third_monday_start = "2020-10-19T03:00:00Z"
      @third_monday_end = "2020-10-19T05:00:00Z"
      @fourth_monday_start = "2020-10-26T03:00:00Z"
      @fourth_monday_end = "2020-10-26T05:00:00Z"
      @weekly_event_starts_at = Time.iso8601(@third_monday_start)
      @weekly_event_ends_at = Time.iso8601(@third_monday_end)

      @volunteer = create :volunteer
    end

    it "returns an empty array" do
      create :shift_event, :single

      result = EventsService.all_user_occurrences(
        @volunteer,
        @weekly_event_starts_at..@weekly_event_ends_at
      )

      expect(result).to be_empty
    end

    it "returns all shift event occurrences for an organization" do
      single_oct_10_start = "2020-10-10T08:45:00Z"
      single_oct_10_end = "2020-10-10T09:30:00Z"

      create :shift_event, :weekly_on_mondays,
             starts_at: @weekly_event_starts_at,
             ends_at: @weekly_event_ends_at,
             user_ids: [@volunteer.id]
      other_volunteer = create :volunteer
      create :shift_event, :single,
             starts_at: single_oct_10_start,
             ends_at: single_oct_10_end,
             user_ids: [other_volunteer.id]

      date_range = Time.iso8601("2020-10-01T00:00:00Z")..Time.iso8601("2020-10-31T24:00:00Z")

      result_for_volunteer = EventsService.all_user_occurrences(
        @volunteer,
        date_range
      )
      result_for_other_volunteer = EventsService.all_user_occurrences(
        other_volunteer,
        date_range
      )

      expect(result_for_volunteer.length).to eq(2)
      expect(result_for_other_volunteer.length).to eq(1)
    end

    it "returns events for single and recurring events within range" do
      create :shift_event, :weekly_on_mondays,
             starts_at: @weekly_event_starts_at,
             ends_at: @weekly_event_ends_at,
             user_ids: [@volunteer.id]

      single_oct_10_start = "2020-10-10T08:45:00Z"
      single_oct_10_end = "2020-10-10T09:30:00Z"
      create :shift_event, :single,
             starts_at: single_oct_10_start,
             ends_at: single_oct_10_end,
             user_ids: [@volunteer.id]

      single_oct_25_start = "2020-10-25T08:45:00Z"
      single_oct_25_end = "2020-10-25T09:30:00Z"
      create :shift_event, :single,
             starts_at: single_oct_25_start,
             ends_at: single_oct_25_end,
             user_ids: [@volunteer.id]
      single_nov_01_start = "2020-11-01T08:45:00Z"
      single_nov_01_end = "2020-11-01T09:30:00Z"
      create :shift_event, :single,
             starts_at: single_nov_01_start,
             ends_at: single_nov_01_end,
             user_ids: [@volunteer.id]
      date_range = Time.iso8601("2020-10-25T00:00:00Z")..Time.iso8601("2020-10-31T24:00:00Z")

      result = EventsService.all_user_occurrences(
        @volunteer,
        date_range
      )
      result_array = result.to_a

      expect(result.size).to eq(2)

      expect(result_array[0].starts_at.iso8601).to eq(single_oct_25_start)
      expect(result_array[0].ends_at.iso8601).to eq(single_oct_25_end)

      expect(result_array[1].starts_at.iso8601).to eq(@fourth_monday_start)
      expect(result_array[1].ends_at.iso8601).to eq(@fourth_monday_end)
    end

    it "returns event occurrences from an event that repeats forever and start earlier than schedule range" do
      create :shift_event, :weekly_on_mondays,
             starts_at: @weekly_event_starts_at,
             ends_at: @weekly_event_ends_at,
             user_ids: [@volunteer.id]

      date_range = Time.iso8601("2020-11-02T00:00:00Z")..Time.iso8601("2020-11-30T24:00:00Z")

      result = EventsService.all_user_occurrences(
        @volunteer,
        date_range
      )

      expect(result.size).to eq(5)
    end
  end

  describe ".find_next_occurrence" do
    before do
      @starts_at = "2020-10-26T05:00:00Z"
      @ends_at = "2020-10-26T06:00:00Z"
      @event = create(:event, :weekly_on_mondays, starts_at: @starts_at, ends_at: @ends_at)
    end

    it "returns the next occurrence" do
      following_starts_at = "2020-11-02T05:00:00Z"

      result = EventsService.find_next_occurrence @event.uid, Time.iso8601(@starts_at) + 1

      expect(result.starts_at).to eq(following_starts_at)
    end

    it "returns nil if there are no more occurrences" do
      following_starts_at = "2020-11-02T05:00:00Z"
      @event.recurrence_options["UNTIL"] = "2020-11-03T00:00:00Z"
      @event.save!

      result = EventsService.find_next_occurrence @event.uid, Time.iso8601(following_starts_at) + 1

      expect(result).to be_nil
    end
  end

  describe ".all_open_user_occurrences" do
    before do
      @third_monday_start = "2020-10-19T03:00:00Z"
      @third_monday_end = "2020-10-19T05:00:00Z"
      @forth_monday_start = "2020-10-26T03:00:00Z"
      @forth_monday_end = "2020-10-26T05:00:00Z"
      @weekly_event_starts_at = Time.iso8601(@third_monday_start)
      @weekly_event_ends_at = Time.iso8601(@third_monday_end)

      @volunteer = create :volunteer
      @other_volunteer = create :volunteer
      @organization = @volunteer.organizations[0]
    end

    it "returns an array of event occurrences for an organization with no volunteers" do
      open_recurring_shift_event = create :shift_event, :weekly_on_mondays,
                                          starts_at: @weekly_event_starts_at,
                                          ends_at: @weekly_event_ends_at,
                                          organization: @organization,
                                          users: []

      open_shift_event = create :shift_event, :single,
                                starts_at: @third_monday_start,
                                ends_at: @third_monday_end,
                                organization: @organization,
                                users: []

      create :shift_event, :single,
             starts_at: @third_monday_start,
             ends_at: @third_monday_end,
             organization: @organization,
             user_ids: [@other_volunteer.id]

      create :shift_event, :single,
             starts_at: @third_monday_start,
             ends_at: @third_monday_end,
             users: []

      create :shift_event, :single,
             starts_at: @forth_monday_start,
             ends_at: @forth_monday_end,
             users: []

      date_range = Time.iso8601("2020-10-01T00:00:00Z")..Time.iso8601("2020-10-31T24:00:00Z")

      result = EventsService.all_open_user_occurrences(
        @volunteer,
        date_range
      )

      expect(result[:recurring].length).to eq(2)
      expect(result[:recurring][0]["title"]).to eq(open_recurring_shift_event.title)
      expect(result[:recurring][1]["title"]).to eq(open_recurring_shift_event.title)

      expect(result[:one_time].length).to eq(1)
      expect(result[:one_time][0]["title"]).to eq(open_shift_event.title)
    end
  end

  describe ".find_occurrence" do
    before do
      @starts_at = "2020-10-26T05:00:00Z"
      @ends_at = "2020-10-26T06:00:00Z"
      @event = create(:event, :weekly_on_mondays, starts_at: @starts_at, ends_at: @ends_at)
    end

    it "returns an event occurrence given a time for the next occurrence" do
      following_starts_at = "2020-11-02T05:00:00Z"
      following_ends_at = "2020-11-02T06:00:00Z"

      result = EventsService.find_occurrence @event.uid, Time.iso8601(following_starts_at)

      expect(result.starts_at).to eq(following_starts_at)
      expect(result.ends_at).to eq(following_ends_at)
    end

    it "raises NotFoundError for invalid identifiers" do
      result = EventsService.find_occurrence @event, nil

      expect(result).to be_nil
    end

    it "raises NotFoundError when there are no Occurrences" do
      next_day_at_five = Time.iso8601("2020-10-26T05:00:00Z") + 1.day

      result = EventsService.find_occurrence @event, next_day_at_five

      expect(result).to be_nil
    end
  end

  describe ".update_into_future" do
    before do
      @third_monday_start = Time.iso8601 "2020-10-19T03:00:00Z"
      @third_monday_end = Time.iso8601 "2020-10-19T05:00:00Z"
      @fourth_monday_start = Time.iso8601 "2020-10-26T03:00:00Z"
      @organization = create :organization
      @site = create :site, organization: @organization
      @volunteer = create :volunteer, organization_ids: [@organization.id]
    end

    it "returns new event starting after updated event ends" do
      weekly_shift = create :shift,
                            :weekly_on_mondays,
                            organization: @organization,
                            starts_at: @third_monday_start,
                            ends_at: @third_monday_end
      single_oct_27_start = Time.iso8601 "2020-10-27T08:45:00Z"
      single_oct_27_end = Time.iso8601 "2020-10-27T09:30:00Z"
      create :shift_event, :single,
             uid: weekly_shift.shift_events.first.uid,
             organization: @organization,
             starts_at: single_oct_27_start,
             ends_at: single_oct_27_end
      event = weekly_shift.shift_events.first
      event.starts_at = @fourth_monday_start # 10/26
      expect(event.recurrence_options["UNTIL"]).to be_blank

      result = EventsService.update_into_future(
        event,
        {}
      )

      expect(result).to be_persisted
      expect(result.id).to_not eq(event.id)
      # the new event should repeat indefinitely like the original event
      expect(result.recurrence_options["UNTIL"]).to be_blank
      # the original event should repeat until the new starts_at time (10-26)
      expect(event.reload.recurrence_options["UNTIL"]).to match(/2020-10-26/)
    end

    it "should allow updating from a one-off date" do
      mondays = [
        "2023-01-02", # A <- This will be the original occurrence
        "2023-01-09", # B <- We'll create a one-off on this monday
        "2023-01-16", # C
        "2023-01-23"  # D
      ].map { |day| [Time.iso8601("#{day}T03:00:00Z"), Time.iso8601("#{day}T05:00:00Z")] }

      weekly_shift = create :shift,
                            :weekly_on_mondays,
                            organization: @organization,
                            starts_at: mondays[0][0],
                            ends_at: mondays[0][1]

      first_shift_event = weekly_shift.shift_events.first

      # This one-off is created to assign a volunteer for this single date
      one_off = create :shift_event, :single,
                       uid: first_shift_event.uid,
                       organization: @organization,
                       user_ids: [@volunteer.id],
                       starts_at: mondays[1][0],
                       ends_at: mondays[1][1]

      # Exclude the one-off from the recurrence:
      first_shift_event.exdates << one_off.starts_at
      first_shift_event.save!

      occurrences = mondays.map { |starts_at, _| EventsService.find_occurrence first_shift_event.uid, starts_at }

      # So at this point we should have:
      expect(occurrences.map { |o| o.eventable.user_ids }).to eql([
        [],
        [@volunteer.id], # Our one-off
        [],
        []
      ])

      # Running update_into_future here should update Mondays B, C, & D, but not A.
      EventsService.update_into_future(
        one_off,
        {
          task_ids: [@site.tasks.first.id]
        }
      )
      occurrences = mondays.map { |starts_at, _| EventsService.find_occurrence first_shift_event.uid, starts_at }

      # So at this point we should have assigned the task to occurrence B, C, & D:
      expect(occurrences.map { |o| o.eventable.task_ids }).to eql([
        [], # This occurrence should not be updated, since we wanted to update into future.
        [@site.tasks.first.id],
        [@site.tasks.first.id],
        [@site.tasks.first.id]
      ])
    end

    it "updates both future and one-off events with new tasks" do
      weekly_shift = create :shift,
                            :weekly_on_mondays,
                            organization: @organization,
                            starts_at: @third_monday_start,
                            ends_at: @third_monday_end
      single_oct_27_start = Time.iso8601 "2020-10-27T08:45:00Z"
      single_oct_27_end = Time.iso8601 "2020-10-27T09:30:00Z"
      one_off = create :shift_event, :single,
                       uid: weekly_shift.shift_events.first.uid,
                       organization: @organization,
                       starts_at: single_oct_27_start,
                       ends_at: single_oct_27_end
      event = weekly_shift.shift_events.first

      result = EventsService.update_into_future(
        event,
        {
          task_ids: [@site.tasks.first.id]
        }
      )

      expect(result.shift_event.reload.task_ids).to eql([@site.tasks.first.id])
      expect(one_off.reload.task_ids).to eql([@site.tasks.first.id])
    end

    it "doesn't touch the original exdates if no frequency params are included" do
      weekly_shift = create :shift,
                            :weekly_on_mondays,
                            organization: @organization,
                            starts_at: @third_monday_start,
                            ends_at: @third_monday_end
      single_oct_27_start = Time.iso8601 "2020-10-27T08:45:00Z"
      single_oct_27_end = Time.iso8601 "2020-10-27T09:30:00Z"
      one_off = create :shift_event, :single,
                       uid: weekly_shift.shift_events.first.uid,
                       organization: @organization,
                       starts_at: single_oct_27_start,
                       ends_at: single_oct_27_end
      event = weekly_shift.shift_events.first

      event.event.exdates << one_off.starts_at
      event.event.save!

      result = EventsService.update_into_future(
        event,
        {
          task_ids: [@site.tasks.first.id]
        }
      )

      expect(result.shift_event.reload.task_ids).to eql([@site.tasks.first.id])
      expect(one_off.reload.task_ids).to eql([@site.tasks.first.id])
    end

    it "updates both future and one-off events with new volunteers" do
      weekly_shift = create :shift,
                            :weekly_on_mondays,
                            organization: @organization,
                            starts_at: @third_monday_start,
                            ends_at: @third_monday_end
      single_oct_27_start = Time.iso8601 "2020-10-27T08:45:00Z"
      single_oct_27_end = Time.iso8601 "2020-10-27T09:30:00Z"
      one_off = create :shift_event, :single,
                       uid: weekly_shift.shift_events.first.uid,
                       organization: @organization,
                       starts_at: single_oct_27_start,
                       ends_at: single_oct_27_end
      event = weekly_shift.shift_events.first

      result = EventsService.update_into_future(
        event,
        {
          user_ids: [@volunteer.id]
        }
      )

      expect(result.shift_event.user_ids).to eql([@volunteer.id])
      expect(one_off.reload.user_ids).to eql([@volunteer.id])
    end

    it "returns updated event" do
      weekly_shift_event = create :shift_event,
                                  :weekly_on_mondays,
                                  organization: @organization,
                                  starts_at: @third_monday_start,
                                  ends_at: @third_monday_end
      expect(weekly_shift_event.tasks).to be_empty
      expect(weekly_shift_event.users).to be_empty

      result = EventsService.update_into_future(
        weekly_shift_event,
        {
          ordered_task_ids: [@site.tasks.first.id],
          user_ids: [@volunteer.id]
        }
      )

      expect(result).to be_persisted
      expect(result.id).to eq(weekly_shift_event.event.id)
      expect(result.eventable.tasks).to include(@site.tasks.first)
      expect(result.eventable.users).to include(@volunteer)
    end
  end

  describe ".post_occurrence_details" do
    before do
      @mondays = [
        "2023-01-02", # A <- This will be the original occurrence
        "2023-01-09", # B
        "2023-01-16", # C <- We'll delete this and following
        "2023-01-23"  # D
      ].map { |day| [Time.iso8601("#{day}T03:00:00Z"), Time.iso8601("#{day}T05:00:00Z")] }
      @organization = create :organization
      @site = create :site, organization: @organization
      @volunteer = create :volunteer, organization_ids: [@organization.id]
      @task_log_attributes = {
        task_id: @site.tasks.first.id,
        notes: Faker::Lorem.paragraph,
        weight: Faker::Number.number(digits: 2),
        temperature: Faker::Number.number(digits: 1),
        food_taxonomy_id: create(:food_taxonomy).id,
        user_id: @volunteer.id
      }
    end

    it "sets the task logs on the correct day" do
      weekly_shift_event = create :shift_event,
                                  :weekly_on_mondays,
                                  organization: @organization,
                                  starts_at: @mondays[0][0],
                                  ends_at: @mondays[0][1],
                                  tasks: [@site.tasks.first],
                                  users: [@volunteer]
      occurrence = EventOccurrence.new(event: weekly_shift_event.event, starts_at: @mondays[2][0],
                                       ends_at: @mondays[2][1])

      result = EventsService.post_occurrence_details(occurrence, {
                                                       task_logs_attributes: [@task_log_attributes]
                                                     })
      occurrences = @mondays.map { |starts_at, _| EventsService.find_occurrence weekly_shift_event.uid, starts_at }
      expect(occurrences.map(&:starts_at)).to eql(@mondays.map(&:first))
      expect(occurrences[2].eventable.id).to eql(result.eventable.id)
      expect(result.eventable.task_logs.first).to have_attributes(@task_log_attributes)
    end

    it "sets the recurrence options on the partitions correctly" do
      weekly_shift_event = create :shift_event,
                                  :weekly_on_mondays,
                                  organization: @organization,
                                  starts_at: @mondays[0][0],
                                  ends_at: @mondays[0][1],
                                  tasks: [@site.tasks.first],
                                  users: [@volunteer]
      weekly_shift_event.recurrence_options["UNTIL"] = @mondays[2][0]
      weekly_shift_event.event.save!
      occurrence = EventOccurrence.new(event: weekly_shift_event.event, starts_at: @mondays[1][0],
                                       ends_at: @mondays[1][1])

      EventsService.post_occurrence_details(occurrence, {
                                              task_logs_attributes: [@task_log_attributes]
                                            })

      occurrences = @mondays.map { |starts_at, _| EventsService.find_occurrence weekly_shift_event.uid, starts_at }
      expect(occurrences.compact.count).to eql(3)
      expect(occurrences[0].recurrence_options["UNTIL"]).to match(/2023-01-09/)
      expect(occurrences[1].recurrence_options).to be_nil # The date with logs is a one-off
      expect(occurrences[2].recurrence_options["UNTIL"]).to match(/2023-01-16/)
    end
  end

  describe ".delete_into_future" do
    before do
      @mondays = [
        "2023-01-02", # A <- This will be the original occurrence
        "2023-01-09", # B
        "2023-01-16", # C <- We'll delete this and following
        "2023-01-23"  # D
      ].map { |day| [Time.iso8601("#{day}T03:00:00Z"), Time.iso8601("#{day}T05:00:00Z")] }
      @organization = create :organization
      @site = create :site, organization: @organization
      @volunteer = create :volunteer, organization_ids: [@organization.id]
    end

    it "removes the affected occurrences from the timeline" do
      weekly_shift_event = create :shift_event,
                                  :weekly_on_mondays,
                                  organization: @organization,
                                  starts_at: @mondays[0][0],
                                  ends_at: @mondays[0][1],
                                  tasks: [@site.tasks.first],
                                  users: [@volunteer]
      occurrence = EventOccurrence.new(event: weekly_shift_event.event, starts_at: @mondays[2][0],
                                       ends_at: @mondays[2][1])
      EventsService.delete_into_future(occurrence)
      occurrences = @mondays.map { |starts_at, _| EventsService.find_occurrence weekly_shift_event.uid, starts_at }
      expect(occurrences.map { |o| o&.starts_at }).to eql([@mondays[0][0], @mondays[1][0], nil, nil])
      expect(occurrences.first.event.recurrence_options["UNTIL"]).to eql((@mondays[2][0] - 1.minute).to_s)
    end

    # TODO: Pass this test
    # it "removes upcoming one-off occurrences from the timeline" do
    #   weekly_shift_event = create :shift_event,
    #                               :weekly_on_mondays,
    #                               organization: @organization,
    #                               starts_at: @mondays[0][0],
    #                               ends_at: @mondays[0][1],
    #                               tasks: [@site.tasks.first],
    #                               users: [@volunteer]

    #   one_off = create :shift_event, :single,
    #                               uid: weekly_shift_event.uid,
    #                               organization: @organization,
    #                               user_ids: [@volunteer.id],
    #                               starts_at: @mondays[3][0],
    #                               ends_at: @mondays[3][1]

    #   # Exclude the one-off from the recurrence:
    #   weekly_shift_event.exdates << one_off.starts_at
    #   weekly_shift_event.save!

    #   occurrence = EventOccurrence.new(
    #     event: weekly_shift_event.event,
    #     starts_at: @mondays[2][0],
    #     ends_at: @mondays[2][1]
    #   )
    #   result = EventsService.delete_into_future(occurrence)
    #   occurrences = @mondays.map { |starts_at, _| EventsService.find_occurrence weekly_shift_event.uid, starts_at }
    #   expect(occurrences.map {|o| o&.starts_at}).to eql([@mondays[0][0], @mondays[1][0], nil, nil])
    #   expect(occurrences.first.event.recurrence_options["UNTIL"]).to eql((@mondays[2][0] - 1.minute).to_s)
    # end

    it "removes the affected occurrences from the timeline" do
      weekly_shift_event = create :shift_event,
                                  :weekly_on_mondays,
                                  organization: @organization,
                                  starts_at: @mondays[0][0],
                                  ends_at: @mondays[0][1],
                                  tasks: [@site.tasks.first],
                                  users: [@volunteer]
      occurrence = EventOccurrence.new(event: weekly_shift_event.event, starts_at: @mondays[2][0],
                                       ends_at: @mondays[2][1])
      EventsService.delete_into_future(occurrence)
      occurrences = @mondays.map { |starts_at, _| EventsService.find_occurrence weekly_shift_event.uid, starts_at }
      expect(occurrences.map { |o| o&.starts_at }).to eql([@mondays[0][0], @mondays[1][0], nil, nil])
      expect(occurrences.first.event.recurrence_options["UNTIL"]).to eql((@mondays[2][0] - 1.minute).to_s)
    end

    it "deletes the shift's events entirely, given it's first date" do
      weekly_shift_event = create :shift_event,
                                  :weekly_on_mondays,
                                  organization: @organization,
                                  starts_at: @mondays[0][0],
                                  ends_at: @mondays[0][1],
                                  tasks: [@site.tasks.first],
                                  users: [@volunteer]
      occurrence = EventOccurrence.new(event: weekly_shift_event.event, starts_at: @mondays[0][0],
                                       ends_at: @mondays[0][1])
      EventsService.delete_into_future(occurrence)
      occurrences = @mondays.map { |starts_at, _| EventsService.find_occurrence weekly_shift_event.uid, starts_at }
      expect(Event.find_by(uid: weekly_shift_event.uid)).to be_nil
      expect(occurrences).to eql([nil, nil, nil, nil])
    end
  end

  describe ".update" do
    before do
      @second_monday_start = Time.iso8601 "2020-10-12T03:00:00Z"
      @second_monday_end = Time.iso8601 "2020-10-12T05:00:00Z"
      @third_monday_start = Time.iso8601 "2020-10-19T03:00:00Z"
      @fourth_monday_start = Time.iso8601 "2020-10-26T03:00:00Z"
      @fourth_monday_end = Time.iso8601 "2020-10-26T05:00:00Z"
      @organization = create :organization
      @site = create :site, organization: @organization
      @volunteer = create :volunteer, organization_ids: [@organization.id]
    end

    it "returns created single event from an existing recurrence with eventable attributes preserved" do
      weekly_shift_event = create :shift_event,
                                  :weekly_on_mondays,
                                  organization: @organization,
                                  starts_at: @second_monday_start,
                                  ends_at: @second_monday_end,
                                  tasks: [@site.tasks.first],
                                  users: [@volunteer]
      occurrence = weekly_shift_event
      occurrence.starts_at = @third_monday_start

      result = EventsService.update(occurrence, {})
      result_prior_event = EventsService.find_occurrence weekly_shift_event.uid, @second_monday_start
      result_next_event = EventsService.find_occurrence weekly_shift_event.uid, @fourth_monday_start

      expect(result).to be_persisted
      expect(result.id).not_to eq(weekly_shift_event.event.id)
      expect(result.recurrence?).to be(false)

      expect(result.eventable.id).not_to eq(weekly_shift_event.id)
      expect(result.eventable.tasks).to include(@site.tasks.first)
      expect(result.eventable.users).to include(@volunteer)

      expect(result_prior_event.starts_at).to eq(@second_monday_start)
      expect(result_prior_event.exdates).to eq([@third_monday_start])
      expect(result_prior_event.eventable.tasks).to include(@site.tasks.first)
      expect(result_prior_event.eventable.users).to include(@volunteer)

      expect(result_next_event.starts_at).to eq(@fourth_monday_start)
      expect(result_next_event.recurrence?).to be(true)
      expect(result_next_event.eventable.tasks).to include(@site.tasks.first)
      expect(result_next_event.eventable.users).to include(@volunteer)
    end

    it "returns single event with several task logs given a shift event with existing logs" do
      single_shift_event = create :shift_event,
                                  :single,
                                  organization: @organization,
                                  starts_at: @second_monday_start,
                                  ends_at: @second_monday_end,
                                  tasks: [@site.tasks.first],
                                  users: [@volunteer]
      single_shift_event.task_logs.create(
        {
          weight: Faker::Number.number(digits: 3),
          temperature: Faker::Number.number(digits: 2),
          notes: Faker::Lorem.paragraph,
          task_id: @site.tasks.first.id,
          user_id: @volunteer.id,
          food_taxonomy: create(:food_taxonomy)
        }
      )
      weight = Faker::Number.number(digits: 2)
      temperature = Faker::Number.number(digits: 1)
      notes = Faker::Lorem.paragraph
      food_taxonomy = create :food_taxonomy

      result = EventsService.update(
        single_shift_event,
        {
          task_logs_attributes: [
            {
              task_id: @site.tasks.first.id,
              notes: notes,
              weight: weight,
              temperature: temperature,
              food_taxonomy_id: food_taxonomy.id,
              user_id: @volunteer.id
            }
          ]
        }
      )

      expect(result).to be_persisted
      expect(result.single?).to be(true)
      expect(result.eventable.task_logs.length).to eq(2)
      expect(result.eventable.task_logs.first).to be_persisted
      expect(result.eventable.task_logs.last).to be_persisted
    end

    it "returns created single event given the first occurrence" do
      weekly_shift_event = create :shift_event,
                                  :weekly_on_mondays,
                                  organization: @organization,
                                  starts_at: @second_monday_start,
                                  ends_at: @second_monday_end

      result = EventsService.update(
        weekly_shift_event,
        {}
      )
      result_next_event = EventsService.find_occurrence weekly_shift_event.uid, @third_monday_start

      expect(result).to be_persisted
      expect(result.id).not_to eq(weekly_shift_event.event.id)
      expect(result.recurrence?).to be(false)
      expect(result.eventable.id).not_to eq(weekly_shift_event.id)

      expect(result_next_event.starts_at).to eq(@third_monday_start)
      expect(result_next_event.recurrence?).to be(true)
      expect(result_next_event.recurrence_options).to eq(weekly_shift_event.recurrence_options)
    end

    it "returns last occurrence as single event given a recurrence with an ending" do
      weekly_shift_event = create :shift_event,
                                  :weekly_on_mondays,
                                  organization: @organization,
                                  starts_at: @second_monday_start,
                                  ends_at: @second_monday_end,
                                  tasks: [@site.tasks.first],
                                  users: [@volunteer]
      weekly_shift_event.recurrence_options["UNTIL"] = @fourth_monday_start
      weekly_shift_event.save!
      occurrence = weekly_shift_event
      occurrence.starts_at = @fourth_monday_start
      result = EventsService.update(
        weekly_shift_event,
        {
          task_logs_attributes: [
            {
              task_id: @site.tasks.first.id,
              notes: Faker::Lorem.paragraph,
              weight: Faker::Number.number(digits: 2),
              temperature: Faker::Number.number(digits: 2),
              user_id: @volunteer.id,
              food_taxonomy: create(:food_taxonomy)
            }
          ]
        }
      )
      result_prior_event = EventsService.find_occurrence weekly_shift_event.uid, @third_monday_start

      expect(result).to be_persisted
      expect(result.id).not_to eq(weekly_shift_event.event.id)
      expect(result.recurrence?).to be(false)

      expect(result.eventable.id).not_to eq(weekly_shift_event.id)
      expect(result.eventable.task_logs.length).to eq(1)
      expect(result.eventable.task_logs.first.persisted?).to be(true)

      expect(result_prior_event.recurrence_options["UNTIL"]).to start_with("2020-10-26")
      expect(result.id).to eql(EventsService.find_occurrence(weekly_shift_event.uid, @fourth_monday_start).event.id)
    end

    context "when updating the date/time and recurrence rules" do
      it "creates a new one off event" do
        weekly_shift_event = create :shift_event,
                                    :weekly_on_mondays,
                                    organization: @organization,
                                    starts_at: @second_monday_start,
                                    ends_at: @second_monday_end

        update_params = {
          "starts_at" => @third_monday_start,
          "ends_at" => weekly_shift_event.ends_at,
          "rrule" => weekly_shift_event.rrule
        }
        event = EventsService.update(weekly_shift_event, {}, update_params)

        expect(Shift.find_by(name: event.eventable.shift.name)).to_not be_nil
      end
    end
  end
end
