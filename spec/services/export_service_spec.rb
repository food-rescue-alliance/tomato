require "rails_helper"

describe ExportService do
  describe ".export" do
    subject(:csv) { described_class.export(organization: organization, start_date: start_date, end_date: end_date) }
    let(:organization) { create(:organization) }
    let(:start_date) { "2022-10-01" }
    let(:end_date) { "2022-10-31" }
    let(:shift_event) do
      create(:shift_event, :with_tasks, :weekly_on_mondays, organization: organization, starts_at: start_date)
    end

    it "generates an empty set of rows" do
      expect(csv).to eql []
    end

    context "when there are no logs" do
      subject { csv }
      before { shift_event.task_logs = [] }

      describe "and no shift_event_note" do
        it { is_expected.to be_empty }
      end

      describe "but there is a shift_event_note" do
        subject { csv.first }
        before { create(:shift_event_note, :walk, shift_event: shift_event) }

        it { is_expected.to match(hash_including(transport: "Walk")) }
      end
    end

    context "when there are no tasks, but there is a shift_event_note" do
      subject { csv.first }
      before { create(:shift_event_note, :car, shift_event: shift_event) }
      let(:shift_event) do
        create(:shift_event, :weekly_on_mondays, users: [create(:volunteer, full_name: "Volunteer A")],
       organization: organization, starts_at: start_date)
      end

      it { is_expected.to match(hash_including(transport: "Car", volunteers: "Volunteer A")) }
    end

    context "given the same start and end date" do
      subject { csv.first[:date] }

      before { create(:task_log, shift_event: shift_event, task: shift_event.tasks.first) }

      let(:shift_event) do
        create(:shift_event, :with_tasks, :weekly_on_mondays, organization: organization, starts_at: start_date)
      end
      let(:end_date) { start_date }

      it { is_expected.to eql(shift_event.starts_at.strftime("%F")) }
    end

    describe "fields not tied to the logs" do
      before { create(:task_log, shift_event: shift_event, task: shift_event.tasks.first) }

      describe "date" do
        subject { csv.first[:date] }

        it { is_expected.to eql(shift_event.starts_at.strftime("%F")) }
      end

      describe "transport" do
        subject { csv.first[:transport] }
        before { create(:shift_event_note, :bike, shift_event: shift_event) }

        it { is_expected.to eql("Bike") }
      end

      describe "hours spent" do
        subject { csv.first[:hours_spent] }
        it { is_expected.to be_nil }

        context "when hours_spent is set" do
          before { create(:shift_event_note, :bike, shift_event: shift_event, hours_spent: 2.5) }
          it { is_expected.to eql(2.5) }
        end
      end

      describe "recipients" do
        subject { csv.first[:recipients] }

        let(:recipients) { create_list(:task, 3, :drop_off) }
        let(:shift_event) do
          create(
            :shift_event,
            :with_tasks,
            :weekly_on_mondays,
            ordered_tasks: [create(:task, :pickup), *recipients],
            organization: organization, starts_at: start_date
          )
        end

        it { is_expected.to eql(recipients.map { |t| t.site.name }.join(":")) }
      end

      describe "donor" do
        subject { csv.first[:donor] }
        let(:shift_event) do
          create(:shift_event, :with_tasks, :weekly_on_mondays, ordered_tasks: [*donors, create(:task, :drop_off)],
         organization: organization, starts_at: start_date)
        end
        let(:donors) { [create(:task, :pickup)] }

        it { is_expected.to eql(donors.first.site.name) }

        describe "when there are multiple donors" do
          let(:donors) { create_list(:task, 2, :pickup) }

          it "creates a row for each donor" do
            expect(csv.pluck(:donor)).to match_array(donors.map { |d| d.site.name })
          end
        end
      end

      describe "volunteers" do
        subject { csv.first[:volunteers] }

        context "when there is one volunteer" do
          before { shift_event.users = [create(:volunteer, full_name: "Volunteer A")] }

          it { is_expected.to eql("Volunteer A") }
        end

        context "when there are multiple volunteers" do
          before do
            shift_event.users = [
              create(:volunteer, full_name: "Volunteer A"),
              create(:volunteer, full_name: "Volunteer B")
            ]
          end

          it { is_expected.to eql("Volunteer A:Volunteer B") }
        end
      end
    end

    context "fields derived from logs" do
      let(:pickups) { create_list(:task, 2, :pickup) }
      let(:dropoffs) { create_list(:task, 2, :drop_off) }
      let(:apple) { create(:food_taxonomy, name: "apple") }
      let(:banana) { create(:food_taxonomy, name: "banana") }
      let(:cherries) { create(:food_taxonomy, name: "cherries") }
      let(:shift_event) do
        create(
          :shift_event,
          :with_tasks,
          :weekly_on_mondays,
          ordered_tasks: pickups + dropoffs,
          organization: organization, starts_at: start_date
        )
      end

      before do
        # Pickup 1
        pickups[0].site.update!(name: "Pickup 1")
        create(:task_log, food_taxonomy: apple, weight: 1, shift_event: shift_event, task: pickups[0],
notes: "some apples")
        create(:task_log, food_taxonomy: banana, weight: 1, shift_event: shift_event, task: pickups[0],
notes: "green bananas")

        # Pickup 2
        pickups[1].site.update!(name: "Pickup 2")
        create(:task_log, food_taxonomy: banana, weight: 1, shift_event: shift_event, task: pickups[1],
notes: "ripe bananas")
        create(:task_log, food_taxonomy: cherries, weight: 3, shift_event: shift_event, task: pickups[1],
notes: "pitted cherries")

        # Dropoffs 1&2 (Should be ignored)
        create(:task_log, food_taxonomy: apple, weight: 1, shift_event: shift_event, task: dropoffs[0])
        create(:task_log, food_taxonomy: banana, weight: 2, shift_event: shift_event, task: dropoffs[1])
        create(:task_log, food_taxonomy: cherries, weight: 3, shift_event: shift_event, task: dropoffs[1])
      end

      context "when a task is removed" do
        subject(:donors) { csv.pluck(:donor) }
        before { shift_event.tasks = shift_event.tasks[1..] }
        it "doesn't show up in the export" do
          expect(donors).to eql(["Pickup 2"])
        end
      end

      context "when all tasks are removed, leaving only defunct logs" do
        subject(:donors) { csv.pluck(:donor) }
        before { shift_event.tasks = [] }
        it "should be empty" do
          expect(donors).to be_empty
        end
      end

      describe "item types" do
        subject { csv.pluck(:donor, :item_types) }

        it { is_expected.to eql([["Pickup 1", "apple:banana"], ["Pickup 2", "banana:cherries"]]) }
      end

      describe "item weights" do
        subject { csv.pluck(:donor, :item_weights) }

        it { is_expected.to eql([["Pickup 1", "1:1"], ["Pickup 2", "1:3"]]) }
      end

      describe "total weight" do
        subject { csv.pluck(:donor, :total_weight) }

        it { is_expected.to eql([["Pickup 1", 2], ["Pickup 2", 4]]) }
      end

      describe "item_descriptions" do
        subject { csv.pluck(:donor, :item_descriptions) }

        it {
          is_expected.to eql([["Pickup 1", "some apples:green bananas"], ["Pickup 2", "ripe bananas:pitted cherries"]])
        }
      end
    end
  end
end
