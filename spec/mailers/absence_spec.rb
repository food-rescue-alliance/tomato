require "rails_helper"

RSpec.describe AbsenceMailer, type: :mailer do
  describe "scheduled_email" do
    let(:selected_dates) do
      {
        start_date: "11/20/22",
        end_date: "11/20/22"
      }
    end

    let(:shifts) do
      [{
        title: "Test Shift 1",
        starts_at: "11/20/22"
      }]
    end

    let(:params) do
      {
        full_name: "Absent Alex",
        email: "test@example.com",
        selected_dates: selected_dates,
        affected_shifts: shifts
      }
    end

    let(:mail) { AbsenceMailer.with(params).scheduled_email }

    it "renders the headers" do
      expect(mail.subject).to eq("[Absence Scheduled] Impacting Test Shift 1.")
      expect(mail.to).to eq(["test@example.com"])
      expect(mail.from).to eq(["software@foodrescuealliance.org"])
    end

    it "includes the volunteer's name of the absence" do
      expect(mail.body.encoded).to match("Absent Alex")
    end

    it "includes the date of the absence" do
      expect(mail.body.encoded).to match(%r{scheduled an absence on 11/20/22\.})
    end

    it "includes the affected shift" do
      expect(mail.body.encoded).to match("11/20/22 - Test Shift 1")
    end

    context "when the absence spans multiple days" do
      let(:selected_dates) do
        {
          start_date: "11/20/22",
          end_date: "11/23/22"
        }
      end

      it "renders the date range in the email body" do
        expect(mail.body.encoded).to match(%r{scheduled an absence on 11/20/22 to 11/23/22\.})
      end
    end

    context "when the absence conflicts with multiple shifts" do
      let(:shifts) do
        [{
          title: "Test Shift 1",
          starts_at: "11/20/22"
        },
         {
           title: "Test Shift 1",
           starts_at: "11/21/22"
         },

         {
           title: "Test Shift 2",
           starts_at: "11/22/22"
         }]
      end

      it "includes the unique names of the shifts in the subject" do
        expect(mail.subject).to eq("[Absence Scheduled] Impacting Test Shift 1, Test Shift 2.")
      end
    end
  end
end
