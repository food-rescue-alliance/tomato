require "rails_helper"

describe ApplicationHelper do
  describe ".time_zones_with_us_zones_first" do
    subject { helper.time_zones_with_us_zones_first }
    it "has the same timezones as ActiveSupport::TimeZone.all" do
      expect(subject.count).to eql(ActiveSupport::TimeZone.all.count)
      expect(subject).to match(array_including(ActiveSupport::TimeZone.all))
    end
  end
end
