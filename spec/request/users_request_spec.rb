require "rails_helper"

RSpec.describe "Users", type: :request do
  describe "PUT api/users/me" do
    before do
      @volunteer = create(:volunteer)
      @user = { phone: "5132934891", full_name: "John Doe", email: "name@example.com", time_zone: "America/New_York" }
      @params = { user: @user }
    end

    subject { put "/api/users/me.json", params: @params }

    it "returns success when user is logged in" do
      sign_in @volunteer

      subject

      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body).symbolize_keys).to include(@user)
    end

    it "renders unauthorized when user is not logged in" do
      subject

      expect(response).to have_http_status(:unauthorized)
    end
  end

  describe "PUT api/users/:id" do
    before do
      @volunteer = create(:volunteer)
      @other_volunteer = create(:volunteer)
    end

    subject { put "/api/users/#{@other_volunteer.id}.json" }

    it "returns not authorized error" do
      sign_in @volunteer

      subject

      expect(response).to have_http_status(403)
    end
  end
end
