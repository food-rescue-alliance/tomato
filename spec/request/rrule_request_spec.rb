require "rails_helper"

RSpec.describe "RRULE API", type: :request do
  before do
    @volunteer = create(:volunteer)
  end

  describe "GET /rrule" do
    before do
      sign_in @volunteer
    end

    context "with no rrule" do
      it "returns an empty string" do
        get "/rrule", headers: { "ACCEPT" => "application/json" }

        json_body = JSON.parse(response.body)
        expect(json_body["sentence"]).to eq ""
      end
    end

    context "with an rrule string" do
      let(:starts_at) { Time.zone.parse("July 11 2021 10:00") }

      it "returns the English equivalent" do
        get "/rrule", headers: { "ACCEPT" => "application/json" },
            params: { rrule: "FREQ=YEARLY;INTERVAL=1", starts_at: starts_at }

        json_body = JSON.parse(response.body)
        expect(json_body["sentence"]).to eq "Repeats on July 11th of every year"
      end
    end
  end
end
