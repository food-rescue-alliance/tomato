require "rails_helper"

RSpec.describe "FoodTaxonomies", type: :request do
  describe "GET api/food_taxonomies" do
    before do
      @volunteer = create :volunteer
      create :food_taxonomy
    end

    subject { get "/api/food_taxonomies", headers: { "ACCEPT" => "application/json" } }

    it "returns success when user is logged in" do
      sign_in @volunteer

      subject

      expect(response).to have_http_status(200)

      json_body = JSON.parse(response.body)
      expect(json_body.length).to eq(FoodTaxonomy.all.length)

      expect(json_body[0]["name"]).to eq(FoodTaxonomy.find(json_body[0]["id"]).name)
    end

    it "returns unauthorized when user is not logged in" do
      subject

      expect(response).to have_http_status(401)
    end
  end
end
