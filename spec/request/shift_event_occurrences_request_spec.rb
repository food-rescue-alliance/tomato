require "rails_helper"

RSpec.describe "ShiftEventOccurrences", type: :request do
  describe "GET api/shift_event_occurrences/:id" do
    before do
      @volunteer = create(:volunteer)
      @other_volunteer = create(:volunteer)
      @shift_event = create(:shift_event, :single, organization: @volunteer.organizations[0])
      @id = "#{@shift_event.event.uid}-#{@shift_event.starts_at.to_i}"
    end

    subject { get "/api/shift_event_occurrences/#{@id}", headers: { "ACCEPT" => "application/json" } }

    it "returns success when user belongs to the shift organization" do
      sign_in @volunteer

      subject

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq("application/json; charset=utf-8")
      json_result = JSON.parse(response.body)
      expect(json_result).to include("id" => @id,
                                     "startsAt" => @shift_event.starts_at.iso8601,
                                     "endsAt" => @shift_event.ends_at.iso8601,
                                     "shiftId" => @shift_event.shift.id,
                                     "shiftEventId" => @shift_event.id)
      expect(json_result["tasks"]).to be_kind_of Array
      expect(json_result["users"]).to be_kind_of Array
      expect(json_result["task_logs"]).to be_kind_of Array
    end

    it "returns not authorized error when user does not belong to the shift organization" do
      sign_in @other_volunteer

      subject

      expect(response).to have_http_status(403)
    end

    it "returns bad request given an invalid occurrence id" do
      sign_in @other_volunteer

      get "/api/shift_event_occurrences/foobarbaz", headers: { "ACCEPT" => "application/json" }

      expect(response).to have_http_status(400)
    end

    it "returns not found given a non-existent start time" do
      sign_in @other_volunteer

      get "/api/shift_event_occurrences/#{@shift_event.uid}-0", headers: { "ACCEPT" => "application/json" }

      expect(response).to have_http_status(404)
    end
  end

  describe "GET api/users/me/shift_event_occurrences" do
    before do
      @volunteer = create(:volunteer)
      @shift_event = create(:shift_event,
                            :single,
                            starts_at: Faker::Time.forward(days: 4, period: :morning),
                            ends_at: Faker::Time.forward(days: 4, period: :morning),
                            user_ids: [@volunteer.id])
    end

    subject { get "/api/users/me/shift_event_occurrences", headers: { "ACCEPT" => "application/json" } }

    it "returns success" do
      sign_in @volunteer

      subject

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq("application/json; charset=utf-8")
      json_result = JSON.parse(response.body)
      expect(json_result).to be_kind_of Array
      expect(json_result.first["shiftEventId"]).to eq(@shift_event.id)
    end

    it "returns unauthorized when not logged in" do
      subject

      expect(response).to have_http_status(:unauthorized)
    end
  end
end
