require "rails_helper"

RSpec.describe "Alerts", type: :request do
  describe "GET api/users/me/alerts" do
    before do
      @volunteer = create(:volunteer)
      @past_shift = create(
        :shift_event,
        :single,
        :with_tasks,
        starts_at: Faker::Time.backward(days: 23, period: :morning),
        ends_at: Faker::Time.backward(days: 23, period: :afternoon),
        user_ids: [@volunteer.id]
      )

      @past_shift_with_logs = create(
        :shift_event,
        :single,
        :with_tasks,
        starts_at: Faker::Time.backward(days: 23, period: :morning),
        ends_at: Faker::Time.backward(days: 23, period: :afternoon),
        user_ids: [@volunteer.id]
      )
      create(:task_log, user: @volunteer, shift_event: @past_shift_with_logs, task: @past_shift_with_logs.tasks.first)

      @future_event = create(
        :shift_event,
        :single,
        :with_tasks,
        user_ids: [@volunteer.id]
      )
    end

    subject { get "/api/users/me/alerts", headers: { "ACCEPT" => "application/json" } }

    it "returns success when user is logged in" do
      sign_in @volunteer

      subject

      expect(response).to have_http_status(200)
      json_body = JSON.parse(response.body)
      expect(json_body.length).to eq(1)
      expect(json_body[0]["message"]).to match(/#{@past_shift.event.title}/)
    end

    it "returns unauthorized when user is not logged in" do
      subject

      expect(response).to have_http_status(401)
    end
  end

  describe "GET api/users/:id/alerts" do
    before do
      @volunteer = create(:volunteer)
      @other_volunteer = create(:volunteer)
    end

    subject { get "/api/users/#{@other_volunteer.id}/alerts", headers: { "ACCEPT" => "application/json" } }

    it "returns not authorized error" do
      sign_in @volunteer

      subject

      expect(response).to have_http_status(403)
    end
  end
end
