require "rails_helper"

RSpec.describe "Pickup", type: :request do
  describe "GET api/users/me/shift_event_occurrences/pickup" do
    before do
      @volunteer = create(:volunteer)
      @other_volunteer = create(:volunteer)
      @organization = @volunteer.organizations[0]
      @past_shift = create(
        :shift_event,
        :single,
        :with_tasks,
        starts_at: Faker::Time.backward(days: 23, period: :morning),
        ends_at: Faker::Time.backward(days: 23, period: :afternoon),
        organization: @organization,
        user_ids: []
      )

      @open_shift = create(
        :shift_event,
        :single,
        :with_tasks,
        organization: @organization,
        user_ids: []
      )

      @open_shift_recurring = create(
        :shift_event,
        :weekly_on_mondays,
        :with_tasks,
        organization: @organization,
        user_ids: []
      )

      @shift_with_users = create(
        :shift_event,
        :single,
        :with_tasks,
        organization: @organization,
        user_ids: [@other_volunteer.id]
      )

      @shift_with_users = create(
        :shift_event,
        :single,
        :with_tasks,
        user_ids: []
      )
    end

    subject { get "/api/users/me/shift_event_occurrences/pickup", headers: { "ACCEPT" => "application/json" } }

    it "returns empty shifts for user's organization when user is logged in" do
      sign_in @volunteer

      subject

      expect(response).to have_http_status(200)
      json_body = JSON.parse(response.body)
      expect(json_body["one_time"].length).to eq(1)
      expect(json_body["one_time"][0]["title"]).to match(/#{@open_shift.event.title}/)

      json_body["recurring"].each do |occurrence|
        expect(occurrence["title"]).to match(/#{@open_shift_recurring.event.title}/)
      end
    end

    it "returns unauthorized when user is not logged in" do
      subject

      expect(response).to have_http_status(401)
    end
  end

  describe "GET api/users/:id/shift_event_occurrences/pickup" do
    before do
      @volunteer = create(:volunteer)
      @other_volunteer = create(:volunteer)
    end

    subject do
      get "/api/users/#{@other_volunteer.id}/shift_event_occurrences/pickup",
          headers: { "ACCEPT" => "application/json" }
    end

    it "returns not authorized error" do
      sign_in @volunteer

      subject

      expect(response).to have_http_status(403)
    end
  end

  describe "POST api/users/me/shift_event_occurences/:id/pickup" do
    before do
      @volunteer = create :volunteer
      @shift_event = create :shift_event, :single, organization: @volunteer.organizations[0], user_ids: []
      @id = "#{@shift_event.event.uid}-#{@shift_event.starts_at.to_i}"
    end

    it "returns updated shift occurrence when user is signed in" do
      sign_in @volunteer

      post "/api/users/me/shift_event_occurrences/#{@id}/pickup",
           headers: { "CONTENT_TYPE" => "application/json", "ACCEPT" => "application/json" }

      expect(response).to have_http_status(200)
      json_body = JSON.parse(response.body)
      expect(json_body["title"]).to eq(@shift_event.title)
      expect(json_body["users"].length).to eq(1)
      expect(json_body["users"][0]["id"]).to eq(@volunteer.id)
    end

    it "returns unauthorized when user is not logged in" do
      post "/api/users/me/shift_event_occurrences/#{@id}/pickup",
           headers: { "CONTENT_TYPE" => "application/json", "ACCEPT" => "application/json" }

      expect(response).to have_http_status(401)
    end
  end

  describe "POST api/users/:id/shift_event_occurrences/:id/pickup" do
    before do
      @volunteer = create :volunteer
      @other_volunteer = create :volunteer
      @shift_event = create :shift_event, :single, organization: @volunteer.organizations[0], user_ids: []
      @id = "#{@shift_event.event.uid}-#{@shift_event.starts_at.to_i}"
    end

    subject do
      post "/api/users/#{@other_volunteer.id}/shift_event_occurrences/#{@id}/pickup",
           headers: { "ACCEPT" => "application/json" }
    end

    it "returns not authorized error" do
      sign_in @volunteer

      subject

      expect(response).to have_http_status(403)
    end
  end
end
