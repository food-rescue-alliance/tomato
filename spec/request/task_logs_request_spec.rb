require "rails_helper"

RSpec.describe "TaskLogs", type: :request do
  describe "POST api/shift_event_occurrences/:id/task_logs" do
    before do
      @volunteer = create :volunteer
      @donor = create :donor
      @task = @donor.tasks.first
      @shift_event = create :shift_event, :single, user_ids: [@volunteer.id], ordered_task_ids: [@task.id]
      @id = "#{@shift_event.event.uid}-#{@shift_event.starts_at.to_i}"
      @food_taxonomy = create :food_taxonomy
    end

    it "returns created when user belongs to the shift" do
      sign_in @volunteer

      task_log = {
        weight: Faker::Number.number(digits: 3),
        temperature: Faker::Number.number(digits: 2),
        notes: Faker::Lorem.paragraph,
        task_id: @task.id,
        food_taxonomy_id: @food_taxonomy.id
      }

      post "/api/shift_event_occurrences/#{@id}/task_logs",
           params: task_log.to_json,
           headers: { "CONTENT_TYPE" => "application/json", "ACCEPT" => "application/json" }

      expect(response).to have_http_status(201)
    end

    it "returns unprocessable entity given task does not belong to shift event" do
      sign_in @volunteer

      task_log = {
        weight: Faker::Number.number(digits: 3),
        temperature: Faker::Number.number(digits: 2),
        notes: Faker::Lorem.paragraph,
        task_id: -1,
        food_taxonomy_id: -1
      }

      post "/api/shift_event_occurrences/#{@id}/task_logs",
           params: task_log.to_json,
           headers: { "CONTENT_TYPE" => "application/json" }

      expect(response).to have_http_status(422)
    end

    it "returns bad request given an invalid occurrence id" do
      sign_in @volunteer

      post "/api/shift_event_occurrences/foobarbaz/task_logs",
           params: "{}",
           headers: { "CONTENT_TYPE" => "application/json" }

      expect(response).to have_http_status(400)
    end

    it "returns not found given a non-existent start time" do
      sign_in @volunteer

      post "/api/shift_event_occurrences/#{@shift_event.uid}-0/task_logs",
           params: "{}",
           headers: { "CONTENT_TYPE" => "application/json" }

      expect(response).to have_http_status(404)
    end
  end
end
