require "rails_helper"

describe "Absences", type: :request do
  let(:absences) { [] }
  let(:volunteer) { create(:volunteer, absences: absences) }
  let(:response_json) { JSON.parse(response.body) }

  describe "GET api/users/me/absences" do
    subject(:make_request) { get "/api/users/me/absences", headers: { "ACCEPT" => "application/json" } }

    it "raises not_authorized when not logged in" do
      make_request

      expect(response).to have_http_status(401)
    end

    context "when logged in" do
      before { sign_in volunteer }

      it "raises not_authorized when given another user_id" do
        get "/api/users/#{volunteer.id + 1}/absences", headers: { "ACCEPT" => "application/json" }

        expect(response).to have_http_status(403)
      end

      it "renders an empty array when the current user has no absences" do
        make_request

        expect(response_json).to eql([])
      end

      context "when the current user has absences" do
        let(:absences) { [create(:absence), create(:absence)] }

        it "lists the current users' absences" do
          make_request

          expect(response_json).to eql(absences.map do |absence|
            {
              "id" => absence.id,
              "starts_at" => absence.starts_at.iso8601,
              "ends_at" => absence.ends_at.iso8601,
              "editable" => absence.editable?
            }
          end)
        end
      end
    end
  end

  describe "POST api/users/me/absences", type: :request do
    let(:absence_params) do
      { starts_at: 10.days.from_now.strftime("%Y-%m-%d"), ends_at: 12.days.from_now.strftime("%Y-%m-%d") }
    end
    subject(:make_request) do
      post "/api/users/me/absences", params: { absence: absence_params }, headers: { "ACCEPT" => "application/json" }
    end

    context "when the user is not logged in" do
      it { is_expected.to be(401) }
    end

    context "when logged in" do
      before { sign_in volunteer }
      it "raises not_authorized when given another user_id" do
        post "/api/users/#{volunteer.id + 1}/absences", params: {}, headers: { "ACCEPT" => "application/json" }

        expect(response).to have_http_status(403)
      end

      it "creates an absence" do
        expect { make_request }.to change { volunteer.absences.count }.by(1)
        expect(response).to have_http_status(201)

        new_absence = volunteer.absences.reload.first

        expect(new_absence.starts_at.iso8601).to eql(absence_params[:starts_at])
        expect(new_absence.ends_at.iso8601).to eql(absence_params[:ends_at])
      end

      context "when there are no impacted shifts" do
        let(:volunteer) do
          create(:volunteer, organizations: [
            create(:organization, :with_internal_alerts_email)
          ])
        end
        let(:other_shift) do
          create(:shift, :single, starts_at: 1.day.from_now, ends_at: 1.day.from_now,
         organization: volunteer.organizations.last)
        end
        before do
          other_shift.shift_events.last.user_ids = [volunteer.id]
        end

        it "doesnt send an internal alert email" do
          make_request

          expect(Devise.mailer.deliveries).to be_empty
        end
      end

      context "when an absence is created" do
        let(:volunteer) do
          create(:volunteer, organizations: [
            create(:organization, :with_internal_alerts_email)
          ])
        end
        let(:impacted_shift) do
          create(:shift, :single, starts_at: 11.days.from_now, ends_at: 11.days.from_now,
            organization: volunteer.organizations.first)
        end
        before do
          impacted_shift.shift_events.first.user_ids = [volunteer.id]
        end

        it "removes the volunteer from the affected shift" do
          make_request

          expect(volunteer.organizations.first.events.first.shift_event.user_ids).to eql([])
        end
      end
      context "when organization has internal alert email present" do
        let(:volunteer) do
          create(:volunteer, organizations: [
            create(:organization, :with_internal_alerts_email),
            create(:organization, :with_internal_alerts_email)
          ])
        end
        let(:impacted_shift) do
          create(:shift, :single, starts_at: 11.days.from_now, ends_at: 11.days.from_now,
         organization: volunteer.organizations.first)
        end
        let(:other_impacted_shift) do
          create(:shift, :single, starts_at: 11.days.from_now, ends_at: 11.days.from_now,
         organization: volunteer.organizations.last)
        end

        before do
          [impacted_shift, other_impacted_shift].each do |shift|
            shift.shift_events.last.user_ids = [volunteer.id]
          end
        end

        it "sends an internal alert email" do
          make_request

          email_sent_to = Devise.mailer.deliveries.first.to
          expect(email_sent_to).to include(volunteer.organizations.first.internal_alerts_email)
        end

        it "sends correct information to respective organizations" do
          make_request

          first_email, second_email = ActionMailer::Base.deliveries.map { |email| email.html_part.body }

          expect(first_email).to include(impacted_shift.name)
          expect(first_email).not_to include(other_impacted_shift.name)

          expect(second_email).not_to include(impacted_shift.name)
          expect(second_email).to include(other_impacted_shift.name)
        end
      end

      it "doesn't send an email if no alert email is set" do
        make_request

        emails_sent = Devise.mailer.deliveries

        expect(emails_sent).to eql([])
      end

      context "when user provides invalid parameters" do
        let(:absence_params) { { starts_at: 2.days.from_now.iso8601, ends_at: 1.day.from_now.iso8601 } }

        it { is_expected.to be(422) }
      end

      context "when start date is less than a week from now" do
        let(:absence_params) { { starts_at: 1.day.from_now.iso8601, ends_at: 2.days.from_now.iso8601 } }

        it { is_expected.to be(422) }
      end
    end
  end

  describe "PATCH api/users/me/absences/:id", type: :request do
    let(:volunteer) do
      create(
        :volunteer,
        organizations: [create(:organization, :with_internal_alerts_email)],
        absences: [create(:absence, starts_at: 10.days.from_now, ends_at: 12.days.from_now)]
      )
    end
    let(:other_volunteer) do
      create(
        :volunteer,
        organizations: [volunteer.organizations.first],
        absences: [create(:absence, starts_at: 13.days.from_now, ends_at: 14.days.from_now)]
      )
    end
    let(:absence_params) do
      { starts_at: 20.days.from_now.strftime("%Y-%m-%d"), ends_at: 22.days.from_now.strftime("%Y-%m-%d") }
    end
    subject(:make_request) do
      patch "/api/users/me/absences/#{volunteer.absences.first.id}", params: { absence: absence_params },
headers: { "ACCEPT" => "application/json" }
    end

    before { sign_in volunteer }

    context "when an absence date is edited" do
      it "updates an absence date" do
        make_request

        expect(response).to have_http_status(200)
        expect(volunteer.absences.reload.first).to have_attributes(absence_params.transform_values(&:to_date))
      end
      it "sends an internal and external email" do
        make_request

        internal_email, external_email = ActionMailer::Base.deliveries.map(&:to)

        expect(internal_email).to include(volunteer.organizations.first.internal_alerts_email)
        expect(external_email).to include(volunteer.email)
      end
    end
    context "when a user edits an absence date less than a week from now" do
      let(:absence_params) do
        { starts_at: 2.days.from_now.strftime("%Y-%m-%d"), ends_at: 3.days.from_now.strftime("%Y-%m-%d") }
      end
      it { is_expected.to be(422) }
    end
    context "when a user edits an invalid absence date" do
      subject(:make_request) do
        patch "/api/users/me/absences/#{other_volunteer.absences.first.id}", params: { absence: absence_params },
headers: { "ACCEPT" => "application/json" }
      end
      it "fails" do
        expect { make_request }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
    context "when a user edits another user's absence date" do
      subject(:make_request) do
        patch "/api/users/#{other_volunteer.id}/absences/#{other_volunteer.absences.first.id}",
              params: { absence: absence_params }, headers: { "ACCEPT" => "application/json" }
      end
      it { is_expected.to be(403) }
    end
    context "when the volunteer has an upcoming absence within 7 days" do
      let(:volunteer) do
        create(
          :volunteer,
          organizations: [create(:organization, :with_internal_alerts_email)],
          absences: [create(:absence, starts_at: 6.days.from_now, ends_at: 7.days.from_now)]
        )
      end
      it { is_expected.to be(422) }
    end
  end

  describe "DELETE api/users/me/absences/:id", type: :request do
    let(:volunteer) do
      create(
        :volunteer,
        organizations: [create(:organization, :with_internal_alerts_email)],
        absences: [create(:absence, starts_at: 10.days.from_now, ends_at: 12.days.from_now)]
      )
    end
    let(:other_volunteer) do
      create(
        :volunteer,
        organizations: [volunteer.organizations.first],
        absences: [create(:absence, starts_at: 13.days.from_now, ends_at: 14.days.from_now)]
      )
    end
    subject(:make_request) do
      delete "/api/users/me/absences/#{volunteer.absences.first.id}", headers: { "ACCEPT" => "application/json" }
    end

    before { sign_in volunteer }

    context "when an absence is deleted" do
      it "deletes an absence date" do
        make_request

        expect(response).to have_http_status(200)
        expect(volunteer.absences.reload.count).to eql(0)
      end

      it "sends an internal and external email" do
        make_request

        internal_email, external_email = ActionMailer::Base.deliveries.map(&:to)

        expect(internal_email).to include(volunteer.organizations.first.internal_alerts_email)
        expect(external_email).to include(volunteer.email)
      end
    end

    context "when a user deletes an invalid absence date" do
      subject(:make_request) do
        delete "/api/users/me/absences/#{other_volunteer.absences.first.id}",
               headers: { "ACCEPT" => "application/json" }
      end

      it "fails" do
        expect { make_request }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end

    context "when a user deletes another user's absence date" do
      subject(:make_request) do
        delete "/api/users/#{other_volunteer.id}/absences/#{other_volunteer.absences.first.id}",
               headers: { "ACCEPT" => "application/json" }
      end

      it { is_expected.to be(403) }
    end

    context "when the volunteer has an upcoming absence within 7 days" do
      let(:volunteer) do
        create(
          :volunteer,
          organizations: [create(:organization, :with_internal_alerts_email)],
          absences: [create(:absence, starts_at: 6.days.from_now, ends_at: 7.days.from_now)]
        )
      end
      it { is_expected.to be(422) }
    end
  end
end
