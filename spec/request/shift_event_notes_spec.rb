require "rails_helper"

RSpec.describe "ShiftEventNotes", type: :request do
  describe "GET api/shift_events/:id/note" do
    let!(:volunteer) { create(:volunteer) }
    let!(:shift_event) do
      create(:shift_event, :single, organization: volunteer.organizations[0])
    end
    let(:shift_event_id) { "#{shift_event.event.uid}-#{shift_event.starts_at.to_i}" }
    let(:response_body) { JSON.parse(response.body) }

    before { sign_in(volunteer) }

    subject(:make_request) do
      get "/api/shift_event_occurrences/#{shift_event_id}/note", headers: { "ACCEPT" => "application/json" }
    end

    it "returns a pre-existing note's details" do
      create(:shift_event_note, shift_event_id: shift_event.id, transportation_type: "car")

      make_request

      expect(response_body).to match(hash_including({
                                                      "transportation_type" => "car"
                                                    }))
    end

    it "returns an empty note when no note exists" do
      make_request

      expect(response_body).to match(hash_including({
                                                      "transportation_type" => nil,
                                                      "hours_spent" => nil
                                                    }))
    end

    context "given an occurrence of a recurring event, that has not yet been saved to the DB" do
      let!(:shift_event) do
        create(:shift_event, :weekly_on_mondays, user_ids: [volunteer.id], organization: volunteer.organizations[0])
      end
      let(:future_occurrence) do
        EventsService.all_occurrences(volunteer.organizations[0], ShiftEvent,
                                      (shift_event.starts_at..(shift_event.starts_at + 3.weeks))).last
      end
      let(:shift_event_id) { "#{shift_event.event.uid}-#{future_occurrence.starts_at.to_i}" }

      it "returns an empty note" do
        make_request

        expect(response_body).to match(hash_including({
                                                        "transportation_type" => nil,
                                                        "hours_spent" => nil
                                                      }))
      end
    end

    it "returns unauthorized when user is not logged in" do
      sign_out volunteer

      subject

      expect(response).to have_http_status(401)
    end
  end

  describe "POST api/shift_events/:id/note" do
    let!(:volunteer) { create(:volunteer) }
    let!(:shift_event) do
      create(:shift_event, :single, user_ids: [volunteer.id], organization: volunteer.organizations[0])
    end
    let(:shift_event_id) { "#{shift_event.event.uid}-#{shift_event.starts_at.to_i}" }
    let(:response_body) { JSON.parse(response.body) }
    let(:transport_type) { %w[car walk bike].sample }
    let(:hours_spent) { Faker::Number.decimal(l_digits: 2) }

    before { sign_in(volunteer) }

    subject(:make_request) do
      post "/api/shift_event_occurrences/#{shift_event_id}/note", headers: { "ACCEPT" => "application/json" },
     params: { shift_event_note: { transportation_type: transport_type, hours_spent: hours_spent.to_s } }
    end

    it "renders a 'created' response" do
      make_request
      expect(response).to have_http_status(:created)
    end

    it "creates a note with the right transportation_type" do
      expect { make_request }.to change { ShiftEventNote.count }.by(1)

      expect(ShiftEventNote.last.transportation_type).to eql(transport_type)
    end

    context "when hours_spent is not provided" do
      let(:hours_spent) { nil }
      it "still saves the transportation_type" do
        expect { make_request }.to change { ShiftEventNote.count }.by(1)

        expect(ShiftEventNote.last.hours_spent).to be_blank
      end
    end
    context "when transportation_type is not provided" do
      let(:transport_type) { nil }
      it "still saves the hours_spent" do
        expect { make_request }.to change { ShiftEventNote.count }.by(1)
        expect(ShiftEventNote.last.hours_spent).to eql(hours_spent)
        expect(ShiftEventNote.last.transportation_type).to be_blank
      end
    end

    it "allows nil hours_spent" do
      expect { make_request }.to change { ShiftEventNote.count }.by(1)

      expect(ShiftEventNote.last.hours_spent).to eql(hours_spent)
    end

    it "allows nil transport_type" do
      expect { make_request }.to change { ShiftEventNote.count }.by(1)

      expect(ShiftEventNote.last.hours_spent).to eql(hours_spent)
    end

    context "when the user is not logged in" do
      subject do
        make_request
        response
      end
      before { sign_out volunteer }
      it { is_expected.to have_http_status(:unauthorized) }
    end

    context "when the user is not assigned to the shift" do
      subject do
        make_request
        response
      end
      before { shift_event.user_ids = [] }
      it { is_expected.to have_http_status(:forbidden) }
    end

    context "given an invalid transportation_type" do
      let(:transport_type) { "sea-plane" }

      it "renders an error" do
        make_request
        expect(response).to have_http_status(422)
        expect(response_body).to eql({ "error" => "Invalid transportation type." })
      end
    end
  end
end
