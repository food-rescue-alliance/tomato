require "rails_helper"

RSpec.describe "shifts", type: :request do
  let(:organization) { create(:organization) }
  let!(:site) { create(:donor, organization: organization) }
  let(:user) { create(:ops_coordinator) }

  before do
    sign_in user
  end

  describe "GET /api/organizations/:id/shifts/new" do
    it "returns the data needed for creating a new shift" do
      get "/api/organizations/#{organization.id}/shifts/new", headers: { "ACCEPT" => "application/json" }

      expect(response).to have_http_status(200)
      organization_response = JSON.parse(response.body)["data"]["organization"]
      expect(organization_response["id"]).to eq organization.id
      expect(organization_response["sites"].length).to eq organization.sites.length
      expect(organization_response["sites"].first["tasks"].first["description"]).to eq site.tasks.first.description
    end
  end

  describe "POST /api/organizations/:id/shifts" do
    it "creates a new shift" do
      shift_params = {
        shift: {
          name: "My Test Shift",
          shift_events_attributes: {
            "0" => {
              event_attributes: {
                date: "2020-09-09",
                starts_at: "2021-09-09 10:30",
                ends_at: "2021-09-09 12:30",
                rrule: "FREQ=WEEKLY;BYDAY=TH;INTERVAL=1;COUNT=5"
              }
            }
          }
        }
      }

      post "/api/organizations/#{organization.id}/shifts",
           params: shift_params,
           headers: { "ACCEPT" => "application/json" }

      expect(response).to have_http_status(201)
      new_shift = Shift.find_by(name: shift_params[:shift][:name])
      expect(new_shift).to_not be_nil
      json_body = JSON.parse(response.body)
      event_id = EventsService.first_occurrence(new_shift.shift_events.first.uid).id
      expect(json_body["data"]["link"]).to eq shift_event_occurrence_path(event_id)
    end

    it "responds with errors when unable to create the shift" do
      shift_params = {
        shift: {
          shift_events_attributes: {
            "0" => {
              event_attributes: {
                date: "2020-09-09",
                starts_at: "2021-09-09 10:30",
                ends_at: "2021-09-09 12:30"
              }
            }
          }
        }
      }

      post "/api/organizations/#{organization.id}/shifts",
           params: shift_params,
           headers: { "ACCEPT" => "application/json" }

      expect(response).to have_http_status(400)
      json_body = JSON.parse(response.body)
      expect(json_body["errors"].first["id"]).to be_nil
    end
  end

  describe "GET /api/organizations/:id/shifts" do
    before do
      create(:shift, :single, organization: organization)
    end

    it "returns a list of shifts for the organization" do
      get "/api/organizations/#{organization.id}/shifts?shift_category=one_time",
          headers: { "ACCEPT" => "application/json" }

      expect(response).to have_http_status(200)
      json_body = JSON.parse(response.body)
      expect(json_body["data"]["shift_category"]).to eq "one_time"
      expect(json_body["data"]["shifts"].length).to eq 1
    end
  end
end
