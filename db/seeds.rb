# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

FoodTaxonomy.find_or_create_by(
  feeding_america_id: "A7003",
  name: "Bread",
  details: "(Bread, Rolls, Tortillas, Biscuits)"
)

FoodTaxonomy.find_or_create_by(
  feeding_america_id: "F70019",
  name: "Dessert",
  details: "Cakes, Pies, Pastries, Donuts"
)

FoodTaxonomy.find_or_create_by(
  feeding_america_id: "B70009",
  name: "Produce",
  details: "unsorted (fresh fruits & vegetables)"
)

FoodTaxonomy.find_or_create_by(
  feeding_america_id: "C70014",
  name: "Meat",
  details: "Fish & Poultry - frozen"
)

FoodTaxonomy.find_or_create_by(
  feeding_america_id: "C70015",
  name: "Eggs"
)

FoodTaxonomy.find_or_create_by(
  feeding_america_id: "D70017",
  name: "Dairy",
  details: "assorted, refrigerated"
)

FoodTaxonomy.find_or_create_by(
  feeding_america_id: "D70646",
  name: "Milk",
  details: "assorted by the pound"
)

FoodTaxonomy.find_or_create_by(
  feeding_america_id: "F70046",
  name: "Drinks",
  details: "refrigerated"
)

FoodTaxonomy.find_or_create_by(
  feeding_america_id: "E70047",
  name: "Prepared Meals (frozen)",
  details: "assorted, frozen"
)

FoodTaxonomy.find_or_create_by(
  feeding_america_id: "E70529",
  name: "Prepared Meals (refrigerated)",
  details: "assorted, refrigerated"
)

FoodTaxonomy.find_or_create_by(
  feeding_america_id: "H70022",
  name: "Mixed",
  details: "unsorted food, non-perishable"
)

FoodTaxonomy.find_or_create_by(
  feeding_america_id: "G70902",
  name: "Non-Food"
)
