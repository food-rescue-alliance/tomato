# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2023_01_03_175759) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "absences", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.date "starts_at", null: false
    t.date "ends_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_absences_on_user_id"
  end

  create_table "addresses", force: :cascade do |t|
    t.string "street_one", null: false
    t.string "street_two"
    t.string "city", null: false
    t.string "state", null: false
    t.string "zip", null: false
    t.string "addressable_type"
    t.bigint "addressable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["addressable_type", "addressable_id"], name: "index_addresses_on_addressable_type_and_addressable_id"
  end

  create_table "donors", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "events", force: :cascade do |t|
    t.string "title", null: false
    t.datetime "starts_at", precision: nil, null: false
    t.datetime "ends_at", precision: nil, null: false
    t.bigint "eventable_id", null: false
    t.string "eventable_type", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.jsonb "recurrence_options"
    t.string "ownable_type"
    t.bigint "ownable_id"
    t.string "uid"
    t.datetime "exdates", precision: nil, default: [], array: true
    t.index ["eventable_id", "eventable_type"], name: "index_events_on_eventable_id_and_eventable_type"
    t.index ["ownable_id", "ownable_type", "eventable_type"], name: "index_events_on_ownable_id_and_ownable_type_and_eventable_type"
    t.index ["ownable_type", "ownable_id"], name: "index_events_on_ownable_type_and_ownable_id"
    t.index ["recurrence_options"], name: "index_events_on_recurrence_options", using: :gin
    t.index ["uid"], name: "index_events_on_uid"
  end

  create_table "food_taxonomies", force: :cascade do |t|
    t.string "feeding_america_id", null: false
    t.string "name", null: false
    t.string "details"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["feeding_america_id"], name: "index_food_taxonomies_on_feeding_america_id", unique: true
  end

  create_table "hubs", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "organizations", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "tagline"
    t.string "email"
    t.string "phone"
    t.string "federal_tax_id"
    t.string "time_zone", default: "Etc/UTC", null: false
    t.string "internal_alerts_email"
  end

  create_table "organizations_users", id: false, force: :cascade do |t|
    t.bigint "organization_id", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["organization_id"], name: "index_organizations_users_on_organization_id"
    t.index ["user_id"], name: "index_organizations_users_on_user_id"
  end

  create_table "recipients", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "shift_event_notes", force: :cascade do |t|
    t.string "transportation_type"
    t.bigint "shift_event_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "hours_spent", precision: 8, scale: 2
    t.index ["shift_event_id"], name: "index_shift_event_notes_on_shift_event_id"
  end

  create_table "shift_event_tasks", force: :cascade do |t|
    t.bigint "shift_event_id", null: false
    t.bigint "task_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "order", default: 0
    t.index ["order"], name: "index_shift_event_tasks_on_order"
    t.index ["shift_event_id"], name: "index_shift_event_tasks_on_shift_event_id"
    t.index ["task_id"], name: "index_shift_event_tasks_on_task_id"
  end

  create_table "shift_events", force: :cascade do |t|
    t.bigint "shift_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "public_notes"
    t.text "admin_notes"
    t.index ["shift_id"], name: "index_shift_events_on_shift_id"
  end

  create_table "shift_events_users", id: false, force: :cascade do |t|
    t.bigint "shift_event_id", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["shift_event_id"], name: "index_shift_events_users_on_shift_event_id"
    t.index ["user_id"], name: "index_shift_events_users_on_user_id"
  end

  create_table "shifts", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name", null: false
  end

  create_table "sites", force: :cascade do |t|
    t.string "name", null: false
    t.bigint "organization_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "siteable_id"
    t.string "siteable_type"
    t.string "instructions", default: "", null: false
    t.decimal "lat"
    t.decimal "lng"
    t.text "detailed_hours_json"
    t.text "admin_notes"
    t.string "receipt_key"
    t.integer "robot_location_id"
    t.string "website"
    t.index ["organization_id", "name"], name: "index_sites_on_organization_id_and_name", unique: true
    t.index ["organization_id"], name: "index_sites_on_organization_id"
    t.index ["robot_location_id"], name: "index_sites_on_robot_location_id"
    t.index ["siteable_id", "siteable_type"], name: "index_sites_on_siteable_id_and_siteable_type"
  end

  create_table "task_logs", force: :cascade do |t|
    t.bigint "task_id", null: false
    t.bigint "shift_event_id", null: false
    t.bigint "user_id", null: false
    t.integer "weight"
    t.text "notes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "food_taxonomy_id", default: -1, null: false
    t.integer "temperature"
    t.index ["shift_event_id"], name: "index_task_logs_on_shift_event_id"
  end

  create_table "tasks", force: :cascade do |t|
    t.bigint "site_id"
    t.integer "task_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["site_id"], name: "index_tasks_on_site_id"
    t.index ["task_type"], name: "index_tasks_on_task_type"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at", precision: nil
    t.datetime "remember_created_at", precision: nil
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "invitation_token"
    t.datetime "invitation_created_at", precision: nil
    t.datetime "invitation_sent_at", precision: nil
    t.datetime "invitation_accepted_at", precision: nil
    t.integer "invitation_limit"
    t.string "invited_by_type"
    t.bigint "invited_by_id"
    t.integer "invitations_count", default: 0
    t.string "full_name", default: "", null: false
    t.integer "roles", default: -1, null: false
    t.string "phone"
    t.string "time_zone", default: "Etc/UTC", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["invitation_token"], name: "index_users_on_invitation_token", unique: true
    t.index ["invitations_count"], name: "index_users_on_invitations_count"
    t.index ["invited_by_id"], name: "index_users_on_invited_by_id"
    t.index ["invited_by_type", "invited_by_id"], name: "index_users_on_invited_by_type_and_invited_by_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "absences", "users"
  add_foreign_key "shift_event_notes", "shift_events"
end
