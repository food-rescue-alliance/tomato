class AddShiftEvents < ActiveRecord::Migration[6.0]
  def change
    create_table(:shift_events) do |t|
      t.bigint :shift_id, null: false
      t.timestamps

      t.index :shift_id
    end

    create_join_table :shift_events, :users do |t|
      t.index :shift_event_id
      t.index :user_id

      t.timestamps
    end

    drop_join_table :shifts, :users

    add_column :shifts, :name, :string, null: true
  end
end
