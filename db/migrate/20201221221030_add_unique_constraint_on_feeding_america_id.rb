class AddUniqueConstraintOnFeedingAmericaId < ActiveRecord::Migration[6.0]
  def change
    add_index :food_taxonomies, :feeding_america_id, unique: true
  end
end
