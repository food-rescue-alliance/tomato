class AddOrganizationReceiptFields < ActiveRecord::Migration[6.0]
  def change
    change_table :organizations, bulk: true do |t|
      t.string :tagline
      t.string :email
      t.string :phone
      t.string :federal_tax_id
    end
  end
end
