class AddUserShiftJoinTable < ActiveRecord::Migration[6.0]
  def change
    create_join_table :shifts, :users do |t|
      t.index :shift_id
      t.index :user_id

      t.timestamps
    end
  end
end
