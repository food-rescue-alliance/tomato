class AddInternalOrgContactEmail < ActiveRecord::Migration[6.0]
  def change
    add_column :organizations, :internal_alerts_email, :string
  end
end
