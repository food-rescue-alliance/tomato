class AddTasks < ActiveRecord::Migration[6.0]
  def up
    create_table :tasks do |t|
      t.bigint :site_id
      t.integer :task_type
      t.timestamps

      t.index :site_id
      t.index :task_type
    end

    create_join_table :shift_events, :tasks do |t|
      t.index :shift_event_id
      t.index :task_id
      t.timestamps
    end

    change_table :events, bulk: true do |t|
      t.remove :uid, :sequence
    end
  end

  def down
    change_table :events, bulk: true do |t|
      t.string :uid, null: false, default: ""
      t.integer :sequence, null: false, default: 0
    end

    drop_join_table :shift_events, :tasks
    drop_table :tasks
  end
end
