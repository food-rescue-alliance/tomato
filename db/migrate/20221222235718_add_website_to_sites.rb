class AddWebsiteToSites < ActiveRecord::Migration[7.0]
  def change
    change_table :sites, bulk: true do |t|
      t.string :website
    end
  end
end
