class ConvertRecurrence < ActiveRecord::Migration[6.0]
  def up
    execute "DELETE from events"

    remove_column :shifts, :organization_id

    change_table :events, bulk: true do |t|
      t.jsonb :recurrence_options, null: true
      t.string :uid, null: false
      t.integer :sequence, null: false, default: 0
      t.references :ownable, polymorphic: true
      t.remove :rrule
    end

    add_index :events, :recurrence_options, using: :gin
    add_index :events, %i[uid sequence starts_at]
    add_index :events, %i[ownable_id ownable_type eventable_type]
    add_index :events, %i[uid starts_at]
  end

  def down
    change_table :events, bulk: true do |t|
      t.remove :recurrence_options
      t.remove :uid
      t.remove :sequence
      t.remove :ownable_id
      t.remove :ownable_type
      t.string :rrule, null: true
    end

    add_column :shifts, :organization_id, :bigint, null: false, default: 1
    add_index :shifts, :organization_id
  end
end
