class AddExdates < ActiveRecord::Migration[6.0]
  def change
    add_column :events, :exdates, :datetime, array: true, default: []
  end
end
