class AddTemperatureToTaskLogs < ActiveRecord::Migration[6.0]
  def change
    add_column :task_logs, :temperature, :integer
  end
end
