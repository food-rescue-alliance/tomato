class AddEvents < ActiveRecord::Migration[6.0]
  def change
    create_table(:events) do |t|
      t.string :title, null: false
      t.datetime :starts_at, null: false
      t.datetime :ends_at, null: true
      t.string :rrule, null: true
      t.bigint :eventable_id, null: false
      t.string :eventable_type, null: false
      t.timestamps

      t.index %i[eventable_id eventable_type]
    end
  end
end
