class AddNoteFieldsToShiftEvent < ActiveRecord::Migration[7.0]
  def change
    change_table :shift_events, bulk: true do |t|
      t.text :public_notes
      t.text :admin_notes
    end
  end
end
