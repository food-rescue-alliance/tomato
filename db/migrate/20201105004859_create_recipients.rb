class CreateRecipients < ActiveRecord::Migration[6.0]
  def change
    create_table :recipients, &:timestamps
  end
end
