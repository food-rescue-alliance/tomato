class AddRobotLocationIdToSites < ActiveRecord::Migration[7.0]
  def change
    change_table :sites, bulk: true do |t|
      t.integer :robot_location_id, index: true
    end
  end
end
