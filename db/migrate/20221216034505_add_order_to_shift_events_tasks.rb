class AddOrderToShiftEventsTasks < ActiveRecord::Migration[7.0]
  def change
    rename_table :shift_events_tasks, :shift_event_tasks
    change_table :shift_event_tasks, bulk: true do |t|
      t.primary_key :id
      t.integer :order, index: true, default: 0
    end
  end
end
