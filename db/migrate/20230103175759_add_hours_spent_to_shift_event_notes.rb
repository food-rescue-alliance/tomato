class AddHoursSpentToShiftEventNotes < ActiveRecord::Migration[7.0]
  def change
    change_table :shift_event_notes, bulk: true do |t|
      t.decimal :hours_spent, precision: 8, scale: 2
    end
  end
end
