class EventUid < ActiveRecord::Migration[6.0]
  def change
    add_column :events, :uid, :string

    Shift.all.each do |shift|
      event_ids = shift.shift_events.map { |shift_event| shift_event.event.id }
      Event.where(id: event_ids).update(uid: SecureRandom.uuid) if event_ids.any?
    end

    add_index :events, :uid
  end
end
