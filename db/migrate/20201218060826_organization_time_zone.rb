class OrganizationTimeZone < ActiveRecord::Migration[6.0]
  def change
    add_column :organizations, :time_zone, :string, null: false, default: "Etc/UTC"
    add_column :users, :time_zone, :string, null: false, default: "Etc/UTC"

    change_column_null :shifts, :name, false, ""
  end
end
