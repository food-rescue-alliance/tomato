class AddLocationDataToSites < ActiveRecord::Migration[7.0]
  def change
    change_table :sites, bulk: true do |t|
      t.decimal :lat
      t.decimal :lng
      t.text :detailed_hours_json
      t.text :admin_notes
      t.string :receipt_key
    end
  end
end
