class CreateHubs < ActiveRecord::Migration[6.0]
  def change
    create_table :hubs, &:timestamps
  end
end
