class CreateDonors < ActiveRecord::Migration[6.0]
  def change
    create_table :donors do |t|
      t.string :name, null: false
      t.bigint :organization_id

      t.timestamps
    end
    add_index :donors, :organization_id
    add_index :donors, %i[organization_id name], unique: true
  end
end
