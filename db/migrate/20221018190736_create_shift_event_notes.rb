class CreateShiftEventNotes < ActiveRecord::Migration[6.0]
  def change
    create_table :shift_event_notes do |t|
      t.string :transportation_type
      t.references :shift_event, null: false, foreign_key: true

      t.timestamps
    end
  end
end
