class TransferColumnsFromDonorsToSites < ActiveRecord::Migration[6.0]
  def change
    rename_table :donors, :sites

    change_table :sites, bulk: true do |t|
      t.bigint :siteable_id
      t.string :siteable_type
      t.index %i[siteable_id siteable_type]
    end

    create_table :donors, &:timestamps

    reversible do |dir|
      dir.up do
        Site.all.each { |site| Donor.create(site: site) }
      end
    end
  end
end
