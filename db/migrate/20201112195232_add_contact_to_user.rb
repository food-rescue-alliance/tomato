class AddContactToUser < ActiveRecord::Migration[6.0]
  def change
    change_table(:users, { bulk: true }) do |t|
      t.column :phone, :string
    end
  end
end
