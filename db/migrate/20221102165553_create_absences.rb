class CreateAbsences < ActiveRecord::Migration[6.0]
  def change
    create_table :absences do |t|
      t.references :user, null: false, foreign_key: true
      t.date :starts_at, null: false
      t.date :ends_at, null: true

      t.timestamps
    end
  end
end
