class CreateFoodTaxonomies < ActiveRecord::Migration[6.0]
  def change
    create_table :food_taxonomies do |t|
      t.string :feeding_america_id, null: false
      t.string :name, null: false
      t.string :details

      t.timestamps
    end

    add_column :task_logs, :food_taxonomy_id, :integer, null: false, default: -1
  end
end
