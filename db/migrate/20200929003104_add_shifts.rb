class AddShifts < ActiveRecord::Migration[6.0]
  def up
    create_table :shifts do |t|
      t.bigint :organization_id, null: false
      t.timestamps
      t.index :organization_id
    end

    execute "UPDATE events set ends_at = '2021-01-01' where ends_at is NULL"
    change_column :events, :ends_at, :datetime, null: false
  end

  def down
    drop_table :shifts
    execute "UPDATE events set eventable_id = 1"
    execute "UPDATE events set eventable_type = 'Organization'"
    change_column :events, :ends_at, :datetime, null: true, default: nil
  end
end
