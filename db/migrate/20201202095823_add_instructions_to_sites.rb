class AddInstructionsToSites < ActiveRecord::Migration[6.0]
  def change
    add_column :sites, :instructions, :string, null: false, default: ""
  end
end
