class CreateOrganizationsUsersJoinTable < ActiveRecord::Migration[6.0]
  def change
    create_join_table :organizations, :users do |t|
      t.index :organization_id
      t.index :user_id

      t.timestamps
    end

    add_column :users, :full_name, :string, null: false, default: ""
  end
end
