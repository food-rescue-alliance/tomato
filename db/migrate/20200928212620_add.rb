class Add < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :roles, :integer, null: false, default: -1
  end
end
