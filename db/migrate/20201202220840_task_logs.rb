class TaskLogs < ActiveRecord::Migration[6.0]
  def change
    create_table :task_logs do |table|
      table.bigint :task_id, null: false
      table.bigint :shift_event_id, null: false
      table.bigint :user_id, null: false
      table.integer :weight
      table.text :notes

      table.timestamps
    end

    add_index :task_logs, :shift_event_id
  end
end
