class AddAddresses < ActiveRecord::Migration[6.0]
  def change
    create_table(:addresses) do |t|
      t.string :street_one, null: false
      t.string :street_two
      t.string :city, null: false
      t.string :state, null: false
      t.string :zip, null: false
      t.references :addressable, polymorphic: true

      t.timestamps
    end
  end
end
