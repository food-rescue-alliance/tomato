source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

# Heroku uses the ruby version to configure your application"s runtime.
ruby "3.0.2"

gem "amazing_print"
gem "bootsnap", require: false
gem "devise"
gem "devise_invitable", "~> 2.0.0"
gem "icalendar"
gem "jbuilder"
gem "pg"
gem "puma"
gem "pundit"
gem "rack-canonical-host"
gem "rails", "~> 7.0.4"
gem "rrule"
gem "simple_form"
gem "slim-rails"
gem "webpacker"

# Env specific dependencies...
group :production do
  gem "rack-timeout"
end

group :development, :test do
  gem "byebug"
  gem "factory_bot_rails"
  gem "rspec_junit_formatter", require: false
  gem "rspec-rails"
  gem "rubocop", require: false
  gem "rubocop-performance", require: false
  gem "rubocop-rails", require: false
end

group :development do
  gem "annotate"
  gem "better_errors"
  gem "binding_of_caller"
  gem "dotenv-rails"
  gem "guard", require: false
  gem "guard-rspec", require: false
  gem "launchy"
  gem "listen"
end

group :test do
  gem "capybara"
  # gem "capybara-email"
  gem "faker"
  gem "rspec-retry"
  gem "selenium-webdriver"
  gem "shoulda-matchers"
  gem "simplecov"
  gem "webdrivers"
end
